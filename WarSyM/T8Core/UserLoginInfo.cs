﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T8Core
{
    public static class UserLoginInfo
    {

        public static int IdLogin { get; set; }

        public static string Username { get; set; }
        
        public static string Password { get; set; }

        public static string HakAkses { get; set; }

        public static string Application { get; set; }

    }
}
