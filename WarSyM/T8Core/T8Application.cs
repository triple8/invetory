﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using T8ApplicationSetting;
using DataAccess;
using DataAccess.Goods;

namespace T8Core
{
    public static class T8Application
    {
        public static string ApplicationName = "";
        public static string ApplicationVersion = "";       
                
        public static void InitializeApplication(string localSettingFilePath)
        {
            ApplicationSetting.Instance.SetupLocalSetting(localSettingFilePath + "\\localsetting\\ServerDatabase.txt");
            Repository.RefreshData();
        }

        private static void SetupLocalSetting()
        {
            throw new NotImplementedException();
        }
    }
}
