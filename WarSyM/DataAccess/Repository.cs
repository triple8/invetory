﻿using DataAccess.Goods;
using DataModel;
using DataModel.Goods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public static class Repository
    {
        public static List<Item> ItemData = new List<Item>();
        public static List<Menu> MenuData = new List<Menu>();
        public static List<MenuDetails> MenuDetailsData = new List<MenuDetails>();
        public static List<Product> ProductData = new List<Product>();
        public static List<Satuan> SatuanData = new List<Satuan>();
        public static List<User> User = new List<User>();

        public static void RefreshData()
        {
            ItemData = DAItem.Instance.GetAllItem();
            MenuDetailsData = DAMenuDetails.Instance.GetAllItem();
            MenuData = DAMenu.Instance.GetAllItem();
            ProductData = DAProduct.Instance.GetAllItem();
            SatuanData = DASatuan.Instance.GetAllItem();
            User = DAUser.Instance.GetAllItem();
        }
    }
}
