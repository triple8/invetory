﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Transaction
{
    public class DAPurchasing
    {

        public DateTime tanggal { get; set; }
        
        public string user { get; set; }

        public decimal diskon { get; set; }

    }
}
