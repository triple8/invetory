﻿using DataAccess.Helper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T8ApplicationSetting;

namespace DataAccess.Transaction
{
    public class DAOperasional
    {
        private static DAOperasional instance;

        public static DAOperasional Instance
        {
            get
            {
                if (instance == null)
                    instance = new DAOperasional();
                return instance;
            }
        }

        public string Insert(DateTime _tanggalTransaksi, decimal _nominal, string _keterangan, string _user, int _idToko)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationSetting.Instance.MySqlApplicationConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertoperasional";

                    cmd.Parameters.AddWithValue("@VTanggal", _tanggalTransaksi);
                    cmd.Parameters["@VTanggal"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNominal", _nominal);
                    cmd.Parameters["@VNominal"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKeterangan", _keterangan);
                    cmd.Parameters["@VKeterangan"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VUser", _user);
                    cmd.Parameters["@VUser"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdToko", _idToko);
                    cmd.Parameters["@VIdToko"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            };
            return returnValue;
        }
    }
}
