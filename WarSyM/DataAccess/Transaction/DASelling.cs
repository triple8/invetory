﻿using DataAccess.Helper;
using DataModel.Transaction;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T8ApplicationSetting;

namespace DataAccess.Transaction
{
    public class DASelling
    {

        private static DASelling instance;

        public static DASelling Instance
        {
            get
            {
                if (instance == null)
                    instance = new DASelling();
                return instance;
            }
        }

        public string Insert(string _vArrayKodeBarang, string _vArrayKodeSatuan, string _vArrayHarga, string _vArrayJumlah,
            string _vArrayDiscount, decimal _vDiscountTransaksi, int _vIDToko, string _vUser, int _vItemCount,string _menuId, string _menuJumlah, 
            string _menuHarga, string _menuDiskon, int _menuCount, string _menuDetailIdBarang, string _menuDetailIdSatuan, string _menuDetailJumlah, int _menuDetailCount)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationSetting.Instance.MySqlApplicationConnection)
            {
                using(MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertpenjualan";

                    cmd.Parameters.AddWithValue("@VArrayKodeBarang",_vArrayKodeBarang);
                    cmd.Parameters["@VArrayKodeBarang"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayKodeSatuan", _vArrayKodeSatuan);
                    cmd.Parameters["@VArrayKodeSatuan"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayHarga", _vArrayHarga);
                    cmd.Parameters["@VArrayHarga"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayJumlah", _vArrayJumlah);
                    cmd.Parameters["@VArrayJumlah"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayDiscount", _vArrayDiscount);
                    cmd.Parameters["@VArrayDiscount"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VDiscountTransaksi", _vDiscountTransaksi);
                    cmd.Parameters["@VDiscountTransaksi"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIDToko", _vIDToko);
                    cmd.Parameters["@VIDToko"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VUser", _vUser);
                    cmd.Parameters["@VArrayKodeBarang"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VItemCount", _vItemCount);
                    cmd.Parameters["@VItemCount"].Direction = ParameterDirection.Input;

                    cmd.Parameters.AddWithValue("@VArrayMenuId", string.Join(",", _menuId));
                    cmd.Parameters["@VArrayMenuId"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayMenuJumlah", string.Join(",", _menuJumlah));
                    cmd.Parameters["@VArrayMenuJumlah"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayMenuHarga", string.Join(",", _menuHarga));
                    cmd.Parameters["@VArrayMenuHarga"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayMenuDiskon", string.Join(",", _menuDiskon));
                    cmd.Parameters["@VArrayMenuDiskon"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VMenuCount", string.Join(",", _menuCount));
                    cmd.Parameters["@VMenuCount"].Direction = ParameterDirection.Input;

                    cmd.Parameters.AddWithValue("@VArrayMenuIdProduk", string.Join(",", _menuDetailIdBarang));
                    cmd.Parameters["@VArrayMenuIdProduk"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayMenuIdSatuan", string.Join(",", _menuDetailIdSatuan));
                    cmd.Parameters["@VArrayMenuIdSatuan"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VArrayMenuIdJumlah", string.Join(",", _menuDetailJumlah));
                    cmd.Parameters["@VArrayMenuIdJumlah"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VMenuBarangCount", string.Join(",", _menuDetailCount));
                    cmd.Parameters["@VMenuBarangCount"].Direction = ParameterDirection.Input;
                    
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            };
            return returnValue;
        }
    }
}
