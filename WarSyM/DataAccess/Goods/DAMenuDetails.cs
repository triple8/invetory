﻿using DataAccess.Interface;
using DataModel.Goods;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T8ApplicationSetting;

namespace DataAccess.Goods
{
    public class DAMenuDetails : IDAItem<MenuDetails>
    {
        private static DAMenuDetails instance;

        public static DAMenuDetails Instance
        {
            get
            {
                if(instance == null)
                    instance = new DAMenuDetails();
                return instance;                
            }
        }

        private DAMenuDetails() { }

        public List<MenuDetails> GetAllItem()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id_menu, id_barang, id_satuan, jumlah from menudetail where 1=1 ");
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationSetting.Instance.MySqlApplicationConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            List<MenuDetails> menuDetails = new List<MenuDetails>();

            foreach (DataRow dr in dt.Rows)
            {
                menuDetails.Add(new MenuDetails()
                { 
                    idMenu = Convert.ToInt32(dr[0]),
                    idBarang = Convert.ToInt32(dr[1]),
                    idSatuan = Convert.ToInt32(dr[2]),
                    jumlah = (float)Convert.ToDouble(dr[3])
                });
            }

            return menuDetails;
        }
    }
}
