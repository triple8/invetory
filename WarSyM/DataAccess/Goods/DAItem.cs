﻿using DataAccess.Interface;
using DataModel.Goods;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T8ApplicationSetting;

namespace DataAccess.Goods
{
    public class DAItem : IDAItem<Item>
    {
        private static DAItem instance;
     
        public static DAItem Instance
        {
            get
            {
                if (instance == null)
                    instance =  new DAItem();
                return instance;
            }
        }

        private DAItem() { }

        public List<Item> GetAllItem()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id, kode, nama from barang where 1 = 1 ");
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationSetting.Instance.MySqlApplicationConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            List<Item> items = new List<Item>();

            foreach (DataRow dr in dt.Rows)
            {
                items.Add(new Item() { 
                    id = Convert.ToInt32(dr[0]), 
                    kode = (string)dr[1], 
                    nama = (string)dr[2] 
                });
            }

            return items;
        }
    }
}
