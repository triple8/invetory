﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using T8ApplicationSetting;
using DataModel.Goods;
using MySql.Data.MySqlClient;
using System.Data;

namespace DataAccess.Goods
{
    public class DAProduct : IDAItem<Product>
    {        
        private  static DAProduct instance;
        
        public static DAProduct Instance
        {
            get
            {
                if (instance == null)
                    instance = new DAProduct();
                return instance;
            }
        }

        private DAProduct() { }

        public List<Product> GetAllItem()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id_barang, id_satuan, id_satuan_parent, retail, minimum_stok, harga_jual from barangdetail where 1 = 1 ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationSetting.Instance.MySqlApplicationConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            List<Product> items = new List<Product>();
            
            foreach (DataRow dr in dt.Rows)
            {
                items.Add(new Product()
                {
                    idBarang = Convert.ToInt32(dr[0]),
                    idSatuan = Convert.ToInt32(dr[1]),
                    idSatuanParent = Convert.ToInt32(dr[2]),
                    retail = (float)Convert.ToDouble(dr[3]),
                    minimumStok = (float)Convert.ToDouble(dr[4]),
                    hargaJual = Convert.ToDecimal(dr[5])
                });
            }

            return items;
        }
    }
}
