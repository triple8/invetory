﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;
using DataModel;
using T8ApplicationSetting;
using DataModel.Goods;
using MySql.Data.MySqlClient;
using System.Data;

namespace DataAccess.Goods
{
    public class DAMenu : IDAItem<Menu>
    {        
        private static DAMenu instance;

        public static DAMenu Instance
        {
            get
            {
                if (instance == null)
                    instance = new DAMenu();
                return instance;
            }
        }

        private DAMenu() { }

        public List<Menu> GetAllItem()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id, kode, nama, harga from menu where 1 = 1 ");
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationSetting.Instance.MySqlApplicationConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            List<Menu> items = new List<Menu>();

            foreach (DataRow dr in dt.Rows)
            {
                items.Add(new Menu()
                {
                    id = Convert.ToInt32(dr[0]),
                    kode = (string)dr[1],
                    nama = (string)dr[2],
                    harga = Convert.ToDecimal(dr[3]),
                    menuDetails = Repository.MenuDetailsData.Where(x => x.idMenu == Convert.ToInt32(dr[0])).ToList()
                });
            }

            return items;
        }
    }
}
