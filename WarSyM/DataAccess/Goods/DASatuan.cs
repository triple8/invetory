﻿using DataAccess.Interface;
using DataModel.Goods;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T8ApplicationSetting;

namespace DataAccess.Goods
{
    public class DASatuan : IDAItem<Satuan>
    {
        private  static DASatuan instance;
        
        public static DASatuan Instance
        {
            get
            {
                if (instance == null)
                    instance = new DASatuan();
                return instance;
            }
        }

        private DASatuan() { }

        public List<Satuan> GetAllItem()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id, kode,nama, keterangan from satuan where 1 = 1 ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationSetting.Instance.MySqlApplicationConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            List<Satuan> units = new List<Satuan>();

            foreach (DataRow dr in dt.Rows)
            {
                units.Add(new Satuan()
                {
                    id = Convert.ToInt32(dr[0]),
                    kode = (string)dr[1],
                    nama = (string)dr[2],
                    keterangan = (string)(dr[3])
                });
            }

            return units;
        }
    }
}
