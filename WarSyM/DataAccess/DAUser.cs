﻿using DataModel;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T8ApplicationSetting;

namespace DataAccess
{
    public class DAUser
    {
        private static DAUser instance;

        public static DAUser Instance
        {
            get
            {
                if (instance == null)
                    instance = new DAUser();
                return instance;
            }
        }

        public List<User> GetAllItem()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select username, password, nama, alamat, telp, akses from user where 1 = 1 ");
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationSetting.Instance.MySqlApplicationConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            List<User> user = new List<User>();

            foreach (DataRow dr in dt.Rows)
            {
                user.Add(new User()
                {
                    Username = (string)dr[0],
                    Password = (string)dr[1],
                    Nama = (string)dr[2],
                    Alamat = (string)dr[3],
                    Telp = (string)dr[4],
                    Akses = (string)dr[5]
                });
            }

            return user;
        }
    }
}
