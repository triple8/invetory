﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Helper
{
    class Common
    {
        public static string ExecuteNonQuery(MySqlConnection cn, MySqlCommand cmd)
        {
            string returnValue = "";
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                returnValue = ex.Message;
            }
            finally
            {
                cn.Close();
            }
            return returnValue;
        }
    }
}
