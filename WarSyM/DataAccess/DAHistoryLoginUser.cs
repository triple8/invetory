﻿using DataAccess.Helper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T8ApplicationSetting;

namespace DataAccess
{
    public class DAHistoryLoginUser
    {
        private static DAHistoryLoginUser instance;

        public static DAHistoryLoginUser Instance
        {
            get
            {
                if (instance == null)
                    instance = new DAHistoryLoginUser();
                return instance;
            }
        }

        public string Insert(string username, string application, int idToko)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationSetting.Instance.MySqlApplicationConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "inserthistoryloginuser";

                    cmd.Parameters.AddWithValue("@VUsername", username);
                    cmd.Parameters["@VUsername"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VApplikasi", application);
                    cmd.Parameters["@VApplikasi"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdToko", idToko);
                    cmd.Parameters["@VIdToko"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            };
            return returnValue;
        }

        public string Update(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationSetting.Instance.MySqlApplicationConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "userlogout";

                    cmd.Parameters.AddWithValue("@VIdLogin", values[0]);
                    cmd.Parameters["@VIdLogin"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdToko", values[1]);
                    cmd.Parameters["@VIdToko"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            };
            return returnValue;
        }
    }
}
