﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using T8Core;

namespace WarSyM
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            T8Application.InitializeApplication(Application.StartupPath);
            Application.Run(new WarSyM.GUI.FrmMain());
        }
    }
}
