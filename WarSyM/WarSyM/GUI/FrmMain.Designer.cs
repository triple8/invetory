﻿namespace WarSyM.GUI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.visualStudio2012DarkTheme1 = new Telerik.WinControls.Themes.VisualStudio2012DarkTheme();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.radButton10 = new Telerik.WinControls.UI.RadButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radButton9 = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radButton8 = new Telerik.WinControls.UI.RadButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.panel5 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.panel5);
            this.radPanel1.Controls.Add(this.panel2);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(875, 230);
            this.radPanel1.TabIndex = 0;
            this.radPanel1.ThemeName = "VisualStudio2012Dark";
            // 
            // radButton7
            // 
            this.radButton7.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton7.Image = ((System.Drawing.Image)(resources.GetObject("radButton7.Image")));
            this.radButton7.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton7.Location = new System.Drawing.Point(581, 12);
            this.radButton7.Name = "radButton7";
            this.radButton7.Size = new System.Drawing.Size(137, 136);
            this.radButton7.TabIndex = 12;
            this.radButton7.Text = "&Operasional";
            this.radButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButton7.TextWrap = true;
            this.radButton7.ThemeName = "VisualStudio2012Dark";
            this.radButton7.Click += new System.EventHandler(this.radButton7_Click);
            // 
            // radButton6
            // 
            this.radButton6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton6.Image = ((System.Drawing.Image)(resources.GetObject("radButton6.Image")));
            this.radButton6.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton6.Location = new System.Drawing.Point(731, 12);
            this.radButton6.Name = "radButton6";
            this.radButton6.Size = new System.Drawing.Size(128, 136);
            this.radButton6.TabIndex = 11;
            this.radButton6.Text = "&Pengaturan";
            this.radButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButton6.TextWrap = true;
            this.radButton6.ThemeName = "VisualStudio2012Dark";
            this.radButton6.Click += new System.EventHandler(this.radButton6_Click);
            // 
            // radButton5
            // 
            this.radButton5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton5.Image = ((System.Drawing.Image)(resources.GetObject("radButton5.Image")));
            this.radButton5.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton5.Location = new System.Drawing.Point(439, 12);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(128, 136);
            this.radButton5.TabIndex = 10;
            this.radButton5.Text = "&Retur Jual";
            this.radButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButton5.TextWrap = true;
            this.radButton5.ThemeName = "VisualStudio2012Dark";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click);
            // 
            // radButton4
            // 
            this.radButton4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton4.Image = ((System.Drawing.Image)(resources.GetObject("radButton4.Image")));
            this.radButton4.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton4.Location = new System.Drawing.Point(297, 12);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(128, 136);
            this.radButton4.TabIndex = 9;
            this.radButton4.Text = "&Retur Beli";
            this.radButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButton4.TextWrap = true;
            this.radButton4.ThemeName = "VisualStudio2012Dark";
            this.radButton4.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // radButton3
            // 
            this.radButton3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton3.Image = ((System.Drawing.Image)(resources.GetObject("radButton3.Image")));
            this.radButton3.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton3.Location = new System.Drawing.Point(155, 12);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(128, 136);
            this.radButton3.TabIndex = 8;
            this.radButton3.Text = "&Pembelian";
            this.radButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButton3.TextWrap = true;
            this.radButton3.ThemeName = "VisualStudio2012Dark";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Image = ((System.Drawing.Image)(resources.GetObject("radButton1.Image")));
            this.radButton1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton1.Location = new System.Drawing.Point(16, 12);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(128, 136);
            this.radButton1.TabIndex = 7;
            this.radButton1.Text = "&Penjualan";
            this.radButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButton1.TextWrap = true;
            this.radButton1.ThemeName = "VisualStudio2012Dark";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 157);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(4);
            this.panel2.Size = new System.Drawing.Size(875, 73);
            this.panel2.TabIndex = 6;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.radButton10);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(6);
            this.panel4.Size = new System.Drawing.Size(134, 65);
            this.panel4.TabIndex = 7;
            // 
            // radButton10
            // 
            this.radButton10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton10.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton10.Image = ((System.Drawing.Image)(resources.GetObject("radButton10.Image")));
            this.radButton10.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton10.Location = new System.Drawing.Point(6, 6);
            this.radButton10.Name = "radButton10";
            this.radButton10.Size = new System.Drawing.Size(122, 53);
            this.radButton10.TabIndex = 3;
            this.radButton10.Text = "&Login";
            this.radButton10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButton10.ThemeName = "VisualStudio2012Dark";
            this.radButton10.Click += new System.EventHandler(this.radButton10_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radButton9);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(470, 4);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(6);
            this.panel3.Size = new System.Drawing.Size(194, 65);
            this.panel3.TabIndex = 6;
            // 
            // radButton9
            // 
            this.radButton9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton9.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton9.Image = ((System.Drawing.Image)(resources.GetObject("radButton9.Image")));
            this.radButton9.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton9.Location = new System.Drawing.Point(6, 6);
            this.radButton9.Name = "radButton9";
            this.radButton9.Size = new System.Drawing.Size(182, 53);
            this.radButton9.TabIndex = 3;
            this.radButton9.Text = "&Refresh Data";
            this.radButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButton9.TextWrap = true;
            this.radButton9.ThemeName = "VisualStudio2012Dark";
            this.radButton9.Click += new System.EventHandler(this.radButton9_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radButton8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(664, 4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(6);
            this.panel1.Size = new System.Drawing.Size(98, 65);
            this.panel1.TabIndex = 5;
            // 
            // radButton8
            // 
            this.radButton8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton8.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton8.Image = ((System.Drawing.Image)(resources.GetObject("radButton8.Image")));
            this.radButton8.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton8.Location = new System.Drawing.Point(6, 6);
            this.radButton8.Name = "radButton8";
            this.radButton8.Size = new System.Drawing.Size(86, 53);
            this.radButton8.TabIndex = 3;
            this.radButton8.Text = "&Lock";
            this.radButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButton8.ThemeName = "VisualStudio2012Dark";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.radButton2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(762, 4);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(6);
            this.panel8.Size = new System.Drawing.Size(109, 65);
            this.panel8.TabIndex = 4;
            // 
            // radButton2
            // 
            this.radButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton2.Image = ((System.Drawing.Image)(resources.GetObject("radButton2.Image")));
            this.radButton2.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton2.Location = new System.Drawing.Point(6, 6);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(97, 53);
            this.radButton2.TabIndex = 3;
            this.radButton2.Text = "&Tutup";
            this.radButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButton2.ThemeName = "VisualStudio2012Dark";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.radButton1);
            this.panel5.Controls.Add(this.radButton6);
            this.panel5.Controls.Add(this.radButton7);
            this.panel5.Controls.Add(this.radButton5);
            this.panel5.Controls.Add(this.radButton3);
            this.panel5.Controls.Add(this.radButton4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(875, 157);
            this.panel5.TabIndex = 13;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 230);
            this.Controls.Add(this.radPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Inventory Management System - Transaksi";
            this.ThemeName = "VisualStudio2012Dark";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Shown += new System.EventHandler(this.FrmMain_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.VisualStudio2012DarkTheme visualStudio2012DarkTheme1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel8;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton radButton6;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadButton radButton7;
        private System.Windows.Forms.Panel panel4;
        private Telerik.WinControls.UI.RadButton radButton10;
        private System.Windows.Forms.Panel panel3;
        private Telerik.WinControls.UI.RadButton radButton9;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton radButton8;
        private System.Windows.Forms.Panel panel5;

    }
}
