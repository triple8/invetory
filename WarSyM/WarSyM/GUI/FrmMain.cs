﻿using BusinessProcess;
using DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using T8Core;
using Telerik.WinControls;
using WarSyM.GUI.Transaction;

namespace WarSyM.GUI
{
    public partial class FrmMain : Telerik.WinControls.UI.RadForm
    {
        public bool isLogin, cancelLogin;

        public FrmMain()
        {
            InitializeComponent();
            isLogin = false;
        }
        
        private void radButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            FrmSelling frmSelling = new FrmSelling();
            this.Hide();
            frmSelling.Show(this);
            this.Show();
            SetUpUserAkses();
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menutup form?", "konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            string message = UserAccess.Instance.Logout(UserLoginInfo.IdLogin, 1);
            if(message != "success")
            {
                MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not saved",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            FrmPurchasing frmPurchasing = new FrmPurchasing();
            frmPurchasing.Show();
        }

        private void radButton7_Click(object sender, EventArgs e)
        {
            FrmOperasional frmOperasional = new FrmOperasional();
            frmOperasional.Show();
        }

        private void radButton4_Click(object sender, EventArgs e)
        {
            FrmReturnPurchasing frmReturnPurchasing = new FrmReturnPurchasing();
            frmReturnPurchasing.Show();
        }

        private void radButton5_Click(object sender, EventArgs e)
        {
            FrmReturnSelling frmReturnSelling = new FrmReturnSelling();
            frmReturnSelling.Show();
        }

        private void radButton9_Click(object sender, EventArgs e)
        {
            Repository.RefreshData();
        }

        private void radButton6_Click(object sender, EventArgs e)
        {
            FrmSetting frmSetting = new FrmSetting();
            frmSetting.Show();
        }

        private void FrmMain_Shown(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin(this);
            this.Hide();
            frmLogin.ShowDialog();
            if(!this.isLogin)
            {
                this.Show();
                return;
            }
            this.Show();
            SetUpUserAkses();
        }

        private void radButton10_Click(object sender, EventArgs e)
        {
            if (radButton10.Text == "&Login")
            {
                FrmLogin frmLogin = new FrmLogin(this);
                frmLogin.ShowDialog();
            }
            else
            {
                if(MessageBox.Show(this, "Anda yakin akan keluar dari aplikasi?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    == System.Windows.Forms.DialogResult.No )
                {
                    return;
                }
                string message = UserAccess.Instance.Logout(UserLoginInfo.IdLogin, 1);
                if (message == "success")
                {
                    this.isLogin = false;
                }
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not saved",
                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            SetUpUserAkses();
        }

        private void SetUpUserAkses()
        {
            if(!this.isLogin)
            {
                radButton10.Text = "&Login";
            }
            else
            {
                radButton10.Text = "&Logout";
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }
    }
}
