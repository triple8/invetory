﻿using BusinessProcess;
using DataModel.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using T8Core;
using Telerik.WinControls;

namespace WarSyM.GUI.Transaction
{
    public partial class FrmOperasional : Telerik.WinControls.UI.RadForm
    {
        public FrmOperasional()
        {
            InitializeComponent();
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radButton5_Click(object sender, EventArgs e)
        {
            Process.Start("osk.exe");
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            dtTanggal.Value = DateTime.Now;
            ntNominal.Value = 0;
            txtKeterangan.Text = "";
            dtTanggal.Focus();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            if (!InsertOperasional()) return;
            Clear();
        }

        private bool InsertOperasional()
        {
            Operasional data = new Operasional()
            {
                IdToko = 1,
                Keterangan = txtKeterangan.Text,
                Nominal = ntNominal.Value,
                User = UserLoginInfo.Username,
                Tanggal = dtTanggal.Value
            };
            string message = OperasionalTransaction.Instance.Insert(data);
            if(message.Split(',')[0] == "success")
            {
                MessageBox.Show(this, "ID transaksi: " + message.Split(',')[1] + " telah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return true;
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not saved",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        private void FrmOperasional_Load(object sender, EventArgs e)
        {
            dtTanggal.Value = DateTime.Now;
        }
    }
}
