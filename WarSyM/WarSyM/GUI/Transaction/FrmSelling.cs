﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using WarSyM.GUI.Transaction.Helper;
using DataModel.Goods;
using System.ComponentModel;

namespace WarSyM.GUI.Transaction
{
    public partial class FrmSelling : Telerik.WinControls.UI.RadForm
    {
        BindingList<Satuan> cboDataSatuan = new BindingList<Satuan>();

        public FrmSelling()
        {
            InitializeComponent();
        }

        private void gridItem_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            e.CellElement.Padding = new Padding(3);
            e.Row.Height = 65;
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            SyncGrid();
            AddEmptyNewItem();
        }

        void SyncGrid()
        {
            if (gridItem.Rows.Count < 1) return;
            //jumlah 0
            // 
            if (float.Parse(gridItem.MasterView.Rows[0].Cells["clmnJumlah"].Value.ToString()) == 0 
                || gridItem.MasterView.Rows[0].Cells["clmnUnit"].Value == null 
                || gridItem.MasterView.Rows[0].Cells["clmnKode"].Value == null)
            {
                gridItem.Rows.RemoveAt(0);
                return;
            }
            //kode produk dan satuan sama
            for(int i=0;i<gridItem.Rows.Count;i++)
            {
                for(int j=i+1;j<gridItem.Rows.Count;j++)
                {
                    if (gridItem.MasterView.Rows[i].Cells["clmnKode"].Value.ToString().ToLower() == 
                        gridItem.MasterView.Rows[j].Cells["clmnKode"].Value.ToString().ToLower() && 
                        gridItem.MasterView.Rows[i].Cells["clmnUnit"].Value.ToString().ToLower() == 
                        gridItem.MasterView.Rows[j].Cells["clmnUnit"].Value.ToString().ToLower())
                    {
                        gridItem.MasterView.Rows[j].Cells["clmnJumlah"].Value = 
                            float.Parse(gridItem.MasterView.Rows[i].Cells["clmnJumlah"].Value.ToString()) 
                            + float.Parse(gridItem.MasterView.Rows[j].Cells["clmnJumlah"].Value.ToString());
                        gridItem.Rows.RemoveAt(i);
                        i = 0;
                    }
                }
            }
            lblGrandTotal.Text = string.Format("{0:N2}", GrandTotal());
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmTransaksi_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menutup form?", "konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
        }

        private void gridItem_MouseUp(object sender, MouseEventArgs e)
        {
            if (gridItem.Rows.Count == 0)
                return;
            if (gridItem.CurrentColumn.Name == "clmnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data item?", "Konfirmasi", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                    gridItem.Rows.RemoveAt(gridItem.CurrentRow.Index);
                gridItem.CurrentColumn = gridItem.Columns[0];
                lblGrandTotal.Text = string.Format("{0:N2}", GrandTotal());
            }
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan membatalkan transaksi?", "Konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                return;
            gridItem.Rows.Clear();

            lblGrandTotal.Text = "Grand Total: 0.00";
        }

        private void gridItem_CellValueChanged(object sender, GridViewCellEventArgs e)
        {
            if (gridItem.Rows.Count == 0 || e.RowIndex == -1)
                return;

            string itemType = "";

            if (gridItem.CurrentColumn.Name == "clmnKode")
            {
                if (e.Value == null)
                {
                    gridItem.MasterView.Rows[e.RowIndex].Cells["clmnNama"].Value = null;
                    return;
                }

                var singleItem =
                    Repository.ItemData
                    .Where(item => item.kode.ToLower() == e.Value.ToString().ToLower())
                    .Select(item => item).FirstOrDefault();

                string itemName = "";

                if (singleItem != null)
                {
                    itemName = singleItem.nama;
                    itemType = "produk";
                }
                else
                {
                    var singleMenu =
                        Repository.MenuData
                        .Where(menu => menu.kode.ToLower() == (string)e.Value.ToString().ToLower())
                        .Select(menu => menu).FirstOrDefault();

                    if (singleMenu != null)
                    {
                        itemName = singleMenu.nama;
                        itemType = "menu";
                        gridItem.MasterView.Rows[e.RowIndex].Cells["clmnUnit"].Value = "menu";
                        gridItem.MasterView.Rows[e.RowIndex].Cells["clmnHarga"].Value = singleMenu.harga;
                    }
                }

                if (itemType == "") return;

                gridItem.MasterView.Rows[e.RowIndex].Cells["clmnNama"].Value = itemName;
                gridItem.MasterView.Rows[e.RowIndex].Cells["clmnItemType"].Value = itemType;

            }
            else if (gridItem.CurrentColumn.Name == "clmnJumlah" || gridItem.CurrentColumn.Name == "clmnDiscount" || gridItem.CurrentColumn.Name == "clmnHarga")
            {
                if (gridItem.Rows[e.RowIndex].Cells["clmnJumlah"].Value == null)
                    gridItem.Rows[e.RowIndex].Cells["clmnJumlah"].Value = 0;

                if (gridItem.Rows[e.RowIndex].Cells["clmnDiscount"].Value == null)
                    gridItem.Rows[e.RowIndex].Cells["clmnDiscount"].Value = 0;

                if (gridItem.Rows[e.RowIndex].Cells["clmnHarga"].Value == null)
                    gridItem.Rows[e.RowIndex].Cells["clmnHarga"].Value = 0;

                gridItem.MasterView.Rows[e.RowIndex].Cells["clmnSubTotal"].Value =
                    ((decimal)gridItem.Rows[e.RowIndex].Cells["clmnJumlah"].Value * (decimal)gridItem.Rows[e.RowIndex].Cells["clmnHarga"].Value) -
                    (((decimal)gridItem.Rows[e.RowIndex].Cells["clmnJumlah"].Value * (decimal)gridItem.Rows[e.RowIndex].Cells["clmnHarga"].Value) * ((decimal)gridItem.Rows[e.RowIndex].Cells["clmnDiscount"].Value / (decimal)100));
            }
            if (itemType == "menu") return;
            BindingHargaJual(e.RowIndex, itemType);
        }

        private void BindingHargaJual(int rowIndex, string itemType)
        {
            if(gridItem.MasterView.Rows[rowIndex].Cells["clmnKode"].Value != null && gridItem.MasterView.Rows[rowIndex].Cells["clmnUnit"].Value != null && itemType == "produk")
            {
                gridItem.MasterView.Rows[rowIndex].Cells["clmnHarga"].Value = Repository.ProductData
                    .Where(product => product.idBarang == Repository.ItemData.Where(item => item.kode.ToLower() == gridItem.MasterView.Rows[rowIndex].Cells["clmnKode"].Value.ToString().ToLower()).Select(item => item.id).FirstOrDefault()
                        && product.idSatuan == Repository.SatuanData.Where(satuan => satuan.kode == gridItem.MasterView.Rows[rowIndex].Cells["clmnUnit"].Value.ToString()).Select(satuan=>satuan.id).FirstOrDefault())
                    .Select(product => product.hargaJual).FirstOrDefault();
                gridItem.MasterView.Rows[rowIndex].Cells["clmnSubTotal"].Value =
                    ((decimal)gridItem.Rows[rowIndex].Cells["clmnJumlah"].Value * (decimal)gridItem.Rows[rowIndex].Cells["clmnHarga"].Value) -
                    (((decimal)gridItem.Rows[rowIndex].Cells["clmnJumlah"].Value * (decimal)gridItem.Rows[rowIndex].Cells["clmnHarga"].Value) * ((decimal)gridItem.Rows[rowIndex].Cells["clmnDiscount"].Value / (decimal)100));
            }
            lblGrandTotal.Text = string.Format("{0:N2}", GrandTotal());
        }

        private decimal GrandTotal()
        {
            decimal grandTotal = 0;
            for (int i = 0; i < gridItem.Rows.Count; i++)
                grandTotal += decimal.Parse(gridItem.MasterView.Rows[i].Cells["clmnSubTotal"].Value.ToString());
            return grandTotal;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTanggal.Text = "Tanggal: " + DateTime.Now.ToLongDateString() + ", Jam: " + DateTime.Now.ToShortTimeString();
        }

        private void radButton4_Click(object sender, EventArgs e)
        {
            SyncGrid();
            FrmMenu frmMenu = new FrmMenu(this);
            frmMenu.ShowDialog(this);
        }

        private void AddEmptyNewItem()
        {
            GridViewDataRowInfo dataRowInfo = new GridViewDataRowInfo(this.gridItem.MasterView);
            dataRowInfo.Cells["clmnJumlah"].Value = 0;
            dataRowInfo.Cells["clmnHarga"].Value = 0;
            dataRowInfo.Cells["clmnSubTotal"].Value = 0;
            dataRowInfo.Cells["clmnDiscount"].Value = 0;
            dataRowInfo.Cells["clmnHapus"].Value = "Hapus";
            gridItem.Rows.Insert(0, dataRowInfo);

            gridItem.Rows[0].Cells["clmnJumlah"].ColumnInfo.ReadOnly = true;
            gridItem.Rows[0].Cells["clmnUnit"].ColumnInfo.ReadOnly = true;
            gridItem.Rows[0].Cells["clmnDiscount"].ColumnInfo.ReadOnly = true;
            gridItem.CurrentColumn = gridItem.Columns[0];
            gridItem.Focus();

        }

        private void radButton5_Click(object sender, EventArgs e)
        {
            Process.Start("osk.exe");
        }

        private void FrmSelling_Load(object sender, EventArgs e)
        {
            gridItem.Rows.Clear();
        }

        private void gridItem_CellEditorInitialized(object sender, GridViewCellEventArgs e)
        {
            if (gridItem.CurrentColumn.Name == "clmnUnit")
            {
                if (gridItem.MasterView.Rows[e.RowIndex].Cells["clmnKode"].Value == null) return;
                var singleItemId =
                    Repository.ItemData.Where(item => item.kode.ToLower() == gridItem.MasterView.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString().ToLower())
                    .Select(item => item.id).FirstOrDefault();
                
                var productSatuan = Repository.ProductData
                   .Where(product => product.idBarang == singleItemId)
                   .Select(product => product.idSatuan).ToList();

                if (productSatuan.Count() == 0) return;

                RadDropDownListEditor listEditor = (RadDropDownListEditor)this.gridItem.ActiveEditor;
                RadDropDownListEditorElement editorElement = (RadDropDownListEditorElement)listEditor.EditorElement;

                editorElement.ListElement.AutoSizeItems = true;
                editorElement.ListElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
                editorElement.ListElement.Font = new System.Drawing.Font(this.Font.Name, this.Font.Size - 2.0f);
                editorElement.DataSource = cboDataSatuan;
                editorElement.DisplayMember = "kode";
                editorElement.ValueMember = "kode";
                cboDataSatuan.Clear();
                
                List<Satuan> listItemSatuan =
                    Repository.SatuanData
                    .Where(satuan => productSatuan.Contains(satuan.id))
                    .Select(satuan => satuan).ToList();

                for (int i = 0; i < listItemSatuan.Count; i++)
                    cboDataSatuan.Add(listItemSatuan[i]);

                editorElement.SelectedIndex = -1;
            }
        }

        object value;

        private void gridItem_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (gridItem.CurrentColumn.Name == "clmnKode")
                value = gridItem.CurrentCell.Value;
        }

        private void gridItem_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (gridItem.CurrentCell.Value == null) return;
            if (gridItem.CurrentColumn.Name == "clmnKode")
            {
                gridItem.CurrentCell.Value = gridItem.CurrentCell.Value.ToString().Trim(' ');
                if (value == null)
                {
                    value = gridItem.CurrentCell.Value.ToString();
                    return;
                }
                if (gridItem.CurrentCell.Value.ToString() != value.ToString())
                {
                    if (gridItem.CurrentRow.Cells["clmnItemType"].Value.ToString() != "menu")
                        gridItem.CurrentRow.Cells["clmnUnit"].Value = "";
                    gridItem.CurrentRow.Cells["clmnJumlah"].Value = 0;
                    cboDataSatuan.Clear();
                }
            }
            else if (gridItem.CurrentColumn.Name == "clmnUnit")
            {
                if (gridItem.MasterView.Rows[e.RowIndex].Cells["clmnItemType"].Value.ToString() == "menu") return;
                BindingHargaJual(e.RowIndex, "produk");
            }
        }

        private void radButton6_Click(object sender, EventArgs e)
        {
            if (gridItem.Rows.Count == 0)
            {
                MessageBox.Show(this, "Data penjualan kosong.", "Data kosong", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            SyncGrid();
            FrmPembayaran frmPembayaran = new FrmPembayaran(this.gridItem);
            frmPembayaran.ShowDialog();
            ClearTransaksi();
        }

        private void ClearTransaksi()
        {
            gridItem.Rows.Clear();
            lblGrandTotal.Text = "0.00";
        }
        
        private void gridItem_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1) return;
            if (gridItem.Rows[e.RowIndex].Cells["clmnItemType"].Value == null)
            {
                gridItem.Rows[e.RowIndex].Cells["clmnJumlah"].ColumnInfo.ReadOnly = true;
                gridItem.Rows[e.RowIndex].Cells["clmnUnit"].ColumnInfo.ReadOnly = true;
                gridItem.Rows[e.RowIndex].Cells["clmnDiscount"].ColumnInfo.ReadOnly = true;
                return;
            }
            if (gridItem.CurrentColumn.Name == "clmnJumlah")
                gridItem.Rows[e.RowIndex].Cells["clmnJumlah"].ColumnInfo.ReadOnly = false;
            else if (gridItem.CurrentColumn.Name == "clmnDiscount")
                gridItem.Rows[e.RowIndex].Cells["clmnDiscount"].ColumnInfo.ReadOnly = false;
            else if (gridItem.CurrentColumn.Name == "clmnUnit")
                gridItem.Rows[e.RowIndex].Cells["clmnUnit"].ColumnInfo.ReadOnly = (gridItem.Rows[e.RowIndex].Cells["clmnItemType"].Value.ToString() != "menu") ? false : true;
            else
                return;
        }

        public void AddNewItemByValue(string itemtype, string kodeBarang, string satuan, decimal jumlah, decimal discount, decimal harga)
        {
            GridViewDataRowInfo dataRowInfo = new GridViewDataRowInfo(this.gridItem.MasterView);
            dataRowInfo.Cells["clmnKode"].Value = kodeBarang;
            
            dataRowInfo.Cells["clmnNama"].Value = (satuan != "menu") ? 
                Repository.ItemData.Where(item => item.kode.ToLower() == 
                    kodeBarang.ToLower()).Select(item => item.nama).FirstOrDefault() : 
                    Repository.MenuData.Where(menu => menu.kode.ToLower() == kodeBarang.ToLower()).Select(menu => menu.nama).FirstOrDefault();
            
            dataRowInfo.Cells["clmnJumlah"].Value = jumlah;
            dataRowInfo.Cells["clmnUnit"].Value = satuan;
            dataRowInfo.Cells["clmnHarga"].Value = harga;
            dataRowInfo.Cells["clmnItemType"].Value = (satuan == "menu") ? "menu" : "produk";
            dataRowInfo.Cells["clmnDiscount"].Value = discount;
            dataRowInfo.Cells["clmnSubTotal"].Value = (harga * jumlah) - (harga * jumlah * (discount / (decimal)100));
            dataRowInfo.Cells["clmnHapus"].Value = "Hapus";

            gridItem.Rows.Insert(0, dataRowInfo); 
            lblGrandTotal.Text = string.Format("{0:N2}", GrandTotal());
            SyncGrid();
        }
    }
}
