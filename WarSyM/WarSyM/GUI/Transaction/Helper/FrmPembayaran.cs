﻿using DataModel.Transaction;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using WarSyM.GUI.Transaction.Helper.Item;
using DataAccess;
using BusinessProcess;
using DataModel.Goods;

namespace WarSyM.GUI.Transaction.Helper
{
    public partial class FrmPembayaran : Telerik.WinControls.UI.RadForm
    {
        RadGridView dataPenjualan;
        public FrmPembayaran(RadGridView _dataPenjualan)
        {
            InitializeComponent();
            this.dataPenjualan = _dataPenjualan;
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            if(ntPembayaran.Value < GetTotalBayar())
            {
                MessageBox.Show(this, "Pembayaran tidak mencukupi dari total bayar, mohon periksa kembali.", "Input not valid", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if(!InsertPenjualan(this.dataPenjualan, ntDiscount.Value))
            {
                return;
            } 
            FrmKembalian frmKembalian = new FrmKembalian(GetTotalBayar(), ntPembayaran.Value);
            frmKembalian.ShowDialog();
            this.Close();
            
        }

        private bool InsertPenjualan(RadGridView radGridView, decimal diskonTransaksi)
        {
            List<SellingDetail> detailPenjualan = new List<SellingDetail>();
            List<SellingDetailMenu> detailPenjualanMenu = new List<SellingDetailMenu>();
            List<string> menuIdBarang = new List<string>();
            List<string> menuIdSatuan = new List<string>();
            List<string> menuJumlah = new List<string>();
            foreach(var row in radGridView.Rows)
            {
                if (row.Cells["clmnItemtype"].Value.ToString() == "menu")
                {
                    detailPenjualanMenu.Add(new SellingDetailMenu()
                    {
                        id_menu = Repository.MenuData
                        .Where(menu => menu.kode.ToLower() == row.Cells["clmnKode"].Value.ToString().ToLower())
                        .Select(menu => menu.id).FirstOrDefault(),
                        harga = decimal.Parse(row.Cells["clmnHarga"].Value.ToString()),
                        jumlah = decimal.Parse(row.Cells["clmnJumlah"].Value.ToString()),
                        diskon = decimal.Parse(row.Cells["clmnDiscount"].Value.ToString()),
                        nomor_transaksi = ""
                    });

                    foreach (var menuDetail in Repository.MenuData
                        .Where(menu => menu.kode.ToLower() == row.Cells["clmnKode"].Value.ToString().ToLower())
                        .Select(menu => menu.menuDetails).FirstOrDefault())
                    {
                        menuIdBarang.Add(menuDetail.idBarang.ToString());
                        menuIdSatuan.Add(menuDetail.idSatuan.ToString());
                        menuJumlah.Add(((decimal)menuDetail.jumlah * decimal.Parse(row.Cells["clmnJumlah"].Value.ToString())).ToString());
                    }
                }
                else
                {
                    detailPenjualan.Add(new SellingDetail()
                    {
                        id_barang = Repository.ItemData
                        .Where(item => item.kode.ToLower() == row.Cells["clmnKode"].Value.ToString().ToLower())
                        .Select(item => item.id).FirstOrDefault(),
                        id_satuan = Repository.SatuanData
                        .Where(satuan => satuan.kode.ToLower() == row.Cells["clmnUnit"].Value.ToString().ToLower()).Select(satuan => satuan.id).FirstOrDefault(),
                        harga = decimal.Parse(row.Cells["clmnHarga"].Value.ToString()),
                        diskon = decimal.Parse(row.Cells["clmnDiscount"].Value.ToString()),
                        jumlah = decimal.Parse(row.Cells["clmnJumlah"].Value.ToString()),
                        nomor_transaksi = ""
                    });
                }
            }

            Selling selling = new Selling()
            {
                nomor_transaksi = "",
                Detail = detailPenjualan,
                DetailMenu = detailPenjualanMenu,
                id_toko = 1,
                user = "admin"
            };

            string message = SellingTransaction.Instance.InsertData(selling, menuIdBarang.ToArray(), menuIdSatuan.ToArray(), menuJumlah.ToArray());
            if ( message.Split(',')[0] == "success")
            {
                MessageBox.Show(this, "ID transaksi: " + message.Split(',')[1] + " telah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return true;
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not saved",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        //tanpa diskon
        private decimal GetGrandTotal()
        {
            decimal grandTotal = 0;
            foreach(var row in this.dataPenjualan.Rows)
                grandTotal += decimal.Parse(row.Cells["clmnSubTotal"].Value.ToString());
            return grandTotal;
        }

        //setelah diskon
        private decimal GetTotalBayar()
        {
            decimal grandTotal = GetGrandTotal();
            return grandTotal - (grandTotal * ntDiscount.Value / (decimal)100);
        }

        private void FrmPembayaran_Load(object sender, EventArgs e)
        {
            lblGrandTotal.Text = string.Format("{0:N}", GetGrandTotal());
            lblTotalBayar.Text = string.Format("{0:N}", GetTotalBayar());
        }

        private void ntDiscount_ValueChanged(object sender, EventArgs e)
        {
            lblTotalBayar.Text = string.Format("{0:N}", GetTotalBayar());
        }

        private void radButton5_Click(object sender, EventArgs e)
        {

        }
    }
}
