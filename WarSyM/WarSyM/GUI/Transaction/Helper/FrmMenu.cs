﻿using DataAccess.Goods;
using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using DataModel;
using WarSyM.GUI.Transaction.Helper.Item;

namespace WarSyM.GUI.Transaction.Helper
{
    public partial class FrmMenu : Telerik.WinControls.UI.RadForm
    {
        FrmSelling frmSelling;
        FrmPurchasing frmPurchasing;

        public FrmMenu(FrmSelling _frmSelling)
        {
            InitializeComponent();
            this.frmSelling = _frmSelling;
        }

        public FrmMenu(FrmPurchasing _frmPurchasing)
        {
            InitializeComponent();
            this.frmPurchasing = _frmPurchasing;
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            FrmItem frmItem = new FrmItem(this.frmSelling, "produk");
            frmItem.ShowDialog(this);
        }

        private void radButton5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            FrmItem frmItem = new FrmItem(this.frmSelling, "menu");
            frmItem.ShowDialog(this);
        }

        private void radButton4_Click(object sender, EventArgs e)
        {
            
        }
    }
}
