﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace WarSyM.GUI.Transaction.Helper.Item
{
    public partial class FrmKembalian : Telerik.WinControls.UI.RadForm
    {
        decimal pembayaran, grandTotal;
        public FrmKembalian(decimal _grandTotal, decimal _pembayaran)
        {
            InitializeComponent();
            this.pembayaran = _pembayaran;
            this.grandTotal = _grandTotal;
        }

        private void FrmKembalian_Load(object sender, EventArgs e)
        {
            lblKembalian.Text = string.Format("{0:N}", pembayaran - grandTotal);
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
