﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using System.Linq;
using DataAccess;
using System.Reflection;
using System.Diagnostics;
using System.Data;

namespace WarSyM.GUI.Transaction.Helper.Item
{
    public partial class FrmItem : Telerik.WinControls.UI.RadForm
    {
        string itemType;

        IEnumerable<dynamic> dataSource;
        int pageCount;

        FrmSelling frmSelling;

        public FrmItem(FrmSelling _frmSelling, string _itemType)
        {
            InitializeComponent();
            this.itemType = _itemType;
            this.frmSelling = _frmSelling;
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmItem_Load(object sender, EventArgs e)
        {
            ntPage.Value = pageCount + 1;
            if(this.itemType == "produk")
            {
                dataSource = (from itemData in Repository.ItemData
                              join produk in Repository.ProductData on itemData.id equals produk.idBarang
                              join satuan in Repository.SatuanData on produk.idSatuan equals satuan.id
                              join satuanP in Repository.SatuanData on produk.idSatuanParent equals satuanP.id
                              select new
                              {
                                  ID = itemData.id.ToString(),
                                  Kode = itemData.kode.ToString(),
                                  Nama = itemData.nama.ToString(),
                                  KodeSatuan = satuan.kode.ToString(),
                                  KodeSatuanParent = satuanP.kode.ToString(),
                                  HargaJual = produk.hargaJual.ToString()
                              }
                );
                ProdukColumns();
            }
            else
            {
                dataSource = (from menuData in Repository.MenuData
                              select new
                              {
                                  ID = menuData.id.ToString(),
                                  Kode = menuData.kode.ToString(),
                                  Nama = menuData.nama.ToString(),
                                  Harga = menuData.harga.ToString()
                              });
                MenuColumns();
            }

            radGridView1.Columns[0].IsVisible = false;
            
            PaggingGrid();

        }

        private void ProdukColumns()
        {

            GridViewTextBoxColumn clmnnID = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            clmnnID.AllowSort = false;
            clmnnID.AutoEllipsis = false;
            clmnnID.HeaderText = "ID";
            clmnnID.Name = "clmnId";
            clmnnID.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            clmnnID.Width = 180;

            GridViewTextBoxColumn clmnnKode = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            clmnnKode.AllowSort = false;
            clmnnKode.AutoEllipsis = false;
            clmnnKode.HeaderText = "Kode";
            clmnnKode.Name = "clmnKode";
            clmnnKode.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            clmnnKode.Width = 180;

            GridViewTextBoxColumn clmnNama = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            clmnNama.AllowSort = false;
            clmnNama.AutoEllipsis = false;
            clmnNama.HeaderText = "Nama";
            clmnNama.Name = "clmnNama";
            clmnNama.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            clmnNama.WrapText = true;
            clmnNama.Width = 320;

            GridViewTextBoxColumn clmnKodeSatuan = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            clmnKodeSatuan.AllowSort = false;
            clmnKodeSatuan.AutoEllipsis = false;
            clmnKodeSatuan.HeaderText = "Kode Satuan";
            clmnKodeSatuan.Name = "clmnKodeSatuan";
            clmnKodeSatuan.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            clmnKodeSatuan.WrapText = true;
            clmnKodeSatuan.Width = 180;

            GridViewTextBoxColumn clmnKodeSatuanParent = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            clmnKodeSatuanParent.AllowSort = false;
            clmnKodeSatuanParent.AutoEllipsis = false;
            clmnKodeSatuanParent.HeaderText = "Kode Satuan Parent";
            clmnKodeSatuanParent.Name = "clmnKodeSatuanParent";
            clmnKodeSatuanParent.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            clmnKodeSatuanParent.WrapText = true;
            clmnKodeSatuanParent.Width = 180;

            GridViewDecimalColumn clmnHarga = new Telerik.WinControls.UI.GridViewDecimalColumn();
            clmnHarga.AllowSort = false;
            clmnHarga.AutoEllipsis = false;
            clmnHarga.HeaderText = "Harga";
            clmnHarga.Name = "clmnHarga";
            clmnHarga.FormatString = "{0:N}";
            clmnHarga.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            clmnHarga.Width = 180;

            GridViewCommandColumn clmnSelect = new GridViewCommandColumn();
            clmnSelect.AllowSort = false;
            clmnSelect.AutoEllipsis = false;
            clmnSelect.DefaultText = "Select";
            clmnSelect.HeaderText = "";
            clmnSelect.Name = "clmnSelect";
            clmnSelect.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            clmnSelect.Width = 140;

            radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] { clmnnID, clmnnKode, clmnNama, clmnKodeSatuan, clmnKodeSatuanParent, clmnHarga, clmnSelect });
            
        }

        private void MenuColumns()
        {
            GridViewTextBoxColumn clmnnID = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            clmnnID.AllowSort = false;
            clmnnID.AutoEllipsis = false;
            clmnnID.HeaderText = "ID";
            clmnnID.Name = "clmnId";
            clmnnID.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            clmnnID.Width = 180;

            GridViewTextBoxColumn clmnnKode = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            clmnnKode.AllowSort = false;
            clmnnKode.AutoEllipsis = false;
            clmnnKode.HeaderText = "Kode";
            clmnnKode.Name = "clmnKode";
            clmnnKode.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            clmnnKode.Width = 180;

            GridViewTextBoxColumn clmnNama = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            clmnNama.AllowSort = false;
            clmnNama.AutoEllipsis = false;
            clmnNama.HeaderText = "Nama";
            clmnNama.Name = "clmnNama";
            clmnNama.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            clmnNama.WrapText = true;
            clmnNama.Width = 320;

            GridViewDecimalColumn clmnHarga = new Telerik.WinControls.UI.GridViewDecimalColumn();
            clmnHarga.AllowSort = false;
            clmnHarga.AutoEllipsis = false;
            clmnHarga.HeaderText = "Harga";
            clmnHarga.Name = "clmnHarga";
            clmnHarga.FormatString = "{0:N}";
            clmnHarga.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            clmnHarga.Width = 180;

            GridViewCommandColumn clmnSelect = new GridViewCommandColumn();
            clmnSelect.AllowSort = false;
            clmnSelect.AutoEllipsis = false;
            clmnSelect.DefaultText = "Select";
            clmnSelect.HeaderText = "";
            clmnSelect.Name = "clmnSelect";
            clmnSelect.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            clmnSelect.Width = 140;

            radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] { clmnnID, clmnnKode, clmnNama, clmnHarga, clmnSelect });  

        }

        private void radGridView1_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Padding = new Padding(3);
            e.Row.Height = 65;
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            Process.Start("osk.exe");
        }

        private void ntPage_ValueChanged(object sender, EventArgs e)
        {
            PaggingGrid();
        }

        private void ntDataCount_ValueChanged(object sender, EventArgs e)
        {
            PaggingGrid();
        }

        private void PaggingGrid()
        {
            if (dataSource == null || dataSource.Count() == 0) return;
            var dataFilter = dataSource.Skip(int.Parse(ntDataCount.Value.ToString()) * (int.Parse(ntPage.Value.ToString()) - 1)).Take(int.Parse(ntDataCount.Value.ToString()));
            radGridView1.Rows.Clear();

            int j = 0;
            foreach (var obj in dataFilter)
            {
                j = 0;
                GridViewDataRowInfo dataRowInfo = new GridViewDataRowInfo(this.radGridView1.MasterView);
                foreach (var property in obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    dataRowInfo.Cells[j].Value = property.GetValue(obj, null);
                    j++;
                }
                dataRowInfo.Cells["clmnHarga"].Value = decimal.Parse(dataRowInfo.Cells["clmnHarga"].Value.ToString());
                dataRowInfo.Cells["clmnSelect"].Value = "Select";
                radGridView1.Rows.Insert(0, dataRowInfo);
            }

            pageCount = dataSource.Count() / int.Parse(ntDataCount.Value.ToString());
            lblPageOf.Text = "of " + (pageCount + 1).ToString();
            ntPage.Maximum = pageCount + 1;
        }

        private void radGridView1_CellClick(object sender, GridViewCellEventArgs e)
        {
            if(radGridView1.Columns[e.ColumnIndex].Name == "clmnSelect")
            {
                string satuan;
                if (this.itemType == "menu")
                    satuan = "menu";
                else
                    satuan = radGridView1.Rows[e.RowIndex].Cells["clmnKodeSatuan"].Value.ToString();
                if (satuan == null) return;
                FrmQuantity frmQuantity = new FrmQuantity(this.frmSelling, radGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString(), satuan, decimal.Parse(radGridView1.Rows[e.RowIndex].Cells["clmnHarga"].Value.ToString()));
                frmQuantity.ShowDialog();
            }
        }
    }
}
