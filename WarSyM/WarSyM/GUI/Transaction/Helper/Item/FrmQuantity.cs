﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace WarSyM.GUI.Transaction.Helper.Item
{
    public partial class FrmQuantity : Telerik.WinControls.UI.RadForm
    {
        FrmSelling frmSelling;
        decimal harga;
        string kodeBarang;
        string kodeSatuan;

        public FrmQuantity(FrmSelling _frmSelling, string _kodeBarang, string _kodeSatuan, decimal _harga)
        {
            InitializeComponent();
            this.frmSelling = _frmSelling;
            this.harga = _harga;
            this.kodeBarang = _kodeBarang;
            this.kodeSatuan = _kodeSatuan;
        }

        private void radButton1_Click_1(object sender, EventArgs e)
        {
            if (ntJumlah.Value == (decimal)0)
            {
                MessageBox.Show(this, "Jumlah tidak boleh nol, mohon periksa kembali.", "Input not valid", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                ntJumlah.Focus();
                return;
            }
            string itemType= (this.kodeSatuan == "menu") ? "menu" : "produk";
            this.frmSelling.AddNewItemByValue(itemType, this.kodeBarang, this.kodeSatuan, ntJumlah.Value, ntDiscount.Value, this.harga);
            this.Close();
        }

        private void radButton5_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmQuantity_Load(object sender, EventArgs e)
        {
            lblHarga.Text = string.Format("{0:N}",this.harga);
            SetSubTotal();
        }

        private void SetSubTotal()
        {
            lblSubTotal.Text = string.Format("{0:N}", (this.harga * ntJumlah.Value) - (this.harga * ntJumlah.Value * (ntDiscount.Value / (decimal)100)));
        }

        private void ntJumlah_ValueChanged(object sender, EventArgs e)
        {
            SetSubTotal();
        }

        private void ntDiscount_ValueChanged(object sender, EventArgs e)
        {
            SetSubTotal();
        }
    }
}
