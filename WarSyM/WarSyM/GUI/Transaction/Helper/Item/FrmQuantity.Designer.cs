﻿namespace WarSyM.GUI.Transaction.Helper.Item
{
    partial class FrmQuantity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.ntJumlah = new Telerik.WinControls.UI.RadSpinEditor();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.visualStudio2012DarkTheme1 = new Telerik.WinControls.Themes.VisualStudio2012DarkTheme();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.ntDiscount = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.lblHarga = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.lblSubTotal = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ntJumlah)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntDiscount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHarga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.lblSubTotal);
            this.radPanel1.Controls.Add(this.radLabel5);
            this.radPanel1.Controls.Add(this.lblHarga);
            this.radPanel1.Controls.Add(this.ntDiscount);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.ntJumlah);
            this.radPanel1.Controls.Add(this.panel2);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(2);
            this.radPanel1.Size = new System.Drawing.Size(294, 263);
            this.radPanel1.TabIndex = 0;
            this.radPanel1.ThemeName = "VisualStudio2012Dark";
            // 
            // ntJumlah
            // 
            this.ntJumlah.DecimalPlaces = 2;
            this.ntJumlah.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ntJumlah.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntJumlah.Location = new System.Drawing.Point(122, 64);
            this.ntJumlah.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.ntJumlah.Name = "ntJumlah";
            this.ntJumlah.Padding = new System.Windows.Forms.Padding(3);
            this.ntJumlah.Size = new System.Drawing.Size(157, 36);
            this.ntJumlah.TabIndex = 0;
            this.ntJumlah.TabStop = false;
            this.ntJumlah.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntJumlah.ThemeName = "VisualStudio2012Dark";
            this.ntJumlah.ValueChanged += new System.EventHandler(this.ntJumlah_ValueChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(2, 205);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(290, 56);
            this.panel2.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(6);
            this.panel1.Size = new System.Drawing.Size(128, 56);
            this.panel1.TabIndex = 0;
            // 
            // radButton1
            // 
            this.radButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(6, 6);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(116, 44);
            this.radButton1.TabIndex = 2;
            this.radButton1.Text = "&OK";
            this.radButton1.ThemeName = "VisualStudio2012Dark";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click_1);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.radButton5);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(162, 0);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(6);
            this.panel8.Size = new System.Drawing.Size(128, 56);
            this.panel8.TabIndex = 1;
            // 
            // radButton5
            // 
            this.radButton5.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton5.Location = new System.Drawing.Point(6, 6);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(116, 44);
            this.radButton5.TabIndex = 3;
            this.radButton5.Text = "&Batal";
            this.radButton5.ThemeName = "VisualStudio2012Dark";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click_1);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(15, 66);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(81, 33);
            this.radLabel1.TabIndex = 11;
            this.radLabel1.Text = "Jumlah:";
            this.radLabel1.ThemeName = "VisualStudio2012Dark";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(15, 20);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(72, 33);
            this.radLabel2.TabIndex = 13;
            this.radLabel2.Text = "Harga:";
            this.radLabel2.ThemeName = "VisualStudio2012Dark";
            // 
            // ntDiscount
            // 
            this.ntDiscount.DecimalPlaces = 2;
            this.ntDiscount.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ntDiscount.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntDiscount.Location = new System.Drawing.Point(122, 110);
            this.ntDiscount.Name = "ntDiscount";
            this.ntDiscount.Padding = new System.Windows.Forms.Padding(3);
            this.ntDiscount.Size = new System.Drawing.Size(157, 36);
            this.ntDiscount.TabIndex = 1;
            this.ntDiscount.TabStop = false;
            this.ntDiscount.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntDiscount.ThemeName = "VisualStudio2012Dark";
            this.ntDiscount.ValueChanged += new System.EventHandler(this.ntDiscount_ValueChanged);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(15, 112);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(98, 33);
            this.radLabel3.TabIndex = 14;
            this.radLabel3.Text = "Discount:";
            this.radLabel3.ThemeName = "VisualStudio2012Dark";
            // 
            // lblHarga
            // 
            this.lblHarga.AutoSize = false;
            this.lblHarga.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHarga.Location = new System.Drawing.Point(122, 18);
            this.lblHarga.Name = "lblHarga";
            this.lblHarga.Size = new System.Drawing.Size(157, 36);
            this.lblHarga.TabIndex = 16;
            this.lblHarga.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.lblHarga.ThemeName = "VisualStudio2012Dark";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(15, 158);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(100, 33);
            this.radLabel5.TabIndex = 17;
            this.radLabel5.Text = "Sub total:";
            this.radLabel5.ThemeName = "VisualStudio2012Dark";
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.AutoSize = false;
            this.lblSubTotal.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTotal.Location = new System.Drawing.Point(122, 156);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(157, 36);
            this.lblSubTotal.TabIndex = 18;
            this.lblSubTotal.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSubTotal.ThemeName = "VisualStudio2012Dark";
            // 
            // FrmQuantity
            // 
            this.AcceptButton = this.radButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton5;
            this.ClientSize = new System.Drawing.Size(294, 263);
            this.Controls.Add(this.radPanel1);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmQuantity";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmJumlah";
            this.ThemeName = "VisualStudio2012Dark";
            this.Load += new System.EventHandler(this.FrmQuantity_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ntJumlah)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntDiscount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHarga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private System.Windows.Forms.Panel panel8;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.Themes.VisualStudio2012DarkTheme visualStudio2012DarkTheme1;
        private Telerik.WinControls.UI.RadSpinEditor ntJumlah;
        private Telerik.WinControls.UI.RadLabel lblSubTotal;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel lblHarga;
        private Telerik.WinControls.UI.RadSpinEditor ntDiscount;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;

    }
}
