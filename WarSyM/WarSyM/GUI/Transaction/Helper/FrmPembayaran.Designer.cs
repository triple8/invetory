﻿namespace WarSyM.GUI.Transaction.Helper
{
    partial class FrmPembayaran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.visualStudio2012DarkTheme1 = new Telerik.WinControls.Themes.VisualStudio2012DarkTheme();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.ntPembayaran = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSeparator4 = new Telerik.WinControls.UI.RadSeparator();
            this.radSeparator3 = new Telerik.WinControls.UI.RadSeparator();
            this.radSeparator2 = new Telerik.WinControls.UI.RadSeparator();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.lblTotalBayar = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.lblGrandTotal = new Telerik.WinControls.UI.RadLabel();
            this.ntDiscount = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.visualStudio2012DarkTheme2 = new Telerik.WinControls.Themes.VisualStudio2012DarkTheme();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ntPembayaran)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalBayar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGrandTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntDiscount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton1
            // 
            this.radButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(6, 6);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(116, 44);
            this.radButton1.TabIndex = 2;
            this.radButton1.Text = "&OK";
            this.radButton1.ThemeName = "VisualStudio2012Dark";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.ntPembayaran);
            this.radPanel1.Controls.Add(this.radSeparator4);
            this.radPanel1.Controls.Add(this.radSeparator3);
            this.radPanel1.Controls.Add(this.radSeparator2);
            this.radPanel1.Controls.Add(this.radLabel4);
            this.radPanel1.Controls.Add(this.radSeparator1);
            this.radPanel1.Controls.Add(this.lblTotalBayar);
            this.radPanel1.Controls.Add(this.radLabel5);
            this.radPanel1.Controls.Add(this.lblGrandTotal);
            this.radPanel1.Controls.Add(this.ntDiscount);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.panel2);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(2);
            this.radPanel1.Size = new System.Drawing.Size(431, 493);
            this.radPanel1.TabIndex = 1;
            this.radPanel1.ThemeName = "VisualStudio2012Dark";
            // 
            // ntPembayaran
            // 
            this.ntPembayaran.DecimalPlaces = 2;
            this.ntPembayaran.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ntPembayaran.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntPembayaran.Location = new System.Drawing.Point(15, 373);
            this.ntPembayaran.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntPembayaran.Name = "ntPembayaran";
            this.ntPembayaran.Padding = new System.Windows.Forms.Padding(3);
            this.ntPembayaran.ShowUpDownButtons = false;
            this.ntPembayaran.Size = new System.Drawing.Size(402, 36);
            this.ntPembayaran.TabIndex = 1;
            this.ntPembayaran.TabStop = false;
            this.ntPembayaran.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.ntPembayaran.ThemeName = "VisualStudio2012Dark";
            this.ntPembayaran.ThousandsSeparator = true;
            // 
            // radSeparator4
            // 
            this.radSeparator4.Location = new System.Drawing.Point(17, 415);
            this.radSeparator4.Name = "radSeparator4";
            this.radSeparator4.Size = new System.Drawing.Size(402, 10);
            this.radSeparator4.TabIndex = 24;
            this.radSeparator4.Text = "radSeparator4";
            this.radSeparator4.ThemeName = "VisualStudio2012Dark";
            // 
            // radSeparator3
            // 
            this.radSeparator3.Location = new System.Drawing.Point(17, 313);
            this.radSeparator3.Name = "radSeparator3";
            this.radSeparator3.Size = new System.Drawing.Size(402, 10);
            this.radSeparator3.TabIndex = 21;
            this.radSeparator3.Text = "radSeparator3";
            this.radSeparator3.ThemeName = "VisualStudio2012Dark";
            // 
            // radSeparator2
            // 
            this.radSeparator2.Location = new System.Drawing.Point(17, 106);
            this.radSeparator2.Name = "radSeparator2";
            this.radSeparator2.Size = new System.Drawing.Size(402, 10);
            this.radSeparator2.TabIndex = 20;
            this.radSeparator2.Text = "radSeparator2";
            this.radSeparator2.ThemeName = "VisualStudio2012Dark";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(17, 329);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(402, 33);
            this.radLabel4.TabIndex = 22;
            this.radLabel4.Text = "Pembayaran";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel4.ThemeName = "VisualStudio2012Dark";
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(17, 210);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(402, 10);
            this.radSeparator1.TabIndex = 19;
            this.radSeparator1.Text = "radSeparator1";
            this.radSeparator1.ThemeName = "VisualStudio2012Dark";
            // 
            // lblTotalBayar
            // 
            this.lblTotalBayar.AutoSize = false;
            this.lblTotalBayar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalBayar.Location = new System.Drawing.Point(17, 271);
            this.lblTotalBayar.Name = "lblTotalBayar";
            this.lblTotalBayar.Size = new System.Drawing.Size(402, 36);
            this.lblTotalBayar.TabIndex = 18;
            this.lblTotalBayar.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTotalBayar.ThemeName = "VisualStudio2012Dark";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(17, 227);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(402, 33);
            this.radLabel5.TabIndex = 17;
            this.radLabel5.Text = "Total Bayar";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel5.ThemeName = "VisualStudio2012Dark";
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.AutoSize = false;
            this.lblGrandTotal.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.Location = new System.Drawing.Point(13, 64);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(404, 36);
            this.lblGrandTotal.TabIndex = 16;
            this.lblGrandTotal.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblGrandTotal.ThemeName = "VisualStudio2012Dark";
            // 
            // ntDiscount
            // 
            this.ntDiscount.DecimalPlaces = 2;
            this.ntDiscount.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ntDiscount.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntDiscount.Location = new System.Drawing.Point(15, 165);
            this.ntDiscount.Name = "ntDiscount";
            this.ntDiscount.Padding = new System.Windows.Forms.Padding(3);
            this.ntDiscount.ShowUpDownButtons = false;
            this.ntDiscount.Size = new System.Drawing.Size(402, 36);
            this.ntDiscount.TabIndex = 0;
            this.ntDiscount.TabStop = false;
            this.ntDiscount.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.ntDiscount.ThemeName = "VisualStudio2012Dark";
            this.ntDiscount.ValueChanged += new System.EventHandler(this.ntDiscount_ValueChanged);
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(15, 121);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(402, 33);
            this.radLabel3.TabIndex = 14;
            this.radLabel3.Text = "Discount";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel3.ThemeName = "VisualStudio2012Dark";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(15, 20);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(402, 33);
            this.radLabel2.TabIndex = 13;
            this.radLabel2.Text = "Grand Total";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel2.ThemeName = "VisualStudio2012Dark";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(2, 435);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(427, 56);
            this.panel2.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(6);
            this.panel1.Size = new System.Drawing.Size(128, 56);
            this.panel1.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.radButton5);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(299, 0);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(6);
            this.panel8.Size = new System.Drawing.Size(128, 56);
            this.panel8.TabIndex = 1;
            // 
            // radButton5
            // 
            this.radButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton5.Location = new System.Drawing.Point(6, 6);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(116, 44);
            this.radButton5.TabIndex = 3;
            this.radButton5.Text = "&Batal";
            this.radButton5.ThemeName = "VisualStudio2012Dark";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click);
            // 
            // FrmPembayaran
            // 
            this.AcceptButton = this.radButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton5;
            this.ClientSize = new System.Drawing.Size(431, 493);
            this.Controls.Add(this.radPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPembayaran";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPembayaran";
            this.ThemeName = "VisualStudio2012Dark";
            this.Load += new System.EventHandler(this.FrmPembayaran_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ntPembayaran)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalBayar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGrandTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntDiscount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.VisualStudio2012DarkTheme visualStudio2012DarkTheme1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel lblTotalBayar;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel lblGrandTotal;
        private Telerik.WinControls.UI.RadSpinEditor ntDiscount;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel8;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.Themes.VisualStudio2012DarkTheme visualStudio2012DarkTheme2;
        private Telerik.WinControls.UI.RadSeparator radSeparator3;
        private Telerik.WinControls.UI.RadSeparator radSeparator2;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private Telerik.WinControls.UI.RadSpinEditor ntPembayaran;
        private Telerik.WinControls.UI.RadSeparator radSeparator4;
        private Telerik.WinControls.UI.RadLabel radLabel4;
    }
}
