﻿namespace WarSyM.GUI.Transaction
{
    partial class FrmSelling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn5 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn6 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn7 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn8 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn2 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSelling));
            this.gridItem = new Telerik.WinControls.UI.RadGridView();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.panel15 = new System.Windows.Forms.Panel();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.lblGrandTotal = new Telerik.WinControls.UI.RadLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.lblTanggal = new Telerik.WinControls.UI.RadLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel13 = new System.Windows.Forms.Panel();
            this.visualStudio2012DarkTheme1 = new Telerik.WinControls.Themes.VisualStudio2012DarkTheme();
            ((System.ComponentModel.ISupportInitialize)(this.gridItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridItem.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGrandTotal)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanggal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gridItem
            // 
            this.gridItem.AutoScroll = true;
            this.gridItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.gridItem.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridItem.EnableGestures = false;
            this.gridItem.EnableHotTracking = false;
            this.gridItem.Font = new System.Drawing.Font("Segoe UI", 15.75F);
            this.gridItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridItem.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.gridItem.Location = new System.Drawing.Point(0, 0);
            // 
            // gridItem
            // 
            this.gridItem.MasterTemplate.AllowAddNewRow = false;
            this.gridItem.MasterTemplate.AllowCellContextMenu = false;
            this.gridItem.MasterTemplate.AllowColumnChooser = false;
            this.gridItem.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gridItem.MasterTemplate.AllowColumnReorder = false;
            this.gridItem.MasterTemplate.AllowColumnResize = false;
            this.gridItem.MasterTemplate.AllowDeleteRow = false;
            this.gridItem.MasterTemplate.AllowDragToGroup = false;
            this.gridItem.MasterTemplate.AllowRowResize = false;
            this.gridItem.MasterTemplate.AutoGenerateColumns = false;
            this.gridItem.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn4.AllowFiltering = false;
            gridViewTextBoxColumn4.AllowGroup = false;
            gridViewTextBoxColumn4.AllowReorder = false;
            gridViewTextBoxColumn4.AllowResize = false;
            gridViewTextBoxColumn4.AllowSort = false;
            gridViewTextBoxColumn4.AutoEllipsis = false;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.HeaderText = "Kode Item";
            gridViewTextBoxColumn4.IsPinned = true;
            gridViewTextBoxColumn4.Name = "clmnKode";
            gridViewTextBoxColumn4.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 160;
            gridViewTextBoxColumn4.WrapText = true;
            gridViewTextBoxColumn5.AllowGroup = false;
            gridViewTextBoxColumn5.AllowResize = false;
            gridViewTextBoxColumn5.AllowSort = false;
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.HeaderText = "Nama Item";
            gridViewTextBoxColumn5.Multiline = true;
            gridViewTextBoxColumn5.Name = "clmnNama";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 380;
            gridViewTextBoxColumn5.WrapText = true;
            gridViewComboBoxColumn2.HeaderText = "Unit";
            gridViewComboBoxColumn2.Name = "clmnUnit";
            gridViewComboBoxColumn2.ReadOnly = true;
            gridViewComboBoxColumn2.Width = 84;
            gridViewDecimalColumn5.AllowGroup = false;
            gridViewDecimalColumn5.AllowResize = false;
            gridViewDecimalColumn5.AllowSort = false;
            gridViewDecimalColumn5.AutoEllipsis = false;
            gridViewDecimalColumn5.EnableExpressionEditor = false;
            gridViewDecimalColumn5.FormatString = "{0:N}";
            gridViewDecimalColumn5.HeaderText = "Jumlah";
            gridViewDecimalColumn5.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            gridViewDecimalColumn5.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn5.Name = "clmnJumlah";
            gridViewDecimalColumn5.ReadOnly = true;
            gridViewDecimalColumn5.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn5.Width = 100;
            gridViewDecimalColumn6.AllowSort = false;
            gridViewDecimalColumn6.AutoEllipsis = false;
            gridViewDecimalColumn6.DecimalPlaces = 0;
            gridViewDecimalColumn6.FormatString = "{0:N}";
            gridViewDecimalColumn6.HeaderText = "Harga";
            gridViewDecimalColumn6.Name = "clmnHarga";
            gridViewDecimalColumn6.ReadOnly = true;
            gridViewDecimalColumn6.Width = 119;
            gridViewDecimalColumn7.FormatString = "{0:N}";
            gridViewDecimalColumn7.HeaderText = "Disc (%)";
            gridViewDecimalColumn7.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn7.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn7.Name = "clmnDiscount";
            gridViewDecimalColumn7.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn7.Width = 77;
            gridViewDecimalColumn7.WrapText = true;
            gridViewDecimalColumn8.AllowFiltering = false;
            gridViewDecimalColumn8.AllowGroup = false;
            gridViewDecimalColumn8.AllowHide = false;
            gridViewDecimalColumn8.AllowReorder = false;
            gridViewDecimalColumn8.AllowResize = false;
            gridViewDecimalColumn8.AllowSort = false;
            gridViewDecimalColumn8.DecimalPlaces = 0;
            gridViewDecimalColumn8.FormatString = "{0:N}";
            gridViewDecimalColumn8.HeaderText = "Sub Total";
            gridViewDecimalColumn8.Name = "clmnSubTotal";
            gridViewDecimalColumn8.ReadOnly = true;
            gridViewDecimalColumn8.Width = 180;
            gridViewCommandColumn2.AllowSort = false;
            gridViewCommandColumn2.AutoEllipsis = false;
            gridViewCommandColumn2.DefaultText = "Hapus";
            gridViewCommandColumn2.HeaderText = "";
            gridViewCommandColumn2.Name = "clmnHapus";
            gridViewCommandColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewCommandColumn2.Width = 88;
            gridViewTextBoxColumn6.HeaderText = "";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "clmnItemType";
            this.gridItem.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewComboBoxColumn2,
            gridViewDecimalColumn5,
            gridViewDecimalColumn6,
            gridViewDecimalColumn7,
            gridViewDecimalColumn8,
            gridViewCommandColumn2,
            gridViewTextBoxColumn6});
            this.gridItem.MasterTemplate.EnableAlternatingRowColor = true;
            this.gridItem.MasterTemplate.EnableGrouping = false;
            this.gridItem.MasterTemplate.HorizontalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            this.gridItem.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.gridItem.MasterTemplate.ShowFilteringRow = false;
            this.gridItem.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridItem.MasterTemplate.VerticalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            this.gridItem.Name = "gridItem";
            this.gridItem.NewRowEnterKeyMode = Telerik.WinControls.UI.RadGridViewNewRowEnterKeyMode.None;
            this.gridItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gridItem.ShowGroupPanel = false;
            this.gridItem.ShowItemToolTips = false;
            this.gridItem.ShowNoDataText = false;
            this.gridItem.Size = new System.Drawing.Size(1198, 453);
            this.gridItem.TabIndex = 0;
            this.gridItem.Text = "radGridView1";
            this.gridItem.ThemeName = "VisualStudio2012Dark";
            this.gridItem.UseScrollbarsInHierarchy = true;
            this.gridItem.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.gridItem_ViewCellFormatting);
            this.gridItem.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.gridItem_CellBeginEdit);
            this.gridItem.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridItem_CellEditorInitialized);
            this.gridItem.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridItem_CellEndEdit);
            this.gridItem.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridItem_CellClick);
            this.gridItem.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridItem_CellValueChanged);
            this.gridItem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridItem_MouseUp);
            // 
            // radButton1
            // 
            this.radButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(6, 6);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(116, 52);
            this.radButton1.TabIndex = 2;
            this.radButton1.Text = "&Tambah";
            this.radButton1.ThemeName = "VisualStudio2012Dark";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radButton2
            // 
            this.radButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton2.Location = new System.Drawing.Point(6, 6);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(153, 52);
            this.radButton2.TabIndex = 3;
            this.radButton2.Text = "&Tutup";
            this.radButton2.ThemeName = "VisualStudio2012Dark";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(8);
            this.panel1.Size = new System.Drawing.Size(1238, 88);
            this.panel1.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(8, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1222, 72);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel14);
            this.panel2.Controls.Add(this.panel13);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.panel10);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 629);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(18, 8, 20, 8);
            this.panel2.Size = new System.Drawing.Size(1238, 80);
            this.panel2.TabIndex = 5;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.panel16);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(474, 8);
            this.panel14.Name = "panel14";
            this.panel14.Padding = new System.Windows.Forms.Padding(6);
            this.panel14.Size = new System.Drawing.Size(386, 64);
            this.panel14.TabIndex = 9;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.radButton3);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(177, 6);
            this.panel16.Name = "panel16";
            this.panel16.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.panel16.Size = new System.Drawing.Size(177, 52);
            this.panel16.TabIndex = 1;
            // 
            // radButton3
            // 
            this.radButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton3.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton3.Location = new System.Drawing.Point(6, 0);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(171, 52);
            this.radButton3.TabIndex = 3;
            this.radButton3.Text = "&Batal";
            this.radButton3.ThemeName = "VisualStudio2012Dark";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.radButton6);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(6, 6);
            this.panel15.Name = "panel15";
            this.panel15.Padding = new System.Windows.Forms.Padding(0, 0, 6, 0);
            this.panel15.Size = new System.Drawing.Size(171, 52);
            this.panel15.TabIndex = 0;
            // 
            // radButton6
            // 
            this.radButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton6.Location = new System.Drawing.Point(0, 0);
            this.radButton6.Name = "radButton6";
            this.radButton6.Size = new System.Drawing.Size(165, 52);
            this.radButton6.TabIndex = 2;
            this.radButton6.Text = "&Pembayaran";
            this.radButton6.ThemeName = "VisualStudio2012Dark";
            this.radButton6.Click += new System.EventHandler(this.radButton6_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.radButton5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(860, 8);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(6);
            this.panel4.Size = new System.Drawing.Size(193, 64);
            this.panel4.TabIndex = 6;
            // 
            // radButton5
            // 
            this.radButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton5.Location = new System.Drawing.Point(6, 6);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(181, 52);
            this.radButton5.TabIndex = 3;
            this.radButton5.Text = "&Virtual Keyboard";
            this.radButton5.ThemeName = "VisualStudio2012Dark";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.radButton4);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(146, 8);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(6);
            this.panel9.Size = new System.Drawing.Size(128, 64);
            this.panel9.TabIndex = 3;
            // 
            // radButton4
            // 
            this.radButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radButton4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton4.Location = new System.Drawing.Point(6, 6);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(116, 52);
            this.radButton4.TabIndex = 2;
            this.radButton4.Text = "&Menu";
            this.radButton4.ThemeName = "VisualStudio2012Dark";
            this.radButton4.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.radButton1);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(18, 8);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(6);
            this.panel10.Size = new System.Drawing.Size(128, 64);
            this.panel10.TabIndex = 5;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.radButton2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(1053, 8);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(6);
            this.panel8.Size = new System.Drawing.Size(165, 64);
            this.panel8.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 88);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(20, 5, 20, 5);
            this.panel5.Size = new System.Drawing.Size(1238, 541);
            this.panel5.TabIndex = 8;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.gridItem);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(20, 83);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1198, 453);
            this.panel7.TabIndex = 8;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel12);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Controls.Add(this.panel11);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(20, 5);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(4);
            this.panel6.Size = new System.Drawing.Size(1198, 78);
            this.panel6.TabIndex = 7;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel17);
            this.panel12.Controls.Add(this.panel18);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(780, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(414, 70);
            this.panel12.TabIndex = 6;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.radLabel4);
            this.panel17.Controls.Add(this.radLabel1);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel17.Location = new System.Drawing.Point(0, 6);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(414, 32);
            this.panel17.TabIndex = 9;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.ForeColor = System.Drawing.Color.White;
            this.radLabel4.Location = new System.Drawing.Point(0, 0);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(162, 32);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Text = "No. Transaksi: ";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.Color.White;
            this.radLabel1.Location = new System.Drawing.Point(107, 0);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(307, 32);
            this.radLabel1.TabIndex = 6;
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.radLabel3);
            this.panel18.Controls.Add(this.lblGrandTotal);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel18.Location = new System.Drawing.Point(0, 38);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(414, 32);
            this.panel18.TabIndex = 8;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.ForeColor = System.Drawing.Color.White;
            this.radLabel3.Location = new System.Drawing.Point(0, 0);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(162, 32);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "Grand Total: ";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.AutoSize = false;
            this.lblGrandTotal.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblGrandTotal.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.ForeColor = System.Drawing.Color.White;
            this.lblGrandTotal.Location = new System.Drawing.Point(104, 0);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(310, 32);
            this.lblGrandTotal.TabIndex = 5;
            this.lblGrandTotal.Text = "0.00";
            this.lblGrandTotal.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(417, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(214, 70);
            this.panel3.TabIndex = 5;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.radLabel2);
            this.panel11.Controls.Add(this.lblTanggal);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(4, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(413, 70);
            this.panel11.TabIndex = 4;
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.ForeColor = System.Drawing.Color.White;
            this.radLabel2.Location = new System.Drawing.Point(0, 6);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(413, 32);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Kasir:";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTanggal
            // 
            this.lblTanggal.AutoSize = false;
            this.lblTanggal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblTanggal.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTanggal.ForeColor = System.Drawing.Color.White;
            this.lblTanggal.Location = new System.Drawing.Point(0, 38);
            this.lblTanggal.Name = "lblTanggal";
            this.lblTanggal.Size = new System.Drawing.Size(413, 32);
            this.lblTanggal.TabIndex = 0;
            this.lblTanggal.Text = "Tanggal: ";
            this.lblTanggal.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(274, 8);
            this.panel13.Name = "panel13";
            this.panel13.Padding = new System.Windows.Forms.Padding(6);
            this.panel13.Size = new System.Drawing.Size(200, 64);
            this.panel13.TabIndex = 8;
            // 
            // FrmSelling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1238, 709);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmSelling";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmTransaksi";
            this.ThemeName = "VisualStudio2012Dark";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmTransaksi_FormClosing);
            this.Load += new System.EventHandler(this.FrmSelling_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridItem.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            this.panel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGrandTotal)).EndInit();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanggal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView gridItem;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadButton radButton2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private Telerik.WinControls.UI.RadButton radButton3;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel lblTanggal;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel4;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadButton radButton6;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel17;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.Panel panel18;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel lblGrandTotal;
        private System.Windows.Forms.Panel panel13;
        private Telerik.WinControls.Themes.VisualStudio2012DarkTheme visualStudio2012DarkTheme1;
    }
}
