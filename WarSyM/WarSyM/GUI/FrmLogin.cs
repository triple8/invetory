﻿using BusinessProcess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using T8Core;
using Telerik.WinControls;

namespace WarSyM.GUI
{
    public partial class FrmLogin : Telerik.WinControls.UI.RadForm
    {
        FrmMain frmMain;
        public FrmLogin(FrmMain _frmMain)
        {
            InitializeComponent();
            this.frmMain = _frmMain;
            this.frmMain.cancelLogin = false;
            this.frmMain.isLogin = false;
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            this.frmMain.cancelLogin = true;
            this.frmMain.isLogin = false;
            this.Close();
        }

        private void btnMasuk_Click(object sender, EventArgs e)
        {
            string password = Logic.GetMd5Hash(txtPassword.Text);
            string message = UserAccess.Instance.Login(txtUsername.Text, password);
            if(message.Split(',')[0] == "success")
            {
                UserLoginInfo.IdLogin = int.Parse(message.Split(',')[1]);
                this.frmMain.isLogin = true;
                this.Close();
            }
            else if(message == "user not found")
            {
                MessageBox.Show(this, "User tidak terdaftar, mohon periksa kembali.", "User not found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not saved",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
