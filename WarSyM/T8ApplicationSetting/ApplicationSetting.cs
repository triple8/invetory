﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T8ApplicationSetting
{
    public class ApplicationSetting
    {
        private string ConnectionString;
        private string Host;
        private string Port;
        private string DatabaseName;
        private string DatabaseUser;
        private string DatabasePassword;

        private static ApplicationSetting instance;

        public static ApplicationSetting Instance
        {
            get
            {
                if (instance == null)
                    instance = new ApplicationSetting();
                return instance;
            }
        }

        private ApplicationSetting() {  }

        public MySqlConnection MySqlApplicationConnection = new MySqlConnection();
        
        void FileSetting(string path)
        {
            StreamReader readFile = new StreamReader(path);
            string[] arraySettings = readFile.ReadLine().Split(';');
            readFile.Close();

            Host = arraySettings[0];
            Port = arraySettings[1];
            DatabaseName = arraySettings[2];
            DatabaseUser = arraySettings[3];
            DatabasePassword = arraySettings[4];
            SetConnectionString(Host, Port, DatabaseName, DatabaseUser, DatabasePassword);
        }

        void SetConnectionString(string _host, string _port, string _dbName, string _dbUser, string _dbPass)
        {
            ConnectionString =
                "server=" + _host + ";uid=" + _dbUser + ";password=" + _dbPass + ";database=" + _dbName + ";port=" + _port;
            MySqlApplicationConnection = new MySqlConnection(ConnectionString);
        }

        public void SetupLocalSetting(string localSettingFilePath)
        {
            FileSetting(localSettingFilePath);
        }
    }
}
