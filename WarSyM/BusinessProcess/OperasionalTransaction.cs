﻿using DataAccess.Transaction;
using DataModel.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessProcess
{
    public class OperasionalTransaction
    {
        private static OperasionalTransaction instance;

        public static OperasionalTransaction Instance
        {
            get
            {
                if (instance == null)
                    instance = new OperasionalTransaction();
                return instance;
            }
        }

        public string Insert(Operasional data)
        {
            return DAOperasional.Instance.Insert(data.Tanggal, data.Nominal, data.Keterangan, data.User, data.IdToko);
        }
    }
}
