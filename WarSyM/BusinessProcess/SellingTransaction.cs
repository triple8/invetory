﻿using DataAccess.Transaction;
using DataModel.Transaction;
using System.Collections.Generic;
using System.Linq;

namespace BusinessProcess
{
    public class SellingTransaction
    {
        private static SellingTransaction instance;

        public static SellingTransaction Instance
        {
            get
            {
                if (instance == null)
                    instance = new SellingTransaction();
                return instance;
            }
        }


        public string InsertData(Selling data, string[] menuIdBarang, string[] menuIdSatuan, string[] menuJumlah)
        {
            var dataArray = data.Detail.ToArray();
            List<string> idBarang = new List<string>();
            List<string> idSatuan = new List<string>();
            List<string> harga = new List<string>();
            List<string> jumlah = new List<string>();
            List<string> diskon = new List<string>();
            for (int i = 0; i < data.Detail.Count(); i++)
            {
                idBarang.Add(dataArray[i].id_barang.ToString());
                idSatuan.Add(dataArray[i].id_satuan.ToString());
                harga.Add(dataArray[i].harga.ToString());
                jumlah.Add(dataArray[i].jumlah.ToString());
                diskon.Add(dataArray[i].diskon.ToString());
            }


            var dataMenuArray = data.DetailMenu.ToArray();
            List<string> idMenu = new List<string>();
            List<string> hargaMenu = new List<string>();
            List<string> jumlahMenu = new List<string>();
            List<string> diskonMenu = new List<string>();

            for (int i = 0; i < data.DetailMenu.Count(); i++)
            {
                idMenu.Add(dataMenuArray[i].id_menu.ToString());
                hargaMenu.Add(dataMenuArray[i].harga.ToString());
                jumlahMenu.Add(dataMenuArray[i].jumlah.ToString());
                diskonMenu.Add(dataMenuArray[i].diskon.ToString());
            }

            return DASelling.Instance.Insert(string.Join(",", idBarang.ToArray()), string.Join(",", idSatuan.ToArray()), string.Join(",", harga.ToArray()),
                string.Join(",", jumlah.ToArray()), string.Join(",", diskon.ToArray()), data.diskon, data.id_toko, data.user,
                data.Detail.Count(), string.Join(",", idMenu.ToArray()), string.Join(",", jumlahMenu.ToArray()), string.Join(",", hargaMenu.ToArray()),  
                string.Join(",", diskonMenu.ToArray()), idMenu.Count(), string.Join(",", menuIdBarang), string.Join(",", menuIdSatuan), 
                string.Join(",", menuJumlah), menuIdBarang.Count());
        }
    }
}
