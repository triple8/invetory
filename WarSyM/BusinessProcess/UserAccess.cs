﻿using DataAccess;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T8Core;

namespace BusinessProcess
{
    public class UserAccess
    {
        private static UserAccess instance;

        public static UserAccess Instance
        {
            get
            {
                if (instance == null)
                    instance = new UserAccess();
                return instance;
            }
        }

        public string Login(string username, string password)
        {
            var appUser = Repository.User
                .Where(user => user.Username.ToLower() == username.ToLower() && user.Password.ToLower() == password.ToLower())
                .Select(user => user).FirstOrDefault();

            if(appUser != null)
            {
                UserLoginInfo.Username = appUser.Username;
                UserLoginInfo.Password = appUser.Password;
                UserLoginInfo.HakAkses = appUser.Akses;
                UserLoginInfo.Application = "Inventory - Transaksi";

                return DAHistoryLoginUser.Instance.Insert(UserLoginInfo.Username, UserLoginInfo.Application, 1);
            }

            return "user not found";
        }

        public string Logout(int idLogin, int idToko)
        {
            return DAHistoryLoginUser.Instance.Update(idLogin, idToko);
        }
    }
}
