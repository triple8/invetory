﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Goods
{
    public class Menu
    {
        public int id { get; set; }

        public string kode { get; set; }
        
        public string nama { get; set; }
        
        public decimal harga { get; set; }

        public List<MenuDetails> menuDetails { get; set; }

    }
}
