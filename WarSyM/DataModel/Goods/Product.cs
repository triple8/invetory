﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Goods
{
    public class Product
    {
        public int idBarang { get; set; }

        public int idSatuan { get; set; }

        public int idSatuanParent { get; set; }

        public float retail { get; set; }

        public float minimumStok { get; set; }

        public decimal hargaJual { get; set; }

    }
}
