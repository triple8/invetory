﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Goods
{
    public class MenuDetails
    {
        
        public int idMenu { get; set; }
        
        public int idBarang { get; set; }
        
        public int idSatuan { get; set; }

        public float jumlah { get; set; }

    }
}
