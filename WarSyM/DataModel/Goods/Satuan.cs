﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Goods
{
    public class Satuan
    {
        public int id { get; set; }

        public string nama { get; set; }

        public string kode { get; set; }

        public string keterangan { get; set; }
    }
}
