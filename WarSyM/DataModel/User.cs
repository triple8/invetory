﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class User
    {
        
        public string Username { get; set; }
        
        public string Password { get; set; }
        
        public string Nama { get; set; }
        
        public string Alamat { get; set; }

        public string Telp { get; set; }

        public string Akses { get; set; }

    }
}
