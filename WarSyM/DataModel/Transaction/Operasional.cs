﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Transaction
{
    public class Operasional
    {

        public string Kode { get; set; }

        public DateTime Tanggal { get; set; }

        public decimal Nominal { get; set; }

        public string Keterangan { get; set; }

        public string User { get; set; }

        public int IdToko { get; set; }

        public DateTime TanggalInput { get; set; }

    }
}
