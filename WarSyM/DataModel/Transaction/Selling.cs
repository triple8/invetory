﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Transaction
{
    public class Selling
    {
        public string nomor_transaksi { get; set; }
        
        public DateTime tanggal_transaksi { get; set; }
        
        public DateTime waktu { get; set; }

        public decimal diskon { get; set; }

        public int id_toko { get; set; }

        public string user { get; set; }

        public List<SellingDetail> Detail { get; set; }

        public List<SellingDetailMenu> DetailMenu { get; set; }


        public Selling()
        {
            //SellingDetail sellingDetail = new SellingDetail();            
        }
    }
}
