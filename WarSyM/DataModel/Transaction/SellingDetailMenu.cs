﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Transaction
{
    public class SellingDetailMenu
    {
        public string nomor_transaksi { get; set; }

        public int id_menu { get; set; }
        
        public decimal harga { get; set; }

        public decimal jumlah { get; set; }

        public decimal diskon { get; set; }

        public decimal SubTotal
        {
            get
            {
                return (harga * jumlah) - ((harga * jumlah) * (diskon / (decimal)100));
            }
        }
    }
}
