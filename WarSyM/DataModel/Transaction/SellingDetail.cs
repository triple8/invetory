﻿
namespace DataModel.Transaction
{
    public class SellingDetail
    {
        public string nomor_transaksi { get; set; }

        public int id_barang { get; set; }

        public int id_satuan { get; set; }

        public decimal harga { get; set; }

        public decimal jumlah { get; set; }

        public decimal diskon { get; set; }

        public decimal SubTotal
        {
            get 
            {
                return (harga * jumlah) - ((harga * jumlah) * (diskon/(decimal)100)); 
            }
        }
    }
}
