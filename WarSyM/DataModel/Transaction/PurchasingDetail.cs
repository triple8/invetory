﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Transaction
{
    public class PurchasingDetail
    {
        public string kodeItem { get; set; }
        public string namaItem { get; set; }
        public decimal jumlah { get; set; }
        public string unit { get; set; }
        public decimal hargaItem { get; set; }
        public decimal subTotal
        {
            get
            {
                return jumlah * hargaItem;
            }
        }
    }
}
