-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: inventory
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang`
--

LOCK TABLES `barang` WRITE;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` VALUES (1,'S12','SURYA 12'),(2,'S16','SURYA 16'),(3,'KA1','KAPAL API SACHET'),(4,'GP','GULA PASIR'),(5,'KPBB','KOPI BUBUK GORENG'),(6,'SSK-NONA','SUSU KALENG NONA'),(7,'DSS','DJISAMSOE');
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barangdetail`
--

DROP TABLE IF EXISTS `barangdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barangdetail` (
  `id_barang` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `id_satuan_parent` int(11) DEFAULT NULL,
  `retail` decimal(11,2) DEFAULT NULL,
  `minimum_stok` decimal(11,2) DEFAULT NULL,
  `harga_jual` decimal(11,2) DEFAULT NULL,
  `bahan_menu` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_barang`,`id_satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barangdetail`
--

LOCK TABLES `barangdetail` WRITE;
/*!40000 ALTER TABLE `barangdetail` DISABLE KEYS */;
INSERT INTO `barangdetail` VALUES (1,2,3,12.00,6.00,1450.00,NULL,NULL),(1,3,4,10.00,80.00,13500.00,NULL,NULL),(1,4,1,0.00,8.00,195000.00,NULL,NULL),(2,4,1,0.00,0.00,0.00,NULL,NULL),(3,5,1,0.00,15.00,2350.00,NULL,NULL),(7,4,1,0.00,0.00,0.00,NULL,NULL);
/*!40000 ALTER TABLE `barangdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hargabeli`
--

DROP TABLE IF EXISTS `hargabeli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hargabeli` (
  `kode_supplier` varchar(50) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `harga` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`kode_supplier`,`id_barang`,`id_satuan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hargabeli`
--

LOCK TABLES `hargabeli` WRITE;
/*!40000 ALTER TABLE `hargabeli` DISABLE KEYS */;
INSERT INTO `hargabeli` VALUES ('S1',1,4,193000.00),('S1',1,3,14000.00),('S1',1,5,1500.00),('S1',1,2,1520.00),('S1',3,5,1800.00),('S1',2,4,165000.00);
/*!40000 ALTER TABLE `hargabeli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historyloginuser`
--

DROP TABLE IF EXISTS `historyloginuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historyloginuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) DEFAULT NULL,
  `login` datetime DEFAULT NULL,
  `logout` datetime DEFAULT NULL,
  `aplikasi` text,
  `id_toko` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=306 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historyloginuser`
--

LOCK TABLES `historyloginuser` WRITE;
/*!40000 ALTER TABLE `historyloginuser` DISABLE KEYS */;
INSERT INTO `historyloginuser` VALUES (239,'admin','2015-12-22 07:46:52','2015-12-22 07:47:22','Inventory - Data Management',NULL),(240,'admin','2015-12-22 07:50:50','2015-12-22 07:51:28','Inventory - Data Management',NULL),(241,'admin','2015-12-22 07:53:50','2015-12-22 07:54:22','Inventory - Data Management',NULL),(242,'admin','2015-12-22 07:55:56','2015-12-22 07:56:52','Inventory - Data Management',NULL),(243,'admin','2015-12-22 07:58:55','2015-12-22 07:59:13','Inventory - Data Management',NULL),(244,'admin','2015-12-22 08:00:09','2015-12-22 08:00:30','Inventory - Data Management',NULL),(245,'admin','2015-12-22 08:01:42','2015-12-22 08:02:54','Inventory - Data Management',NULL),(246,'admin','2015-12-22 12:13:29','2015-12-22 12:26:13','Inventory - Data Management',NULL),(247,'admin','2015-12-22 12:27:06','2015-12-22 12:27:42','Inventory - Data Management',NULL),(248,'admin','2015-12-22 12:28:58','2015-12-22 12:30:38','Inventory - Data Management',NULL),(249,'admin','2015-12-22 12:31:41',NULL,'Inventory - Data Management',NULL),(250,'admin','2015-12-22 12:32:52','2015-12-22 12:33:02','Inventory - Data Management',NULL),(251,'admin','2015-12-22 12:34:16','2015-12-22 12:35:20','Inventory - Data Management',NULL),(252,'admin','2015-12-22 12:36:12','2015-12-22 12:36:32','Inventory - Data Management',NULL),(253,'admin','2015-12-22 12:43:43','2015-12-22 12:47:42','Inventory - Data Management',NULL),(254,'admin','2015-12-22 12:48:34','2015-12-22 12:48:51','Inventory - Data Management',NULL),(255,'admin','2015-12-22 12:49:01','2015-12-22 12:51:17','Inventory - Data Management',NULL),(256,'admin','2015-12-22 18:12:34',NULL,'Inventory - Data Management',NULL),(257,'admin','2015-12-22 18:13:12','2015-12-22 18:15:13','Inventory - Data Management',NULL),(258,'admin','2015-12-22 18:15:56','2015-12-22 18:16:11','Inventory - Data Management',NULL),(259,'admin','2015-12-22 18:21:03','2015-12-22 18:21:25','Inventory - Data Management',NULL),(260,'admin','2015-12-22 18:26:58','2015-12-22 18:29:26','Inventory - Data Management',NULL),(261,'admin','2015-12-22 18:41:58','2015-12-22 18:42:11','Inventory - Data Management',NULL),(262,'admin','2015-12-24 14:23:31',NULL,'Inventory - Data Management',NULL),(263,'admin','2015-12-24 14:24:40','2015-12-24 14:24:42','Inventory - Data Management',NULL),(264,'admin','2015-12-24 14:29:30','2015-12-24 14:38:08','Inventory - Data Management',NULL),(265,'admin','2015-12-24 14:53:05','2015-12-24 14:53:06','Inventory - Data Management',NULL),(266,'admin','2015-12-24 15:13:15','2015-12-24 15:13:22','Inventory - Data Management',NULL),(267,'admin','2015-12-24 15:13:35','2015-12-24 15:13:41','Inventory - Data Management',NULL),(268,'admin','2015-12-25 22:40:49','2015-12-25 22:47:47','Inventory - Data Management',NULL),(269,'admin','2015-12-26 04:53:30','2015-12-26 04:54:05','Inventory - Data Management',NULL),(270,'admin','2015-12-28 12:11:49','2015-12-28 12:13:00','Inventory - Data Management',NULL),(271,'admin','2015-12-28 12:13:58','2015-12-28 12:16:36','Inventory - Data Management',NULL),(272,'admin','2015-12-28 12:25:15',NULL,'Inventory - Data Management',NULL),(273,'admin','2015-12-28 12:27:19','2015-12-28 12:33:20','Inventory - Data Management',NULL),(305,'admin','2015-12-30 15:20:05','2015-12-30 15:20:23','Inventory - Transaksi',1),(303,'admin','2015-12-30 15:16:05',NULL,'Inventory - Data Management',1),(304,'admin','2015-12-30 15:18:22','2015-12-30 15:19:06','Inventory - Transaksi',1),(302,'admin','2015-12-30 15:15:58','2015-12-30 15:16:42','Inventory - Transaksi',1);
/*!40000 ALTER TABLE `historyloginuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historypenyesuaianhargabeli`
--

DROP TABLE IF EXISTS `historypenyesuaianhargabeli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historypenyesuaianhargabeli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `kode_supplier` varchar(45) DEFAULT NULL,
  `harga_beli_awal` decimal(11,2) DEFAULT NULL,
  `harga_beli_sekarang` decimal(11,2) DEFAULT NULL,
  `info` text,
  `user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historypenyesuaianhargabeli`
--

LOCK TABLES `historypenyesuaianhargabeli` WRITE;
/*!40000 ALTER TABLE `historypenyesuaianhargabeli` DISABLE KEYS */;
INSERT INTO `historypenyesuaianhargabeli` VALUES (14,'2015-12-22 12:50:16',2,4,'S1',160000.00,165000.00,'','admin'),(4,'2015-12-21 08:10:12',0,0,'S1',1200.00,1345.00,'','admin'),(5,'2015-12-21 08:11:41',1,2,'S1',1200.00,1234.00,'','admin'),(6,'2015-12-21 08:12:25',1,2,'S1',1234.00,1345.00,'','admin'),(7,'2015-12-21 08:13:25',1,3,'S1',13500.00,14000.00,'','admin'),(8,'2015-12-21 13:34:47',1,0,'s1',1345.00,1567.00,'','admin'),(9,'2015-12-21 13:35:58',1,0,'s1',1345.00,1567.00,'','admin'),(10,'2015-12-21 13:37:23',1,0,'s1',1345.00,1567.00,'','admin'),(11,'2015-12-21 13:42:56',1,0,'s1',1345.00,1500.00,'','admin'),(12,'2015-12-21 13:50:11',1,2,'s1',1345.00,1456.00,'','admin'),(13,'2015-12-22 07:34:08',1,2,'S1',1456.00,1520.00,'','admin');
/*!40000 ALTER TABLE `historypenyesuaianhargabeli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historypenyesuaianhargajual`
--

DROP TABLE IF EXISTS `historypenyesuaianhargajual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historypenyesuaianhargajual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `harga_jual_awal` decimal(11,2) DEFAULT NULL,
  `harga_jual_sekarang` decimal(11,2) DEFAULT NULL,
  `info` text,
  `user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historypenyesuaianhargajual`
--

LOCK TABLES `historypenyesuaianhargajual` WRITE;
/*!40000 ALTER TABLE `historypenyesuaianhargajual` DISABLE KEYS */;
INSERT INTO `historypenyesuaianhargajual` VALUES (10,'2015-12-22 12:15:09',1,3,14560.00,13500.00,'','admin'),(2,'2015-12-19 09:44:31',3,5,2500.00,2350.00,'','administrator'),(3,'2015-12-19 19:30:41',0,0,0.00,0.00,'','admin'),(4,'2015-12-19 19:44:28',0,0,13500.00,14560.00,'','admin'),(5,'2015-12-19 19:45:31',0,0,13500.00,14560.00,'','admin'),(6,'2015-12-19 19:51:56',1,0,13500.00,145600.00,'','admin'),(7,'2015-12-19 19:55:56',1,3,13500.00,14560.00,'','admin'),(8,'2015-12-21 13:39:17',1,0,1500.00,1345.00,'','admin'),(9,'2015-12-22 07:24:06',1,2,1500.00,1450.00,'','admin');
/*!40000 ALTER TABLE `historypenyesuaianhargajual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historypenyesuaianhargajualmenu`
--

DROP TABLE IF EXISTS `historypenyesuaianhargajualmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historypenyesuaianhargajualmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `harga_lama` decimal(11,2) DEFAULT NULL,
  `harga_baru` decimal(11,2) DEFAULT NULL,
  `info` text,
  `user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historypenyesuaianhargajualmenu`
--

LOCK TABLES `historypenyesuaianhargajualmenu` WRITE;
/*!40000 ALTER TABLE `historypenyesuaianhargajualmenu` DISABLE KEYS */;
INSERT INTO `historypenyesuaianhargajualmenu` VALUES (4,'2015-12-22 07:35:08',1,4500.00,5500.00,'','admin'),(5,'2015-12-22 12:35:07',1,5500.00,5000.00,'','admin');
/*!40000 ALTER TABLE `historypenyesuaianhargajualmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historypenyesuaianstok`
--

DROP TABLE IF EXISTS `historypenyesuaianstok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historypenyesuaianstok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `id_toko` int(11) DEFAULT NULL,
  `stok_awal` decimal(11,2) DEFAULT NULL,
  `stok_sekarang` decimal(11,2) DEFAULT NULL,
  `info` text,
  `user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historypenyesuaianstok`
--

LOCK TABLES `historypenyesuaianstok` WRITE;
/*!40000 ALTER TABLE `historypenyesuaianstok` DISABLE KEYS */;
INSERT INTO `historypenyesuaianstok` VALUES (9,'2015-12-20 10:30:23',1,2,1,0.00,0.00,'','admin'),(10,'2015-12-20 10:30:57',1,3,1,0.00,100.00,'','admin'),(11,'2015-12-20 13:45:14',1,4,2,3.00,10.00,'','admin'),(12,'2015-12-20 13:45:38',3,5,2,8.00,100.00,'','admin'),(13,'2015-12-21 13:25:10',1,3,1,100.00,14.00,'','admin'),(14,'2015-12-21 13:33:51',1,2,1,0.00,7.00,'','admin'),(15,'2015-12-21 13:41:00',1,2,1,7.00,2.00,'','admin'),(16,'2015-12-22 07:32:11',1,4,1,3.00,2.00,'','admin');
/*!40000 ALTER TABLE `historypenyesuaianstok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kartustok`
--

DROP TABLE IF EXISTS `kartustok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kartustok` (
  `id_barang` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `id_toko` int(11) DEFAULT NULL,
  `stok` decimal(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kartustok`
--

LOCK TABLES `kartustok` WRITE;
/*!40000 ALTER TABLE `kartustok` DISABLE KEYS */;
INSERT INTO `kartustok` VALUES (1,4,1,1.00),(3,5,1,2.00),(1,3,1,12.00),(1,3,2,7.00),(1,4,2,10.00),(3,5,2,100.00),(1,2,1,9.00),(1,2,2,0.00),(2,4,1,0.00),(2,4,2,0.00),(7,4,1,-1.00),(7,4,2,0.00);
/*!40000 ALTER TABLE `kartustok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `harga` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'KH-KA1','KOPI HITAM KAPAL API SACHET',5000.00),(2,'KOPISUSU','KOPI SUSU',3500.00);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menudetail`
--

DROP TABLE IF EXISTS `menudetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menudetail` (
  `id_menu` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `jumlah` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`id_menu`,`id_barang`,`id_satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menudetail`
--

LOCK TABLES `menudetail` WRITE;
/*!40000 ALTER TABLE `menudetail` DISABLE KEYS */;
INSERT INTO `menudetail` VALUES (1,3,5,1.00),(2,3,5,1.00);
/*!40000 ALTER TABLE `menudetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operasional`
--

DROP TABLE IF EXISTS `operasional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operasional` (
  `kode` varchar(255) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `nominal` decimal(11,2) DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `id_toko` int(11) DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operasional`
--

LOCK TABLES `operasional` WRITE;
/*!40000 ALTER TABLE `operasional` DISABLE KEYS */;
INSERT INTO `operasional` VALUES ('NJ10','2015-12-29 14:46:56',15000.00,'pembelian gas lpg 2,5 kg',NULL,1,'2015-12-29 14:53:17'),('OP11','2015-12-29 14:46:56',15000.00,'pembelian gas lpg',NULL,1,'2015-12-29 14:53:50'),('OP12','2015-12-29 14:46:56',25000.00,'beli air',NULL,1,'2015-12-30 15:18:36'),('OP13','2015-12-29 14:46:56',1000.00,'beli pulpen','admin',1,'2015-12-30 15:20:18');
/*!40000 ALTER TABLE `operasional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembelian`
--

DROP TABLE IF EXISTS `pembelian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_nota_supplier` text,
  `kode_supplier` varchar(50) DEFAULT NULL,
  `id_toko` int(11) DEFAULT NULL,
  `tanggal_beli` date DEFAULT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `diskon` decimal(11,2) DEFAULT NULL,
  `info` text,
  `user` varchar(50) DEFAULT NULL,
  `jenis_pembayaran` text,
  `uang_muka` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_2` (`id`),
  KEY `nonotasupplier` (`user`),
  KEY `id` (`id`,`tanggal_beli`,`tanggal_input`,`diskon`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembelian`
--

LOCK TABLES `pembelian` WRITE;
/*!40000 ALTER TABLE `pembelian` DISABLE KEYS */;
/*!40000 ALTER TABLE `pembelian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembeliandetail`
--

DROP TABLE IF EXISTS `pembeliandetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembeliandetail` (
  `id_pembelian` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `harga` decimal(11,2) DEFAULT NULL,
  `jumlah` decimal(11,2) DEFAULT NULL,
  `diskon` decimal(11,2) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `id_toko` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembeliandetail`
--

LOCK TABLES `pembeliandetail` WRITE;
/*!40000 ALTER TABLE `pembeliandetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `pembeliandetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembeliandetailmenu`
--

DROP TABLE IF EXISTS `pembeliandetailmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembeliandetailmenu` (
  `nomor_transaksi` varchar(255) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `harga` decimal(11,2) DEFAULT NULL,
  `jumlah` decimal(11,2) DEFAULT NULL,
  `diskon` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`nomor_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembeliandetailmenu`
--

LOCK TABLES `pembeliandetailmenu` WRITE;
/*!40000 ALTER TABLE `pembeliandetailmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `pembeliandetailmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penjualan`
--

DROP TABLE IF EXISTS `penjualan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `penjualan` (
  `nomor_transaksi` varchar(255) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `diskon` decimal(11,2) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `id_toko` int(11) DEFAULT NULL,
  `jenis_pembayaran` text,
  `uang_muka` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`nomor_transaksi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penjualan`
--

LOCK TABLES `penjualan` WRITE;
/*!40000 ALTER TABLE `penjualan` DISABLE KEYS */;
INSERT INTO `penjualan` VALUES ('NJ1','2015-12-28 12:10:02',0.00,'admin',1,NULL,NULL),('NJ2','2015-12-28 12:13:43',0.00,'admin',1,NULL,NULL),('NJ3','2015-12-28 12:15:00',0.00,'admin',1,NULL,NULL),('NJ4','2015-12-28 12:16:00',0.00,'admin',1,NULL,NULL),('NJ5','2015-12-28 12:25:02',0.00,'admin',1,NULL,NULL),('NJ6','2015-12-28 12:27:06',0.00,'admin',1,NULL,NULL),('NJ7','2015-12-28 12:28:00',0.00,'admin',1,NULL,NULL),('NJ8','2015-12-28 12:31:34',0.00,'admin',1,NULL,NULL),('NJ9','2015-12-29 14:40:15',0.00,'admin',1,NULL,NULL);
/*!40000 ALTER TABLE `penjualan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penjualandetail`
--

DROP TABLE IF EXISTS `penjualandetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `penjualandetail` (
  `nomor_transaksi` varchar(255) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `harga` decimal(11,2) DEFAULT NULL,
  `jumlah` decimal(11,2) DEFAULT NULL,
  `diskon` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`id_barang`,`nomor_transaksi`,`id_satuan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penjualandetail`
--

LOCK TABLES `penjualandetail` WRITE;
/*!40000 ALTER TABLE `penjualandetail` DISABLE KEYS */;
INSERT INTO `penjualandetail` VALUES ('NJ1',1,2,1450.00,1.00,0.00),('NJ1',1,4,195000.00,1.00,0.00),('NJ1',0,2,1450.00,2.00,0.00),('NJ1',7,4,0.00,1.00,0.00),('NJ2',1,2,1450.00,3.00,0.00),('NJ3',1,2,1450.00,3.00,0.00),('NJ4',1,2,1450.00,3.00,0.00),('NJ5',0,2,1450.00,7.00,0.00),('NJ6',0,2,1450.00,2.00,0.00),('NJ7',1,2,1450.00,2.00,0.00),('NJ8',1,2,1450.00,3.00,0.00),('NJ9',1,2,1450.00,2.00,0.00);
/*!40000 ALTER TABLE `penjualandetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penjualandetailmenu`
--

DROP TABLE IF EXISTS `penjualandetailmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `penjualandetailmenu` (
  `nomor_transaksi` varchar(255) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `harga` decimal(11,2) DEFAULT NULL,
  `jumlah` decimal(11,2) DEFAULT NULL,
  `diskon` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`nomor_transaksi`,`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penjualandetailmenu`
--

LOCK TABLES `penjualandetailmenu` WRITE;
/*!40000 ALTER TABLE `penjualandetailmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `penjualandetailmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returbeli`
--

DROP TABLE IF EXISTS `returbeli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `returbeli` (
  `nomor_retur` varchar(50) NOT NULL,
  `nomor_nota_beli` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `id_toko` int(11) DEFAULT NULL,
  PRIMARY KEY (`nomor_retur`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returbeli`
--

LOCK TABLES `returbeli` WRITE;
/*!40000 ALTER TABLE `returbeli` DISABLE KEYS */;
/*!40000 ALTER TABLE `returbeli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returbelidetail`
--

DROP TABLE IF EXISTS `returbelidetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `returbelidetail` (
  `nomor_retur_beli` varchar(255) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `jumlah_retur` decimal(11,2) DEFAULT NULL,
  `uang_kembali` decimal(11,2) DEFAULT NULL,
  `jenis_retur` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`nomor_retur_beli`,`id_barang`,`id_satuan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returbelidetail`
--

LOCK TABLES `returbelidetail` WRITE;
/*!40000 ALTER TABLE `returbelidetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `returbelidetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returbelidetailmenu`
--

DROP TABLE IF EXISTS `returbelidetailmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `returbelidetailmenu` (
  `nomor_retur_beli` varchar(50) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `jumlah_retur` decimal(11,2) DEFAULT NULL,
  `uang_kembali` decimal(11,2) DEFAULT NULL,
  `jenis_retur` text,
  PRIMARY KEY (`nomor_retur_beli`,`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returbelidetailmenu`
--

LOCK TABLES `returbelidetailmenu` WRITE;
/*!40000 ALTER TABLE `returbelidetailmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `returbelidetailmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returjual`
--

DROP TABLE IF EXISTS `returjual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `returjual` (
  `nomor_retur` varchar(50) NOT NULL,
  `nomor_nota_jual` varchar(50) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `id_toko` int(11) DEFAULT NULL,
  PRIMARY KEY (`nomor_retur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returjual`
--

LOCK TABLES `returjual` WRITE;
/*!40000 ALTER TABLE `returjual` DISABLE KEYS */;
/*!40000 ALTER TABLE `returjual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returjualdetail`
--

DROP TABLE IF EXISTS `returjualdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `returjualdetail` (
  `nomor_retur_jual` varchar(50) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `jumlah_retur` decimal(11,2) DEFAULT NULL,
  `uang_kembali` decimal(11,2) DEFAULT NULL,
  `jenis_retur` text,
  PRIMARY KEY (`nomor_retur_jual`,`id_barang`,`id_satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returjualdetail`
--

LOCK TABLES `returjualdetail` WRITE;
/*!40000 ALTER TABLE `returjualdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `returjualdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returjualdetailmenu`
--

DROP TABLE IF EXISTS `returjualdetailmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `returjualdetailmenu` (
  `nomor_retur_jual` varchar(50) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `jumlah_retur` decimal(11,2) DEFAULT NULL,
  `uang_kembali` decimal(11,2) DEFAULT NULL,
  `jenis_retur` text,
  PRIMARY KEY (`nomor_retur_jual`,`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returjualdetailmenu`
--

LOCK TABLES `returjualdetailmenu` WRITE;
/*!40000 ALTER TABLE `returjualdetailmenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `returjualdetailmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `satuan`
--

DROP TABLE IF EXISTS `satuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `satuan`
--

LOCK TABLES `satuan` WRITE;
/*!40000 ALTER TABLE `satuan` DISABLE KEYS */;
INSERT INTO `satuan` VALUES (1,'root','root','root'),(2,'ECR','ECERAN','SATUAN STOK ECERAN'),(3,'BKS','BUNGKUS','SATUAN STOK BUNGKUS'),(4,'GS','GROS','SATUAN GROSS'),(5,'SC','SACHET','SATUAN SACHET'),(6,'KL','KALENG','SATUAN KALENG');
/*!40000 ALTER TABLE `satuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `key` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES ('UseCapitalForInputTextBox','normal'),('DecimalPlaceForNumeric','2'),('HeaderNota1',''),('HeaderNota2',''),('HeaderNota3',''),('HeaderNota4',' '),('BottomNota',' '),('InfoNota','');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `kota` text,
  `telp` text,
  PRIMARY KEY (`kode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES ('S1','SUPPLIER 1','MENANGGAL','SURABAYA','031');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toko`
--

DROP TABLE IF EXISTS `toko`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(45) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `telp` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toko`
--

LOCK TABLES `toko` WRITE;
/*!40000 ALTER TABLE `toko` DISABLE KEYS */;
INSERT INTO `toko` VALUES (1,'T1','TOKO 1','MENANGGAL UTARA','031'),(2,'T2','TOKO 2','SURABAYA','031');
/*!40000 ALTER TABLE `toko` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `telp` text,
  `akses` text,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('admin','21232f297a57a5a743894a0e4a801fc3','ADMINISTRATOR','SURABAYA','08175005221','Administrator'),('user1','24c9e15e52afc47c225b757e7bee1f9d','user 1','surabaya','031','Cetak Laporan / History, Pembelian / Retur Beli, Pengaturan Aplikasi, Penjualan / Retur Jual, Penyesuaian Stok/Harga'),('administrator','200ceb26807d6bf99fd6f4f0d1ca54d4','administrator','surabaya','031','Administrator');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'inventory'
--

--
-- Dumping routines for database 'inventory'
--
/*!50003 DROP FUNCTION IF EXISTS `GenerateKodeOperasional` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `GenerateKodeOperasional`() RETURNS varchar(45) CHARSET utf8
BEGIN 
	declare maxID int(11) default 0;
 
	select max(cast(substring(kode, 3, length(kode - 2)) as SIGNED)) into maxID from operasional where 1=1;
 
	if(maxID is null) then
		return 'OP1';
	else
		return  concat('OP', maxID + 1);
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GenerateKodeSupplier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `GenerateKodeSupplier`() RETURNS varchar(45) CHARSET utf8
BEGIN 
	declare maxID int(11) default 0;
 
	select max(cast(substring(kode, 2, length(kode - 1)) as SIGNED)) into maxID from supplier where 1=1;
 
	if(maxID is null) then
		return 'S1';
	else
		return  concat('S', maxID + 1);
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GenerateKodeTransaksi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `GenerateKodeTransaksi`() RETURNS varchar(45) CHARSET utf8
BEGIN 
	declare maxID int(11) default 0;
 
	select max(cast(substring(nomor_transaksi, 3, length(nomor_transaksi - 2)) as SIGNED)) into maxID from penjualan where 1=1;
 
	if(maxID is null) then
		return 'NJ1';
	else
		return  concat('NJ', maxID + 1);
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `UpdateStok` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `UpdateStok`(
	VIdBarang int(11),
    VIdSatuan int(11),
    VJumlah decimal(11,2),
    VIdToko int(11)
) RETURNS varchar(45) CHARSET utf8
BEGIN		      
	declare newStok decimal(11,2);
    
    declare VArrayId text;
    declare VArrayStok text;
    declare VArrayRetail text;
    declare VArrayStokNeeded text; 
    
    declare VCount int(11);
    
    declare parentId int(11);
    declare parentName varchar(45);
    
    declare retailStok decimal(11,2);
    declare parentStok decimal(11,2);
    declare stokNeeded decimal(11,2);
        
	update kartustok
	set stok=stok-VJumlah
	where id_barang=VIdBarang and id_satuan=VIdSatuan and id_toko=VIdToko;
    
    select stok into newStok 
    from kartustok
    where id_barang=VIdBarang and id_satuan=VIdSatuan and id_toko=VIdToko;
    
    if(newStok < 0) then
    
		set VArrayId='';
		set VArrayStok='';
		set VArrayRetail = '';     
		set VArrayStokNeeded = '';
        set stokNeeded = newStok*(-1); 
        set VCount=0;
        
		m_loop:loop
        
			select id_satuan_parent, retail into parentId, retailStok
            from barangdetail
            where id_barang=VIdBarang and id_satuan=VIdSatuan;
            
            select nama into parentName
            from satuan 
            where id=parentId;
            
            if(parentName='root') then
				leave m_loop;
			else
            
				select stok into parentstok
                from kartustok
                where id_barang=VIdBarang and id_satuan=parentId and id_toko=VIdToko;                               				
                                
				set stokNeeded = ceil(stokNeeded/retailStok);
                
                if(VarrayId='')then
					set VarrayId=cast(parentId as char);
					set VArrayStok=cast(parentstok as char);
					set VArrayRetail =cast(retailStok as char);
					set VArrayStokNeeded=cast(stokNeeded as char); 
                else
					set VarrayId=concat(cast(parentId as char), ',', VArrayId);
					set VArrayStok=concat(cast(parentstok as char), ',', VArrayStok);
					set VArrayRetail =concat(cast(retailStok as char), ',', VArrayRetail);
					set VArrayStokNeeded=concat(cast(stokNeeded as char), ',', VArrayStokNeeded); 
                end if;
				
                set VCount = VCount+1;
                
                if(stokNeeded <= parentStok) then
					leave m_loop;
                end if;
                
			end if;
            
			set VIdSatuan = parentId;
            
        end loop;
        
        while(VCount>0) do
			select stok into newStok
            from kartustok
            where id_barang=VIdBarang and id_satuan=substring_index(VarrayId, ',', 1) and id_toko=VIdToko;
            
            if(newStok>0)then     
				if(newStok>cast(substring_index(VArrayStokNeeded,',',1) as decimal(11,2)))then
					set newStok = cast(substring_index(VArrayStokNeeded,',',1) as decimal(11,2));
                end if;
				update kartustok
				set stok=stok-newStok
				where id_barang=VIdBarang and id_satuan=substring_index(VarrayId, ',', 1) and id_toko=VIdToko;
				
				if(VCount=1)then
					update kartustok
					set stok=stok+(newStok*(cast(substring_index(VArrayRetail,',',1) as decimal(11,2))))
					where id_barang=VIdBarang and id_satuan=(select id_satuan from barangdetail where id_barang=VIdBarang and id_satuan_parent=substring_index(VarrayId, ',', 1)) and id_toko=VIdToko;
				end if;     
			end if;
			
            set VarrayId = SUBSTRING_INDEX(VarrayId, ',', 1 - VCount);
            set VArrayStokNeeded = SUBSTRING_INDEX(VArrayStokNeeded, ',', 1 - VCount);
            set VArrayRetail = SUBSTRING_INDEX(VArrayRetail, ',', 1 - VCount);
            set VArrayStok = SUBSTRING_INDEX(VArrayStok, ',', 1 - VCount);
            
            set VCount = VCount-1;
        end while;
    end if;
    
    return '1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `hapusbarang` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `hapusbarang`(in VId int(11), out VMessage varchar(50))
BEGIN
	if exists(select id_barang from barangdetail where id_barang = VId)then
		set VMessage = 'barang in used in barangdetail';
    else
		delete from barang where id=VId;
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `hapusbarangdetail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `hapusbarangdetail`(
in VIdBarang int(11),
in VIdSatuan int(11),
in VIdSatuanParent int(11), 
out VMessage varchar(50))
BEGIN
	if exists(select id_barang from penjualandetail where id_barang = VIdBarang and id_satuan=VIdSatuan) or 
    exists(select id_barang from pembeliandetail where id_barang = VIdBarang and id_satuan=VIdSatuan) or 
    exists(select id_barang from historypenyesuaianhargabeli where id_barang = VIdBarang and id_satuan=VIdSatuan) or
    exists(select id_barang from historypenyesuaianhargajual where id_barang = VIdBarang and id_satuan=VIdSatuan) or
    exists(select id_barang from historypenyesuaianstok where id_barang = VIdBarang and id_satuan=VIdSatuan) or
    exists(select id_barang from kartustok where id_barang = VIdBarang and id_satuan=VIdSatuan) or
    exists(select id_barang from menudetail where id_barang = VIdBarang and id_satuan=VIdSatuan) or
    exists(select id_barang from returbelidetail where id_barang = VIdBarang and id_satuan=VIdSatuan) or
    exists(select id_barang from returjualdetail where id_barang = VIdBarang and id_satuan=VIdSatuan)then
		set VMessage = 'produk in used';
    else
		delete from barang where id=VId;
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `hapusmenu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `hapusmenu`(
in VId int(11), 
out VMessage varchar(50))
BEGIN
	if exists(select id_menu from penjualandetailmenu where id_menu = VId) or
    exists(select id_menu from pembeliandetailmenu where id_menu = VId) or
    exists(select id_menu from returbelidetailmenu where id_menu = VId) or
    exists(select id_menu from returjualdetailmenu where id_menu = VId) or
    exists(select id_menu from menudetail where id_menu = VId)then
		set VMessage = 'menu in used in other table';
    else
		delete from satuan where id=VId;
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `hapussatuan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `hapussatuan`(in VId int(11), out VMessage varchar(50))
BEGIN
	if exists(select id_satuan from barangdetail where id_satuan = VId or id_satuan_parent = VId)then
		set VMessage = 'satuan in used in barangdetail';
    else
		delete from satuan where id=VId;
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `hapussupplier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `hapussupplier`(in VKode varchar(50), out VMessage varchar(50))
BEGIN
	if exists(select kode_supplier from pembelian where kode_supplier = VKode) or 
    exists(select kode_supplier from hargabeli where kode_supplier = VKode)then
		set VMessage = 'supplier in used in other table';
    else
		delete from supplier where kode=VKode;
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `hapustoko` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `hapustoko`(in VId int(11), out VMessage varchar(50))
BEGIN
	if exists(select id_toko from kartustok where id_toko = VId) or 
    exists(select id_toko from penjualan where id_toko = VId) or 
    exists(select id_toko from pembelian where id_toko = VId) or
    exists(select id_toko from returbeli where id_toko = VId)then
		set VMessage = 'toko in used in other table';
    else
		delete from toko where id=VId;
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `hapususer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `hapususer`(
in VUsername varchar(50), 
out VMessage varchar(50))
BEGIN
	if exists(select user from historyloginuser where user = VUsername) or
    exists(select user from historypenyesuaianhargabeli where user = VUsername) or
    exists(select user from historypenyesuaianhargajual where user = VUsername) or
    exists(select user from historypenyesuaianstok where user = VUsername) or
    exists(select user from penjualan where user = VUsername) or 
    exists(select user from pembelian where user = VUsername) or
    exists(select user from returbeli where user = VUsername) or 
    exists(select user from operasional where user = VUsername)then
		set VMessage = 'user in used in other data';
    else
		delete from user where username=VUsername;
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertbarang` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertbarang`(
 IN VKode varchar(45),
 IN VNama varchar(45), 
 out VMessage varchar(45))
BEGIN	       
    if exists( select kode from barang where kode = VKode) then
		set VMessage = 'kode already exist';
    else
		-- query
		insert into barang (kode, nama) values (VKode,VNama); 	
        set VMessage = concat('success',',',cast((select max(id) from barang) as char(50)));
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertbarangdetail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertbarangdetail`(
 IN VIdBarang varchar(45),
 IN VIdSatuan varchar(45), 
 IN VIdSatuanParent varchar(45),
 IN VRetail float(11),
 IN VMinimumStok float(11),
 IN VHargaJual decimal(11),
 OUT VMessage varchar(45))
BEGIN	  
	DECLARE done INT DEFAULT FALSE; 
    declare CIdToko int(11);
    
    declare CToko cursor for select id from toko where 1=1;  
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    if exists( select id_barang from barangdetail where (id_barang = VIdBarang and id_satuan = VIdSatuan) or (id_barang = VIdBarang and id_satuan_parent = VIdSatuanParent)) then
		set VMessage = 'kode already exist';
	elseif not exists(select id_barang from barangdetail where id_barang=VIdBarang and id_satuan=VIdSatuanParent) and VIdSatuanParent != '1' then
		set VMessage = 'parent not found';
	else
		-- insert to barangdetail
        insert into barangdetail (id_barang, id_satuan, id_satuan_parent, retail, minimum_stok, harga_jual) values (VIdBarang, VIdSatuan, VIdSatuanParent, VRetail, VMinimumStok, VHargaJual); 
        -- insert to stok                
		open CToko;
			read_loop: LOOP
				fetch CToko into CIdToko;                
				IF done THEN
				   LEAVE read_loop;
				END IF;                
				insert into kartustok (id_barang, id_satuan, id_toko, stok) values(VIdBarang, VIdSatuan, CIdToko, 0);
			END LOOP;
        close CToko;        
        set VMessage = 'success';
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inserthargabeli` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inserthargabeli`(
 IN VKodeSupplier varchar(45),
 IN VIdBarang varchar(45), 
 in VIdSatuan varchar(45),
 IN VHarga decimal(11,2),
 out VMessage varchar(45))
BEGIN	       
    if exists( select kode_supplier from hargabeli where kode_supplier=VKodeSupplier and id_barang=VIdBarang and id_satuan=VIdSatuan) then
		set VMessage = 'kode already exist';
    else		
		-- query
		insert into hargabeli (kode_supplier, id_barang, id_satuan, harga) values (VKodeSupplier,VIdBarang, VIdSatuan, VHarga); 	
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inserthistoryloginuser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inserthistoryloginuser`(
 IN VUsername varchar(45),
 IN VApplikasi text,
 IN VIdToko int(11),
 out VMessage varchar(45))
BEGIN	       
    insert into historyloginuser(user,login,aplikasi, id_toko) values (VUsername,NOW(),VApplikasi, VIdToko);
	set VMessage = concat('success',',',cast((select max(id) from historyloginuser) as char(50)));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertmenu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertmenu`(
 IN VKode varchar(45),
 IN VNama varchar(45), 
 IN VHarga decimal(11,2),
 IN VKodeBarangDetails text,
 IN VKodeSatuanDetails text,
 IN VJumlahDetails text,
 IN VItemCount int(11),
 OUT VMessage varchar(45))
BEGIN	
	-- last id from menu
	declare VMenuLastID int(11);   
    declare arrayIndex int(11) default 1; -- index for array
    
    -- detail item 
    declare VIdBarang varchar(45);
    declare VIdSatuan varchar(45);
    
    if exists( select kode from menu where kode = VKode) then
		set VMessage = 'kode already exist';
    else		
		-- query
		insert into menu (kode, nama, harga) values (VKode,VNama, VHarga); 
        set VMenuLastID = (select id from menu where kode=VKode);
        while(arrayIndex < VItemCount + 1) do
			-- get id by kode
			set VIdBarang = (select id from barang where kode=SUBSTRING_INDEX(VKodeBarangDetails, ',', 1));
			set VIdSatuan = (select id from satuan where kode=SUBSTRING_INDEX(VKodeSatuanDetails, ',', 1));
			-- insert into menu detail
            insert into menudetail (id_menu, id_barang, id_satuan, jumlah) values(VMenuLastID, VIdBarang, VIdSatuan,  SUBSTRING_INDEX(VJumlahDetails, ',', 1));
			-- 
            set VKodeBarangDetails = SUBSTRING_INDEX(VKodeBarangDetails, ',', arrayIndex - VItemCount);
            set VKodeSatuanDetails = SUBSTRING_INDEX(VKodeSatuanDetails, ',', arrayIndex - VItemCount);
            set VJumlahDetails = SUBSTRING_INDEX(VJumlahDetails, ',', arrayIndex - VItemCount);
            set arrayIndex = arrayIndex + 1; 
        end while;
        
        set VMessage = concat('success',',',cast((select max(id) from menu) as char(50)));
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertmenudetail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertmenudetail`(
 IN VIdMenu int(11),
 IN VIdBarang int(11), 
 in VIdSatuan int(11),
 in VJumlah decimal(11,2),
 out VMessage varchar(255))
BEGIN	       
    if exists( select id_menu from menudetail where id_menu = VIdMenu and id_barang = VIdBarang and id_satuan=VIdSatuan) then
		set VMessage = 'kode already exist';
    else		
		-- query
		insert into menudetail (id_menu, id_barang, id_satuan, jumlah) values (VIdMenu,VIdBarang,VIdSatuan, VJumlah); 	
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertoperasional` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertoperasional`(
 IN VTanggal datetime,
 IN VNominal decimal(11,2), 
 in VKeterangan varchar(255),
 in VUser varchar(50),
 in VIdToko int(11),
 out VMessage varchar(255))
BEGIN	
    declare VKodeOperasional varchar(255);
    
    set VKodeOperasional = GenerateKodeOperasional();    
	
    insert into operasional (kode,tanggal,nominal,keterangan,user,id_toko,tanggal_input) 
    values (VKodeOperasional,VTanggal,VNominal,VKeterangan,VUser,VIdToko,NOW()); 	
	set VMessage = concat('success',',',VKodeOperasional);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertpenjualan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertpenjualan`(
 IN VArrayKodeBarang text,
 IN VArrayKodeSatuan text,
 IN VArrayHarga text,
 IN VArrayJumlah text,
 IN VArrayDiscount text,
 IN VDiscountTransaksi decimal(11,2),
 IN VIDToko int(11),
 IN VUser varchar(45),
 IN VItemCount int(11),
 IN VArrayMenuId text,
 In VArrayMenuJumlah text,
 In VArrayMenuHarga text,
 IN VArrayMenuDiskon text,
 IN VMenuCount int(11),
 IN VArrayMenuIdProduk text,
 In VArrayMenuIdSatuan text,
 IN VArrayMenuIdJumlah text,
 IN VMenuBarangCount int(11),
 OUT VMessage varchar(45))
BEGIN		    
    declare VNomorTransaksi varchar(45);
    declare IsStokUpdated boolean default false;
    
    set VNomorTransaksi = GenerateKodeTransaksi();
    
    -- insert into penjualan
	insert into penjualan (nomor_transaksi, tanggal, diskon, id_toko, user) 
    values (VNomorTransaksi, NOW(), VDiscountTransaksi, VIDToko, VUser); 	
	if(VItemCount!=0)then
		m_loop:loop    
			
			-- insert into penjualandetail
			insert into penjualandetail (nomor_transaksi, id_barang, id_satuan, harga, jumlah, diskon) 
			values(VNomorTransaksi, cast(SUBSTRING_INDEX(VArrayKodeBarang, ',', 1) as signed), cast(SUBSTRING_INDEX(VArrayKodeSatuan, ',', 1) as signed), cast(SUBSTRING_INDEX(VArrayHarga, ',', 1) as decimal(11,2)), cast(SUBSTRING_INDEX(VArrayJumlah, ',', 1) as decimal(11,2)), cast(SUBSTRING_INDEX(VArrayDiscount, ',', 1) as decimal(11,2))); 
			-- 
			set IsStokUpdated = UpdateStok(cast(SUBSTRING_INDEX(VArrayKodeBarang, ',', 1) as signed), cast(SUBSTRING_INDEX(VArrayKodeSatuan, ',', 1) as signed), cast(SUBSTRING_INDEX(VArrayJumlah, ',', 1) as decimal(11,2)), VIDToko);    
														
			set VArrayKodeBarang = SUBSTRING_INDEX(VArrayKodeBarang, ',', 1 - VItemCount);
			set VArrayKodeSatuan = SUBSTRING_INDEX(VArrayKodeSatuan, ',', 1 - VItemCount);
			set VArrayHarga = SUBSTRING_INDEX(VArrayHarga, ',', 1 - VItemCount);
			set VArrayJumlah = SUBSTRING_INDEX(VArrayJumlah, ',', 1 - VItemCount);
			set VArrayDiscount = SUBSTRING_INDEX(VArrayDiscount, ',', 1 - VItemCount); 
			 
			set VItemCount = VItemCount - 1; 
            
			if(VItemCount = 0)then
				leave m_loop;
			end if;            
		end loop;
    end if;
    if(VMenuCount!=0)then
		mdi_loop:loop     			
			-- insert into penjualandetail
			insert into penjualandetailmenu (nomor_transaksi, id_menu, harga, jumlah, diskon) 
			values(VNomorTransaksi, cast(SUBSTRING_INDEX(VArrayMenuId, ',', 1) as signed), cast(SUBSTRING_INDEX(VArrayMenuHarga, ',', 1) as decimal(11,2)), cast(SUBSTRING_INDEX(VArrayMenuJumlah, ',', 1) as decimal(11,2)), cast(SUBSTRING_INDEX(VArrayMenuDiskon, ',', 1) as decimal(11,2))); 
			
			set VArrayMenuId = SUBSTRING_INDEX(VArrayMenuId, ',', 1 - VMenuCount);
			set VArrayMenuHarga = SUBSTRING_INDEX(VArrayMenuHarga, ',', 1 - VMenuCount);
			set VArrayMenuJumlah = SUBSTRING_INDEX(VArrayMenuJumlah, ',', 1 - VMenuCount);
			set VArrayMenuDiskon = SUBSTRING_INDEX(VArrayMenuDiskon, ',', 1 - VMenuCount); 
			
			set VMenuCount = VMenuCount - 1; 
            
			if(VMenuCount = 0)then
				leave mdi_loop;
			end if;  
			
		end loop;
    end if;
    
    if(VMenuBarangCount!=0)then
		mn_loop:loop 
			
			set IsStokUpdated = UpdateStok(cast(SUBSTRING_INDEX(VArrayMenuIdProduk, ',', 1) as signed), cast(SUBSTRING_INDEX(VArrayMenuIdSatuan, ',', 1) as signed), cast(SUBSTRING_INDEX(VArrayMenuIdJumlah, ',', 1) as decimal(11,2)), VIDToko);    
						
			set VArrayMenuIdProduk = SUBSTRING_INDEX(VArrayMenuIdProduk, ',', 1 - VMenuBarangCount);
			set VArrayMenuIdSatuan = SUBSTRING_INDEX(VArrayMenuIdSatuan, ',', 1 - VMenuBarangCount);
			set VArrayMenuIdJumlah = SUBSTRING_INDEX(VArrayMenuIdJumlah, ',', 1 - VMenuBarangCount);
			 
			set VMenuBarangCount = VMenuBarangCount - 1; 
            
			if(VMenuBarangCount = 0)then
				leave mn_loop;
			end if;   
		end loop;
	end if;
    
   set VMessage = concat('success', ',', VNomorTransaksi);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertpenyesuaianhargabeli` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertpenyesuaianhargabeli`(
 IN VIdBarang int(11),
 IN VIdSatuan decimal(11,2),
 IN VKodeSupplier varchar(50),
 IN VHargaLama decimal(11,2),
 IN VHargaBaru decimal(11,2),
 IN VInfo text,
 IN VUser varchar(50),
 out VMessage varchar(50))
BEGIN
	update hargabeli set harga=VHargaBaru 
    where id_barang=VIdBarang and id_satuan=VIdSatuan and kode_supplier=VKodeSupplier;    
	-- query
	insert into historypenyesuaianhargabeli
    (tanggal, id_barang, id_satuan, kode_supplier, harga_beli_awal, harga_beli_sekarang, info, user) 
    values 
    (NOW(), VIdBarang, VIdSatuan, VKodeSupplier, VHargaLama, VHargaBaru, VInfo, VUser); 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertpenyesuaianhargajual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertpenyesuaianhargajual`(
 IN VIdBarang int(11),
 IN VIdSatuan decimal(11,2),
 IN VHargaLama decimal(11,2),
 IN VHargaBaru decimal(11,2),
 IN VInfo text,
 IN VUser varchar(50),
 out VMessage varchar(50))
BEGIN
	update barangdetail set harga_jual=VHargaBaru 
    where id_barang=VIdBarang and id_satuan=VIdSatuan;    
	-- query
	insert into historypenyesuaianhargajual
    (tanggal, id_barang, id_satuan, harga_jual_awal, harga_jual_sekarang, info, user) 
    values 
    (NOW(), VIdBarang, VIdSatuan, VHargaLama, VHargaBaru, VInfo, VUser); 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertpenyesuaianhargajualmenu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertpenyesuaianhargajualmenu`(
 IN VIdMenu int(11),
 IN VHargaLama decimal(11,2),
 IN VHargaBaru decimal(11,2),
 IN VInfo text,
 IN VUser varchar(50),
 out VMessage varchar(50))
BEGIN
	update menu set harga=VHargaBaru 
    where id=VIdMenu;    
	-- query
	insert into historypenyesuaianhargajualmenu
    (tanggal, id_menu, harga_lama, harga_baru, info, user) 
    values 
    (NOW(), VIdMenu, VHargaLama, VHargaBaru, VInfo, VUser); 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertpenyesuaianstok` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertpenyesuaianstok`(
 IN VIdBarang int(11),
 IN VIdSatuan int(11), 
 IN VIdToko int(11),
 IN VStokAwal decimal(11,2),
 IN VStokSekarang decimal(11,2),
 IN VInfo text,
 IN VUser varchar(50),
 out VMessage varchar(45))
BEGIN
	update kartustok set stok=VStokSekarang 
    where id_barang=VIdBarang and id_satuan=VIdSatuan and id_toko=VIdToko;    
	-- query
	insert into historypenyesuaianstok
    (tanggal, id_barang, id_satuan, id_toko, stok_awal, stok_sekarang, info, user) 
    values 
    (NOW(), VIdBarang, VIdSatuan, VIdToko, VStokAwal, VStokSekarang, VInfo, VUser); 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertsatuan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertsatuan`(
 IN VKode varchar(50),
 IN VNama varchar(50), 
 in VKeterangan varchar(255),
 out VMessage varchar(255))
BEGIN	       
    if exists( select kode from satuan where kode = VKode) then
		set VMessage = 'kode already exist';
    else		
		-- query
		insert into satuan (kode, nama, keterangan) values (VKode,VNama, VKeterangan); 	
        set VMessage = concat('success',',',cast((select max(id) from satuan) as char(50)));
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertsupplier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertsupplier`(
 IN VNama varchar(50), 
 in VAlamat text,
 in VKota text,
 in VTelp text,
 out VMessage varchar(255))
BEGIN	 
	-- query
	insert into supplier (kode, nama, alamat, kota, telp) 
    values (GenerateKodeSupplier(), VNama, VAlamat, VKota, VTelp); 	
	set VMessage = concat('success',',',cast((select max(kode) from supplier) as char(50)));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inserttoko` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inserttoko`(
 IN VKode varchar(45),
 IN VNama varchar(45), 
 IN VAlamat varchar(45), 
 IN VTelp varchar(45), 
 OUT VMessage varchar(45))
BEGIN	  
	DECLARE done INT DEFAULT FALSE; 
    declare CIdBarang int(11);
	declare CIdSatuan int(11);
    
    declare CBarangDetail cursor for select id_barang, id_satuan from barangdetail where 1=1;  
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    if exists( select kode from toko where kode = VKode) then
		set VMessage = 'kode already exist';
    else
		-- query
		insert into toko (kode, nama, alamat, telp) values (VKode, VNama, VAlamat, VTelp); 	
        -- insert to stok                
		open CBarangDetail;
			read_loop: LOOP
				fetch CBarangDetail into CIdBarang, CIdSatuan;                
				IF done THEN
				   LEAVE read_loop;
				END IF;                
				insert into kartustok (id_barang, id_satuan, id_toko, stok) values(CIdBarang, CIdSatuan, (select id from toko where kode=VKode), 0);
			END LOOP;
        close CBarangDetail;        
        set VMessage = concat('success',',',cast((select max(id) from toko) as char(50)));
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertuser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertuser`(
 IN VUsername Varchar(50),
 IN VPass Varchar(50), 
 IN VNama Varchar(50),
 IN VAlamat Text (255),
 IN VTelp Text,
 IN VAkses Text,
 OUT VMessage varchar(50))
BEGIN	  
	if exists(select username from user where username=VUsername) then
		set VMessage = 'username already exists';
	else
		insert into user (username, password, nama, alamat, telp, akses) 
        values (VUsername, MD5(VPass), VNama, VAlamat, VTelp, VAkses); 	
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateaksesuser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateaksesuser`(
 IN VUsername varchar(50),
 IN VAkses Varchar(50),
 OUT VMessage varchar(50))
BEGIN		
	update user set akses=VAkses 
    where username=VUsername; 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatebarang` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updatebarang`(
 IN VId int(11),
 IN VKode varchar(45),
 IN VNama varchar(45), 
 out VMessage varchar(45))
BEGIN	       
    if exists(select kode from barang where kode = VKode and id!=VId) then
		set VMessage = 'kode already exist';
    else
		-- query
		update barang set kode=VKode, nama=VNama where id=VId; 	
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatejumlahmenudetail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updatejumlahmenudetail`(
 IN VIdMenu int(11),
 IN VIdBarang int(11),
 IN VIdSatuan int(11),
 IN VJumlah decimal(11,2),
 OUT VMessage varchar(50))
BEGIN		
	update menudetail set jumlah=VJumlah 
    where id_menu=VIdMenu and id_barang=VIdBarang and id_satuan=VIdSatuan; 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatemenu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updatemenu`(
 IN VId varchar(45),
 IN VKode varchar(45),
 IN VNama varchar(45), 
 out VMessage varchar(45))
BEGIN	
    if exists( select kode from menu where kode = VKode and id!=VId) then
		set VMessage = 'menu already exist';
    else
		-- query
		update menu set kode=VKode, nama=VNama where id=VId; 	
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatemenudetail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updatemenudetail`(
 IN VIdMenu int(11),
 IN VIdBarang int(11),
 IN VIdSatuan int(11), 
 in VJumlah decimal(11,2),
 out VMessage varchar(50))
BEGIN	
	-- query
	update menudetail set jumlah=VJumlah
    where id_menu=VIdMenu and id_barang=VIdBarang and id_satuan=VIdSatuan; 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatesatuan` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updatesatuan`(
 IN VId varchar(45),
 IN VKode varchar(45),
 IN VNama varchar(45), 
 in VKeterangan varchar(45),
 out VMessage varchar(45))
BEGIN	
    if exists( select kode from satuan where kode = VKode and id!=VId) then
		set VMessage = 'kode already exist';
    else
		-- query
		update satuan set kode=VKode, nama=VNama, keterangan=VKeterangan where id=VId; 	
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatesupplier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updatesupplier`(
 IN VKode varchar(50),
 IN VNama varchar(50),
 IN VAlamat text, 
 IN VKota text, 
 IN VTelp text, 
 out VMessage varchar(50))
BEGIN	
	-- query
	update supplier set nama=VNama, alamat=VAlamat, kota=Vkota, telp=VTelp where kode=VKode; 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatetoko` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updatetoko`(
 IN VId int(45),
 IN VKode varchar(45),
 IN VNama varchar(45), 
 IN VAlamat varchar(45), 
 IN VTelp varchar(45), 
 out VMessage varchar(45))
BEGIN	       
    if exists( select kode from toko where kode = VKode and id!=VId) then
		set VMessage = 'kode already exist';
    else
		-- query
		update toko set kode=VKode, nama=VNama, alamat=VAlamat, telp=VTelp where id=VId; 	
        set VMessage = 'success';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateuser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateuser`(
 IN VUsername varchar(50),
 IN VNama Varchar(50),
 IN VAlamat Text (255),
 IN VTelp Text,
 OUT VMessage varchar(50))
BEGIN		
	update user set nama=VNama, alamat=VAlamat, telp=VTelp 
    where username=VUsername; 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userlogout` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `userlogout`(
 IN VIdLogin varchar(50),
 IN VIdToko int(11),
 OUT VMessage varchar(50))
BEGIN		
	update historyloginuser set logout=Now() 
    where id=VIdLogin; 	
	set VMessage = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-30 15:38:48
