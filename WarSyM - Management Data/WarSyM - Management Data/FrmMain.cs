﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Forms;
using WarSyM_Management_Data.Forms.Master.History;
using WarSyM_Management_Data.Forms.Master;
using WarSyM_Management_Data.Forms.Master.Ubah;
using WarSyM_Management_Data.Forms.Penyesuaian;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Report;
////testing wan
namespace WarSyM_Management_Data
{
    public partial class FrmMain : Form
    {
        FrmLogin frmLogin;
        MdiClient ctlMDI;

        bool isLogout;

        public FrmMain(FrmLogin _frmLogin)
        {
            InitializeComponent();
            isLogout = false;
            frmLogin = _frmLogin;
        }

        private void keluarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();   
        }

        private void barangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMasterBarang frmMasterBarang = new FrmMasterBarang();
            frmMasterBarang.MdiParent = this;
            frmMasterBarang.StartPosition = FormStartPosition.CenterScreen;
            frmMasterBarang.Text = "Master Barang";
            frmMasterBarang.Show();
        }

        private void supplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMasterSupplier frmMasterSupplier = new FrmMasterSupplier();
            frmMasterSupplier.MdiParent = this;
            frmMasterSupplier.StartPosition = FormStartPosition.CenterScreen;
            frmMasterSupplier.Text = "Master Supplier";
            frmMasterSupplier.Show();
        }

        private void userDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMasterUser frmMasterUser = new FrmMasterUser();
            frmMasterUser.MdiParent = this;
            frmMasterUser.StartPosition = FormStartPosition.CenterScreen;
            frmMasterUser.Text = "User Data Maintenace";
            frmMasterUser.Show();
        }

        private void ubahPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmUbahPassword frmUbahPassword = new FrmUbahPassword();
            frmUbahPassword.MdiParent = this;
            frmUbahPassword.StartPosition = FormStartPosition.CenterScreen;
            frmUbahPassword.Text = "Ubah Password";
            frmUbahPassword.Show();
        }

        private void stokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPenyesuaianStok frmPenyesuaianStok = new FrmPenyesuaianStok();
            frmPenyesuaianStok.MdiParent = this;
            frmPenyesuaianStok.StartPosition = FormStartPosition.CenterScreen;
            frmPenyesuaianStok.Text = "Penyesuaian Stok";
            frmPenyesuaianStok.Show();
        }

        private void hargaBeliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPenyesuaianHargaBeli frmPenyesuaianHargaBeli = new FrmPenyesuaianHargaBeli();
            frmPenyesuaianHargaBeli.MdiParent = this;
            frmPenyesuaianHargaBeli.StartPosition = FormStartPosition.CenterScreen;
            frmPenyesuaianHargaBeli.Text = "Penyesuaian Harga Beli";
            frmPenyesuaianHargaBeli.Show();
        }

        private void pengaturanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSettings frmSettings = new FrmSettings(false);
            //frmSettings.MdiParent = this;
            frmSettings.StartPosition = FormStartPosition.CenterScreen;
            frmSettings.Text = "Pengaturan Aplikasi";
            frmSettings.ShowDialog();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isLogout = true;
            frmLogin.Show();
            this.Close();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    ctlMDI = (MdiClient)ctl;
                    ctlMDI.BackColor = this.BackColor;
                }
                catch { }
            }
            if (UserLoginInfo.isSakti) return;
            if(!UserLoginInfo.HakAkses.Contains("Administrator") )
                InitFirst();
        }

        private void InitFirst()
        {
            MenuPegawai();
        }

        public void ExportToExcel(DataGridView dgv, string sheetName)
        {
            Tools.ExportToExcel.ExportToExcel exp = new Tools.ExportToExcel.ExportToExcel();
            sfdExportToxcel.FileName = sheetName + ".xls";
            sfdExportToxcel.Filter = "Excel 97-2003 Workbook (*.xls)|*.xls|All Files (*.*)|*.*";
            sfdExportToxcel.FilterIndex = 1;
            sfdExportToxcel.RestoreDirectory = true;

            if (sfdExportToxcel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = sfdExportToxcel.FileName;
                exp.dataGridView2Excel(dgv, path, sheetName);
            }
        }

        public void MenuPegawai()
        {
            if (!UserLoginInfo.HakAkses.Contains("Penyesuaian"))
                penyesuaianToolStripMenuItem.Enabled = false;
            if (!UserLoginInfo.HakAkses.Contains("Maintenance"))
            {
                masterToolStripMenuItem.Enabled = false;
                userDataToolStripMenuItem.Enabled = false;
            }
            if (!UserLoginInfo.HakAkses.Contains("Pengaturan"))
                pengaturanToolStripMenuItem.Enabled = false;
            if (!UserLoginInfo.HakAkses.Contains("Laporan"))
            {
                historyToolStripMenuItem.Enabled = false;
                reportToolStripMenuItem.Enabled = false;
            }
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (UserLoginInfo.isSakti) frmLogin.Close();
            else DBAHistoryUser.Instance.UpdateLogoutUser(UserLoginInfo.IdLogin, DateTime.Now, 1);
            if (isLogout) return;
            if (MessageBox.Show(this, "Anda yakin akan menutup aplikasi ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                frmLogin.Close();
            else
                e.Cancel = true;            
        }

        private void loginUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHistoryUser frmHistoryUser = new FrmHistoryUser();
            frmHistoryUser.MdiParent = this;
            frmHistoryUser.Show();
        }

        private void penyesuaianStokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHistoryPenyesuaianStok frmHistoryPenyesuaianStok = new FrmHistoryPenyesuaianStok();
            frmHistoryPenyesuaianStok.MdiParent = this;
            frmHistoryPenyesuaianStok.Show();
        }

        private void penyesuaianHargaBeliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHistoryPenyesuaianHargaBeli frmHistoryPenyesuaianHargaBeli = new FrmHistoryPenyesuaianHargaBeli();
            frmHistoryPenyesuaianHargaBeli.MdiParent = this;
            frmHistoryPenyesuaianHargaBeli.Show();
        }

        private void fakturToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRptFakturPembelian frmReportFakturPembelian = new FrmRptFakturPembelian();
            frmReportFakturPembelian.MdiParent = this;
            frmReportFakturPembelian.Show();
        }

        private void fakturToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmRptFakturPenjualan frmReportFakturPenjualan = new FrmRptFakturPenjualan();
            frmReportFakturPenjualan.MdiParent = this;
            frmReportFakturPenjualan.Show();
        }

        private void pembatalanItemJualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRptPembatalanItemPenjualan frmReportPembatalanItemPenjualan = new FrmRptPembatalanItemPenjualan();
            frmReportPembatalanItemPenjualan.MdiParent = this;
            frmReportPembatalanItemPenjualan.Show();
        }

        private void detailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRptDetailPembelian frmDetailPembelian = new FrmRptDetailPembelian();
            frmDetailPembelian.MdiParent = this;
            frmDetailPembelian.Show();
        }

        private void detailToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmRptDetailPenjualan frmRprPenjualan = new FrmRptDetailPenjualan();
            frmRprPenjualan.MdiParent = this;
            frmRprPenjualan.Show();
        }

        private void returBeliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRptReturPembelian frmReturPembelian = new FrmRptReturPembelian();
            frmReturPembelian.MdiParent = this;
            frmReturPembelian.Show();
        }

        private void tokoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMasterToko frmMasterToko = new FrmMasterToko();
            frmMasterToko.MdiParent = this;
            frmMasterToko.Show();
        }

        private void produkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPenyesuaianHargaJual frmPenyesuaianHargaJual = new FrmPenyesuaianHargaJual();
            frmPenyesuaianHargaJual.MdiParent = this;
            frmPenyesuaianHargaJual.StartPosition = FormStartPosition.CenterScreen;
            frmPenyesuaianHargaJual.Text = "Penyesuaian Harga Jual";
            frmPenyesuaianHargaJual.Show();
        }

        private void produkToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmHistoryPenyesuaianHargaJual frmHistoryPenyesuaianHargaJual = new FrmHistoryPenyesuaianHargaJual();
            frmHistoryPenyesuaianHargaJual.MdiParent = this;
            frmHistoryPenyesuaianHargaJual.Show();
        }

        private void menuToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmHistoryPenyesuaianHargaJualMenu frmHistoryPenyesuaianHargaJualMenu = new FrmHistoryPenyesuaianHargaJualMenu();
            frmHistoryPenyesuaianHargaJualMenu.MdiParent = this;
            frmHistoryPenyesuaianHargaJualMenu.Show();
        }

        private void hargaBeliToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmMasterHargaBeli frmMasterHargaBeli = new FrmMasterHargaBeli();
            frmMasterHargaBeli.MdiParent = this;
            frmMasterHargaBeli.Show();
        }

        private void kartuStokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMasterStok frmMasterStok = new FrmMasterStok();
            frmMasterStok.MdiParent = this;
            frmMasterStok.Show();
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPenyesuaianHargaJualMenu frmPenyesuaianHargaJualMene = new FrmPenyesuaianHargaJualMenu();
            frmPenyesuaianHargaJualMene.MdiParent = this;
            frmPenyesuaianHargaJualMene.Show();
        }
    }
}
