﻿namespace WarSyM_Management_Data
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubahPasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tokoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penyesuaianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hargaBeliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hargaJualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fakturToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fakturToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.detailToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.operasionalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.returBeliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fakturToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.detailToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.returJualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fakturToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.detailToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.kartuStokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hargaBeliToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penyesuaianStokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penyesuaianHargaBeliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penyesuaianHargaJualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produkToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.loginUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pengaturanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keluarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sfdExportToxcel = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 501);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(875, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adminToolStripMenuItem,
            this.masterToolStripMenuItem,
            this.penyesuaianToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.historyToolStripMenuItem,
            this.pengaturanToolStripMenuItem,
            this.keluarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(875, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userDataToolStripMenuItem,
            this.ubahPasswordToolStripMenuItem,
            this.toolStripMenuItem1,
            this.logoutToolStripMenuItem});
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.adminToolStripMenuItem.Text = "&Admin";
            // 
            // userDataToolStripMenuItem
            // 
            this.userDataToolStripMenuItem.Name = "userDataToolStripMenuItem";
            this.userDataToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.userDataToolStripMenuItem.Text = "&User Data";
            this.userDataToolStripMenuItem.Click += new System.EventHandler(this.userDataToolStripMenuItem_Click);
            // 
            // ubahPasswordToolStripMenuItem
            // 
            this.ubahPasswordToolStripMenuItem.Name = "ubahPasswordToolStripMenuItem";
            this.ubahPasswordToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.ubahPasswordToolStripMenuItem.Text = "&Ubah Password";
            this.ubahPasswordToolStripMenuItem.Click += new System.EventHandler(this.ubahPasswordToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 6);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.logoutToolStripMenuItem.Text = "&Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // masterToolStripMenuItem
            // 
            this.masterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.barangToolStripMenuItem,
            this.supplierToolStripMenuItem,
            this.tokoToolStripMenuItem});
            this.masterToolStripMenuItem.Name = "masterToolStripMenuItem";
            this.masterToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.masterToolStripMenuItem.Text = "&Master";
            // 
            // barangToolStripMenuItem
            // 
            this.barangToolStripMenuItem.Name = "barangToolStripMenuItem";
            this.barangToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.barangToolStripMenuItem.Text = "&Barang";
            this.barangToolStripMenuItem.Click += new System.EventHandler(this.barangToolStripMenuItem_Click);
            // 
            // supplierToolStripMenuItem
            // 
            this.supplierToolStripMenuItem.Name = "supplierToolStripMenuItem";
            this.supplierToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.supplierToolStripMenuItem.Text = "&Supplier";
            this.supplierToolStripMenuItem.Click += new System.EventHandler(this.supplierToolStripMenuItem_Click);
            // 
            // tokoToolStripMenuItem
            // 
            this.tokoToolStripMenuItem.Name = "tokoToolStripMenuItem";
            this.tokoToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.tokoToolStripMenuItem.Text = "&Toko";
            this.tokoToolStripMenuItem.Click += new System.EventHandler(this.tokoToolStripMenuItem_Click);
            // 
            // penyesuaianToolStripMenuItem
            // 
            this.penyesuaianToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stokToolStripMenuItem,
            this.hargaBeliToolStripMenuItem,
            this.hargaJualToolStripMenuItem});
            this.penyesuaianToolStripMenuItem.Name = "penyesuaianToolStripMenuItem";
            this.penyesuaianToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.penyesuaianToolStripMenuItem.Text = "&Penyesuaian";
            // 
            // stokToolStripMenuItem
            // 
            this.stokToolStripMenuItem.Name = "stokToolStripMenuItem";
            this.stokToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stokToolStripMenuItem.Text = "&Stok";
            this.stokToolStripMenuItem.Click += new System.EventHandler(this.stokToolStripMenuItem_Click);
            // 
            // hargaBeliToolStripMenuItem
            // 
            this.hargaBeliToolStripMenuItem.Name = "hargaBeliToolStripMenuItem";
            this.hargaBeliToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.hargaBeliToolStripMenuItem.Text = "&Harga Beli";
            this.hargaBeliToolStripMenuItem.Click += new System.EventHandler(this.hargaBeliToolStripMenuItem_Click);
            // 
            // hargaJualToolStripMenuItem
            // 
            this.hargaJualToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produkToolStripMenuItem,
            this.menuToolStripMenuItem});
            this.hargaJualToolStripMenuItem.Name = "hargaJualToolStripMenuItem";
            this.hargaJualToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.hargaJualToolStripMenuItem.Text = "&Harga Jual";
            // 
            // produkToolStripMenuItem
            // 
            this.produkToolStripMenuItem.Name = "produkToolStripMenuItem";
            this.produkToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.produkToolStripMenuItem.Text = "&Produk";
            this.produkToolStripMenuItem.Click += new System.EventHandler(this.produkToolStripMenuItem_Click);
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.menuToolStripMenuItem.Text = "&Menu";
            this.menuToolStripMenuItem.Click += new System.EventHandler(this.menuToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pembelianToolStripMenuItem,
            this.penjualanToolStripMenuItem,
            this.toolStripSeparator2,
            this.operasionalToolStripMenuItem,
            this.toolStripSeparator1,
            this.returBeliToolStripMenuItem,
            this.returJualToolStripMenuItem,
            this.toolStripMenuItem2,
            this.kartuStokToolStripMenuItem,
            this.hargaBeliToolStripMenuItem1});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.reportToolStripMenuItem.Text = "&Report";
            // 
            // pembelianToolStripMenuItem
            // 
            this.pembelianToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fakturToolStripMenuItem,
            this.detailToolStripMenuItem});
            this.pembelianToolStripMenuItem.Name = "pembelianToolStripMenuItem";
            this.pembelianToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.pembelianToolStripMenuItem.Text = "&Pembelian";
            // 
            // fakturToolStripMenuItem
            // 
            this.fakturToolStripMenuItem.Name = "fakturToolStripMenuItem";
            this.fakturToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.fakturToolStripMenuItem.Text = "&Faktur";
            this.fakturToolStripMenuItem.Click += new System.EventHandler(this.fakturToolStripMenuItem_Click);
            // 
            // detailToolStripMenuItem
            // 
            this.detailToolStripMenuItem.Name = "detailToolStripMenuItem";
            this.detailToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.detailToolStripMenuItem.Text = "&Detail";
            this.detailToolStripMenuItem.Click += new System.EventHandler(this.detailToolStripMenuItem_Click);
            // 
            // penjualanToolStripMenuItem
            // 
            this.penjualanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fakturToolStripMenuItem1,
            this.detailToolStripMenuItem1});
            this.penjualanToolStripMenuItem.Name = "penjualanToolStripMenuItem";
            this.penjualanToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.penjualanToolStripMenuItem.Text = "P&enjualan";
            // 
            // fakturToolStripMenuItem1
            // 
            this.fakturToolStripMenuItem1.Name = "fakturToolStripMenuItem1";
            this.fakturToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.fakturToolStripMenuItem1.Text = "&Faktur";
            this.fakturToolStripMenuItem1.Click += new System.EventHandler(this.fakturToolStripMenuItem1_Click);
            // 
            // detailToolStripMenuItem1
            // 
            this.detailToolStripMenuItem1.Name = "detailToolStripMenuItem1";
            this.detailToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.detailToolStripMenuItem1.Text = "&Detail";
            this.detailToolStripMenuItem1.Click += new System.EventHandler(this.detailToolStripMenuItem1_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(134, 6);
            // 
            // operasionalToolStripMenuItem
            // 
            this.operasionalToolStripMenuItem.Name = "operasionalToolStripMenuItem";
            this.operasionalToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.operasionalToolStripMenuItem.Text = "&Operasional";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(134, 6);
            // 
            // returBeliToolStripMenuItem
            // 
            this.returBeliToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fakturToolStripMenuItem2,
            this.detailToolStripMenuItem2});
            this.returBeliToolStripMenuItem.Name = "returBeliToolStripMenuItem";
            this.returBeliToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.returBeliToolStripMenuItem.Text = "&Retur Beli";
            this.returBeliToolStripMenuItem.Click += new System.EventHandler(this.returBeliToolStripMenuItem_Click);
            // 
            // fakturToolStripMenuItem2
            // 
            this.fakturToolStripMenuItem2.Name = "fakturToolStripMenuItem2";
            this.fakturToolStripMenuItem2.Size = new System.Drawing.Size(107, 22);
            this.fakturToolStripMenuItem2.Text = "&Faktur";
            // 
            // detailToolStripMenuItem2
            // 
            this.detailToolStripMenuItem2.Name = "detailToolStripMenuItem2";
            this.detailToolStripMenuItem2.Size = new System.Drawing.Size(107, 22);
            this.detailToolStripMenuItem2.Text = "&Detail";
            // 
            // returJualToolStripMenuItem
            // 
            this.returJualToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fakturToolStripMenuItem3,
            this.detailToolStripMenuItem3});
            this.returJualToolStripMenuItem.Name = "returJualToolStripMenuItem";
            this.returJualToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.returJualToolStripMenuItem.Text = "Retur &Jual";
            // 
            // fakturToolStripMenuItem3
            // 
            this.fakturToolStripMenuItem3.Name = "fakturToolStripMenuItem3";
            this.fakturToolStripMenuItem3.Size = new System.Drawing.Size(107, 22);
            this.fakturToolStripMenuItem3.Text = "&Faktur";
            // 
            // detailToolStripMenuItem3
            // 
            this.detailToolStripMenuItem3.Name = "detailToolStripMenuItem3";
            this.detailToolStripMenuItem3.Size = new System.Drawing.Size(107, 22);
            this.detailToolStripMenuItem3.Text = "&Detail";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(134, 6);
            // 
            // kartuStokToolStripMenuItem
            // 
            this.kartuStokToolStripMenuItem.Name = "kartuStokToolStripMenuItem";
            this.kartuStokToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.kartuStokToolStripMenuItem.Text = "&Kartu Stok";
            this.kartuStokToolStripMenuItem.Click += new System.EventHandler(this.kartuStokToolStripMenuItem_Click);
            // 
            // hargaBeliToolStripMenuItem1
            // 
            this.hargaBeliToolStripMenuItem1.Name = "hargaBeliToolStripMenuItem1";
            this.hargaBeliToolStripMenuItem1.Size = new System.Drawing.Size(137, 22);
            this.hargaBeliToolStripMenuItem1.Text = "&Harga Beli";
            this.hargaBeliToolStripMenuItem1.Click += new System.EventHandler(this.hargaBeliToolStripMenuItem1_Click);
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penyesuaianStokToolStripMenuItem,
            this.penyesuaianHargaBeliToolStripMenuItem,
            this.penyesuaianHargaJualToolStripMenuItem,
            this.toolStripMenuItem3,
            this.loginUserToolStripMenuItem});
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.historyToolStripMenuItem.Text = "&History";
            // 
            // penyesuaianStokToolStripMenuItem
            // 
            this.penyesuaianStokToolStripMenuItem.Name = "penyesuaianStokToolStripMenuItem";
            this.penyesuaianStokToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.penyesuaianStokToolStripMenuItem.Text = "&Penyesuaian Stok";
            this.penyesuaianStokToolStripMenuItem.Click += new System.EventHandler(this.penyesuaianStokToolStripMenuItem_Click);
            // 
            // penyesuaianHargaBeliToolStripMenuItem
            // 
            this.penyesuaianHargaBeliToolStripMenuItem.Name = "penyesuaianHargaBeliToolStripMenuItem";
            this.penyesuaianHargaBeliToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.penyesuaianHargaBeliToolStripMenuItem.Text = "Penyesuaian Harga &Beli";
            this.penyesuaianHargaBeliToolStripMenuItem.Click += new System.EventHandler(this.penyesuaianHargaBeliToolStripMenuItem_Click);
            // 
            // penyesuaianHargaJualToolStripMenuItem
            // 
            this.penyesuaianHargaJualToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produkToolStripMenuItem1,
            this.menuToolStripMenuItem1});
            this.penyesuaianHargaJualToolStripMenuItem.Name = "penyesuaianHargaJualToolStripMenuItem";
            this.penyesuaianHargaJualToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.penyesuaianHargaJualToolStripMenuItem.Text = "Penyesuaian &Harga Jual";
            // 
            // produkToolStripMenuItem1
            // 
            this.produkToolStripMenuItem1.Name = "produkToolStripMenuItem1";
            this.produkToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.produkToolStripMenuItem1.Text = "&Produk";
            this.produkToolStripMenuItem1.Click += new System.EventHandler(this.produkToolStripMenuItem1_Click);
            // 
            // menuToolStripMenuItem1
            // 
            this.menuToolStripMenuItem1.Name = "menuToolStripMenuItem1";
            this.menuToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.menuToolStripMenuItem1.Text = "&Menu";
            this.menuToolStripMenuItem1.Click += new System.EventHandler(this.menuToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(195, 6);
            // 
            // loginUserToolStripMenuItem
            // 
            this.loginUserToolStripMenuItem.Name = "loginUserToolStripMenuItem";
            this.loginUserToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.loginUserToolStripMenuItem.Text = "&Login User";
            this.loginUserToolStripMenuItem.Click += new System.EventHandler(this.loginUserToolStripMenuItem_Click);
            // 
            // pengaturanToolStripMenuItem
            // 
            this.pengaturanToolStripMenuItem.Name = "pengaturanToolStripMenuItem";
            this.pengaturanToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.pengaturanToolStripMenuItem.Text = "P&engaturan";
            this.pengaturanToolStripMenuItem.Click += new System.EventHandler(this.pengaturanToolStripMenuItem_Click);
            // 
            // keluarToolStripMenuItem
            // 
            this.keluarToolStripMenuItem.Name = "keluarToolStripMenuItem";
            this.keluarToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.keluarToolStripMenuItem.Text = "&Keluar";
            this.keluarToolStripMenuItem.Click += new System.EventHandler(this.keluarToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(875, 523);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inventory Management System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubahPasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem barangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penyesuaianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hargaJualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hargaBeliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pengaturanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keluarToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog sfdExportToxcel;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penyesuaianStokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penyesuaianHargaJualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penyesuaianHargaBeliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returBeliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fakturToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fakturToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem detailToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tokoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fakturToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem detailToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem returJualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fakturToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem detailToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem kartuStokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operasionalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produkToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem hargaBeliToolStripMenuItem1;
    }
}



