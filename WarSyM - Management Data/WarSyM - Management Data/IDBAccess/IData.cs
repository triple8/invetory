﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WarSyM_Management_Data.IDBAccess
{
    public interface IData
    {
        DataTable GetAll(int start, int count, string filter);

        DataRow GetSingle(int id);
        
        DataRow GetSingle(string kode);

        object GetSingleColumn(int id, string columnName);

        object GetSingleColumn(string kode, string columnName);

        object GetSingleColumnByFilter(string columnName, string filter);

        string EditSingleColumn(int id, string columnName, object value);

        string EditSingleColumn(string kode, string columnName, object value);

        string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id);

        string Insert(params object[] values);

        string Edit(int id, params object[] values);

        string Edit(string kode, params object[] values);

        string Delete(int id);

        string Delete(string kode);

        string DeleteByMultipleId(params object[] id);

        string DeleteByFilter(string filter);
        
        int Count(string filter);

        //int CountByFilter(string filter);
    }
}
