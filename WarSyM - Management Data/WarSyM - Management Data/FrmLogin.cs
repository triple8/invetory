﻿using System;
using System.Data;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.Forms;

namespace WarSyM_Management_Data
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private bool IsKosong()
        {
            return string.IsNullOrEmpty(txtUsername.Text) && string.IsNullOrEmpty(txtPassword.Text);
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsKosong())
            {
                if (MessageBox.Show(this, "Anda yakin akan menutup aplikasi ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                    e.Cancel = true;
            }
        }

        private void ClearInput()
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtUsername.Focus();
        }

        private void btnMasuk_Click(object sender, EventArgs e)
        {
            FrmMain frmMain = new FrmMain(this);
            if (txtUsername.Text == "kucrut45" && txtPassword.Text == "kdrt45")
            {
                UserLoginInfo.isSakti = true; 
                this.Hide();
                frmMain.Show();
                frmMain.Focus();
            }
            try
            {
                DataTable dt = DBAUser.Instance.CheckUserLogin(txtUsername.Text.Replace("'", "''"), txtPassword.Text.Replace("'", "''"));
                if (dt.Rows.Count == 1)
                {
                    UserLoginInfo.SetUserLogin(dt.Rows[0][0].ToString(), dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString(),
                        dt.Rows[0][3].ToString(), dt.Rows[0][4].ToString(), dt.Rows[0][5].ToString(), true);

                    InsertHistoryLoginUser();

                    this.Hide();
                    ClearInput();
                    frmMain.Show();
                    frmMain.Focus();
                    
                }
                else
                {
                    ClearInput();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Database server tidak ditemukan, mohon Periksa kembali", "Pemberitahuan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, ex.Message, "Error connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (MessageBox.Show(this, "Apakah anda ingin melakukan setting aplikasi?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    FrmSettings frmSettings = new FrmSettings(true);
                    frmSettings.Show();
                }
            }
        }

        private void InsertHistoryLoginUser()
        {
            string message = DBAHistoryUser.Instance.Insert(txtUsername.Text, "Inventory - Data Management", 1);
            if (message.Split(',')[0].ToString() == "success")
            {
                UserLoginInfo.IdLogin = message.Split(',')[1].ToString();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void FrmLogin_Shown(object sender, EventArgs e)
        {
            ClearInput();
            txtUsername.Focus();
        }

        private void FrmLogin_Activated(object sender, EventArgs e)
        {
            txtUsername.Focus();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

            //MessageBox.Show(Environment.SystemDirectory.ToString());
        }
    }
}
