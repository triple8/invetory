﻿using System;
using System.Data;
using System.Threading;
using System.Windows.Forms;
using WarSyM_Management_Data.IDBAccess;

namespace WarSyM_Management_Data.Core
{
    public static class Pagging
    {
        delegate void addRowDelegate(DataRow row, DataGridView dg);
        delegate void frmLoadingDelegate();
        static FrmLoading frmLoading = new FrmLoading();
        static DataGridView dg;
        static IData dataAccess;
        static int start;
        static int count;
        static string filter;

        public static void BindingGrid(DataGridView _dg, IData _dataAccess, int _start, int _count, string _filter)
        {
            dg = _dg;
            dg.Rows.Clear();

            dataAccess = _dataAccess;

            start = _start;
            count = _count;
            filter = _filter;

            Thread bindingData;
            bindingData = new Thread(GenerateData);
            bindingData.IsBackground = true;
            bindingData.Start();            
            frmLoading.ShowDialog();   
        }

        static void GenerateData(object obj)
        {
            DataTable dt = dataAccess.GetAll(start, count, filter);
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.NewRow();
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    row[j] = dt.Rows[i][j];
                }
                if (dg.InvokeRequired)
                {
                    dg.Invoke(new addRowDelegate(addRow), row, dg);
                }
                else
                {
                    dg.Rows.Add(row.ItemArray);
                }
                Thread.Sleep(50);
            }

            if (frmLoading.InvokeRequired)
            {
                frmLoading.Invoke(new frmLoadingDelegate(closeFrmLoading));
            }
            else
            {
                frmLoading.Close();
            }
        }

        static void closeFrmLoading()
        {
            frmLoading.Close();
        }

        static void addRow(DataRow row, DataGridView dg)
        {
            dg.Rows.Add(row.ItemArray);
        }

        public static void First(DataGridView dgBinding, IData dataAccess, Label lblPage, ComboBox cboCountToDisplay, string filter)
        {
            if (lblPage.Text == "1") return;
            lblPage.Text = "1";
            BindingGrid(dgBinding, dataAccess, 0, int.Parse(cboCountToDisplay.Text), filter);
        }

        public static void Previous(DataGridView dgBinding, IData dataAccess, Label lblPage, ComboBox cboDisplay, string filter)
        {
            if (int.Parse(lblPage.Text) < 2) return;

            lblPage.Text = (int.Parse(lblPage.Text) - 1).ToString();

            int start = int.Parse(lblPage.Text) * int.Parse(cboDisplay.Text) - int.Parse(cboDisplay.Text);

            BindingGrid(dgBinding, dataAccess, start, int.Parse(cboDisplay.Text), filter);
        }

        public static void Next(int countAll, DataGridView dgBinding, IData dataAccess, Label lblPage, ComboBox cboDisplay, string filter)
        {
            if (int.Parse(lblPage.Text) == (int)Math.Ceiling(((decimal)countAll / decimal.Parse(cboDisplay.Text)))) return;

            lblPage.Text = (int.Parse(lblPage.Text) + 1).ToString();

            BindingGrid(dgBinding, dataAccess, int.Parse(lblPage.Text) * int.Parse(cboDisplay.Text) - int.Parse(cboDisplay.Text), int.Parse(cboDisplay.Text), filter);
        }

        public static void Last(int countAll, DataGridView dgBinding, IData dataAccess, Label lblPage, ComboBox cboDisplay, string filter)
        {
            int displayPerPage = int.Parse(cboDisplay.Text);

            if (countAll == 0 || lblPage.Text == ((int)Math.Ceiling((decimal)countAll / (decimal)displayPerPage)).ToString()) return;

            lblPage.Text = ((int)Math.Ceiling((decimal)countAll / (decimal)displayPerPage)).ToString();

            int start = int.Parse(lblPage.Text) * int.Parse(cboDisplay.Text) - (int.Parse(cboDisplay.Text));

            BindingGrid(dgBinding, dataAccess, start, displayPerPage, filter);
        }

        public static void DataCountChange(DataGridView dgBinding, IData dataAccess, Label lblPage, ComboBox cboDisplay, string filter)
        {
            lblPage.Text = "1";
            int countToDisplay = (cboDisplay.Text == "All") ? dataAccess.Count("") : int.Parse(cboDisplay.Text);
            BindingGrid(dgBinding, dataAccess, 0, countToDisplay, filter);
        }
    }
}
