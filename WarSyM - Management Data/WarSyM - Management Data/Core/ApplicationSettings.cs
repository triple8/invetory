﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Core
{
    public class ApplicationSettings
    {
        private static ApplicationSettings instance;

        public Dictionary<string, string> databaseSettings;

        private ApplicationSettings()
        {
            databaseSettings = new Dictionary<string, string>();
        }

        public static ApplicationSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ApplicationSettings();
                }
                return instance;
            }
        }

        //setting untuk database
        public string DataBaseHost { get; set; }

        public string DatabasePort { get; set; }

        public string DatabaseName { get; set; }

        public string DatabaseUser { get; set; }
        
        public string DatabasePassword { get; set; }
        //

        //setting untuk aplikasi
        public string UseCapitalForInputTextBox
        {
            get
            {
                return databaseSettings["UseCapitalForInputTextBox"];
            }
        }

        public int DecimalPlaceForNumeric
        {
            get
            {
                return int.Parse(databaseSettings["DecimalPlaceForNumeric"]);
            }
        }

        public string HeaderNota1
        {
            get
            {
                return databaseSettings["HeaderNota1"];
            }
        }

        public string HeaderNota2
        {
            get
            {
                return databaseSettings["HeaderNota2"];
            }
        }

        public string HeaderNota3
        {
            get
            {
                return databaseSettings["HeaderNota3"];
            }
        }

        public string HeaderNota4
        {
            get
            {
                return databaseSettings["HeaderNota4"];
            }
        }

        public string BottomNota
        {
            get
            {
                return databaseSettings["BottomNota"];
            }
        }

        public string InfoNota
        {
            get
            {
                return databaseSettings["InfoNota"];
            }
        }
        //
    }

}
