﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.IDBAccess;

namespace WarSyM_Management_Data.Core
{
    public static class ControlBinding
    {
        public static void BindingComboBox(IData dataAccess, ComboBox cbo, int columnIndex, string filter)
        {
            DataTable dt = dataAccess.GetAll(0, dataAccess.Count(filter), filter);
            cbo.Items.Clear();
            cbo.Items.Add("All");
            for (int i = 0; i < dt.Rows.Count; i++)
                cbo.Items.Add(dt.Rows[i][columnIndex]);
            cbo.SelectedIndex = -1;
        }

        public static void BindingComboBox(IData dataAccess, ToolStripComboBox cbo, int columnIndex, string filter)
        {
            DataTable dt = dataAccess.GetAll(0, dataAccess.Count(filter), filter);
            cbo.Items.Clear();
            cbo.Items.Add("All");
            for (int i = 0; i < dt.Rows.Count; i++)
                cbo.Items.Add(dt.Rows[i][columnIndex]);
            cbo.SelectedIndex = -1;
        }

        public static string BindingSingleColumn(TextBox txtKode, TextBox txtToBinding, IData dataAccess, string columnToBinding)
        {
            string returnValue = "";
            if (string.IsNullOrWhiteSpace(txtKode.Text)) returnValue = "empty text";
            else
            {
                try
                {
                    txtToBinding.Text = (string)dataAccess.GetSingleColumn(txtKode.Text, columnToBinding);
                    returnValue = "success";
                }
                catch (Exception ex)
                {
                    returnValue = ex.Message;
                }
            }
            return returnValue;
        }

        public static string BindingSingleColumn(ComboBox cboKode, TextBox txtToBinding, IData dataAccess, string columnToBinding)
        {
            string returnValue = "";
            if (string.IsNullOrWhiteSpace(cboKode.Text)) returnValue = "empty text";
            else
            {
                try
                {
                    txtToBinding.Text = (string)dataAccess.GetSingleColumn(cboKode.Text, columnToBinding);
                    returnValue = "success";
                }
                catch (Exception ex)
                {
                    returnValue = ex.Message;
                }
            }
            return returnValue;
        }
        
    }
}
