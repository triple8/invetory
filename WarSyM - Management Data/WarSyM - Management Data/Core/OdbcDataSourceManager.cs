﻿using System.Collections;

namespace DevToolShed
{
    public enum DataSourceType { System, User }

    public class OdbcDataSourceManager
    {
        public SortedList GetAllDataSourceNames()
        {
            SortedList dsnList = GetUserDataSourceNames();

            SortedList systemDsnList = GetSystemDataSourceNames();
            for (int i = 0; i < systemDsnList.Count; i++)
            {
                string sName = systemDsnList.GetKey(i) as string;
                DataSourceType type = (DataSourceType)systemDsnList.GetByIndex(i);
                try
                {
                    dsnList.Add(sName, type);
                }
                catch
                {

                }
            }

            return dsnList;
        }

        public SortedList GetSystemDataSourceNames()
        {
            SortedList dsnList = new SortedList();

            Microsoft.Win32.RegistryKey reg = (Microsoft.Win32.Registry.LocalMachine).OpenSubKey("Software");
            if (reg != null)
            {
                reg = reg.OpenSubKey("ODBC");
                if (reg != null)
                {
                    reg = reg.OpenSubKey("ODBC.INI");
                    if (reg != null)
                    {
                        reg = reg.OpenSubKey("ODBC Data Sources");
                        if (reg != null)
                        {
                            foreach (string sName in reg.GetValueNames())
                            {
                                dsnList.Add(sName, DataSourceType.System);
                            }
                        }
                        try
                        {
                            reg.Close();
                        }
                        catch { }
                    }
                }
            }

            return dsnList;
        }

        public SortedList GetUserDataSourceNames()
        {
            SortedList dsnList = new SortedList();

            Microsoft.Win32.RegistryKey reg = (Microsoft.Win32.Registry.LocalMachine).OpenSubKey("Software");
            if (reg != null)
            {
                reg = reg.OpenSubKey("ODBC");
                if (reg != null)
                {
                    reg = reg.OpenSubKey("ODBCINST.INI");
                    if (reg != null)
                    {
                        reg = reg.OpenSubKey("ODBC Drivers");
                        if (reg != null)
                        {
                            foreach (string sName in reg.GetValueNames())
                            {
                                dsnList.Add(sName, DataSourceType.User);
                            }
                        }
                        try
                        {
                            reg.Close();
                        }
                        catch { }
                    }
                }
            }

            return dsnList;
        }
    }
}
