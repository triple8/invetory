﻿using MySql.Data.MySqlClient;
using System.IO;
using System.Windows.Forms;

namespace WarSyM_Management_Data.Core
{
    public class ApplicationConnection
    {
        public MySqlConnection MySqlDatabaseConnection { get; set; }

        public string ConnectionString { get; set; }

        private static ApplicationConnection instance;

        private ApplicationConnection()
        {
            ConnectionString =
                "server=" + ApplicationSettings.Instance.DataBaseHost + ";uid=" + ApplicationSettings.Instance.DatabaseUser + ";password=" 
                + ApplicationSettings.Instance.DatabasePassword + ";database=" + ApplicationSettings.Instance.DatabaseName + ";port=" 
                + ApplicationSettings.Instance.DatabasePort;

            MySqlDatabaseConnection = new MySqlConnection(ConnectionString);
        }

        public static ApplicationConnection Instance 
        { 
            get 
            {
                if (instance == null)
                    instance = new ApplicationConnection();
                return instance;
            } 
        }

    }
}
