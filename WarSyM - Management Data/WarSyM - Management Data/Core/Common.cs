﻿using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace WarSyM_Management_Data.Core
{
    public static class Common
    {
        public static string ExecuteNonQuery(MySqlConnection cn, MySqlCommand cmd)
        {
            string returnValue = "";
            try
            {
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                returnValue = ex.Message;
            }
            finally
            {
                cn.Close();
            }
            return returnValue;
        }

        public static string GetHardiskSerialNumber()
        {
            ManagementObjectSearcher mosDisks = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
            string serialNumber = "";
            foreach (ManagementObject moDisk in mosDisks.Get())
            {
                serialNumber = moDisk["SerialNumber"].ToString();
            }
            return GetReal(serialNumber);
        }

        private static string GetReal(string serialNumber)
        {
            if (serialNumber.Length < 20)
            {

                return serialNumber;
            }
            else
                return serialNumber;
        }

        public static string GetMD5SerialNumber()
        {
            MD5 md5Hash = MD5.Create();

            return GetMd5Hash(md5Hash, GetHardiskSerialNumber());
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static string[] ReadLocalSettingsFile()
        {
            StreamReader readFile = new StreamReader(Application.StartupPath + "\\Text\\ServerDatabase.txt");

            string[] arraySettings = readFile.ReadLine().Split(';');

            readFile.Close();

            return arraySettings;
        }

        public static void WriteSettingsDatabase(string _dbhost, string _dbPort, string _dbName, string _dbUser, string _dbPass)
        {
            StreamWriter rw = new StreamWriter(Application.StartupPath + "\\Text\\ServerDatabase.txt");
            string data;
            data = _dbhost + ";" + _dbPort + ";" + _dbName + ";" + _dbUser + ";" + _dbPass + ";";
            rw.WriteLine(data);
            rw.Close();
            ReadLocalSettingsFile();
        }
    }
}
