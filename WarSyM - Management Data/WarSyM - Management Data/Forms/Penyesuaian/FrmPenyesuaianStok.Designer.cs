﻿namespace WarSyM_Management_Data.Forms.Penyesuaian
{
    partial class FrmPenyesuaianStok
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPenyesuaianStok));
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaItem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCariBarang = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNamaSatuan = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCariToko = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNamaToko = new System.Windows.Forms.TextBox();
            this.lblNamaToko = new System.Windows.Forms.Label();
            this.ntRealStok = new System.Windows.Forms.NumericUpDown();
            this.ntStokSystem = new System.Windows.Forms.NumericUpDown();
            this.txtKodeToko = new System.Windows.Forms.TextBox();
            this.txtKodeBarang = new System.Windows.Forms.TextBox();
            this.cboKodeSatuan = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntRealStok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntStokSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Image = ((System.Drawing.Image)(resources.GetObject("btnBatal.Image")));
            this.btnBatal.Location = new System.Drawing.Point(213, 302);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(109, 32);
            this.btnBatal.TabIndex = 13;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Image = ((System.Drawing.Image)(resources.GetObject("btnSimpan.Image")));
            this.btnSimpan.Location = new System.Drawing.Point(99, 302);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(109, 32);
            this.btnSimpan.TabIndex = 12;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(14, 292);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(309, 4);
            this.radSeparator1.TabIndex = 46;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(99, 231);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(223, 54);
            this.txtInfo.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(65, 234);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 15);
            this.label5.TabIndex = 45;
            this.label5.Text = "Info:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 206);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 15);
            this.label4.TabIndex = 44;
            this.label4.Text = "Stok gudang:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 15);
            this.label3.TabIndex = 43;
            this.label3.Text = "Stok sistem:";
            // 
            // txtNamaItem
            // 
            this.txtNamaItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaItem.Location = new System.Drawing.Point(99, 35);
            this.txtNamaItem.Name = "txtNamaItem";
            this.txtNamaItem.ReadOnly = true;
            this.txtNamaItem.Size = new System.Drawing.Size(223, 21);
            this.txtNamaItem.TabIndex = 2;
            this.txtNamaItem.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 42;
            this.label2.Text = "Nama barang:";
            // 
            // btnCariBarang
            // 
            this.btnCariBarang.Image = ((System.Drawing.Image)(resources.GetObject("btnCariBarang.Image")));
            this.btnCariBarang.Location = new System.Drawing.Point(289, 6);
            this.btnCariBarang.Name = "btnCariBarang";
            this.btnCariBarang.Size = new System.Drawing.Size(33, 23);
            this.btnCariBarang.TabIndex = 1;
            this.btnCariBarang.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariBarang.UseVisualStyleBackColor = true;
            this.btnCariBarang.Click += new System.EventHandler(this.btnCariBarang_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 15);
            this.label1.TabIndex = 34;
            this.label1.Text = "Kode barang:";
            // 
            // txtNamaSatuan
            // 
            this.txtNamaSatuan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaSatuan.Location = new System.Drawing.Point(99, 91);
            this.txtNamaSatuan.Name = "txtNamaSatuan";
            this.txtNamaSatuan.ReadOnly = true;
            this.txtNamaSatuan.Size = new System.Drawing.Size(223, 21);
            this.txtNamaSatuan.TabIndex = 5;
            this.txtNamaSatuan.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 15);
            this.label6.TabIndex = 87;
            this.label6.Text = "Nama satuan:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 15);
            this.label7.TabIndex = 86;
            this.label7.Text = "Kode satuan:";
            // 
            // btnCariToko
            // 
            this.btnCariToko.Image = ((System.Drawing.Image)(resources.GetObject("btnCariToko.Image")));
            this.btnCariToko.Location = new System.Drawing.Point(289, 118);
            this.btnCariToko.Name = "btnCariToko";
            this.btnCariToko.Size = new System.Drawing.Size(33, 23);
            this.btnCariToko.TabIndex = 7;
            this.btnCariToko.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariToko.UseVisualStyleBackColor = true;
            this.btnCariToko.Click += new System.EventHandler(this.btnCariToko_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 15);
            this.label8.TabIndex = 90;
            this.label8.Text = "Kode toko:";
            // 
            // txtNamaToko
            // 
            this.txtNamaToko.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaToko.Location = new System.Drawing.Point(99, 147);
            this.txtNamaToko.Name = "txtNamaToko";
            this.txtNamaToko.ReadOnly = true;
            this.txtNamaToko.Size = new System.Drawing.Size(223, 21);
            this.txtNamaToko.TabIndex = 8;
            this.txtNamaToko.TabStop = false;
            // 
            // lblNamaToko
            // 
            this.lblNamaToko.AutoSize = true;
            this.lblNamaToko.Location = new System.Drawing.Point(25, 150);
            this.lblNamaToko.Name = "lblNamaToko";
            this.lblNamaToko.Size = new System.Drawing.Size(70, 15);
            this.lblNamaToko.TabIndex = 92;
            this.lblNamaToko.Text = "Nama toko:";
            // 
            // ntRealStok
            // 
            this.ntRealStok.DecimalPlaces = 2;
            this.ntRealStok.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntRealStok.Location = new System.Drawing.Point(99, 203);
            this.ntRealStok.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntRealStok.Name = "ntRealStok";
            this.ntRealStok.Size = new System.Drawing.Size(223, 21);
            this.ntRealStok.TabIndex = 10;
            this.ntRealStok.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntRealStok.ThousandsSeparator = true;
            // 
            // ntStokSystem
            // 
            this.ntStokSystem.BackColor = System.Drawing.Color.White;
            this.ntStokSystem.DecimalPlaces = 2;
            this.ntStokSystem.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntStokSystem.Location = new System.Drawing.Point(99, 175);
            this.ntStokSystem.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntStokSystem.Name = "ntStokSystem";
            this.ntStokSystem.ReadOnly = true;
            this.ntStokSystem.Size = new System.Drawing.Size(223, 21);
            this.ntStokSystem.TabIndex = 9;
            this.ntStokSystem.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntStokSystem.ThousandsSeparator = true;
            // 
            // txtKodeToko
            // 
            this.txtKodeToko.Location = new System.Drawing.Point(99, 119);
            this.txtKodeToko.MaxLength = 50;
            this.txtKodeToko.Name = "txtKodeToko";
            this.txtKodeToko.Size = new System.Drawing.Size(184, 21);
            this.txtKodeToko.TabIndex = 6;
            this.txtKodeToko.Leave += new System.EventHandler(this.txtKodeToko_Leave);
            // 
            // txtKodeBarang
            // 
            this.txtKodeBarang.Location = new System.Drawing.Point(99, 7);
            this.txtKodeBarang.MaxLength = 50;
            this.txtKodeBarang.Name = "txtKodeBarang";
            this.txtKodeBarang.Size = new System.Drawing.Size(184, 21);
            this.txtKodeBarang.TabIndex = 0;
            this.txtKodeBarang.Leave += new System.EventHandler(this.txtKodeBarang_Leave);
            // 
            // cboKodeSatuan
            // 
            this.cboKodeSatuan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKodeSatuan.FormattingEnabled = true;
            this.cboKodeSatuan.Location = new System.Drawing.Point(99, 62);
            this.cboKodeSatuan.Name = "cboKodeSatuan";
            this.cboKodeSatuan.Size = new System.Drawing.Size(223, 23);
            this.cboKodeSatuan.TabIndex = 93;
            this.cboKodeSatuan.SelectedIndexChanged += new System.EventHandler(this.cboKodeSatuan_SelectedIndexChanged);
            // 
            // FrmPenyesuaianStok
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(331, 341);
            this.Controls.Add(this.cboKodeSatuan);
            this.Controls.Add(this.txtKodeToko);
            this.Controls.Add(this.txtKodeBarang);
            this.Controls.Add(this.ntStokSystem);
            this.Controls.Add(this.ntRealStok);
            this.Controls.Add(this.txtNamaToko);
            this.Controls.Add(this.lblNamaToko);
            this.Controls.Add(this.btnCariToko);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtNamaSatuan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNamaItem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCariBarang);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmPenyesuaianStok";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Penyesuaian Stok";
            this.Load += new System.EventHandler(this.FrmPenyesuaianStok_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntRealStok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntStokSystem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnSimpan;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCariBarang;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtNamaItem;
        public System.Windows.Forms.TextBox txtNamaSatuan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnCariToko;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtNamaToko;
        private System.Windows.Forms.Label lblNamaToko;
        private System.Windows.Forms.NumericUpDown ntRealStok;
        private System.Windows.Forms.NumericUpDown ntStokSystem;
        public System.Windows.Forms.TextBox txtKodeToko;
        public System.Windows.Forms.TextBox txtKodeBarang;
        private System.Windows.Forms.ComboBox cboKodeSatuan;

    }
}