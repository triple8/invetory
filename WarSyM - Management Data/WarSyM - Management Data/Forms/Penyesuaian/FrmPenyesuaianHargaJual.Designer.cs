﻿namespace WarSyM_Management_Data.Forms.Penyesuaian
{
    partial class FrmPenyesuaianHargaJual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPenyesuaianHargaJual));
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaItem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCariItem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNamaSatuan = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ntHargaBaru = new System.Windows.Forms.NumericUpDown();
            this.ntHargaLama = new System.Windows.Forms.NumericUpDown();
            this.txtKodeBarang = new System.Windows.Forms.TextBox();
            this.cboKodeSatuan = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaBaru)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaLama)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Image = ((System.Drawing.Image)(resources.GetObject("btnBatal.Image")));
            this.btnBatal.Location = new System.Drawing.Point(227, 247);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(105, 32);
            this.btnBatal.TabIndex = 9;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Image = ((System.Drawing.Image)(resources.GetObject("btnSimpan.Image")));
            this.btnSimpan.Location = new System.Drawing.Point(110, 247);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(105, 32);
            this.btnSimpan.TabIndex = 8;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(13, 236);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(318, 4);
            this.radSeparator1.TabIndex = 65;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(109, 177);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(222, 54);
            this.txtInfo.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(75, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 15);
            this.label5.TabIndex = 64;
            this.label5.Text = "Info:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 15);
            this.label4.TabIndex = 63;
            this.label4.Text = "Harga jual baru:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 15);
            this.label3.TabIndex = 62;
            this.label3.Text = "Harga jual lama:";
            // 
            // txtNamaItem
            // 
            this.txtNamaItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaItem.Location = new System.Drawing.Point(109, 36);
            this.txtNamaItem.Name = "txtNamaItem";
            this.txtNamaItem.ReadOnly = true;
            this.txtNamaItem.Size = new System.Drawing.Size(222, 21);
            this.txtNamaItem.TabIndex = 2;
            this.txtNamaItem.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 61;
            this.label2.Text = "Nama barang:";
            // 
            // btnCariItem
            // 
            this.btnCariItem.Image = ((System.Drawing.Image)(resources.GetObject("btnCariItem.Image")));
            this.btnCariItem.Location = new System.Drawing.Point(299, 7);
            this.btnCariItem.Name = "btnCariItem";
            this.btnCariItem.Size = new System.Drawing.Size(33, 23);
            this.btnCariItem.TabIndex = 1;
            this.btnCariItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariItem.UseVisualStyleBackColor = true;
            this.btnCariItem.Click += new System.EventHandler(this.btnCariItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 15);
            this.label1.TabIndex = 60;
            this.label1.Text = "Kode barang:";
            // 
            // txtNamaSatuan
            // 
            this.txtNamaSatuan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaSatuan.Location = new System.Drawing.Point(109, 92);
            this.txtNamaSatuan.Name = "txtNamaSatuan";
            this.txtNamaSatuan.ReadOnly = true;
            this.txtNamaSatuan.Size = new System.Drawing.Size(222, 21);
            this.txtNamaSatuan.TabIndex = 4;
            this.txtNamaSatuan.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 15);
            this.label6.TabIndex = 92;
            this.label6.Text = "Nama satuan:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 15);
            this.label7.TabIndex = 91;
            this.label7.Text = "Kode satuan:";
            // 
            // ntHargaBaru
            // 
            this.ntHargaBaru.DecimalPlaces = 2;
            this.ntHargaBaru.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntHargaBaru.Location = new System.Drawing.Point(109, 148);
            this.ntHargaBaru.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntHargaBaru.Name = "ntHargaBaru";
            this.ntHargaBaru.Size = new System.Drawing.Size(223, 21);
            this.ntHargaBaru.TabIndex = 6;
            this.ntHargaBaru.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntHargaBaru.ThousandsSeparator = true;
            // 
            // ntHargaLama
            // 
            this.ntHargaLama.DecimalPlaces = 2;
            this.ntHargaLama.Enabled = false;
            this.ntHargaLama.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntHargaLama.Location = new System.Drawing.Point(109, 120);
            this.ntHargaLama.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntHargaLama.Name = "ntHargaLama";
            this.ntHargaLama.Size = new System.Drawing.Size(223, 21);
            this.ntHargaLama.TabIndex = 5;
            this.ntHargaLama.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntHargaLama.ThousandsSeparator = true;
            // 
            // txtKodeBarang
            // 
            this.txtKodeBarang.Location = new System.Drawing.Point(109, 8);
            this.txtKodeBarang.MaxLength = 50;
            this.txtKodeBarang.Name = "txtKodeBarang";
            this.txtKodeBarang.Size = new System.Drawing.Size(184, 21);
            this.txtKodeBarang.TabIndex = 0;
            this.txtKodeBarang.Leave += new System.EventHandler(this.txtKodeBarang_Leave);
            // 
            // cboKodeSatuan
            // 
            this.cboKodeSatuan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKodeSatuan.FormattingEnabled = true;
            this.cboKodeSatuan.Location = new System.Drawing.Point(109, 63);
            this.cboKodeSatuan.Name = "cboKodeSatuan";
            this.cboKodeSatuan.Size = new System.Drawing.Size(223, 23);
            this.cboKodeSatuan.TabIndex = 101;
            this.cboKodeSatuan.SelectedIndexChanged += new System.EventHandler(this.cboKodeSatuan_SelectedIndexChanged);
            // 
            // FrmPenyesuaianHargaJual
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(341, 287);
            this.Controls.Add(this.cboKodeSatuan);
            this.Controls.Add(this.txtKodeBarang);
            this.Controls.Add(this.ntHargaLama);
            this.Controls.Add(this.ntHargaBaru);
            this.Controls.Add(this.txtNamaSatuan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNamaItem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCariItem);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmPenyesuaianHargaJual";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Penyesuaian Harga Jual";
            this.Load += new System.EventHandler(this.FrmPenyesuaianHargaJual_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaBaru)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaLama)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnSimpan;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCariItem;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtNamaItem;
        public System.Windows.Forms.TextBox txtNamaSatuan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown ntHargaBaru;
        private System.Windows.Forms.NumericUpDown ntHargaLama;
        public System.Windows.Forms.TextBox txtKodeBarang;
        private System.Windows.Forms.ComboBox cboKodeSatuan;
    }
}