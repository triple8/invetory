﻿namespace WarSyM_Management_Data.Forms.Penyesuaian
{
    partial class FrmPenyesuaianHargaJualMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPenyesuaianHargaJualMenu));
            this.txtKodeMenu = new System.Windows.Forms.TextBox();
            this.ntHargaLama = new System.Windows.Forms.NumericUpDown();
            this.ntHargaBaru = new System.Windows.Forms.NumericUpDown();
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaMenu = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCariMenu = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaLama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaBaru)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtKodeMenu
            // 
            this.txtKodeMenu.Location = new System.Drawing.Point(118, 9);
            this.txtKodeMenu.Margin = new System.Windows.Forms.Padding(4);
            this.txtKodeMenu.MaxLength = 50;
            this.txtKodeMenu.Name = "txtKodeMenu";
            this.txtKodeMenu.Size = new System.Drawing.Size(214, 21);
            this.txtKodeMenu.TabIndex = 93;
            this.txtKodeMenu.Leave += new System.EventHandler(this.txtKodeMenu_Leave);
            // 
            // ntHargaLama
            // 
            this.ntHargaLama.DecimalPlaces = 2;
            this.ntHargaLama.Enabled = false;
            this.ntHargaLama.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntHargaLama.Location = new System.Drawing.Point(118, 63);
            this.ntHargaLama.Margin = new System.Windows.Forms.Padding(4);
            this.ntHargaLama.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntHargaLama.Name = "ntHargaLama";
            this.ntHargaLama.Size = new System.Drawing.Size(260, 21);
            this.ntHargaLama.TabIndex = 98;
            this.ntHargaLama.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntHargaLama.ThousandsSeparator = true;
            // 
            // ntHargaBaru
            // 
            this.ntHargaBaru.DecimalPlaces = 2;
            this.ntHargaBaru.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntHargaBaru.Location = new System.Drawing.Point(118, 90);
            this.ntHargaBaru.Margin = new System.Windows.Forms.Padding(4);
            this.ntHargaBaru.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntHargaBaru.Name = "ntHargaBaru";
            this.ntHargaBaru.Size = new System.Drawing.Size(260, 21);
            this.ntHargaBaru.TabIndex = 99;
            this.ntHargaBaru.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntHargaBaru.ThousandsSeparator = true;
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Image = ((System.Drawing.Image)(resources.GetObject("btnBatal.Image")));
            this.btnBatal.Location = new System.Drawing.Point(255, 199);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(4);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(122, 33);
            this.btnBatal.TabIndex = 102;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Image = ((System.Drawing.Image)(resources.GetObject("btnSimpan.Image")));
            this.btnSimpan.Location = new System.Drawing.Point(118, 199);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(122, 33);
            this.btnSimpan.TabIndex = 101;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(17, 187);
            this.radSeparator1.Margin = new System.Windows.Forms.Padding(4);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(360, 5);
            this.radSeparator1.TabIndex = 108;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(118, 118);
            this.txtInfo.Margin = new System.Windows.Forms.Padding(4);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(259, 62);
            this.txtInfo.TabIndex = 100;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(82, 121);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 15);
            this.label5.TabIndex = 107;
            this.label5.Text = "Info:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 93);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 15);
            this.label4.TabIndex = 106;
            this.label4.Text = "Harga jual baru:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 66);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 15);
            this.label3.TabIndex = 105;
            this.label3.Text = "Harga jual lama:";
            // 
            // txtNamaMenu
            // 
            this.txtNamaMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaMenu.Location = new System.Drawing.Point(118, 36);
            this.txtNamaMenu.Margin = new System.Windows.Forms.Padding(4);
            this.txtNamaMenu.Name = "txtNamaMenu";
            this.txtNamaMenu.ReadOnly = true;
            this.txtNamaMenu.Size = new System.Drawing.Size(259, 21);
            this.txtNamaMenu.TabIndex = 95;
            this.txtNamaMenu.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 15);
            this.label2.TabIndex = 104;
            this.label2.Text = "Nama menu:";
            // 
            // btnCariMenu
            // 
            this.btnCariMenu.Image = ((System.Drawing.Image)(resources.GetObject("btnCariMenu.Image")));
            this.btnCariMenu.Location = new System.Drawing.Point(339, 9);
            this.btnCariMenu.Margin = new System.Windows.Forms.Padding(4);
            this.btnCariMenu.Name = "btnCariMenu";
            this.btnCariMenu.Size = new System.Drawing.Size(38, 21);
            this.btnCariMenu.TabIndex = 94;
            this.btnCariMenu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariMenu.UseVisualStyleBackColor = true;
            this.btnCariMenu.Click += new System.EventHandler(this.btnCariMenu_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 103;
            this.label1.Text = "Kode menu:";
            // 
            // FrmPenyesuaianHargaJualMenu
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(387, 242);
            this.Controls.Add(this.txtKodeMenu);
            this.Controls.Add(this.ntHargaLama);
            this.Controls.Add(this.ntHargaBaru);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNamaMenu);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCariMenu);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmPenyesuaianHargaJualMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Penyesuaian harga jual menu";
            this.Load += new System.EventHandler(this.FrmPenyesuaianHargaJualMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaLama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaBaru)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtKodeMenu;
        private System.Windows.Forms.NumericUpDown ntHargaLama;
        private System.Windows.Forms.NumericUpDown ntHargaBaru;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnSimpan;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtNamaMenu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCariMenu;
        private System.Windows.Forms.Label label1;
    }
}