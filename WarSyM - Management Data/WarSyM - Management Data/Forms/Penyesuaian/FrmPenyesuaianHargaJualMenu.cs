﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master;
using WarSyM_Management_Data.Forms.Master.Pencarian;
using WarSyM_Management_Data.GlobalClass;

namespace WarSyM_Management_Data.Forms.Penyesuaian
{
    public partial class FrmPenyesuaianHargaJualMenu : Form
    {
        FrmMasterBarang frmMasterBarang;
        public int idMenu;
        int rowIndex;

        public FrmPenyesuaianHargaJualMenu()
        {
            InitializeComponent();
        }

        public FrmPenyesuaianHargaJualMenu(FrmMasterBarang _frmMasterBarang, int _idMenu, string _kodeMenu, string _namaMenu, decimal _hargaJual, int _rowIndex)
        {
            InitializeComponent();

            this.frmMasterBarang = _frmMasterBarang;
            txtKodeMenu.Text = _kodeMenu;
            txtKodeMenu.ReadOnly = true;
            txtNamaMenu.Text = _namaMenu;
            ntHargaLama.Value = _hargaJual;
            txtKodeMenu.ReadOnly = true;
            this.idMenu = _idMenu;
            this.rowIndex = _rowIndex;
            btnCariMenu.Enabled = false;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menginputkan data ini?", "Konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            if (this.idMenu == -1)
            {
                MessageBox.Show(this, "Data tidak lengkap. Mohon periksa kembali", "Data tidak lengkap", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string message = DBAPenyesuaianHargaJualMenu.Instance.Insert(this.idMenu, ntHargaLama.Value, ntHargaBaru.Value, txtInfo.Text, UserLoginInfo.Username);
            if (message == "success")
            {
                MessageBox.Show(this, "Data sudah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (this.frmMasterBarang != null)
                {
                    this.frmMasterBarang.UpdateHargaMenu(this.rowIndex, ntHargaBaru.Value);
                    this.Close();
                    return;
                }
                ClearInput();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKodeMenu.Text = "";
            txtNamaMenu.Text = "";
            txtInfo.Text = "";
            ntHargaLama.Value = 0;
            ntHargaBaru.Value = 0;
            this.idMenu = -1;
            txtKodeMenu.Focus();
        }

        private void txtKodeMenu_Leave(object sender, EventArgs e)
        {
            if (txtKodeMenu.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeMenu, txtNamaMenu, DBAMenu.Instance, "nama");
            if (message == "success")
            {
                TryGetHarga();
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                // MessageBox.Show(this, "Kode barang tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data menu tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.idMenu = -1;
            this.Cursor = Cursors.Default;
        }

        private void TryGetHarga()
        {
            try
            {
                if (string.IsNullOrEmpty(txtKodeMenu.Text))
                {
                    return;
                }
                this.idMenu = (int)DBAMenu.Instance.GetSingleColumn(txtKodeMenu.Text, "id");

                ntHargaLama.Value = (decimal)DBAMenu.Instance.GetSingleColumnByFilter("harga", " id='" + this.idMenu + "' ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, ex.Message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmPenyesuaianHargaJualMenu_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void btnCariMenu_Click(object sender, EventArgs e)
        {
            FrmCariMenu frmCariMenu = new FrmCariMenu(this);
            frmCariMenu.ShowDialog();
            txtKodeMenu.Focus();
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
