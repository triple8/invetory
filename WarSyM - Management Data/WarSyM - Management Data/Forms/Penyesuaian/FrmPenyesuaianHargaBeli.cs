﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.Forms.Master.Pencarian;

namespace WarSyM_Management_Data.Forms.Penyesuaian
{
    public partial class FrmPenyesuaianHargaBeli : Form
    {
        FrmMasterHargaBeli frmMasterHargaBeli;
        int rowIndex;
        public int id_barang, id_satuan;
        public string kodeSupplier;

        public FrmPenyesuaianHargaBeli()
        {
            InitializeComponent();
        }

        public FrmPenyesuaianHargaBeli(FrmMasterHargaBeli _frmMasterHargaBeli, int _id_barang, string _kodeBarang, string _namaBarang, int _id_satuan, 
            string _kodeSatuan, string _namaSatuan, string _kodeSupplier, decimal _hargaBeli, int _rowIndex)
        {
            InitializeComponent();
            this.frmMasterHargaBeli = _frmMasterHargaBeli;
            this.id_barang = _id_barang;
            txtKodeBarang.Text = _kodeBarang;
            txtKodeBarang.ReadOnly = true;
            txtNamaItem.Text = _namaBarang;
            this.id_satuan = _id_satuan;

            ntHargaLama.Value = _hargaBeli;

            btnCariItem.Enabled = false;
            
            ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, " id in(select id_satuan from barangdetail where id_barang='"
                     + DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id") + "') ");
            
            cboKodeSatuan.Text = _kodeSatuan;
            cboKodeSatuan.Enabled = false;
            txtNamaSatuan.Text = _namaSatuan;
            
            this.kodeSupplier = _kodeSupplier;
            txtKodeSupplier.Text = this.kodeSupplier;
            txtKodeSupplier.ReadOnly = true;
            btnCariSupplier.Enabled = false;

            this.rowIndex = _rowIndex;
            txtNamaSupplier.Text = (string)DBASupplier.Instance.GetSingleColumn(this.kodeSupplier, "nama");
        }

        private void txtKodeSupplier_Leave(object sender, EventArgs e)
        {
            if (txtKodeSupplier.ReadOnly) return;
            string message = ControlBinding.BindingSingleColumn(txtKodeSupplier, txtNamaSupplier, DBASupplier.Instance, "nama");
            if (message == "success")
            {
                this.kodeSupplier = txtKodeSupplier.Text;
                TryGetHargaBeli();
                return;
            }
            else if (message == "empty text")
            {
               // MessageBox.Show(this, "Kode supplier tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data supplier tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ntHargaLama.Value = 0;
            this.kodeSupplier = "";

        }

        private void TxtKodeBarang_Leave(object sender, EventArgs e)
        {
            if (txtKodeBarang.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeBarang, txtNamaItem, DBABarang.Instance, "nama");
            if (message == "success")
            {
                this.id_barang = (int)DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id");
                ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, " id in(select id_satuan from barangdetail where id_barang='"
                    + DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id") + "') ");
                cboKodeSatuan.Items.RemoveAt(0);
                txtNamaSatuan.Text = "";
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //MessageBox.Show(this, "Kode barang tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data barang tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            cboKodeSatuan.Items.Clear();
            txtNamaSatuan.Text = "";
            txtNamaItem.Text = "";
            this.id_barang = -1;
            ntHargaLama.Value = 0;
            this.Cursor = Cursors.Default;
        }

        private void TryGetHargaBeli()
        {
            try
            {
                if (string.IsNullOrEmpty(txtKodeBarang.Text)) return;
                this.id_barang = (int)DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id");

                if (string.IsNullOrEmpty(cboKodeSatuan.Text)) return;
                this.id_satuan = (int)DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id");

                if (string.IsNullOrEmpty(txtKodeSupplier.Text)) return;
                kodeSupplier = txtKodeSupplier.Text;

                ntHargaLama.Value = (decimal)DBAHargaBeli.Instance.GetSingleColumnByFilter("harga", " id_barang='" + this.id_barang + "' and id_satuan='" + this.id_satuan + "' and kode_supplier='" + txtKodeSupplier.Text + "' ");
            }
            catch (Exception ex)
            {
                ntHargaLama.Value = 0;
                if (ex.Message == "There is no row at position 0.")
                {
                    MessageBox.Show(this, "Harga beli untuk supplier ini belum terdaftar. Mohon periksa kembali", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    MessageBox.Show(this, ex.Message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menginputkan data ini?", "Konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            if (this.id_barang == -1 || this.id_satuan == -1 || this.kodeSupplier == "")
            {
                MessageBox.Show(this, "Data tidak lengkap. Mohon periksa kembali", "Data tidak lengkap", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string message = DBAPenyesuaianHargaBeli.Instance.Insert(this.id_barang, this.id_satuan, this.kodeSupplier, ntHargaLama.Value, ntHargaBaru.Value, txtInfo.Text, UserLoginInfo.Username);
            if (message == "success")
            {
                MessageBox.Show(this, "Data sudah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (this.frmMasterHargaBeli != null)
                {
                    this.frmMasterHargaBeli.UpdateHargaBeli(this.rowIndex, ntHargaBaru.Value);
                    this.Close();
                    return;
                }
                ClearInput();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKodeBarang.Text = "";
            txtNamaItem.Text = "";
            cboKodeSatuan.Items.Clear();
            txtNamaSatuan.Text = "";
            txtKodeSupplier.Text = "";
            txtNamaSupplier.Text = "";
            ntHargaBaru.Value = 0;
            ntHargaLama.Value = 0;
            this.kodeSupplier = "";
            this.id_barang = -1;
            this.id_satuan = -1;
            txtKodeSupplier.Focus();
        }

        private void FrmPenyesuaianHargaBeli_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(cboKodeSatuan, txtNamaSatuan, DBASatuan.Instance, "nama");
            if (message == "success")
            {
                txtNamaSatuan.Text = (string)DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "nama");
                TryGetHargaBeli();
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //  MessageBox.Show(this, "Kode supplier tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data satuan tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            txtNamaSatuan.Text = "";
            ntHargaLama.Value = 0;
            this.Cursor = Cursors.Default;
        }

        private void btnCariSupplier_Click(object sender, EventArgs e)
        {
            FrmCariSupplier frmCariSupplier = new FrmCariSupplier(this);
            frmCariSupplier.ShowDialog();
            txtKodeSupplier.Focus();
        }

        private void btnCariItem_Click(object sender, EventArgs e)
        {
            FrmCariBarang frmCariBarang = new FrmCariBarang(this);
            frmCariBarang.ShowDialog();
            txtKodeBarang.Focus();
        }
    }
}
