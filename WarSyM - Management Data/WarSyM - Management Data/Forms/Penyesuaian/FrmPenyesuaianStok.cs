﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.Forms.Master;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.Forms.Master.Pencarian;

namespace WarSyM_Management_Data.Forms.Penyesuaian
{
    public partial class FrmPenyesuaianStok : Form
    {
        FrmMasterStok frmMasterStok = null;
        public int idBarang, idSatuan, idToko, rowIndex;

        public FrmPenyesuaianStok()
        {
            InitializeComponent();
            idBarang = -1;
            idSatuan = -1;
            idToko = -1;
        }

        public FrmPenyesuaianStok(FrmMasterStok _frmMasterStok, int _idBarang, string _kodeBarang, string _namaBarang, int _idSatuan, string _kodeSatuan, 
            int _idToko, string _kodeToko, decimal _stok,  int _rowIndex)
        {
            InitializeComponent();
    
            this.idBarang = _idBarang;
            txtKodeBarang.Text = _kodeBarang;
            txtNamaItem.Text = _namaBarang;
            txtKodeBarang.ReadOnly = true;
            btnCariBarang.Enabled = false;

            ntStokSystem.Value = _stok;

            this.idSatuan = _idSatuan;

            ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, " id in(select id_satuan from barangdetail where id_barang='"
                    + DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id") + "') ");

            cboKodeSatuan.Text = _kodeSatuan;
            txtNamaSatuan.Text = (string)DBASatuan.Instance.GetSingleColumn(this.idSatuan, "nama");
            cboKodeSatuan.Enabled = false;
            //btnCariSatuan.Enabled = false;

            this.idToko = _idToko;
            txtKodeToko.Text = _kodeToko;
            txtNamaToko.Text = (string)DBAToko.Instance.GetSingleColumn(this.idToko, "nama");
            txtKodeToko.ReadOnly = true;
            btnCariToko.Enabled = false;

            frmMasterStok = _frmMasterStok;
            this.rowIndex = _rowIndex;
        }

        private void txtKodeBarang_Leave(object sender, EventArgs e)
        {
            if (txtKodeBarang.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeBarang, txtNamaItem, DBABarang.Instance, "nama");
            if (message == "success")
            {
                this.idBarang = (int)DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id");
                ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, " id in(select id_satuan from barangdetail where id_barang='"
                    + DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id") + "') ");
                cboKodeSatuan.Items.RemoveAt(0);
                txtNamaSatuan.Text = "";
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //MessageBox.Show(this, "Kode barang tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data barang tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            cboKodeSatuan.Items.Clear();
            txtNamaSatuan.Text = "";
            txtNamaItem.Text = "";
            this.idBarang = -1;
            this.Cursor = Cursors.Default;
        }

        private void txtKodeToko_Leave(object sender, EventArgs e)
        {
            if (txtKodeToko.ReadOnly) return;
            string message = ControlBinding.BindingSingleColumn(txtKodeToko, txtNamaToko, DBAToko.Instance, "nama");
            if (message == "success")
            {
                TryGetStok();
                return;
            }
            else if (message == "empty text")
            {
               // MessageBox.Show(this, "Kode toko tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data toko tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.idToko = -1;
        }

        private void TryGetStok()
        {
            try
            {
                if (string.IsNullOrEmpty(txtKodeBarang.Text))
                {
                    return;
                }
                this.idBarang = (int)DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id");

                if (string.IsNullOrEmpty(cboKodeSatuan.Text))
                {
                    return;
                }
                this.idSatuan = (int)DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id");

                if (string.IsNullOrEmpty(txtKodeToko.Text))
                {
                    return;
                }
                this.idToko = (int)DBAToko.Instance.GetSingleColumn(txtKodeToko.Text, "id");

                ntStokSystem.Value = (decimal)DBAKartuStok.Instance.GetSingleColumnByFilter("stok", " id_barang='" + this.idBarang + "' and id_satuan='" + this.idSatuan 
                    + "' and id_toko='" + this.idToko + "' ");
            }
            catch (Exception ex)
            {
                if (ex.Message == "There is no row at position 0.")
                {
                    MessageBox.Show(this, "Produk belum terdaftar. Mohon periksa kembali", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    MessageBox.Show(this, ex.Message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menginputkan data ini?", "Konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            if (this.idBarang == -1 || this.idSatuan == -1 || this.idToko == -1)
            {
                MessageBox.Show(this, "Data tidak lengkap. Mohon periksa kembali", "Data tidak lengkap", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
                        
            string message = DBAPenyesuaianStok.Instance.Insert(this.idBarang, this.idSatuan, this.idToko, ntStokSystem.Value, ntRealStok.Value, txtInfo.Text, 
                UserLoginInfo.Username);
            if (message == "success")
            {
                MessageBox.Show(this, "Data sudah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (this.frmMasterStok != null)
                {
                    this.frmMasterStok.UpdateStok(this.rowIndex, ntRealStok.Value);
                    this.Close();
                    return;
                }
                ClearInput();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKodeToko.Text = "";
            txtNamaItem.Text = "";
            cboKodeSatuan.Items.Clear();
            txtNamaSatuan.Text = "";
            txtKodeBarang.Text = "";
            txtNamaToko.Text = "";
            ntRealStok.Value = 0;
            ntStokSystem.Value = 0;
            txtInfo.Text = "";
            this.idBarang = -1;
            this.idSatuan = -1;
            this.idToko = -1;
            this.Focus();
        }

        private void FrmPenyesuaianStok_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cboKodeSatuan.Enabled) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(cboKodeSatuan, txtNamaSatuan, DBASatuan.Instance, "nama");
            if (message == "success")
            {
                txtNamaSatuan.Text = (string)DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "nama");
                TryGetStok();
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //  MessageBox.Show(this, "Kode supplier tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data satuan tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            txtNamaSatuan.Text = "";
            this.Cursor = Cursors.Default;
        }

        private void btnCariBarang_Click(object sender, EventArgs e)
        {
            FrmCariBarang frmCariBarang = new FrmCariBarang(this);
            frmCariBarang.ShowDialog();
            txtKodeBarang.Focus();
        }

        private void btnCariToko_Click(object sender, EventArgs e)
        {
            FrmCariToko frmCariToko = new FrmCariToko(this);
            frmCariToko.ShowDialog();
            txtKodeToko.Focus();
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
