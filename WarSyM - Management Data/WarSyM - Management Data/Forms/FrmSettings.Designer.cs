﻿namespace WarSyM_Management_Data.Forms
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettings));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.txtHostName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.radSeparator2 = new Telerik.WinControls.UI.RadSeparator();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBottom = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtHeader4 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtHeader3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHeader2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHeader1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtInfoNotaJual = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.radSeparator3 = new Telerik.WinControls.UI.RadSeparator();
            this.ntDecimalPlace = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.chkCharacterCashing = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntDecimalPlace)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(331, 279);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.radSeparator1);
            this.tabPage1.Controls.Add(this.txtHostName);
            this.tabPage1.Controls.Add(this.txtPassword);
            this.tabPage1.Controls.Add(this.txtName);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.txtUsername);
            this.tabPage1.Controls.Add(this.txtPort);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(323, 251);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Database";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(215, 213);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 30);
            this.button4.TabIndex = 67;
            this.button4.Text = "&Simpan";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(5, 201);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(309, 6);
            this.radSeparator1.TabIndex = 65;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // txtHostName
            // 
            this.txtHostName.Location = new System.Drawing.Point(75, 9);
            this.txtHostName.MaxLength = 255;
            this.txtHostName.Name = "txtHostName";
            this.txtHostName.Size = new System.Drawing.Size(237, 21);
            this.txtHostName.TabIndex = 54;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(75, 129);
            this.txtPassword.MaxLength = 255;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(237, 21);
            this.txtPassword.TabIndex = 60;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(75, 69);
            this.txtName.MaxLength = 255;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(237, 21);
            this.txtName.TabIndex = 57;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 15);
            this.label5.TabIndex = 63;
            this.label5.Text = "Password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 15);
            this.label3.TabIndex = 62;
            this.label3.Text = "Username:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 15);
            this.label4.TabIndex = 59;
            this.label4.Text = "Name:";
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(73, 161);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 29);
            this.button1.TabIndex = 61;
            this.button1.Text = "&Test Connection";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(75, 99);
            this.txtUsername.MaxLength = 255;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(237, 21);
            this.txtUsername.TabIndex = 58;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(75, 39);
            this.txtPort.MaxLength = 255;
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(237, 21);
            this.txtPort.TabIndex = 55;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 15);
            this.label2.TabIndex = 56;
            this.label2.Text = "Port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 53;
            this.label1.Text = "Host:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.radSeparator2);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.txtBottom);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.txtHeader4);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.txtHeader3);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.txtHeader2);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txtHeader1);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.txtInfoNotaJual);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(323, 251);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Nota Jual";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(215, 213);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(100, 30);
            this.button5.TabIndex = 70;
            this.button5.Text = "&Simpan";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // radSeparator2
            // 
            this.radSeparator2.Location = new System.Drawing.Point(5, 201);
            this.radSeparator2.Name = "radSeparator2";
            this.radSeparator2.Size = new System.Drawing.Size(309, 6);
            this.radSeparator2.TabIndex = 68;
            this.radSeparator2.Text = "radSeparator2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(44, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 15);
            this.label11.TabIndex = 28;
            this.label11.Text = "Bottom:";
            // 
            // txtBottom
            // 
            this.txtBottom.Location = new System.Drawing.Point(98, 126);
            this.txtBottom.MaxLength = 100;
            this.txtBottom.Name = "txtBottom";
            this.txtBottom.Size = new System.Drawing.Size(213, 21);
            this.txtBottom.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 15);
            this.label10.TabIndex = 27;
            this.label10.Text = "Header 4:";
            // 
            // txtHeader4
            // 
            this.txtHeader4.Location = new System.Drawing.Point(98, 96);
            this.txtHeader4.MaxLength = 50;
            this.txtHeader4.Name = "txtHeader4";
            this.txtHeader4.Size = new System.Drawing.Size(213, 21);
            this.txtHeader4.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(31, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 15);
            this.label9.TabIndex = 26;
            this.label9.Text = "Header 3:";
            // 
            // txtHeader3
            // 
            this.txtHeader3.Location = new System.Drawing.Point(98, 66);
            this.txtHeader3.MaxLength = 50;
            this.txtHeader3.Name = "txtHeader3";
            this.txtHeader3.Size = new System.Drawing.Size(213, 21);
            this.txtHeader3.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 15);
            this.label8.TabIndex = 25;
            this.label8.Text = "Header 2:";
            // 
            // txtHeader2
            // 
            this.txtHeader2.Location = new System.Drawing.Point(98, 36);
            this.txtHeader2.MaxLength = 50;
            this.txtHeader2.Name = "txtHeader2";
            this.txtHeader2.Size = new System.Drawing.Size(213, 21);
            this.txtHeader2.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 15);
            this.label7.TabIndex = 24;
            this.label7.Text = "Header 1:";
            // 
            // txtHeader1
            // 
            this.txtHeader1.Location = new System.Drawing.Point(98, 7);
            this.txtHeader1.MaxLength = 50;
            this.txtHeader1.Name = "txtHeader1";
            this.txtHeader1.Size = new System.Drawing.Size(213, 21);
            this.txtHeader1.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 15);
            this.label6.TabIndex = 23;
            this.label6.Text = "Info Nota Jual:";
            // 
            // txtInfoNotaJual
            // 
            this.txtInfoNotaJual.Location = new System.Drawing.Point(98, 156);
            this.txtInfoNotaJual.MaxLength = 100;
            this.txtInfoNotaJual.Name = "txtInfoNotaJual";
            this.txtInfoNotaJual.Size = new System.Drawing.Size(213, 21);
            this.txtInfoNotaJual.TabIndex = 22;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.chkCharacterCashing);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.radSeparator3);
            this.tabPage3.Controls.Add(this.ntDecimalPlace);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(323, 251);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Aplikasi";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(215, 213);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 30);
            this.button2.TabIndex = 72;
            this.button2.Text = "&Simpan";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // radSeparator3
            // 
            this.radSeparator3.Location = new System.Drawing.Point(7, 202);
            this.radSeparator3.Name = "radSeparator3";
            this.radSeparator3.Size = new System.Drawing.Size(309, 4);
            this.radSeparator3.TabIndex = 71;
            this.radSeparator3.Text = "radSeparator3";
            // 
            // ntDecimalPlace
            // 
            this.ntDecimalPlace.Location = new System.Drawing.Point(149, 40);
            this.ntDecimalPlace.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.ntDecimalPlace.Name = "ntDecimalPlace";
            this.ntDecimalPlace.Size = new System.Drawing.Size(166, 21);
            this.ntDecimalPlace.TabIndex = 2;
            this.ntDecimalPlace.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 43);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 15);
            this.label12.TabIndex = 1;
            this.label12.Text = "Jumlah angka desimal:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(41, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(102, 15);
            this.label13.TabIndex = 73;
            this.label13.Text = "Character casing:";
            // 
            // chkCharacterCashing
            // 
            this.chkCharacterCashing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chkCharacterCashing.FormattingEnabled = true;
            this.chkCharacterCashing.Items.AddRange(new object[] {
            "Normal",
            "Upper",
            "Lower"});
            this.chkCharacterCashing.Location = new System.Drawing.Point(149, 10);
            this.chkCharacterCashing.Name = "chkCharacterCashing";
            this.chkCharacterCashing.Size = new System.Drawing.Size(166, 23);
            this.chkCharacterCashing.TabIndex = 74;
            // 
            // FrmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 279);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSettings_FormClosing);
            this.Load += new System.EventHandler(this.FrmSettings_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntDecimalPlace)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button4;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.TextBox txtHostName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button5;
        private Telerik.WinControls.UI.RadSeparator radSeparator2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBottom;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtHeader4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtHeader3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtHeader2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtHeader1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtInfoNotaJual;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.NumericUpDown ntDecimalPlace;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button2;
        private Telerik.WinControls.UI.RadSeparator radSeparator3;
        private System.Windows.Forms.ComboBox chkCharacterCashing;
        private System.Windows.Forms.Label label13;
    }
}