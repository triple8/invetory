﻿using System;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms
{
    public partial class FrmSettings : Form
    {
        bool isSaveMode;
        public FrmSettings(bool _isSaveMode)
        {
            InitializeComponent();
            this.isSaveMode = _isSaveMode;
        }

        private void FrmSettings_Load(object sender, EventArgs e)
        {
            InitSettings();
            chkCharacterCashing.Text = ApplicationSettings.Instance.UseCapitalForInputTextBox;
            ntDecimalPlace.Value = ApplicationSettings.Instance.DecimalPlaceForNumeric;
        }

        private void InitSettings()
        {
            settingDatabase();
            settingNotaJual();
            if (this.isSaveMode)
            {
                tabPage2.Dispose();
            }
        }

        private void settingNotaJual()
        {
            txtBottom.Text = ApplicationSettings.Instance.BottomNota;
            txtHeader1.Text =ApplicationSettings.Instance.HeaderNota1;
            txtHeader2.Text = ApplicationSettings.Instance.HeaderNota2;
            txtHeader3.Text = ApplicationSettings.Instance.HeaderNota3;
            txtHeader4.Text = ApplicationSettings.Instance.HeaderNota4;
            txtInfoNotaJual.Text = ApplicationSettings.Instance.InfoNota;
        }

        private void settingDatabase()
        {
            txtHostName.Text = ApplicationSettings.Instance.DataBaseHost;
            txtName.Text = ApplicationSettings.Instance.DatabaseName;
            txtPassword.Text = ApplicationSettings.Instance.DatabasePassword;
            txtPort.Text = ApplicationSettings.Instance.DatabasePort;
            txtUsername.Text = ApplicationSettings.Instance.DatabaseUser;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string ConnectionString =
                    "server=" + txtHostName.Text + ";uid=" + txtUsername.Text + ";password=" + txtPassword.Text + ";database="
                    + txtName.Text + ";port=" + txtPort.Text;

                MySqlConnection cn = new MySqlConnection(ConnectionString);
                cn.Open();
                MessageBox.Show(this,"Database server ditemukan", "Pemberitahuan", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cn.Close();
            }
            catch (Exception f)
            {
                MessageBox.Show(this,"Database server tidak ditemukan, mohon periksa kembali.","Server Tidak Ditemukan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, f.Message, "Server Tidak Ditemukan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Apakah anda yakin akan menyimpan settingan database?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
            Common.WriteSettingsDatabase(txtHostName.Text, txtPort.Text, txtName.Text, txtUsername.Text, txtPassword.Text);
            MessageBox.Show(this,"Setting aplikasi sudah disimpan, mohon restart apliaksi.","Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show(this, "Apakah anda yakin akan menyimpan settingan nota?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
           
            //DBASettings.Instance.SaveSettings(AppInit.id, txtInfoNotaJual.Text, txtHeader1.Text.Replace("'", "''"), txtHeader2.Text.Replace("'", "''"),
            //    txtHeader3.Text.Replace("'", "''"), txtHeader4.Text.Replace("'", "''"), txtBottom.Text.Replace("'", "''"));
            //MessageBox.Show(this, "Setting nota jual sudah disimpan, mohon restart apliaksi.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void FrmSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.isSaveMode)
                Application.Exit();
        }
    }
}
