﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Penyesuaian;

namespace WarSyM_Management_Data.Forms.Master
{
    public partial class FrmMasterStok : Form
    {
        bool initLoad = false;
        public FrmMasterStok()
        {
            InitializeComponent();
        }

        private void FrmMasterStok_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            GridSetting();
            cboDisplay.SelectedIndex = 0;

            ControlBinding.BindingComboBox(DBABarang.Instance, cboKodeBarang, 1, "");
            cboKodeBarang.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBAToko.Instance, cboKodeToko, 1, "");
            cboKodeToko.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, "");
            cboKodeSatuan.SelectedIndex = 0;
            cboKodeSatuan.Items.RemoveAt(1);
            initLoad = true;
            dgStok.Sort(dgStok.Columns["clmnKodeBarang"], System.ComponentModel.ListSortDirection.Ascending);
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dgStok, DBAKartuStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dgStok, DBAKartuStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAKartuStok.Instance.Count(GetFilter()), dgStok, DBAKartuStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void GridSetting()
        {
            dgStok.Columns.Add("clmnIdBarang", "Id Barang");
            dgStok.Columns.Add("clmnKodeBarang", "Kode Barang");
            dgStok.Columns.Add("clmnNamaBarang", "Nama Barang");
            dgStok.Columns.Add("clmnIdSatuan", "Id Satuan"); 
            dgStok.Columns.Add("clmnKodeSatuan", "Kode Satuan");
            dgStok.Columns.Add("clmnIdToko", "Id Toko");
            dgStok.Columns.Add("clmnKodeToko", "Kode Toko");
            dgStok.Columns.Add("clmnStok", "Stok"); 
            dgStok.Columns.Add("clmnMinimumStok", "Minimum Stok");

            dgStok.Columns["clmnIdBarang"].Visible = false;
            dgStok.Columns["clmnIdSatuan"].Visible = false;
            dgStok.Columns["clmnIdToko"].Visible = false;

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Penyesuaian Stok";
            bcol.Name = "btnPenyesuaianStok";
            bcol.UseColumnTextForButtonValue = true;
            dgStok.Columns.Add(bcol);

            dgStok.Columns["clmnStok"].DefaultCellStyle.Format = "N2";
            dgStok.Columns["clmnStok"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgStok.Columns["clmnMinimumStok"].DefaultCellStyle.Format = "N2";
            dgStok.Columns["clmnMinimumStok"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgStok.Columns["btnPenyesuaianStok"].Width = 150;

            dgStok.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAKartuStok.Instance.Count(GetFilter()), dgStok, DBAKartuStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dgStok, DBAKartuStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dgStok, "Master Stok");
        }

        private void dgStok_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (dgStok.Columns[e.ColumnIndex].Name == "btnPenyesuaianStok")
            {
                FrmPenyesuaianStok frmPenyesuaianStok = new FrmPenyesuaianStok(this, int.Parse(dgStok.Rows[e.RowIndex].Cells["clmnIdBarang"].Value.ToString()), 
                    dgStok.Rows[e.RowIndex].Cells["clmnKodeBarang"].Value.ToString(), dgStok.Rows[e.RowIndex].Cells["clmnNamaBarang"].Value.ToString(),
                    int.Parse(dgStok.Rows[e.RowIndex].Cells["clmnIdSatuan"].Value.ToString()), dgStok.Rows[e.RowIndex].Cells["clmnKodeSatuan"].Value.ToString(),
                    int.Parse(dgStok.Rows[e.RowIndex].Cells["clmnIdToko"].Value.ToString()), dgStok.Rows[e.RowIndex].Cells["clmnKodeToko"].Value.ToString(),
                    decimal.Parse(dgStok.Rows[e.RowIndex].Cells["clmnStok"].Value.ToString()), e.RowIndex);
                frmPenyesuaianStok.ShowDialog();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            HighLightMinimumStok();
        }

        private void HighLightMinimumStok()
        {
            if (checkBox1.Checked)
            {
                for (int i = 0; i < dgStok.RowCount; i++)
                {
                    if (decimal.Parse(dgStok.Rows[i].Cells["clmnStok"].Value.ToString()) < decimal.Parse(dgStok.Rows[i].Cells["clmnMinimumStok"].Value.ToString()))
                        dgStok.Rows[i].DefaultCellStyle.ForeColor = Color.Red;
                }
            }
            else
            {
                for (int i = 0; i < dgStok.RowCount; i++)
                {
                    dgStok.Rows[i].DefaultCellStyle.ForeColor = Color.Black;
                }
            }
        }

        private void dgStok_Paint(object sender, PaintEventArgs e)
        {
            HighLightMinimumStok();
        }

        private void cboKodeBarang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboKodeToko_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplay.Text == "All") ? DBAKartuStok.Instance.Count("") : int.Parse(cboDisplay.Text);
            Pagging.BindingGrid(dgStok, DBAKartuStok.Instance, 0, countToDisplay, filter);
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (cboKodeBarang.Text != "" && cboKodeBarang.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " b.kode='" + cboKodeBarang.Text + "' " : returnValue + " and b.kode='" + cboKodeBarang.Text + "' ";

            if (cboKodeSatuan.Text != "" && cboKodeSatuan.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " s.kode='" + cboKodeSatuan.Text + "' " : returnValue + " and s.kode='" + cboKodeSatuan.Text + "' ";

            if (cboKodeToko.Text != "" && cboKodeToko.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " t.kode='" + cboKodeToko.Text + "' " : returnValue + " and t.kode='" + cboKodeToko.Text + "' ";

            return returnValue;

        }

        public void UpdateStok(int rowIndex, decimal value)
        {
            dgStok.Rows[rowIndex].Cells["clmnStok"].Value = value;
        }
    }
}
