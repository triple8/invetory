﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Forms.Master.Input;
using WarSyM_Management_Data.Forms.Master.Ubah;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master
{
    public partial class FrmMasterSupplier : Form
    {
        public FrmMasterSupplier()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmInputSupplier frmInputSupplier = new FrmInputSupplier(this);
            frmInputSupplier.ShowDialog();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "Master Supplier");
        }

        private void FrmMasterSupplier_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            SetGridButton();

            cboDisplay.SelectedIndex = 0;
            dataGridView1.Sort(dataGridView1.Columns["clmnKode"], System.ComponentModel.ListSortDirection.Ascending);
        }

        private void SetGridButton()
        {
            dataGridView1.Columns.Add("clmnKode", "Kode");
            dataGridView1.Columns.Add("clmnNama", "Nama");
            dataGridView1.Columns.Add("clmnAlamat", "Alamat");
            dataGridView1.Columns.Add("clmnKota", "Kota");
            dataGridView1.Columns.Add("clmnTelp", "Telp");

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Ubah";
            bcol.Name = "btnUbah";
            bcol.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol);

            DataGridViewButtonColumn bcol2 = new DataGridViewButtonColumn();
            bcol2.HeaderText = "";
            bcol2.Text = "Hapus";
            bcol2.Name = "btnHapus";
            bcol2.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol2);
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBASupplier.Instance, lblPage, cboDisplay, "");
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBASupplier.Instance, lblPage, cboDisplay, "");
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBASupplier.Instance.Count(""), dataGridView1, DBASupplier.Instance, lblPage, cboDisplay, "");
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBASupplier.Instance.Count(""), dataGridView1, DBASupplier.Instance, lblPage, cboDisplay, "");
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBASupplier.Instance, lblPage, cboDisplay, "");
        }


        public void InsertSupplier(params object[] values)
        {
            dataGridView1.Rows.Add(values);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnUbah")
            {
                if (dataGridView1.Rows.Count == 0) return;
                FrmUbahSupplier frmUbahSupplier = new FrmUbahSupplier(this, dataGridView1["clmnKode", e.RowIndex].Value.ToString(), dataGridView1["clmnNama", e.RowIndex].Value.ToString(),
                    dataGridView1["clmnAlamat", e.RowIndex].Value.ToString(), dataGridView1["clmnKota", e.RowIndex].Value.ToString(), dataGridView1["clmnTelp", e.RowIndex].Value.ToString(), e.RowIndex);
                frmUbahSupplier.ShowDialog();
            }
            else if (dataGridView1.Columns[e.ColumnIndex].Name == "btnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data supplier?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    == System.Windows.Forms.DialogResult.No) return;
                string message = DBASupplier.Instance.Delete(dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString());
                if (message == "success")
                {
                    MessageBox.Show(this, "Data supplier sudah diubah", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.Rows.RemoveAt(e.RowIndex);
                }
                else if (message == "supplier in used in other table")
                {
                    MessageBox.Show(this, "Data supplier tidak dapat dihapus. Data supplier memiliki koresponden dengan data lain.", "Data not deleted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void UbahSupplier(params object[] values)
        {
            dataGridView1.Rows[(int)values[0]].Cells["clmnNama"].Value = values[1];
            dataGridView1.Rows[(int)values[0]].Cells["clmnAlamat"].Value = values[2];
            dataGridView1.Rows[(int)values[0]].Cells["clmnKota"].Value = values[3];
            dataGridView1.Rows[(int)values[0]].Cells["clmnTelp"].Value = values[4];
        }
    }
}
