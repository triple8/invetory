﻿namespace WarSyM_Management_Data.Forms.Master.Input
{
    partial class FrmInputMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInputMenu));
            this.txtKode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNama = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ntHarga = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboKodeSatuan = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtKodeBarang = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNamaSatuan = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.radSeparator2 = new Telerik.WinControls.UI.RadSeparator();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.clmnKodeBarang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnKodeSatuan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnJumlah = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ntJumlah = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNamaItem = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCariItem = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ntHarga)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntJumlah)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtKode
            // 
            this.txtKode.Location = new System.Drawing.Point(64, 7);
            this.txtKode.Name = "txtKode";
            this.txtKode.Size = new System.Drawing.Size(220, 21);
            this.txtKode.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 15);
            this.label3.TabIndex = 46;
            this.label3.Text = "Kode:";
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(64, 34);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(220, 21);
            this.txtNama.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 15);
            this.label1.TabIndex = 45;
            this.label1.Text = "Nama:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 15);
            this.label2.TabIndex = 47;
            this.label2.Text = "Harga:";
            // 
            // ntHarga
            // 
            this.ntHarga.DecimalPlaces = 2;
            this.ntHarga.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntHarga.Location = new System.Drawing.Point(64, 61);
            this.ntHarga.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntHarga.Name = "ntHarga";
            this.ntHarga.Size = new System.Drawing.Size(220, 21);
            this.ntHarga.TabIndex = 2;
            this.ntHarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntHarga.ThousandsSeparator = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboKodeSatuan);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtKodeBarang);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtNamaSatuan);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.radSeparator2);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.ntJumlah);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtNamaItem);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnCariItem);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(9, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 367);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detail Menu";
            // 
            // cboKodeSatuan
            // 
            this.cboKodeSatuan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKodeSatuan.FormattingEnabled = true;
            this.cboKodeSatuan.Location = new System.Drawing.Point(102, 72);
            this.cboKodeSatuan.Name = "cboKodeSatuan";
            this.cboKodeSatuan.Size = new System.Drawing.Size(169, 23);
            this.cboKodeSatuan.TabIndex = 91;
            this.cboKodeSatuan.SelectedIndexChanged += new System.EventHandler(this.cboKodeSatuan_SelectedIndexChanged);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(143, 331);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(128, 28);
            this.button3.TabIndex = 10;
            this.button3.Text = "&Hapus Detail";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 331);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 28);
            this.button1.TabIndex = 9;
            this.button1.Text = "&Hapus";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtKodeBarang
            // 
            this.txtKodeBarang.Location = new System.Drawing.Point(102, 21);
            this.txtKodeBarang.MaxLength = 50;
            this.txtKodeBarang.Name = "txtKodeBarang";
            this.txtKodeBarang.Size = new System.Drawing.Size(130, 21);
            this.txtKodeBarang.TabIndex = 0;
            this.txtKodeBarang.Leave += new System.EventHandler(this.txtKodeBarang_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 15);
            this.label8.TabIndex = 90;
            this.label8.Text = "Nama satuan:";
            // 
            // txtNamaSatuan
            // 
            this.txtNamaSatuan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaSatuan.Location = new System.Drawing.Point(102, 99);
            this.txtNamaSatuan.Name = "txtNamaSatuan";
            this.txtNamaSatuan.ReadOnly = true;
            this.txtNamaSatuan.Size = new System.Drawing.Size(169, 21);
            this.txtNamaSatuan.TabIndex = 5;
            this.txtNamaSatuan.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(8, 159);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(264, 25);
            this.button2.TabIndex = 7;
            this.button2.Text = "&Tambah";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // radSeparator2
            // 
            this.radSeparator2.Location = new System.Drawing.Point(8, 150);
            this.radSeparator2.Name = "radSeparator2";
            this.radSeparator2.Size = new System.Drawing.Size(263, 5);
            this.radSeparator2.TabIndex = 88;
            this.radSeparator2.Text = "radSeparator2";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnKodeBarang,
            this.clmnKodeSatuan,
            this.clmnJumlah});
            this.dataGridView1.Location = new System.Drawing.Point(8, 189);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(263, 137);
            this.dataGridView1.TabIndex = 8;
            // 
            // clmnKodeBarang
            // 
            this.clmnKodeBarang.HeaderText = "Kode Barang";
            this.clmnKodeBarang.Name = "clmnKodeBarang";
            this.clmnKodeBarang.ReadOnly = true;
            this.clmnKodeBarang.Width = 80;
            // 
            // clmnKodeSatuan
            // 
            this.clmnKodeSatuan.HeaderText = "Kode Satuan";
            this.clmnKodeSatuan.Name = "clmnKodeSatuan";
            this.clmnKodeSatuan.ReadOnly = true;
            this.clmnKodeSatuan.Width = 80;
            // 
            // clmnJumlah
            // 
            this.clmnJumlah.HeaderText = "Jumlah";
            this.clmnJumlah.Name = "clmnJumlah";
            this.clmnJumlah.ReadOnly = true;
            this.clmnJumlah.Width = 80;
            // 
            // ntJumlah
            // 
            this.ntJumlah.DecimalPlaces = 2;
            this.ntJumlah.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntJumlah.Location = new System.Drawing.Point(102, 125);
            this.ntJumlah.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntJumlah.Name = "ntJumlah";
            this.ntJumlah.Size = new System.Drawing.Size(169, 21);
            this.ntJumlah.TabIndex = 6;
            this.ntJumlah.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntJumlah.ThousandsSeparator = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 15);
            this.label4.TabIndex = 86;
            this.label4.Text = "Jumlah:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 15);
            this.label6.TabIndex = 84;
            this.label6.Text = "Kode satuan:";
            // 
            // txtNamaItem
            // 
            this.txtNamaItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaItem.Location = new System.Drawing.Point(102, 46);
            this.txtNamaItem.Name = "txtNamaItem";
            this.txtNamaItem.ReadOnly = true;
            this.txtNamaItem.Size = new System.Drawing.Size(169, 21);
            this.txtNamaItem.TabIndex = 2;
            this.txtNamaItem.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 15);
            this.label5.TabIndex = 83;
            this.label5.Text = "Nama barang:";
            // 
            // btnCariItem
            // 
            this.btnCariItem.Image = ((System.Drawing.Image)(resources.GetObject("btnCariItem.Image")));
            this.btnCariItem.Location = new System.Drawing.Point(238, 20);
            this.btnCariItem.Name = "btnCariItem";
            this.btnCariItem.Size = new System.Drawing.Size(33, 23);
            this.btnCariItem.TabIndex = 1;
            this.btnCariItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariItem.UseVisualStyleBackColor = true;
            this.btnCariItem.Click += new System.EventHandler(this.btnCariItem_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 15);
            this.label7.TabIndex = 82;
            this.label7.Text = "Kode barang:";
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(10, 458);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(279, 5);
            this.radSeparator1.TabIndex = 87;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(9, 469);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(136, 31);
            this.btnSimpan.TabIndex = 4;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Location = new System.Drawing.Point(154, 469);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(136, 31);
            this.btnBatal.TabIndex = 5;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // FrmInputMenu
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(298, 507);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ntHarga);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtKode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmInputMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Input menu";
            this.Load += new System.EventHandler(this.FrmInputMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ntHarga)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntJumlah)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtKode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNama;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown ntHarga;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtNamaItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCariItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown ntJumlah;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnKodeBarang;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnKodeSatuan;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnJumlah;
        private System.Windows.Forms.Button button2;
        private Telerik.WinControls.UI.RadSeparator radSeparator2;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtNamaSatuan;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox txtKodeBarang;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox cboKodeSatuan;
    }
}