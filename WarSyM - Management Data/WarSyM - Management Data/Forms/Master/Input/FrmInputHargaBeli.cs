﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Forms.Master.Pencarian;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputHargaBeli : Form
    {
        FrmMasterHargaBeli frmMasterHargaBeli;
        public int idBarang, idSatuan;
        public string kodeSupplier;

        public FrmInputHargaBeli(FrmMasterHargaBeli _frmMasterHargabeli)
        {
            InitializeComponent();
            this.idBarang = -1;
            this.idSatuan = -1;
            this.kodeSupplier = "";
            this.frmMasterHargaBeli = _frmMasterHargabeli;
        }

        private void btnCariItem_Click(object sender, EventArgs e)
        {
            FrmCariBarang frmCariBarang = new FrmCariBarang(this);
            frmCariBarang.ShowDialog();
            txtKodeBarang.Focus();
        }

        private void btnCariSupplier_Click(object sender, EventArgs e)
        {
            FrmCariSupplier frmCariSupplier = new FrmCariSupplier(this);
            frmCariSupplier.ShowDialog();
            txtKodeSupplier.Focus();
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan memasukkan data ini?", "Konfirmasi", MessageBoxButtons.YesNo, 
                MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
            if (IsNullInput())
            {
                MessageBox.Show(this, "Data tidak lengkap, mohon diperiksa kembali.", "Pemberitahuan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtKodeBarang.Focus();
                return;
            }
            string message = DBAHargaBeli.Instance.Insert(this.kodeSupplier, this.idBarang, this.idSatuan, ntHarga.Value);
            if (message.Split(',')[0].ToString() == "success")
            {
                MessageBox.Show(this, "Data harga beli produk sudah disimpan.", "Data tersimpan.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterHargaBeli.InsertData(txtKodeSupplier.Text, this.idBarang, txtKodeBarang.Text, txtNamaItem.Text, this.idSatuan, cboKodeSatuan.Text, txtNamaSatuan.Text, ntHarga.Value);
                ClearInput();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Harga beli untuk produk ini sudah ada di database, jika ingin melakukan perubahan harga beli, gunakan menu penyesuaian harga beli.",
                    "Data sudah ada.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IsNullInput()
        {
            return this.idBarang == -1 || this.idSatuan == -1 || this.kodeSupplier == "";
        }

        private void ClearInput()
        {
            txtKodeBarang.Text = "";
            cboKodeSatuan.Items.Clear();
            txtKodeSupplier.Text = "";
            txtNamaItem.Text = "";
            txtNamaSatuan.Text = "";
            txtNamaSupplier.Text = "";
            ntHarga.Value = 0;
        }

        private void txtKodeBarang_Leave(object sender, EventArgs e)
        {
            if (txtKodeBarang.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeBarang, txtNamaItem, DBABarang.Instance, "nama"); 
            if (message == "success")
            {
                this.idBarang = (int)DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id");
                ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, " id in(select id_satuan from barangdetail where id_barang='"
                    + DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id") + "') ");
                cboKodeSatuan.Items.RemoveAt(0);
                txtNamaSatuan.Text = "";
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //MessageBox.Show(this, "Kode barang tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if(message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data barang tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            cboKodeSatuan.Items.Clear();
            txtNamaSatuan.Text = "";
            txtNamaItem.Text = "";
            this.idBarang = -1;
            this.Cursor = Cursors.Default;
        }

        private void txtKodeSupplier_Leave(object sender, EventArgs e)
        {
            if (txtKodeSupplier.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeSupplier, txtNamaSupplier, DBASupplier.Instance, "nama");
            if (message == "success")
            {
                this.kodeSupplier = txtKodeSupplier.Text;
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
              //  MessageBox.Show(this, "Kode supplier tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data supplier tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.kodeSupplier = "";
            txtNamaSupplier.Text = "";
            this.Cursor = Cursors.Default;
        }

        private void FrmInputHargaBeli_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(cboKodeSatuan, txtNamaSatuan, DBASatuan.Instance, "nama");
            if (message == "success")
            {
                this.idSatuan = int.Parse(DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id").ToString());
                txtNamaSatuan.Text = (string)DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "nama");
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //  MessageBox.Show(this, "Kode supplier tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data satuan tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.idSatuan = -1;
            txtNamaSatuan.Text = "";
            this.Cursor = Cursors.Default;

        }
    }
}
