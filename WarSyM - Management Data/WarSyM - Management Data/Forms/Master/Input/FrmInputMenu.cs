﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Pencarian;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputMenu : Form
    {
        FrmMasterBarang frmMasterBarang;
        int idBarang;

        public FrmInputMenu(FrmMasterBarang _frmMasterBarang)
        {
            InitializeComponent();
            this.frmMasterBarang = _frmMasterBarang;
        }

        private void txtKodeBarang_Leave(object sender, EventArgs e)
        {
            if (txtKodeBarang.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeBarang, txtNamaItem, DBABarang.Instance, "nama");
            if (message == "success")
            {
                this.idBarang = (int)DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id");
                ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, " id in(select id_satuan from barangdetail where id_barang='"
                    + DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id") + "') ");
                cboKodeSatuan.Items.RemoveAt(0);
                txtNamaSatuan.Text = "";
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //MessageBox.Show(this, "Kode barang tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data barang tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            cboKodeSatuan.Items.Clear();
            txtNamaSatuan.Text = "";
            txtNamaItem.Text = "";
            this.idBarang = -1;
            this.Cursor = Cursors.Default;
        }

        private void FrmInputMenu_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            foreach (Control gb in this.Controls)
            {
                if (gb is GroupBox)
                {
                    foreach (Control tb in gb.Controls)
                    {
                        if (tb is TextBox)
                        {
                            if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                                ((TextBox)tb).CharacterCasing = CharacterCasing.Normal;
                            else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                                ((TextBox)tb).CharacterCasing = CharacterCasing.Upper;
                            else
                                ((TextBox)tb).CharacterCasing = CharacterCasing.Lower;
                        }
                        else if (tb is NumericUpDown)
                        {
                            ((NumericUpDown)tb).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                        }
                    }
                }
            }
            this.idBarang = -1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (IsNullDataMenu())
            {
                MessageBox.Show(this, "Data tidak lengkap. Mohon periksa kembali.", "Data tidak lengkap", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtKodeBarang.Focus();
                return;
            }

            dataGridView1.Rows.Add(txtKodeBarang.Text, cboKodeSatuan.Text, ntJumlah.Value);
            ClearDetail();
        }

        private void ClearDetail()
        {
            txtKodeBarang.Text = "";
            txtNamaItem.Text = "";
            cboKodeSatuan.Items.Clear();
            txtNamaSatuan.Text = "";
            ntJumlah.Value = 0;
            this.idBarang = -1;
            txtKodeBarang.Focus();
        }

        private bool IsNullDataMenu()
        {
            return this.idBarang == -1 || cboKodeSatuan.SelectedIndex == -1 || ntJumlah.Value == 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menghapus data detail?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
            dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menghapus semua data detail?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
            dataGridView1.Rows.Clear();
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(cboKodeSatuan, txtNamaSatuan, DBASatuan.Instance, "nama");
            if (message == "success")
            {
                txtNamaSatuan.Text = (string)DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "nama");
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //  MessageBox.Show(this, "Kode supplier tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data satuan tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            txtNamaSatuan.Text = "";
            this.Cursor = Cursors.Default;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (IsNullInput())
            {
                MessageBox.Show(this, "Data belum lengkap, mohon periksa kembali", "Data belum lengkap", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (MessageBox.Show(this, "Apakah Anda yakin akan menginputkan data menu?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            InputMenu();
        }

        private void InputMenu()
        {
            List<string> detailKodeBarang = new List<string>(),
                detailKodeSatuan = new List<string>(), detailJumlah = new List<string>();

            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                detailKodeBarang.Add(dr.Cells[0].Value.ToString());
                detailKodeSatuan.Add(dr.Cells[1].Value.ToString());
                detailJumlah.Add(dr.Cells[2].Value.ToString());
            }

            string message = DBAMenu.Instance.InsertMenu(txtKode.Text, txtNama.Text, ntHarga.Value.ToString(), detailKodeBarang.ToArray(),
                detailKodeSatuan.ToArray(), detailJumlah.ToArray(), dataGridView1.Rows.Count);

            if (message.Split(',')[0] == "success")
            {
                MessageBox.Show(this, "Data menu sudah disimpan", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterBarang.InsertMenu(message.Split(',')[1], txtKode.Text, txtNama.Text, ntHarga.Value);
                ClearInput();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Kode menu sudah ada, mohon diubah dan diperiksa kembali.", "Duplicate kode", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtKode.Focus();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan query pada database, mohon periksa input/jaringan Anda.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ClearInput()
        {
            txtKode.Text = "";
            txtNama.Text = "";
            ntHarga.Value = 0;
            dataGridView1.Rows.Clear();
            txtKode.Focus();
        }

        private bool IsNullInput()
        {
            return dataGridView1.Rows.Count == 0 || string.IsNullOrEmpty(txtKode.Text);
        }

        private void btnCariItem_Click(object sender, EventArgs e)
        {
            FrmCariBarang frmCariBarang = new FrmCariBarang(this);
            frmCariBarang.ShowDialog();
            txtKodeBarang.Focus();
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }     
    }
}
