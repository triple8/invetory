﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputSupplier : Form
    {
        FrmMasterSupplier frmMasterSupplier;
        public FrmInputSupplier(FrmMasterSupplier _frmMasterSupplier)
        {
            InitializeComponent();
            this.frmMasterSupplier = _frmMasterSupplier;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan memasukkan data ini?","Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 
                == System.Windows.Forms.DialogResult.No) return;
            string message = DBASupplier.Instance.Insert(txtNama.Text.Replace("'", "''"), txtAlamat.Text.Replace("'", "''"),
                    txtKota.Text.Replace("'", "''"), txtTelp.Text.Replace("'", "''"));
            if (message.Split(',')[0] == "success")
            {
                MessageBox.Show(this, "Data supplier sudah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterSupplier.InsertSupplier(message.Split(',')[1], txtNama.Text, txtAlamat.Text, txtKota.Text, txtTelp.Text);
                ClearInput();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ClearInput();    
        }

        private void ClearInput()
        {
            txtTelp.Text = "";
            txtNama.Text = "";
            txtKota.Text = "";
            txtAlamat.Text = "";
            txtNama.Focus();
        }

        private void FrmInputSupplier_Load(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
