﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Pencarian;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputProduk : Form
    {
        FrmMasterBarang frmMasterBarang;
        public int id_barang, id_satuan, id_parent;

        public FrmInputProduk(FrmMasterBarang _frmMasterBarang)
        {
            InitializeComponent();
            this.frmMasterBarang = _frmMasterBarang;
        }

        public FrmInputProduk(FrmMasterBarang _frmMasterBarang, int _id_barang, string _kodeBarang, string _namaBarang)
        {
            InitializeComponent();
            this.frmMasterBarang = _frmMasterBarang;
            this.id_barang = _id_barang;
            
            txtKodeBarang.Text = _kodeBarang;
            txtKodeBarang.ReadOnly = true;
            txtNamaItem.Text = _namaBarang;
        }

        private void btnCariItem_Click(object sender, EventArgs e)
        {
            FrmCariBarang frmCariBarang = new FrmCariBarang(this);
            frmCariBarang.ShowDialog();
            txtKodeBarang.Focus();
        }

        private void btnCariSupplier_Click(object sender, EventArgs e)
        {
            FrmCariSatuan frmCariSatuan = new FrmCariSatuan(this, false);
            frmCariSatuan.ShowDialog();
            txtKodeSatuan.Focus();
        }

        private void txtKodeBarang_Leave(object sender, EventArgs e)
        {
            if (txtKodeBarang.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeBarang, txtNamaItem, DBABarang.Instance, "nama");
            if (message == "success")
            {
                this.id_barang = int.Parse(DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id").ToString());
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
               // MessageBox.Show(this, "Kode barang tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data barang tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.id_barang = -1;
            this.Cursor = Cursors.Default;
        }

        private void txtKodeSatuan_Leave(object sender, EventArgs e)
        {
            if (txtKodeSatuan.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeSatuan, txtNamaSatuan, DBASatuan.Instance, "nama");
            if (message == "success")
            {
                if (txtNamaSatuan.Text.ToLower() == "root")
                {
                    MessageBox.Show(this, "Kode satuan root hanya boleh digunakan oleh satuan parent.", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtNamaSatuan.Text = "";
                    txtKodeSatuan.Text = "";
                    txtKodeSatuan.Focus();
                    this.Cursor = Cursors.Default;
                    return;
                }
                this.id_satuan = int.Parse(DBASatuan.Instance.GetSingleColumn(txtKodeSatuan.Text, "id").ToString());
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
               // MessageBox.Show(this, "Kode satuan tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data satuan tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.id_satuan = -1;
            this.Cursor = Cursors.Default;
        }

        private void txtKodeSatuanParent_Leave(object sender, EventArgs e)
        {
            if (txtKodeSatuanParent.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeSatuanParent, txtNamaParent, DBASatuan.Instance, "nama");
            if (message == "success")
            {
                this.id_parent = int.Parse(DBASatuan.Instance.GetSingleColumn(txtKodeSatuanParent.Text, "id").ToString());
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
              //  MessageBox.Show(this, "Kode satuan parent tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data satuan parent tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.id_parent = -1;
            this.Cursor = Cursors.Default;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan memasukkan data ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            if (IsNull())
            {
                MessageBox.Show(this, "Data belum lengkap!", "Inputan tidak lengkap.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int idBarang = (int)DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text.Replace("'", "''"), "id");
            int idSatuan = (int)DBASatuan.Instance.GetSingleColumn(txtKodeSatuan.Text.Replace("'", "''"), "id");
            int idSatuanParent = (int)DBASatuan.Instance.GetSingleColumn(txtKodeSatuanParent.Text.Replace("'", "''"), "id");

            string message = DBABarangDetail.Instance.Insert(idBarang, idSatuan, idSatuanParent, ntRetailStok.Value, ntMinimumStok.Value, ntHargaJual.Value);
            if (message == "success")
            {
                MessageBox.Show(this, "Data produk sudah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterBarang.InsertProduk(idBarang, txtKodeBarang.Text, txtNamaItem.Text, idSatuan, txtKodeSatuan.Text, idSatuanParent, txtKodeSatuanParent.Text,
                    ntRetailStok.Value, ntHargaJual.Value, ntMinimumStok.Value);
                ClearInput();
            }
            else if(message == "kode already exist")
            {
                MessageBox.Show(this, "Data barang untuk satuan ini sudah terdaftar di database. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if(message == "parent not found")
            {
                MessageBox.Show(this, "Parent produk tidak ditemukan dalam database.", "Parent not found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IsNull()
        {
            return this.id_barang == -1 || this.id_satuan == -1 || this.id_parent == -1;
        }

        private void ClearInput()
        {
            txtKodeBarang.Text = "";
            txtNamaItem.Text = "";
            txtKodeSatuan.Text = "";
            txtNamaSatuan.Text = "";
            txtKodeSatuanParent.Text = "";
            txtNamaParent.Text = "";
            this.id_barang = -1;
            this.id_satuan = -1;
            this.id_parent = -1;
            this.Focus();
        }

        private void FrmInputProduk_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if(c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
        }

        private void btnSatuanParent_Click(object sender, EventArgs e)
        {
            FrmCariSatuan frmCariParent = new FrmCariSatuan(this, true);
            frmCariParent.ShowDialog();
            txtKodeSatuanParent.Focus();
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
