﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputBarang : Form
    {
        FrmMasterBarang frmMasterBarang;
        public FrmInputBarang(FrmMasterBarang _frmMasterBarang)
        {
            InitializeComponent();
            frmMasterBarang = _frmMasterBarang;
        }

        private void FrmInputBarang_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmInputBarang_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsNullInput())
            {
                if (MessageBox.Show(this, "Anda yakin akan membatalkan inputan ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private bool IsNullInput()
        {
            return string.IsNullOrEmpty(txtKodeBarang.Text);
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menginputkan data ini?", "Konfirmasi", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            if (string.IsNullOrEmpty(txtKodeBarang.Text))
            {
                MessageBox.Show(this,"Kode barang tidak boleh kosong.","Pemberitahuan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtKodeBarang.Focus();
                return;
            }
            string message = DBABarang.Instance.Insert(txtKodeBarang.Text.Replace("'", "''"), txtNama.Text.Replace("'", "''"));
            if (message.Split(',')[0].ToString() == "success")
            {
                MessageBox.Show(this, "Data barang sudah disimpan.", "Simpan Barang", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterBarang.InsertDataTitle(message.Split(',')[1].ToString(), txtKodeBarang.Text, txtNama.Text);
                ClearInput();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Kode sudah dimiliki barang lain. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKodeBarang.Text = "";
            txtNama.Text = "";
            txtKodeBarang.Focus();
        }
    }
}
