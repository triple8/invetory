﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputUser : Form
    {
        FrmMasterUser frmMasterUser;
        public FrmInputUser(FrmMasterUser _frmMasterUser)
        {
            InitializeComponent();
            frmMasterUser = _frmMasterUser;
        }

        private void ClearCheckboxPegawai(bool state)
        {
            chkCetakLaporan.Enabled = state;
            chkMaintenanceMaster.Enabled = state;
            chkPembelian.Enabled = state;
            chkPengaturan.Enabled = state;
            chkPenjualan.Enabled = state;
            chkPenyesuaian.Enabled = state;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan menginputkan data?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                System.Windows.Forms.DialogResult.No) return;
            InsertUser();
        }

        private void InsertUser()
        {
            string hakAksesUser = GetAkses();
            if (IsNullInput(hakAksesUser))
            {
                MessageBox.Show(this,"Username, password dan hak akses tidak boleh kosong.","Pemberitahuan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtUsername.Focus();
                return;
            }
            string message = DBAUser.Instance.Insert(txtUsername.Text, txtPassword.Text, txtNama.Text, txtAlamat.Text, txtTelp.Text, hakAksesUser);
            if (message == "success")
            {
                MessageBox.Show(this, "Data user sudah disimpan.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterUser.InsertUser(txtUsername.Text, txtPassword.Text, txtNama.Text, txtAlamat.Text, txtTelp.Text, hakAksesUser);
                ClearInput();
            }
            else if (message == "username already exists")
            {
                MessageBox.Show(this, "Username sudah digunakan oleh user lain. Mohon diperiksa kembali.", "Duplicate username", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtUsername.Focus();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        bool IsNullInput(string _hakAksesUser)
        {
            return string.IsNullOrEmpty(txtUsername.Text) || string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrEmpty(_hakAksesUser);
        }

        private void ClearInput()
        {
            txtAlamat.Text = "";
            txtNama.Text = "";
            txtPassword.Text = "";
            txtTelp.Text = "";
            txtUsername.Text = "";

            rdoAdministrator.Checked = false;

            rdoPegawai.Checked = false;

            ClearCheckboxPegawai(false);

            txtUsername.Focus();
        }

        private string GetAkses()
        {
            string hakAkses="";
            if (rdoAdministrator.Checked)
                hakAkses = "Administrator";
            else
            {
                if (rdoPegawai.Checked)
                {
                    if (chkCetakLaporan.Checked) hakAkses = (hakAkses == "") ? chkCetakLaporan.Text : hakAkses + ", " + chkCetakLaporan.Text;
                    if (chkMaintenanceMaster.Checked) hakAkses = (hakAkses == "") ? chkMaintenanceMaster.Text : hakAkses + ", " + chkMaintenanceMaster.Text;
                    if (chkPembelian.Checked) hakAkses = (hakAkses == "") ? chkPembelian.Text : hakAkses + ", " + chkPembelian.Text;
                    if (chkPengaturan.Checked) hakAkses = (hakAkses == "") ? chkPengaturan.Text : hakAkses + ", " + chkPengaturan.Text;
                    if (chkPenjualan.Checked) hakAkses = (hakAkses == "") ? chkPenjualan.Text : hakAkses + ", " + chkPenjualan.Text;
                    if (chkPenyesuaian.Checked) hakAkses = (hakAkses == "") ? chkPenyesuaian.Text : hakAkses + ", " + chkPenyesuaian.Text;
                }
            }
            return hakAkses;
        }

        private void rdoPegawai_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoAdministrator.Checked)
                ClearCheckboxPegawai(false);
            else
                ClearCheckboxPegawai(true);
        }

        private void FrmInputUser_Load(object sender, EventArgs e)
        {
            //foreach (Control c in this.Controls)
            //{
            //    if (c is TextBox)
            //    {
            //       if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
            //    }
            //} 
            ClearCheckboxPegawai(false);
            ClearInput();
        }

        private void rdoAdministrator_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoAdministrator.Checked)
                ClearCheckboxPegawai(false);
            else
                ClearCheckboxPegawai(true);
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
