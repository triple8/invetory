﻿namespace WarSyM_Management_Data.Forms.Master.Input
{
    partial class FrmInputProduk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInputProduk));
            this.txtNamaItem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCariBarang = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNamaSatuan = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCariSatuan = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.txtNamaParent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSatuanParent = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ntRetailStok = new System.Windows.Forms.NumericUpDown();
            this.ntMinimumStok = new System.Windows.Forms.NumericUpDown();
            this.ntHargaJual = new System.Windows.Forms.NumericUpDown();
            this.txtKodeBarang = new System.Windows.Forms.TextBox();
            this.txtKodeSatuan = new System.Windows.Forms.TextBox();
            this.txtKodeSatuanParent = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntRetailStok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntMinimumStok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaJual)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNamaItem
            // 
            this.txtNamaItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaItem.Location = new System.Drawing.Point(132, 34);
            this.txtNamaItem.Name = "txtNamaItem";
            this.txtNamaItem.ReadOnly = true;
            this.txtNamaItem.Size = new System.Drawing.Size(222, 21);
            this.txtNamaItem.TabIndex = 2;
            this.txtNamaItem.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 72;
            this.label2.Text = "Nama barang:";
            // 
            // btnCariBarang
            // 
            this.btnCariBarang.Image = ((System.Drawing.Image)(resources.GetObject("btnCariBarang.Image")));
            this.btnCariBarang.Location = new System.Drawing.Point(322, 6);
            this.btnCariBarang.Name = "btnCariBarang";
            this.btnCariBarang.Size = new System.Drawing.Size(33, 23);
            this.btnCariBarang.TabIndex = 1;
            this.btnCariBarang.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariBarang.UseVisualStyleBackColor = true;
            this.btnCariBarang.Click += new System.EventHandler(this.btnCariItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 15);
            this.label1.TabIndex = 71;
            this.label1.Text = "Kode barang:";
            // 
            // txtNamaSatuan
            // 
            this.txtNamaSatuan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaSatuan.Location = new System.Drawing.Point(132, 89);
            this.txtNamaSatuan.Name = "txtNamaSatuan";
            this.txtNamaSatuan.ReadOnly = true;
            this.txtNamaSatuan.Size = new System.Drawing.Size(222, 21);
            this.txtNamaSatuan.TabIndex = 5;
            this.txtNamaSatuan.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 15);
            this.label5.TabIndex = 77;
            this.label5.Text = "Nama satuan:";
            // 
            // btnCariSatuan
            // 
            this.btnCariSatuan.Image = ((System.Drawing.Image)(resources.GetObject("btnCariSatuan.Image")));
            this.btnCariSatuan.Location = new System.Drawing.Point(322, 60);
            this.btnCariSatuan.Name = "btnCariSatuan";
            this.btnCariSatuan.Size = new System.Drawing.Size(33, 23);
            this.btnCariSatuan.TabIndex = 4;
            this.btnCariSatuan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariSatuan.UseVisualStyleBackColor = true;
            this.btnCariSatuan.Click += new System.EventHandler(this.btnCariSupplier_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 15);
            this.label6.TabIndex = 76;
            this.label6.Text = "Kode satuan:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 80;
            this.label4.Text = "Harga jual:";
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(9, 254);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(345, 5);
            this.radSeparator1.TabIndex = 82;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Location = new System.Drawing.Point(249, 265);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(106, 31);
            this.btnBatal.TabIndex = 13;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(132, 265);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(106, 31);
            this.btnSimpan.TabIndex = 12;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // txtNamaParent
            // 
            this.txtNamaParent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaParent.Location = new System.Drawing.Point(132, 144);
            this.txtNamaParent.Name = "txtNamaParent";
            this.txtNamaParent.ReadOnly = true;
            this.txtNamaParent.Size = new System.Drawing.Size(222, 21);
            this.txtNamaParent.TabIndex = 8;
            this.txtNamaParent.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(42, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 89;
            this.label7.Text = "Nama satuan:";
            // 
            // btnSatuanParent
            // 
            this.btnSatuanParent.Image = ((System.Drawing.Image)(resources.GetObject("btnSatuanParent.Image")));
            this.btnSatuanParent.Location = new System.Drawing.Point(322, 115);
            this.btnSatuanParent.Name = "btnSatuanParent";
            this.btnSatuanParent.Size = new System.Drawing.Size(33, 23);
            this.btnSatuanParent.TabIndex = 7;
            this.btnSatuanParent.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSatuanParent.UseVisualStyleBackColor = true;
            this.btnSatuanParent.Click += new System.EventHandler(this.btnSatuanParent_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 15);
            this.label8.TabIndex = 88;
            this.label8.Text = "Kode parent satuan:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 203);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 15);
            this.label3.TabIndex = 78;
            this.label3.Text = "Minimum stok:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(59, 172);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 90;
            this.label9.Text = "Retail stok:";
            // 
            // ntRetailStok
            // 
            this.ntRetailStok.DecimalPlaces = 2;
            this.ntRetailStok.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntRetailStok.Location = new System.Drawing.Point(132, 172);
            this.ntRetailStok.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntRetailStok.Name = "ntRetailStok";
            this.ntRetailStok.Size = new System.Drawing.Size(223, 21);
            this.ntRetailStok.TabIndex = 9;
            this.ntRetailStok.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntRetailStok.ThousandsSeparator = true;
            // 
            // ntMinimumStok
            // 
            this.ntMinimumStok.DecimalPlaces = 2;
            this.ntMinimumStok.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntMinimumStok.Location = new System.Drawing.Point(132, 200);
            this.ntMinimumStok.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntMinimumStok.Name = "ntMinimumStok";
            this.ntMinimumStok.Size = new System.Drawing.Size(223, 21);
            this.ntMinimumStok.TabIndex = 10;
            this.ntMinimumStok.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntMinimumStok.ThousandsSeparator = true;
            // 
            // ntHargaJual
            // 
            this.ntHargaJual.DecimalPlaces = 2;
            this.ntHargaJual.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntHargaJual.Location = new System.Drawing.Point(132, 228);
            this.ntHargaJual.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntHargaJual.Name = "ntHargaJual";
            this.ntHargaJual.Size = new System.Drawing.Size(223, 21);
            this.ntHargaJual.TabIndex = 11;
            this.ntHargaJual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntHargaJual.ThousandsSeparator = true;
            // 
            // txtKodeBarang
            // 
            this.txtKodeBarang.Location = new System.Drawing.Point(132, 7);
            this.txtKodeBarang.MaxLength = 50;
            this.txtKodeBarang.Name = "txtKodeBarang";
            this.txtKodeBarang.Size = new System.Drawing.Size(184, 21);
            this.txtKodeBarang.TabIndex = 0;
            this.txtKodeBarang.Leave += new System.EventHandler(this.txtKodeBarang_Leave);
            // 
            // txtKodeSatuan
            // 
            this.txtKodeSatuan.Location = new System.Drawing.Point(132, 61);
            this.txtKodeSatuan.MaxLength = 50;
            this.txtKodeSatuan.Name = "txtKodeSatuan";
            this.txtKodeSatuan.Size = new System.Drawing.Size(184, 21);
            this.txtKodeSatuan.TabIndex = 3;
            this.txtKodeSatuan.Leave += new System.EventHandler(this.txtKodeSatuan_Leave);
            // 
            // txtKodeSatuanParent
            // 
            this.txtKodeSatuanParent.Location = new System.Drawing.Point(132, 116);
            this.txtKodeSatuanParent.MaxLength = 50;
            this.txtKodeSatuanParent.Name = "txtKodeSatuanParent";
            this.txtKodeSatuanParent.Size = new System.Drawing.Size(184, 21);
            this.txtKodeSatuanParent.TabIndex = 6;
            this.txtKodeSatuanParent.Leave += new System.EventHandler(this.txtKodeSatuanParent_Leave);
            // 
            // FrmInputProduk
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(364, 304);
            this.Controls.Add(this.txtKodeSatuanParent);
            this.Controls.Add(this.txtKodeSatuan);
            this.Controls.Add(this.txtKodeBarang);
            this.Controls.Add(this.ntHargaJual);
            this.Controls.Add(this.ntMinimumStok);
            this.Controls.Add(this.ntRetailStok);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtNamaParent);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnSatuanParent);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNamaSatuan);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCariSatuan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNamaItem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCariBarang);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmInputProduk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tambah Produk";
            this.Load += new System.EventHandler(this.FrmInputProduk_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntRetailStok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntMinimumStok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHargaJual)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtNamaItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCariBarang;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtNamaSatuan;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCariSatuan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnSimpan;
        public System.Windows.Forms.TextBox txtNamaParent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSatuanParent;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown ntRetailStok;
        private System.Windows.Forms.NumericUpDown ntMinimumStok;
        private System.Windows.Forms.NumericUpDown ntHargaJual;
        public System.Windows.Forms.TextBox txtKodeBarang;
        public System.Windows.Forms.TextBox txtKodeSatuan;
        public System.Windows.Forms.TextBox txtKodeSatuanParent;


    }
}