﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputSatuan : Form
    {
        FrmMasterBarang frmMasterBarang;
        public FrmInputSatuan(FrmMasterBarang _frmMasterBarang)
        {
            InitializeComponent();
            this.frmMasterBarang = _frmMasterBarang;
        }

        private void FrmInputSatuan_Load(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }//ClearInput();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan memasukkan data ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            string message = DBASatuan.Instance.Insert(txtKode.Text.Replace("'", "''"), txtNama.Text.Replace("'", "''"),
                    txtKeterangan.Text.Replace("'", "''"));
            if (message.Split(',')[0].ToString() == "success")
            {
                MessageBox.Show(this, "Data satuan sudah disimpan.", "Simpan satuan", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterBarang.InsertSatuan(message.Split(',')[1].ToString(), txtKode.Text, txtNama.Text, txtKeterangan.Text);
                ClearInput();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Kode sudah dimiliki satuan lain. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKode.Text = "";
            txtNama.Text = "";
            txtKeterangan.Text = "";
            txtKode.Focus();
        }

        private void FrmInputSatuan_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (!IsNullInput())
            //{
            //    if (MessageBox.Show(this, "Anda yakin akan membatalkan inputan ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
            //    {
            //        e.Cancel = true;
            //    }
            //}
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //private bool IsNullInput()
        //{
        //    //return false;
        //}
    }
}
