﻿namespace WarSyM_Management_Data.Forms.Master.Input
{
    partial class FrmInputMenuDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInputMenuDetail));
            this.cboKodeSatuan = new System.Windows.Forms.ComboBox();
            this.txtKodeBarang = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNamaSatuan = new System.Windows.Forms.TextBox();
            this.radSeparator2 = new Telerik.WinControls.UI.RadSeparator();
            this.ntJumlah = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNamaItem = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCariItem = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntJumlah)).BeginInit();
            this.SuspendLayout();
            // 
            // cboKodeSatuan
            // 
            this.cboKodeSatuan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKodeSatuan.FormattingEnabled = true;
            this.cboKodeSatuan.Location = new System.Drawing.Point(109, 67);
            this.cboKodeSatuan.Name = "cboKodeSatuan";
            this.cboKodeSatuan.Size = new System.Drawing.Size(196, 23);
            this.cboKodeSatuan.TabIndex = 104;
            this.cboKodeSatuan.SelectedIndexChanged += new System.EventHandler(this.cboKodeSatuan_SelectedIndexChanged);
            // 
            // txtKodeBarang
            // 
            this.txtKodeBarang.Location = new System.Drawing.Point(109, 8);
            this.txtKodeBarang.MaxLength = 50;
            this.txtKodeBarang.Name = "txtKodeBarang";
            this.txtKodeBarang.Size = new System.Drawing.Size(151, 21);
            this.txtKodeBarang.TabIndex = 92;
            this.txtKodeBarang.Leave += new System.EventHandler(this.txtKodeBarang_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 15);
            this.label8.TabIndex = 103;
            this.label8.Text = "Nama satuan:";
            // 
            // txtNamaSatuan
            // 
            this.txtNamaSatuan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaSatuan.Location = new System.Drawing.Point(109, 98);
            this.txtNamaSatuan.Name = "txtNamaSatuan";
            this.txtNamaSatuan.ReadOnly = true;
            this.txtNamaSatuan.Size = new System.Drawing.Size(196, 21);
            this.txtNamaSatuan.TabIndex = 96;
            this.txtNamaSatuan.TabStop = false;
            // 
            // radSeparator2
            // 
            this.radSeparator2.Location = new System.Drawing.Point(12, 155);
            this.radSeparator2.Name = "radSeparator2";
            this.radSeparator2.Size = new System.Drawing.Size(294, 4);
            this.radSeparator2.TabIndex = 102;
            this.radSeparator2.Text = "radSeparator2";
            // 
            // ntJumlah
            // 
            this.ntJumlah.DecimalPlaces = 2;
            this.ntJumlah.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntJumlah.Location = new System.Drawing.Point(109, 128);
            this.ntJumlah.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntJumlah.Name = "ntJumlah";
            this.ntJumlah.Size = new System.Drawing.Size(197, 21);
            this.ntJumlah.TabIndex = 97;
            this.ntJumlah.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntJumlah.ThousandsSeparator = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 15);
            this.label4.TabIndex = 101;
            this.label4.Text = "Jumlah:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 15);
            this.label6.TabIndex = 100;
            this.label6.Text = "Kode satuan:";
            // 
            // txtNamaItem
            // 
            this.txtNamaItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaItem.Location = new System.Drawing.Point(109, 37);
            this.txtNamaItem.Name = "txtNamaItem";
            this.txtNamaItem.ReadOnly = true;
            this.txtNamaItem.Size = new System.Drawing.Size(196, 21);
            this.txtNamaItem.TabIndex = 94;
            this.txtNamaItem.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 15);
            this.label5.TabIndex = 99;
            this.label5.Text = "Nama barang:";
            // 
            // btnCariItem
            // 
            this.btnCariItem.Image = ((System.Drawing.Image)(resources.GetObject("btnCariItem.Image")));
            this.btnCariItem.Location = new System.Drawing.Point(268, 8);
            this.btnCariItem.Name = "btnCariItem";
            this.btnCariItem.Size = new System.Drawing.Size(38, 21);
            this.btnCariItem.TabIndex = 93;
            this.btnCariItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariItem.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 15);
            this.label7.TabIndex = 98;
            this.label7.Text = "Kode barang:";
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(109, 165);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(89, 32);
            this.btnSimpan.TabIndex = 105;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Location = new System.Drawing.Point(217, 165);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(89, 32);
            this.btnBatal.TabIndex = 106;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // FrmInputMenuDetail
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(316, 205);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.cboKodeSatuan);
            this.Controls.Add(this.txtKodeBarang);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtNamaSatuan);
            this.Controls.Add(this.radSeparator2);
            this.Controls.Add(this.ntJumlah);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNamaItem);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCariItem);
            this.Controls.Add(this.label7);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmInputMenuDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmInputMenuDetail";
            this.Load += new System.EventHandler(this.FrmInputMenuDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntJumlah)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboKodeSatuan;
        public System.Windows.Forms.TextBox txtKodeBarang;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtNamaSatuan;
        private Telerik.WinControls.UI.RadSeparator radSeparator2;
        private System.Windows.Forms.NumericUpDown ntJumlah;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtNamaItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCariItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
    }
}