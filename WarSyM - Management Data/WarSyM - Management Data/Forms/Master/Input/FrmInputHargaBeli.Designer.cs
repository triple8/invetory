﻿namespace WarSyM_Management_Data.Forms.Master.Input
{
    partial class FrmInputHargaBeli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInputHargaBeli));
            this.txtNamaSupplier = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCariSupplier = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaItem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCariItem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNamaSatuan = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ntHarga = new System.Windows.Forms.NumericUpDown();
            this.txtKodeBarang = new System.Windows.Forms.TextBox();
            this.txtKodeSupplier = new System.Windows.Forms.TextBox();
            this.cboKodeSatuan = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHarga)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNamaSupplier
            // 
            this.txtNamaSupplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaSupplier.Location = new System.Drawing.Point(105, 154);
            this.txtNamaSupplier.Name = "txtNamaSupplier";
            this.txtNamaSupplier.ReadOnly = true;
            this.txtNamaSupplier.Size = new System.Drawing.Size(222, 21);
            this.txtNamaSupplier.TabIndex = 7;
            this.txtNamaSupplier.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 15);
            this.label5.TabIndex = 72;
            this.label5.Text = "Nama supplier:";
            // 
            // btnCariSupplier
            // 
            this.btnCariSupplier.Image = ((System.Drawing.Image)(resources.GetObject("btnCariSupplier.Image")));
            this.btnCariSupplier.Location = new System.Drawing.Point(295, 124);
            this.btnCariSupplier.Name = "btnCariSupplier";
            this.btnCariSupplier.Size = new System.Drawing.Size(33, 23);
            this.btnCariSupplier.TabIndex = 6;
            this.btnCariSupplier.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariSupplier.UseVisualStyleBackColor = true;
            this.btnCariSupplier.Click += new System.EventHandler(this.btnCariSupplier_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 15);
            this.label6.TabIndex = 71;
            this.label6.Text = "Kode supplier:";
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Image = ((System.Drawing.Image)(resources.GetObject("btnBatal.Image")));
            this.btnBatal.Location = new System.Drawing.Point(224, 221);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(104, 31);
            this.btnBatal.TabIndex = 10;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Image = ((System.Drawing.Image)(resources.GetObject("btnSimpan.Image")));
            this.btnSimpan.Location = new System.Drawing.Point(105, 221);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(104, 31);
            this.btnSimpan.TabIndex = 9;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(11, 210);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(315, 5);
            this.radSeparator1.TabIndex = 70;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 15);
            this.label3.TabIndex = 68;
            this.label3.Text = "Harga Beli:";
            // 
            // txtNamaItem
            // 
            this.txtNamaItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaItem.Location = new System.Drawing.Point(105, 38);
            this.txtNamaItem.Name = "txtNamaItem";
            this.txtNamaItem.ReadOnly = true;
            this.txtNamaItem.Size = new System.Drawing.Size(222, 21);
            this.txtNamaItem.TabIndex = 2;
            this.txtNamaItem.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 67;
            this.label2.Text = "Nama barang:";
            // 
            // btnCariItem
            // 
            this.btnCariItem.Image = ((System.Drawing.Image)(resources.GetObject("btnCariItem.Image")));
            this.btnCariItem.Location = new System.Drawing.Point(295, 9);
            this.btnCariItem.Name = "btnCariItem";
            this.btnCariItem.Size = new System.Drawing.Size(33, 23);
            this.btnCariItem.TabIndex = 1;
            this.btnCariItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCariItem.UseVisualStyleBackColor = true;
            this.btnCariItem.Click += new System.EventHandler(this.btnCariItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 15);
            this.label1.TabIndex = 66;
            this.label1.Text = "Kode barang:";
            // 
            // txtNamaSatuan
            // 
            this.txtNamaSatuan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaSatuan.Location = new System.Drawing.Point(105, 95);
            this.txtNamaSatuan.Name = "txtNamaSatuan";
            this.txtNamaSatuan.ReadOnly = true;
            this.txtNamaSatuan.Size = new System.Drawing.Size(222, 21);
            this.txtNamaSatuan.TabIndex = 4;
            this.txtNamaSatuan.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 15);
            this.label4.TabIndex = 82;
            this.label4.Text = "Nama satuan:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 15);
            this.label7.TabIndex = 81;
            this.label7.Text = "Kode satuan:";
            // 
            // ntHarga
            // 
            this.ntHarga.DecimalPlaces = 2;
            this.ntHarga.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntHarga.Location = new System.Drawing.Point(105, 183);
            this.ntHarga.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntHarga.Name = "ntHarga";
            this.ntHarga.Size = new System.Drawing.Size(222, 21);
            this.ntHarga.TabIndex = 8;
            this.ntHarga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtKodeBarang
            // 
            this.txtKodeBarang.Location = new System.Drawing.Point(105, 10);
            this.txtKodeBarang.MaxLength = 50;
            this.txtKodeBarang.Name = "txtKodeBarang";
            this.txtKodeBarang.Size = new System.Drawing.Size(184, 21);
            this.txtKodeBarang.TabIndex = 0;
            this.txtKodeBarang.Leave += new System.EventHandler(this.txtKodeBarang_Leave);
            // 
            // txtKodeSupplier
            // 
            this.txtKodeSupplier.Location = new System.Drawing.Point(105, 125);
            this.txtKodeSupplier.MaxLength = 50;
            this.txtKodeSupplier.Name = "txtKodeSupplier";
            this.txtKodeSupplier.Size = new System.Drawing.Size(184, 21);
            this.txtKodeSupplier.TabIndex = 5;
            this.txtKodeSupplier.Leave += new System.EventHandler(this.txtKodeSupplier_Leave);
            // 
            // cboKodeSatuan
            // 
            this.cboKodeSatuan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKodeSatuan.FormattingEnabled = true;
            this.cboKodeSatuan.Location = new System.Drawing.Point(105, 65);
            this.cboKodeSatuan.Name = "cboKodeSatuan";
            this.cboKodeSatuan.Size = new System.Drawing.Size(222, 23);
            this.cboKodeSatuan.TabIndex = 3;
            this.cboKodeSatuan.SelectedIndexChanged += new System.EventHandler(this.cboKodeSatuan_SelectedIndexChanged);
            // 
            // FrmInputHargaBeli
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(337, 260);
            this.Controls.Add(this.cboKodeSatuan);
            this.Controls.Add(this.txtKodeSupplier);
            this.Controls.Add(this.txtKodeBarang);
            this.Controls.Add(this.ntHarga);
            this.Controls.Add(this.txtNamaSatuan);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNamaSupplier);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCariSupplier);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNamaItem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCariItem);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmInputHargaBeli";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Input Harga Beli";
            this.Load += new System.EventHandler(this.FrmInputHargaBeli_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntHarga)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCariSupplier;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnSimpan;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCariItem;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtNamaSupplier;
        public System.Windows.Forms.TextBox txtNamaItem;
        public System.Windows.Forms.TextBox txtNamaSatuan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown ntHarga;
        public System.Windows.Forms.TextBox txtKodeBarang;
        public System.Windows.Forms.TextBox txtKodeSupplier;
        private System.Windows.Forms.ComboBox cboKodeSatuan;
    }
}