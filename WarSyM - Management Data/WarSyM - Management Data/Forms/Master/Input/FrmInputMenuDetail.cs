﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Detail;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputMenuDetail : Form
    {
        FrmMenuDetail frmMenuDetail;

        int idBarang, idMenu;
        string kodeMenu;
        
        public FrmInputMenuDetail(FrmMenuDetail _frmMenuDetail, int _idMenu, string _kodeMenu)
        {
            InitializeComponent();
            this.frmMenuDetail = _frmMenuDetail;
            this.idMenu = _idMenu;
            this.kodeMenu = _kodeMenu;
        }

        private void txtKodeBarang_Leave(object sender, EventArgs e)
        {
            if (txtKodeBarang.ReadOnly) return;
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(txtKodeBarang, txtNamaItem, DBABarang.Instance, "nama");
            if (message == "success")
            {
                this.idBarang = (int)DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id");
                ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, " id in(select id_satuan from barangdetail where id_barang='"
                    + DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id") + "') ");
                cboKodeSatuan.Items.RemoveAt(0);
                txtNamaSatuan.Text = "";
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //MessageBox.Show(this, "Kode barang tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data barang tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            cboKodeSatuan.Items.Clear();
            txtNamaSatuan.Text = "";
            txtNamaItem.Text = "";
            this.idBarang = -1;
            this.Cursor = Cursors.Default;
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string message = ControlBinding.BindingSingleColumn(cboKodeSatuan, txtNamaSatuan, DBASatuan.Instance, "nama");
            if (message == "success")
            {
                txtNamaSatuan.Text = (string)DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "nama");
                this.Cursor = Cursors.Default;
                return;
            }
            else if (message == "empty text")
            {
                //  MessageBox.Show(this, "Kode supplier tidak boleh kosong", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (message == "There is no row at position 0.")
            {
                MessageBox.Show(this, "Data satuan tidak ditemukan", "Data not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            txtNamaSatuan.Text = "";
            this.Cursor = Cursors.Default;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (IsNullInput())
            {
                MessageBox.Show(this, "Data tidak lengkap. Mohon periksa kembali", "Pemberitahuan", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtKodeBarang.Focus();
                return;
            }
            string message = DBAMenuDetail.Instance.Insert(this.idMenu, this.idBarang, DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id"), ntJumlah.Value);
            if (message == "success")
            {
                MessageBox.Show(this, "Data detail menu sudah disimpan.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMenuDetail.InsertDetail(this.idMenu, this.kodeMenu, this.idBarang, DBABarang.Instance.GetSingleColumn(this.idBarang, "kode"),
                    DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id"), cboKodeSatuan.Text, ntJumlah.Value);
                ClearInput();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Data detail menu sudah ada. Mohon diperiksa kembali.", "Duplicate username", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtKodeBarang.Focus();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKodeBarang.Text = "";
            txtNamaItem.Text = "";
            txtNamaSatuan.Text = "";
            cboKodeSatuan.Items.Clear();
            ntJumlah.Value = 0;
        }

        private bool IsNullInput()
        {
            return this.idBarang == -1 || string.IsNullOrEmpty(cboKodeSatuan.Text) || ntJumlah.Value == 0;
        }

        private void FrmInputMenuDetail_Load(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
