﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.Input
{
    public partial class FrmInputToko : Form
    {
        FrmMasterToko frmMasterToko;
        public FrmInputToko(FrmMasterToko _frmMasterToko)
        {
            InitializeComponent();
            this.frmMasterToko = _frmMasterToko;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan memasukkan data ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            string message = DBAToko.Instance.Insert(txtKode.Text.Replace("'", "''"), txtNama.Text.Replace("'", "''"),
                    txtAlamat.Text.Replace("'", "''"), txtTelp.Text.Replace("'", "''"));
            if (message.Split(',')[0] == "success")
            {
                MessageBox.Show(this, "Data toko sudah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterToko.InsertToko(message.Split(',')[1], txtKode.Text, txtNama.Text, txtAlamat.Text, txtTelp.Text);
                ClearInput();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Kode sudah dimiliki toko lain. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKode.Text = "";
            txtNama.Text = "";
            txtAlamat.Text = "";
            txtTelp.Text = "";
            txtKode.Focus();
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmInputToko_Load(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }
    }
}
