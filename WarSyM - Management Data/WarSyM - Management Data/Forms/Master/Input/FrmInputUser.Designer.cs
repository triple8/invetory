﻿namespace WarSyM_Management_Data.Forms.Master.Input
{
    partial class FrmInputUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInputUser));
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.radSeparator2 = new Telerik.WinControls.UI.RadSeparator();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtTelp = new System.Windows.Forms.TextBox();
            this.chkPenyesuaian = new System.Windows.Forms.CheckBox();
            this.chkMaintenanceMaster = new System.Windows.Forms.CheckBox();
            this.chkPengaturan = new System.Windows.Forms.CheckBox();
            this.txtNama = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkCetakLaporan = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkPenjualan = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkPembelian = new System.Windows.Forms.CheckBox();
            this.txtAlamat = new System.Windows.Forms.TextBox();
            this.rdoPegawai = new System.Windows.Forms.RadioButton();
            this.rdoAdministrator = new System.Windows.Forms.RadioButton();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.radSeparator3 = new Telerik.WinControls.UI.RadSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Image = ((System.Drawing.Image)(resources.GetObject("btnBatal.Image")));
            this.btnBatal.Location = new System.Drawing.Point(197, 435);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(92, 32);
            this.btnBatal.TabIndex = 14;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Image = ((System.Drawing.Image)(resources.GetObject("btnSimpan.Image")));
            this.btnSimpan.Location = new System.Drawing.Point(88, 435);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(92, 32);
            this.btnSimpan.TabIndex = 13;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // radSeparator2
            // 
            this.radSeparator2.Location = new System.Drawing.Point(11, 281);
            this.radSeparator2.Name = "radSeparator2";
            this.radSeparator2.Size = new System.Drawing.Size(275, 5);
            this.radSeparator2.TabIndex = 45;
            this.radSeparator2.Text = "radSeparator2";
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(11, 424);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(275, 5);
            this.radSeparator1.TabIndex = 44;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(86, 35);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(201, 21);
            this.txtPassword.TabIndex = 1;
            // 
            // txtTelp
            // 
            this.txtTelp.Location = new System.Drawing.Point(88, 397);
            this.txtTelp.Name = "txtTelp";
            this.txtTelp.Size = new System.Drawing.Size(201, 21);
            this.txtTelp.TabIndex = 12;
            // 
            // chkPenyesuaian
            // 
            this.chkPenyesuaian.AutoSize = true;
            this.chkPenyesuaian.Location = new System.Drawing.Point(110, 206);
            this.chkPenyesuaian.Name = "chkPenyesuaian";
            this.chkPenyesuaian.Size = new System.Drawing.Size(161, 19);
            this.chkPenyesuaian.TabIndex = 7;
            this.chkPenyesuaian.Text = "Penyesuaian Stok/Harga";
            this.chkPenyesuaian.UseVisualStyleBackColor = true;
            // 
            // chkMaintenanceMaster
            // 
            this.chkMaintenanceMaster.AutoSize = true;
            this.chkMaintenanceMaster.Location = new System.Drawing.Point(110, 181);
            this.chkMaintenanceMaster.Name = "chkMaintenanceMaster";
            this.chkMaintenanceMaster.Size = new System.Drawing.Size(139, 19);
            this.chkMaintenanceMaster.TabIndex = 6;
            this.chkMaintenanceMaster.Text = "Maintenance Master";
            this.chkMaintenanceMaster.UseVisualStyleBackColor = true;
            // 
            // chkPengaturan
            // 
            this.chkPengaturan.AutoSize = true;
            this.chkPengaturan.Location = new System.Drawing.Point(110, 256);
            this.chkPengaturan.Name = "chkPengaturan";
            this.chkPengaturan.Size = new System.Drawing.Size(135, 19);
            this.chkPengaturan.TabIndex = 9;
            this.chkPengaturan.Text = "Pengaturan Aplikasi";
            this.chkPengaturan.UseVisualStyleBackColor = true;
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(88, 293);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(201, 21);
            this.txtNama.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 296);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 15);
            this.label6.TabIndex = 30;
            this.label6.Text = "Nama:";
            // 
            // chkCetakLaporan
            // 
            this.chkCetakLaporan.AutoSize = true;
            this.chkCetakLaporan.Location = new System.Drawing.Point(110, 231);
            this.chkCetakLaporan.Name = "chkCetakLaporan";
            this.chkCetakLaporan.Size = new System.Drawing.Size(152, 19);
            this.chkCetakLaporan.TabIndex = 8;
            this.chkCetakLaporan.Text = "Cetak Laporan / History";
            this.chkCetakLaporan.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 323);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 32;
            this.label5.Text = "Alamat:";
            // 
            // chkPenjualan
            // 
            this.chkPenjualan.AutoSize = true;
            this.chkPenjualan.Location = new System.Drawing.Point(110, 156);
            this.chkPenjualan.Name = "chkPenjualan";
            this.chkPenjualan.Size = new System.Drawing.Size(147, 19);
            this.chkPenjualan.TabIndex = 5;
            this.chkPenjualan.Text = "Penjualan / Retur Jual";
            this.chkPenjualan.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 400);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 15);
            this.label4.TabIndex = 34;
            this.label4.Text = "Telp:";
            // 
            // chkPembelian
            // 
            this.chkPembelian.AutoSize = true;
            this.chkPembelian.Location = new System.Drawing.Point(110, 131);
            this.chkPembelian.Name = "chkPembelian";
            this.chkPembelian.Size = new System.Drawing.Size(149, 19);
            this.chkPembelian.TabIndex = 4;
            this.chkPembelian.Text = "Pembelian / Retur Beli";
            this.chkPembelian.UseVisualStyleBackColor = true;
            // 
            // txtAlamat
            // 
            this.txtAlamat.Location = new System.Drawing.Point(88, 320);
            this.txtAlamat.Multiline = true;
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.Size = new System.Drawing.Size(201, 71);
            this.txtAlamat.TabIndex = 11;
            // 
            // rdoPegawai
            // 
            this.rdoPegawai.AutoSize = true;
            this.rdoPegawai.Location = new System.Drawing.Point(86, 99);
            this.rdoPegawai.Name = "rdoPegawai";
            this.rdoPegawai.Size = new System.Drawing.Size(73, 19);
            this.rdoPegawai.TabIndex = 3;
            this.rdoPegawai.TabStop = true;
            this.rdoPegawai.Text = "Pegawai";
            this.rdoPegawai.UseVisualStyleBackColor = true;
            this.rdoPegawai.CheckedChanged += new System.EventHandler(this.rdoPegawai_CheckedChanged);
            // 
            // rdoAdministrator
            // 
            this.rdoAdministrator.AutoSize = true;
            this.rdoAdministrator.Location = new System.Drawing.Point(86, 73);
            this.rdoAdministrator.Name = "rdoAdministrator";
            this.rdoAdministrator.Size = new System.Drawing.Size(97, 19);
            this.rdoAdministrator.TabIndex = 2;
            this.rdoAdministrator.TabStop = true;
            this.rdoAdministrator.Text = "Administrator";
            this.rdoAdministrator.UseVisualStyleBackColor = true;
            this.rdoAdministrator.CheckedChanged += new System.EventHandler(this.rdoAdministrator_CheckedChanged);
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(86, 8);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(201, 21);
            this.txtUsername.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 28;
            this.label3.Text = "Hak Akses:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 26;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 24;
            this.label1.Text = "Username:";
            // 
            // radSeparator3
            // 
            this.radSeparator3.Location = new System.Drawing.Point(11, 63);
            this.radSeparator3.Name = "radSeparator3";
            this.radSeparator3.Size = new System.Drawing.Size(275, 5);
            this.radSeparator3.TabIndex = 46;
            this.radSeparator3.Text = "radSeparator3";
            // 
            // FrmInputUser
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(299, 475);
            this.Controls.Add(this.radSeparator3);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.radSeparator2);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtTelp);
            this.Controls.Add(this.chkPenyesuaian);
            this.Controls.Add(this.chkMaintenanceMaster);
            this.Controls.Add(this.chkPengaturan);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chkCetakLaporan);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chkPenjualan);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chkPembelian);
            this.Controls.Add(this.txtAlamat);
            this.Controls.Add(this.rdoPegawai);
            this.Controls.Add(this.rdoAdministrator);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmInputUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Input User";
            this.Load += new System.EventHandler(this.FrmInputUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnSimpan;
        private Telerik.WinControls.UI.RadSeparator radSeparator2;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtTelp;
        private System.Windows.Forms.CheckBox chkPenyesuaian;
        private System.Windows.Forms.CheckBox chkMaintenanceMaster;
        private System.Windows.Forms.CheckBox chkPengaturan;
        private System.Windows.Forms.TextBox txtNama;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkCetakLaporan;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkPenjualan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkPembelian;
        private System.Windows.Forms.TextBox txtAlamat;
        private System.Windows.Forms.RadioButton rdoPegawai;
        private System.Windows.Forms.RadioButton rdoAdministrator;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadSeparator radSeparator3;
    }
}