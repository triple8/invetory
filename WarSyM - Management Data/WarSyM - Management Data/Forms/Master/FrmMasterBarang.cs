﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Forms.Master.Input;
using WarSyM_Management_Data.Forms.Master.Ubah;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Penyesuaian;
using WarSyM_Management_Data.Forms.Master.Detail;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master
{
    public partial class FrmMasterBarang : Form
    {
        bool firstLoadBarang, firstLoadMenu, firstLoadSatuan;
        bool initLoad = false;
        public FrmMasterBarang()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmInputBarang frmInputBarang = new FrmInputBarang(this);
            frmInputBarang.ShowDialog();
        }

        private void FrmMasterBarang_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }

            GridSettingTitle();
            GridSettingSatuan();
            GridSettingProduk();
            GridSettingMenu();

            ControlBinding.BindingComboBox(DBABarang.Instance, cboFilterProductKodeBarang, 1, "");
            cboFilterProductKodeBarang.SelectedIndex = 0;

            ControlBinding.BindingComboBox(DBASatuan.Instance, cboFilterProductKodeSatuan, 1, "");
            cboFilterProductKodeSatuan.SelectedIndex = 0;            
            cboFilterProductKodeSatuan.Items.RemoveAt(1);
            initLoad = true;
            dgProduct.Sort(dgProduct.Columns["clmnKodeBarang"], System.ComponentModel.ListSortDirection.Ascending);

            firstLoadBarang = false;
            firstLoadMenu = false;
            firstLoadSatuan = false;
        }

        void GridSettingMenu()
        {
            dgMenu.Columns.Add("clmnId", "ID");
            dgMenu.Columns.Add("clmnKode", "Kode");
            dgMenu.Columns.Add("clmnNama", "Nama");
            dgMenu.Columns.Add("clmnHarga", "Harga");
            dgMenu.Columns["clmnId"].Visible = false;            

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Ubah";
            bcol.Name = "btnUbah";
            bcol.UseColumnTextForButtonValue = true;
            dgMenu.Columns.Add(bcol);

            DataGridViewButtonColumn bcol2 = new DataGridViewButtonColumn();
            bcol2.HeaderText = "";
            bcol2.Text = "Hapus";
            bcol2.Name = "btnHapus";
            bcol2.UseColumnTextForButtonValue = true;
            dgMenu.Columns.Add(bcol2);

            DataGridViewButtonColumn bcol3 = new DataGridViewButtonColumn();
            bcol3.HeaderText = "";
            bcol3.Text = "Penyesuaian Harga Jual";
            bcol3.Name = "btnHarga";
            bcol3.UseColumnTextForButtonValue = true;
            dgMenu.Columns.Add(bcol3);
            dgMenu.Columns["btnHarga"].Width = 180;

            DataGridViewButtonColumn bcol4 = new DataGridViewButtonColumn();
            bcol4.HeaderText = "";
            bcol4.Text = "Details";
            bcol4.Name = "btnDetails";
            bcol4.UseColumnTextForButtonValue = true;
            dgMenu.Columns.Add(bcol4);

            dgMenu.Columns["clmnHarga"].DefaultCellStyle.Format = "N2";
            dgMenu.Columns["clmnHarga"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dgMenu.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            lblPageMenu.Text = "0";
            //cboDisplayMenu.SelectedIndex = 0;
        }

        void GridSettingProduk()
        {
            dgProduct.Columns.Add("clmnIdBarang", "ID Barang");
            dgProduct.Columns.Add("clmnKodeBarang", "Kode Barang");
            dgProduct.Columns.Add("clmnNamaBarang", "Nama Barang");
            dgProduct.Columns.Add("clmnIdSatuan", "ID Satuan");
            dgProduct.Columns.Add("clmnKodeSatuan", "Kode Satuan");
            dgProduct.Columns.Add("clmnIdSatuanParent", "ID Satuan Parent");
            dgProduct.Columns.Add("clmnKodeSatuanParent", "Kode Satuan Parent");
            dgProduct.Columns.Add("clmnRetailStok", "Retail Stok");            
            dgProduct.Columns.Add("clmnHargaJual", "Harga Jual");
            dgProduct.Columns.Add("clmnMinimumStok", "Minimum Stok");

            dgProduct.Columns["clmnIdBarang"].Visible = false;
            dgProduct.Columns["clmnIdSatuan"].Visible = false;
            dgProduct.Columns["clmnIdSatuanParent"].Visible = false;

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Ubah";
            bcol.Name = "btnUbah";
            bcol.UseColumnTextForButtonValue = true;
            dgProduct.Columns.Add(bcol);

            DataGridViewButtonColumn bcol2 = new DataGridViewButtonColumn();
            bcol2.HeaderText = "";
            bcol2.Text = "Hapus";
            bcol2.Name = "btnHapus";
            bcol2.UseColumnTextForButtonValue = true;
            dgProduct.Columns.Add(bcol2);

            DataGridViewButtonColumn bcol4 = new DataGridViewButtonColumn();
            bcol4.HeaderText = "";
            bcol4.Text = "Penyesuaian Harga Jual";
            bcol4.Name = "btnHargaJual";
            bcol4.UseColumnTextForButtonValue = true;
            dgProduct.Columns.Add(bcol4);
            dgProduct.Columns["btnHargaJual"].Width = 180;


            dgProduct.Columns["clmnRetailStok"].DefaultCellStyle.Format = "N2";
            dgProduct.Columns["clmnRetailStok"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgProduct.Columns["clmnHargaJual"].DefaultCellStyle.Format = "N2";
            dgProduct.Columns["clmnHargaJual"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgProduct.Columns["clmnMinimumStok"].DefaultCellStyle.Format = "N2";
            dgProduct.Columns["clmnMinimumStok"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dgProduct.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            lblPageProduk.Text = "0";
            cboDisplayProduk.SelectedIndex = 0;
        }

        void GridSettingTitle()
        {
            dgTitle.Columns.Add("clmnId", "ID");
            dgTitle.Columns.Add("clmnKode", "Kode");
            dgTitle.Columns.Add("clmnNama", "Nama");
            dgTitle.Columns["clmnId"].Visible = false;

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Ubah";
            bcol.Name = "btnUbah";
            bcol.UseColumnTextForButtonValue = true;
            dgTitle.Columns.Add(bcol);

            DataGridViewButtonColumn bcol2 = new DataGridViewButtonColumn();
            bcol2.HeaderText = "";
            bcol2.Text = "Hapus";
            bcol2.Name = "btnHapus";
            bcol2.UseColumnTextForButtonValue = true;
            dgTitle.Columns.Add(bcol2);

            DataGridViewButtonColumn bcol3 = new DataGridViewButtonColumn();
            bcol3.HeaderText = "";
            bcol3.Text = "Set Satuan";
            bcol3.Name = "btnProduk";
            bcol3.UseColumnTextForButtonValue = true;
            dgTitle.Columns.Add(bcol3);

            dgTitle.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            lblPageBarang.Text = "1";
            //cboDisplayTitle.SelectedIndex = 0;
        }

        void GridSettingSatuan()
        {
            dgSatuan.Columns.Add("clmnId", "ID");
            dgSatuan.Columns.Add("clmnKode", "Kode");
            dgSatuan.Columns.Add("clmnNama", "Nama");
            dgSatuan.Columns.Add("clmnKeterangan", "Keterangan");
            dgSatuan.Columns["clmnId"].Visible = false;

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Ubah";
            bcol.Name = "btnUbah";
            bcol.UseColumnTextForButtonValue = true;
            dgSatuan.Columns.Add(bcol);

            DataGridViewButtonColumn bcol2 = new DataGridViewButtonColumn();
            bcol2.HeaderText = "";
            bcol2.Text = "Hapus";
            bcol2.Name = "btnHapus";
            bcol2.UseColumnTextForButtonValue = true;
            dgSatuan.Columns.Add(bcol2);

            dgSatuan.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            lblPageSatuan.Text = "1";
            //cboDisplaySatuan.SelectedIndex = 0;
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dgTitle, DBABarang.Instance, lblPageBarang, cboDisplayTitle, "");
            if (!firstLoadBarang)
            {
                dgTitle.Sort(dgTitle.Columns["clmnKode"], System.ComponentModel.ListSortDirection.Ascending);
                firstLoadBarang = true;
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dgTitle, DBABarang.Instance, lblPageBarang, cboDisplayTitle, "");
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dgTitle, DBABarang.Instance, lblPageBarang, cboDisplayTitle, "");
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBABarang.Instance.Count(""), dgTitle, DBABarang.Instance, lblPageBarang, cboDisplayTitle, "");
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBABarang.Instance.Count(""), dgTitle, DBABarang.Instance, lblPageBarang, cboDisplayTitle, "");
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dgTitle, "Master Barang");
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            FrmInputSatuan frmInputSatuan = new FrmInputSatuan(this);
            frmInputSatuan.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pagging.First(dgSatuan, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dgSatuan, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBASatuan.Instance.Count(""), dgSatuan, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBASatuan.Instance.Count(""), dgSatuan, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dgSatuan, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
            if (!firstLoadSatuan)
            {
                dgSatuan.Sort(dgSatuan.Columns["clmnKode"], System.ComponentModel.ListSortDirection.Ascending);
                firstLoadSatuan = true;
            }
        }

        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dgSatuan, "Master Satuan");
        }

        private void dgTitle_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (dgTitle.Columns[e.ColumnIndex].Name == "btnUbah")
            {
                FrmUbahBarang frmUbahBarang = new FrmUbahBarang(this, int.Parse(dgTitle[0, e.RowIndex].Value.ToString()), dgTitle[1, e.RowIndex].Value.ToString(), dgTitle[2, e.RowIndex].Value.ToString(), e.RowIndex);
                frmUbahBarang.Text = "Ubah Data Barang :: " + dgTitle[0, e.RowIndex].Value.ToString();
                frmUbahBarang.ShowDialog();
            }
            else if (dgTitle.Columns[e.ColumnIndex].Name == "btnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data barang ini?", "Konfirmasi",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
                HapusBarang(e.RowIndex);
            }
            else if (dgTitle.Columns[e.ColumnIndex].Name == "btnProduk")
            {
                FrmInputProduk frmInputProduk = new FrmInputProduk(this, int.Parse(dgTitle["clmnId", e.RowIndex].Value.ToString()), dgTitle["clmnKode", e.RowIndex].Value.ToString(),
                    dgTitle["clmnNama", e.RowIndex].Value.ToString());
                frmInputProduk.ShowDialog();
            }
        }

        public void UpdateBarang(int row, string kode, string nama)
        {
            dgTitle["clmnKode", row].Value = kode;
            dgTitle["clmnNama", row].Value = nama;
        }

        private void HapusBarang(int row)
        {
            string message = DBABarang.Instance.Delete(int.Parse(dgTitle["clmnId", row].Value.ToString()));

            if (message == "success")
            {
                MessageBox.Show(this, "Data barang telah dihapus.", "Data deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgTitle.Rows.RemoveAt(row);
            }
            else if (message == "barang in used in barangdetail")
                MessageBox.Show(this, "Data barang tidak dapat dihapus. Data barang memiliki koresponden dengan data lain.", "Data not deleted",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not deleted",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void cboDisplayProduk_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dgProduct, DBABarangDetail.Instance, lblPageProduk, cboDisplayProduk, GetFilterProduct());
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBABarangDetail.Instance.Count(GetFilterProduct()), dgProduct, DBABarangDetail.Instance, lblPageProduk, cboDisplayProduk, GetFilterProduct());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Pagging.First(dgProduct, DBABarangDetail.Instance, lblPageProduk, cboDisplayProduk, GetFilterProduct());
        }


        private void button6_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dgProduct, DBABarangDetail.Instance, lblPageProduk, cboDisplayProduk, GetFilterProduct());
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBABarangDetail.Instance.Count(GetFilterProduct()), dgProduct, DBABarangDetail.Instance, lblPageProduk, cboDisplayProduk, GetFilterProduct());
        }

        private void dgSatuan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (dgSatuan.Columns[e.ColumnIndex].Name == "btnUbah")
            {
                if (dgSatuan.Rows.Count == 0) return;
                FrmUbahSatuan frmUbahSatuan = new FrmUbahSatuan(this, int.Parse(dgSatuan["clmnId", e.RowIndex].Value.ToString()), dgSatuan["clmnKode", e.RowIndex].Value.ToString(), dgSatuan["clmnNama", e.RowIndex].Value.ToString(),
                    dgSatuan["clmnKeterangan", e.RowIndex].Value.ToString(), e.RowIndex);
                frmUbahSatuan.ShowDialog();
            }
            else if (dgSatuan.Columns[e.ColumnIndex].Name == "btnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data satuan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                System.Windows.Forms.DialogResult.No) return;
                HapusSatuan(e.RowIndex);
            }
        }

        private void HapusSatuan(int row)
        {
            if (dgSatuan["clmnNama", row].Value.ToString().ToLower() == "root")
            {
                MessageBox.Show(this, "Kode satuan ini tidak boleh dihapus!", "Data not deleted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string message = DBASatuan.Instance.Delete(int.Parse(dgSatuan["clmnId", row].Value.ToString()));

            if (message == "success")
            {
                MessageBox.Show(this, "Data satuan telah dihapus.", "Data deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgSatuan.Rows.RemoveAt(row);
            }
            else if (message == "satuan in used in barangdetail")
                MessageBox.Show(this, "Data satuan tidak dapat dihapus. Data satuan memiliki koresponden dengan data lain.", "Data not deleted",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not deleted",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void toolStripButton10_Click_1(object sender, EventArgs e)
        {
            FrmMasterStok frmMasterStok = new FrmMasterStok();
            frmMasterStok.ShowDialog();
        }

        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dgProduct, "Master Produk");
        }

        private void dgProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (dgProduct.Columns[e.ColumnIndex].Name == "btnUbah")
            {
                FrmUbahProduk frmUbahProduk = new FrmUbahProduk(this, int.Parse(dgProduct["clmnIdBarang", e.RowIndex].Value.ToString()), 
                    dgProduct["clmnKodeBarang", e.RowIndex].Value.ToString(), 
                    dgProduct["clmnNamaBarang", e.RowIndex].Value.ToString(),
                    int.Parse(dgProduct["clmnIdSatuan", e.RowIndex].Value.ToString()),
                    dgProduct["clmnKodeSatuan", e.RowIndex].Value.ToString(),
                    int.Parse(dgProduct["clmnIdSatuanParent", e.RowIndex].Value.ToString()),
                    dgProduct["clmnKodeSatuanParent", e.RowIndex].Value.ToString(),
                    decimal.Parse(dgProduct["clmnRetailStok", e.RowIndex].Value.ToString()),
                    decimal.Parse(dgProduct["clmnHargaJual", e.RowIndex].Value.ToString()), 
                    decimal.Parse(dgProduct["clmnMinimumStok", e.RowIndex].Value.ToString()), e.RowIndex);
                frmUbahProduk.ShowDialog();
            }
            else if(dgProduct.Columns[e.ColumnIndex].Name == "btnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data produk?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;

                string message = DBABarangDetail.Instance.DeleteByMultipleId(int.Parse(dgProduct.Rows[e.RowIndex].Cells["clmnIdBarang"].Value.ToString()),
                int.Parse(dgProduct.Rows[e.RowIndex].Cells["clmnIdSatuan"].Value.ToString()), int.Parse(dgProduct.Rows[e.RowIndex].Cells["clmnIdSatuanParent"].Value.ToString()));

                if (message == "success")
                {
                    MessageBox.Show(this, "Data produk telah dihapus.", "Data deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgSatuan.Rows.RemoveAt(e.RowIndex);
                }
                else if (message == "produk in used")
                    MessageBox.Show(this, "Data produk tidak dapat dihapus. Data produk memiliki koresponden dengan data lain.", "Data not deleted",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not deleted",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (dgProduct.Columns[e.ColumnIndex].Name == "btnHargaBeli")
            {
                FrmPenyesuaianHargaBeli frmPenyesuaianHargaBeli = new FrmPenyesuaianHargaBeli();
                frmPenyesuaianHargaBeli.ShowDialog();
            }
            else if (dgProduct.Columns[e.ColumnIndex].Name == "btnHargaJual")
            {
                FrmPenyesuaianHargaJual frmPenyesuaianHargaJual = 
                    new FrmPenyesuaianHargaJual(this, int.Parse(dgProduct["clmnIdBarang", e.RowIndex].Value.ToString()), dgProduct["clmnKodeBarang", e.RowIndex].Value.ToString(),
                        dgProduct["clmnNamaBarang", e.RowIndex].Value.ToString(), int.Parse(dgProduct["clmnIdSatuan", e.RowIndex].Value.ToString()),
                        dgProduct["clmnKodeSatuan", e.RowIndex].Value.ToString(), decimal.Parse(dgProduct["clmnHargaJual", e.RowIndex].Value.ToString()), e.RowIndex);
                frmPenyesuaianHargaJual.ShowDialog();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Pagging.First(dgMenu, DBAMenu.Instance, lblPageMenu, cboDisplayMenu, "");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dgMenu, DBAMenu.Instance, lblPageMenu, cboDisplayMenu, "");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAMenu.Instance.Count(""), dgMenu, DBAMenu.Instance, lblPageMenu, cboDisplayMenu, "");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAMenu.Instance.Count(""), dgMenu, DBAMenu.Instance, lblPageMenu, cboDisplayMenu, "");
        }

        private void cboDisplayMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dgMenu, DBAMenu.Instance, lblPageMenu, cboDisplayMenu, "");
            if(!firstLoadMenu)
            {
                dgMenu.Sort(dgMenu.Columns["clmnKode"], System.ComponentModel.ListSortDirection.Ascending);
                firstLoadMenu = true;
            }
        }

        private void toolStripButton6_Click_1(object sender, EventArgs e)
        {
            FrmInputMenu frmInputMenu = new FrmInputMenu(this);
            frmInputMenu.ShowDialog();
        }

        private void dgMenu_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (dgMenu.Columns[e.ColumnIndex].Name == "btnDetails")
            {
                FrmMenuDetail frmMenuDetail = new FrmMenuDetail(int.Parse(dgMenu["clmnId", e.RowIndex].Value.ToString()));
                frmMenuDetail.ShowDialog();
            }
            else if(dgMenu.Columns[e.ColumnIndex].Name == "btnUbah")
            {
                FrmUbahMenu frmUbahMenu = new FrmUbahMenu(this, int.Parse(dgMenu["clmnId", e.RowIndex].Value.ToString()), 
                    dgMenu["clmnKode", e.RowIndex].Value.ToString(), dgMenu["clmnNama", e.RowIndex].Value.ToString(), e.RowIndex);
                frmUbahMenu.ShowDialog();
            }
            else if(dgMenu.Columns[e.ColumnIndex].Name == "btnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data menu?", "Konfirmasi", MessageBoxButtons.OK, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
                HapusMenu(e.RowIndex);
            }
            else if (dgMenu.Columns[e.ColumnIndex].Name == "btnHarga")
            {
                FrmPenyesuaianHargaJualMenu frmPenyesuaianHargaJualMenu = new FrmPenyesuaianHargaJualMenu(this, int.Parse(dgMenu["clmnId", e.RowIndex].Value.ToString()),
                    dgMenu["clmnKode", e.RowIndex].Value.ToString(), dgMenu["clmnNama", e.RowIndex].Value.ToString(), 
                    decimal.Parse(dgMenu["clmnHarga", e.RowIndex].Value.ToString()), e.RowIndex);
                frmPenyesuaianHargaJualMenu.ShowDialog();
            }
        }

        private void HapusMenu(int row)
        {
            string message = DBAMenu.Instance.Delete(int.Parse(dgMenu["clmnId", row].Value.ToString()));

            if (message == "success")
            {
                MessageBox.Show(this, "Data menu telah dihapus.", "Data deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgMenu.Rows.RemoveAt(row);
            }
            else if (message == "menu in used in other table")
                MessageBox.Show(this, "Data menu tidak dapat dihapus. Data menu memiliki koresponden dengan data lain.", "Data not deleted",
                    MessageBoxButtons.OK, MessageBoxIcon.Stop);
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan pada database, mohon periksa kembali atau hubungi administrator.", "Data not deleted",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(this, message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }  
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            FrmInputProduk frmInputProduk = new FrmInputProduk(this);
            frmInputProduk.ShowDialog();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            FrmMasterHargaBeli frmMasterHargaBeli = new FrmMasterHargaBeli();
            frmMasterHargaBeli.ShowDialog();

        }

        public void InsertDataTitle(params object[] values)
        {
            dgTitle.Rows.Add(values);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dgMenu, "Master Menu");
        }

        public void UbahSatuan(params object[] values)
        {
            dgSatuan.Rows[(int)values[0]].Cells["clmnKode"].Value = values[1];
            dgSatuan.Rows[(int)values[0]].Cells["clmnNama"].Value = values[2];
            dgSatuan.Rows[(int)values[0]].Cells["clmnKeterangan"].Value = values[3];
        }

        public void UpdateMenu(params object[] values)
        {
            dgMenu.Rows[(int)values[0]].Cells["clmnKode"].Value = values[1];
            dgMenu.Rows[(int)values[0]].Cells["clmnNama"].Value = values[2];
        }

        public void InsertSatuan(params object[] values)
        {
            dgSatuan.Rows.Add(values);
        }

        public void InsertProduk(params object[] values)
        {
            dgProduct.Rows.Add(values);
        }

        public void InsertMenu(params object[] values)
        {
            dgMenu.Rows.Add(values);
        }

        public void UpdateHargaMenu(int rowIndex, decimal value)
        {
            dgMenu.Rows[rowIndex].Cells["clmnHarga"].Value = value;
        }

        public void UpdateHarga(int rowIndex, decimal value)
        {
            dgProduct.Rows[rowIndex].Cells["clmnHargaJual"].Value = value;
        }

        private void FilteringData()
        {
            string filter = GetFilterProduct();
            int countToDisplay = (cboDisplayProduk.Text == "All") ? DBABarangDetail.Instance.Count("") : int.Parse(cboDisplayProduk.Text);
            Pagging.BindingGrid(dgProduct, DBABarangDetail.Instance, 0, countToDisplay, filter);
        }

        private string GetFilterProduct()
        {
            string returnValue = "";
            if (cboFilterProductKodeBarang.Text != "" && cboFilterProductKodeBarang.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " b.kode='" + cboFilterProductKodeBarang.Text + "' " : returnValue + " and b.kode='" + cboFilterProductKodeBarang.Text + "' ";

            if (cboFilterProductKodeSatuan.Text != "" && cboFilterProductKodeSatuan.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " s.kode='" + cboFilterProductKodeSatuan.Text + "' " : returnValue + " and s.kode='" + cboFilterProductKodeSatuan.Text + "' ";
            
            return returnValue;
        }

        private void cboFilterProductKodeBarang_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPageProduk.Text = "1";
            FilteringData();
        }

        private void cboFilterProductKodeSatuan_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPageProduk.Text = "1";
            FilteringData();
        }

        public void UpdateProduk(int rowIndex, decimal retailStok, decimal minimumStok)
        {
            dgProduct.Rows[rowIndex].Cells["clmnRetailStok"].Value = retailStok;
            dgProduct.Rows[rowIndex].Cells["clmnMinimumStok"].Value = minimumStok;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0 && cboDisplayProduk.SelectedIndex == -1)
                cboDisplayProduk.SelectedIndex = 0;
            else if (tabControl1.SelectedIndex == 1 && cboDisplayMenu.SelectedIndex == -1)
                cboDisplayMenu.SelectedIndex = 0;
            else if (tabControl1.SelectedIndex == 2 && cboDisplayTitle.SelectedIndex == -1)
                cboDisplayTitle.SelectedIndex = 0;
            else
                cboDisplaySatuan.SelectedIndex = 0;
        }
    }
}
