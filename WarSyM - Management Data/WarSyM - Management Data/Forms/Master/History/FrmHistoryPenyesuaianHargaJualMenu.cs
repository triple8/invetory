﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.History
{
    public partial class FrmHistoryPenyesuaianHargaJualMenu : Form
    {
        bool initLoad = false;

        public FrmHistoryPenyesuaianHargaJualMenu()
        {
            InitializeComponent();
        }

        private void FrmHistoryPenyesuaianHargaJualMenu_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            GridSetting();
            cboDisplay.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBAUser.Instance, cboUsername, 0, "");
            cboUsername.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBAMenu.Instance, cboKodeMenu, 1, "");
            cboKodeMenu.SelectedIndex = 0;
            initLoad = true;

            dataGridView1.Sort(dataGridView1.Columns["clmnTanggal"], System.ComponentModel.ListSortDirection.Descending);
        }

        private void GridSetting()
        {
            dataGridView1.Columns.Add("clmnTanggal", "Tanggal");
            dataGridView1.Columns.Add("clmnKodeMenu", "Kode Menu");
            dataGridView1.Columns.Add("clmnNamaMenu", "Nama Menu");
            dataGridView1.Columns.Add("clmnHargaJualAwal", "Harga Jual Awal");
            dataGridView1.Columns.Add("clmnHargaJualSekarang", "Harga Jual Sekarang");
            dataGridView1.Columns.Add("clmnInfo", "Info");
            dataGridView1.Columns.Add("clmnUser", "user");

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            dataGridView1.Columns["clmnTanggal"].DefaultCellStyle.Format = "dd MMMM yyyy";
            dataGridView1.Columns["clmnHargaJualAwal"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnHargaJualAwal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns["clmnHargaJualSekarang"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnHargaJualSekarang"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "History Penyesuaian Harga Jual Menu");
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBAPenyesuaianHargaJualMenu.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBAPenyesuaianHargaJualMenu.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAPenyesuaianHargaJualMenu.Instance.Count(GetFilter()), dataGridView1, DBAPenyesuaianHargaJualMenu.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAPenyesuaianHargaJualMenu.Instance.Count(GetFilter()), dataGridView1, DBAPenyesuaianHargaJualMenu.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBAPenyesuaianHargaJualMenu.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplay.Text == "All") ? DBAPenyesuaianHargaJualMenu.Instance.Count("") : int.Parse(cboDisplay.Text);
            Pagging.BindingGrid(dataGridView1, DBAPenyesuaianHargaJualMenu.Instance, 0, countToDisplay, filter);
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (cboUsername.Text != "" && cboUsername.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " p.user='" + cboUsername.Text + "' " : returnValue + " and p.user='" + cboUsername.Text + "' ";

            if (cboKodeMenu.Text != "" && cboKodeMenu.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " m.kode='" + cboKodeMenu.Text + "' " : returnValue + " and m.kode='" + cboKodeMenu.Text + "' ";
            
            if (chkRange.Checked)
                returnValue = (returnValue == "") ? " date(p.tanggal) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) " : returnValue + " and date(p.tanggal) between Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" + string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) ";

            return returnValue;
        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dtAwal_ValueChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dtAkhir_ValueChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboKodeMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboUsername_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Apakah Anda yakin akan menghapus history penyesuaian harga jual menu?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == System.Windows.Forms.DialogResult.No) return;

            string filter = GetFilterDeleteHistory();
            string message = DBAPenyesuaianHargaJualMenu.Instance.DeleteByFilter((filter == "") ? " 1=1 " : filter);
            if (message == "success")
            {
                MessageBox.Show(this, "Data history penyesuaian harga jual menu sudah dihapus.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.Rows.Clear();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetFilterDeleteHistory()
        {
            string returnValue = "";
            if (cboUsername.Text != "" && cboUsername.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " user='" + cboUsername.Text + "' " : returnValue + " and user='" + cboUsername.Text + "' ";

            if (cboKodeMenu.Text != "" && cboKodeMenu.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " id_menu='" 
                    + DBAMenu.Instance.GetSingleColumn(cboKodeMenu.Text, "id") + "' " : returnValue + " and id_menu='"
                    + DBAMenu.Instance.GetSingleColumn(cboKodeMenu.Text, "id") + "' ";

            if (chkRange.Checked)
                returnValue = (returnValue == "") ? " date(tanggal) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) " : returnValue + " and date(tanggal) between Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" + string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) ";

            return returnValue;
        }
    }
}
