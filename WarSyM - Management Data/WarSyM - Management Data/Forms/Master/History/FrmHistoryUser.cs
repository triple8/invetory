﻿using System;
using System.Data;
using System.Windows.Forms;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.History
{
    public partial class FrmHistoryUser : Form
    {
        bool initLoad = false;

        public FrmHistoryUser()
        {
            InitializeComponent();
        }

        private void FrmHistoryUser_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            GridSetting();
            cboDisplay.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBAUser.Instance, cboUserName, 0, GetFilter());
            ControlBinding.BindingComboBox(DBAToko.Instance, cboKodeToko, 1, GetFilter());
            cboUserName.SelectedIndex = 0;
            cboAplikasi.SelectedIndex = 0;
            cboKodeToko.SelectedIndex = 0;
            initLoad = true;

            dataGridView1.Sort(dataGridView1.Columns["clmnTanggalLogin"], System.ComponentModel.ListSortDirection.Descending);
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            Pagging.DataCountChange(dataGridView1, DBAHistoryUser.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void GridSetting()
        {
            dataGridView1.Columns.Add("clmnUserName", "Username");
            dataGridView1.Columns.Add("clmnTanggalLogin", "Tanggal Login");
            dataGridView1.Columns.Add("clmnJamLogin", "Jam Login");
            dataGridView1.Columns.Add("clmnTanggalLogout", "Tanggal Logout");
            dataGridView1.Columns.Add("clmnJamLogout", "Jam Logout");
            dataGridView1.Columns.Add("clmnAplikasi", "Aplikasi");
            dataGridView1.Columns.Add("clmnKodeToko", "Kode Toko");

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBAHistoryUser.Instance, lblPage, cboDisplay, GetFilter());  
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBAHistoryUser.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAHistoryUser.Instance.Count(GetFilter()), dataGridView1, DBAHistoryUser.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAHistoryUser.Instance.Count(GetFilter()), dataGridView1, DBAHistoryUser.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void cboUserName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplay.Text == "All") ? DBAHistoryUser.Instance.Count(GetFilter()) : int.Parse(cboDisplay.Text);
            Pagging.BindingGrid(dataGridView1, DBAHistoryUser.Instance, 0, countToDisplay, filter);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "History Login User");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Apakah Anda yakin akan menghapus history login user?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == System.Windows.Forms.DialogResult.No) return;

            string filter = GetFilterDelete();
            string message = DBAHistoryUser.Instance.DeleteByFilter((filter == "") ? " 1=1 " : filter);
            if(message == "success")
            {
                MessageBox.Show(this, "Data history login user sudah dihapus.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.Rows.Clear();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetFilterDelete()
        {
            string returnValue = "";
            if (cboUserName.Text != "" && cboUserName.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " user='" + cboUserName.Text + "' " : returnValue + " and user='" + cboUserName.Text + "' ";

            if (cboAplikasi.Text != "" && cboAplikasi.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " aplikasi='" + cboAplikasi.Text + "' " : returnValue + " and aplikasi='" + cboAplikasi.Text + "' ";

            if (cboKodeToko.Text != "" && cboKodeToko.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " id_toko='" + DBAToko.Instance.GetSingleColumn(cboKodeToko.Text, "id") + "' "
                    : returnValue + " and id_toko='" + DBAToko.Instance.GetSingleColumn(cboKodeToko.Text, "id") + "' ";

            if (chkRange.Checked)
                returnValue = (returnValue == "") ? " date(login) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtLogin.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtLogout.Value) + "' as Date) " : returnValue + " and date(login) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtLogin.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtLogout.Value) + "' as Date) ";

            return returnValue;
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (cboUserName.Text != "" && cboUserName.SelectedIndex != 0) 
                returnValue = (returnValue == "") ? " h.user='" + cboUserName.Text + "' " : returnValue + " and h.user='" + cboUserName.Text + "' ";

            if(cboAplikasi.Text != "" && cboAplikasi.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " h.aplikasi='" + cboAplikasi.Text + "' " : returnValue + " and h.aplikasi='" + cboAplikasi.Text + "' ";

            if (cboKodeToko.Text != "" && cboKodeToko.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " h.id_toko='" + DBAToko.Instance.GetSingleColumn(cboKodeToko.Text, "id") + "' " 
                    : returnValue + " and h.id_toko='" + DBAToko.Instance.GetSingleColumn(cboKodeToko.Text, "id") + "' ";

            if (chkRange.Checked)
                returnValue = (returnValue == "") ? " date(h.login) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtLogin.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtLogout.Value) + "' as Date) " : returnValue + " and date(h.login) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtLogin.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtLogout.Value) + "' as Date) ";

            return returnValue;
        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboAplikasi_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "clmnTanggalLogin" || dataGridView1.Columns[e.ColumnIndex].Name == "clmnTanggalLogout")
            {
                dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = string.Format("{0:dd MMMM yyyy}", dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
            }
            else if (dataGridView1.Columns[e.ColumnIndex].Name == "clmnJamLogin" || dataGridView1.Columns[e.ColumnIndex].Name == "clmnJamLogout")
            {
                dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = string.Format("{0:t}", dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
            }
        }

        private void cboUserName_Click(object sender, EventArgs e)
        {

        }

        private void cboKodeToko_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }
    }
}
