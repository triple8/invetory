﻿using System;
using System.Data;
using System.Windows.Forms;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.History
{
    public partial class FrmHistoryPenyesuaianHargaJual : Form
    {
        bool initLoad = false;
        public FrmHistoryPenyesuaianHargaJual()
        {
            InitializeComponent();
        }

        private void FrmHistoryPenyesuaianHargaJual_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            GridSetting();
            cboDisplay.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBAUser.Instance, cboUsername, 0, "");
            cboUsername.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBABarang.Instance, cboKodeBarang, 1, "");
            cboKodeBarang.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBABarang.Instance, cboKodeSatuan, 1, "");
            cboKodeSatuan.SelectedIndex = 0;
            cboKodeSatuan.Items.RemoveAt(1);
            initLoad = true;

            dataGridView1.Sort(dataGridView1.Columns["clmnTanggal"], System.ComponentModel.ListSortDirection.Descending);
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBAPenyesuaianHargaJual.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void GridSetting()
        {
            dataGridView1.Columns.Add("clmnTanggal", "Tanggal");
            dataGridView1.Columns.Add("clmnKodeBarang", "Kode Barang");
            dataGridView1.Columns.Add("clmnNamaBarang", "Nama Barang");
            dataGridView1.Columns.Add("clmnKodeSatuan", "Kode Satuan");
            dataGridView1.Columns.Add("clmnHargaJualAwal", "Harga Jual Awal");
            dataGridView1.Columns.Add("clmnHargaJualSekarang", "Harga Jual Sekarang");
            dataGridView1.Columns.Add("clmnInfo", "Info");
            dataGridView1.Columns.Add("clmnUser", "user");

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);
            dataGridView1.Columns["clmnTanggal"].DefaultCellStyle.Format = "dd MMMM yyyy";
            dataGridView1.Columns["clmnHargaJualAwal"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnHargaJualAwal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns["clmnHargaJualSekarang"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnHargaJualSekarang"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "History Penyesuaian Harga Jual");
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBAPenyesuaianHargaJual.Instance, lblPage, cboDisplay, GetFilter());
        }
        
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBAPenyesuaianHargaJual.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAPenyesuaianHargaJual.Instance.Count(GetFilter()), dataGridView1, DBAPenyesuaianHargaJual.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAPenyesuaianHargaJual.Instance.Count(GetFilter()), dataGridView1, DBAPenyesuaianHargaJual.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void cboKodeBarang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboUsername_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplay.Text == "All") ? DBAPenyesuaianHargaJual.Instance.Count("") : int.Parse(cboDisplay.Text);
            Pagging.BindingGrid(dataGridView1, DBAPenyesuaianHargaJual.Instance, 0, countToDisplay, filter);
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (cboUsername.Text != "" && cboUsername.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " p.user='" + cboUsername.Text + "' " : returnValue + " and p.user='" + cboUsername.Text + "' ";

            if (cboKodeBarang.Text != "" && cboKodeBarang.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " b.kode='" + cboKodeBarang.Text + "' " : returnValue + " and b.kode='" + cboKodeBarang.Text + "' ";

            if (cboKodeSatuan.Text != "" && cboKodeSatuan.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " s.kode='" + cboKodeSatuan.Text + "' " : returnValue + " and s.kode='" + cboKodeSatuan.Text + "' ";

            if (chkRange.Checked)
                returnValue = (returnValue == "") ? " date(p.tanggal) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) " : returnValue + " and date(p.tanggal) between Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" + string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) ";

            return returnValue;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Apakah Anda yakin akan menghapus history penyesuaian harga jual ", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == System.Windows.Forms.DialogResult.No) return;

            string filter = GetFilterDeleteHistory();
            string message = DBAPenyesuaianHargaJual.Instance.DeleteByFilter((filter == "") ? " 1=1 " : filter);
            if (message == "success")
            {
                MessageBox.Show(this, "Data history penyesuaian harga jual sudah dihapus.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.Rows.Clear();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetFilterDeleteHistory()
        {
            string returnValue = "";
            if (cboUsername.Text != "" && cboUsername.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " user='" + cboUsername.Text + "' " : returnValue + " and user='" + cboUsername.Text + "' ";

            if (cboKodeBarang.Text != "" && cboKodeBarang.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " id_barang='" 
                    + DBABarang.Instance.GetSingleColumn(cboKodeBarang.Text, "id") + "' " : returnValue + " and id_barang='"
                    + DBABarang.Instance.GetSingleColumn(cboKodeBarang.Text, "id") + "' ";

            if (cboKodeSatuan.Text != "" && cboKodeSatuan.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " id_satuan='"
                    + DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id") + "' " : returnValue + " and id_satuan='"
                    + DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id") + "' ";

            if (chkRange.Checked)
                returnValue = (returnValue == "") ? " date(tanggal) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) " : returnValue + " and date(tanggal) between Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" + string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) ";

            return returnValue;
        }

    }
}
