﻿using System;
using System.Data;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.GlobalClass;

namespace WarSyM_Management_Data.Forms.Master.History
{
    public partial class FrmHistoryPenyesuaianStok : Form
    {
        bool initLoad = false;

        public FrmHistoryPenyesuaianStok()
        {
            InitializeComponent();
        }

        private void FrmHistoryPenyesuaianStok_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            GridSetting();
            cboDisplay.SelectedIndex = 0;

            ControlBinding.BindingComboBox(DBAUser.Instance, cboUser, 0, "");
            cboUser.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBABarang.Instance, cboKodeBarang, 1, "");
            cboKodeBarang.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBAToko.Instance, cboKodeToko, 1, "");
            cboKodeToko.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, "");
            cboKodeSatuan.SelectedIndex = 0;
            cboKodeSatuan.Items.RemoveAt(1);
            initLoad = true;

            dataGridView1.Sort(dataGridView1.Columns["clmnTanggal"], System.ComponentModel.ListSortDirection.Descending);
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBAPenyesuaianStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBAPenyesuaianStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAPenyesuaianStok.Instance.Count(GetFilter()), dataGridView1, DBAPenyesuaianStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAPenyesuaianStok.Instance.Count(GetFilter()), dataGridView1, DBAPenyesuaianStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBAPenyesuaianStok.Instance, lblPage, cboDisplay, GetFilter());
        }

        private void GridSetting()
        {
            dataGridView1.Columns.Add("clmnTanggal", "Tanggal");
            dataGridView1.Columns.Add("clmnKodeBarang", "Kode Barang");
            dataGridView1.Columns.Add("clmnNamaBarang", "Nama Barang");
            dataGridView1.Columns.Add("clmlnKodeSatuan", "Kode Satuan");
            dataGridView1.Columns.Add("clmnKodeToko", "Kode Toko");
            dataGridView1.Columns.Add("clmnStokAwal", "Stok Awal");
            dataGridView1.Columns.Add("clmnStokSekarang", "Stok Sekarang");
            dataGridView1.Columns.Add("clmnInfo", "Info");
            dataGridView1.Columns.Add("clmnUser", "User");

            dataGridView1.Columns["clmnTanggal"].DefaultCellStyle.Format = "dd MMMM yyyy";
            dataGridView1.Columns["clmnStokAwal"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnStokAwal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns["clmnStokSekarang"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnStokSekarang"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "History Penyesuaian Stok");
        }

        private void cboKodeBarang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            lblPage.Text = "1";
            FilteringData();
        }

        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplay.Text == "All") ? DBAPenyesuaianStok.Instance.Count("") : int.Parse(cboDisplay.Text);
            Pagging.BindingGrid(dataGridView1, DBAPenyesuaianStok.Instance, 0, countToDisplay, filter);
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (cboUser.Text != "" && cboUser.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " h.user='" + cboUser.Text + "' " : returnValue + " and h.user='" + cboUser.Text + "' ";

            if (cboKodeBarang.Text != "" && cboKodeBarang.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " b.kode='" + cboKodeBarang.Text + "' " : returnValue + " and b.kode='" + cboKodeBarang.Text + "' ";

            if (cboKodeSatuan.Text != "" && cboKodeSatuan.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " s.kode='" + cboKodeSatuan.Text + "' " : returnValue + " and s.kode='" + cboKodeSatuan.Text + "' ";

            if (cboKodeToko.Text != "" && cboKodeToko.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " t.kode='" + cboKodeToko.Text + "' " : returnValue + " and t.kode='" + cboKodeToko.Text + "' ";

            if (chkRange.Checked)
                returnValue = (returnValue == "") ? " date(h.tanggal) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) " : returnValue + " and date(h.tanggal) between Cast('" + 
                    string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" + string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) ";

            return returnValue;
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboKodeToko_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Apakah Anda yakin akan menghapus history penyesuaian stok?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == System.Windows.Forms.DialogResult.No) return;

            string filter = GetFilterDeleteHistory();
            string message = DBAPenyesuaianStok.Instance.DeleteByFilter((filter == "") ? " 1=1 " : filter);
            if (message == "success")
            {
                MessageBox.Show(this, "Data history penyesuaian stok sudah dihapus.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.Rows.Clear();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetFilterDeleteHistory()
        {
            string returnValue = "";
            if (cboUser.Text != "" && cboUser.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " user='" + cboUser.Text + "' " : returnValue + " and user='" + cboUser.Text + "' ";

            if (cboKodeBarang.Text != "" && cboKodeBarang.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " id_barang='"
                    + DBABarang.Instance.GetSingleColumn(cboKodeBarang.Text, "id") + "' " : returnValue + " and id_barang='" +
                    DBABarang.Instance.GetSingleColumn(cboKodeBarang.Text, "id") + "' ";

            if (cboKodeSatuan.Text != "" && cboKodeSatuan.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " id_satuan='" 
                    + DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id") + "' " : returnValue + " and id_satuan='" 
                    + DBASatuan.Instance.GetSingleColumn(cboKodeSatuan.Text, "id") + "' ";

            if (cboKodeToko.Text != "" && cboKodeToko.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " id_toko='" 
                    + DBAToko.Instance.GetSingleColumn(cboKodeToko.Text, "id") + "' " : returnValue + " and id_toko='"
                    + DBAToko.Instance.GetSingleColumn(cboKodeToko.Text, "id") + "' ";

            if (chkRange.Checked)
                returnValue = (returnValue == "") ? " date(tanggal) between Cast('" + string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) " : returnValue + " and date(tanggal) between Cast('" +
                    string.Format("{0:yyyy-MM-dd}", dtAwal.Value) + "' as Date) and Cast('" + string.Format("{0:yyyy-MM-dd}", dtAkhir.Value) + "' as Date) ";

            return returnValue;
        }
    }
}
