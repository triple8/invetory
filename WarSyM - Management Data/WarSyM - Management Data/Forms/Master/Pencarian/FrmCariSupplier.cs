﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Input;
using WarSyM_Management_Data.Forms.Penyesuaian;

namespace WarSyM_Management_Data.Forms.Master.Pencarian
{
    public partial class FrmCariSupplier : Form
    {
        object obj;
        public FrmCariSupplier(object _obj)
        {
            InitializeComponent();
            obj = _obj;
        }

        private void FrmCariSupplier_Load(object sender, EventArgs e)
        {
            GridSettingTitle();
        }

        private void GridSettingTitle()
        {
            dataGridView1.Columns.Add("clmnKode", "Kode");
            dataGridView1.Columns.Add("clmnNama", "Nama");
            dataGridView1.Columns.Add("clmnAlamat", "Alamat");
            dataGridView1.Columns.Add("clmnKota", "Kota");
            dataGridView1.Columns.Add("clmnTelp", "Telp");

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Select";
            bcol.Name = "btnSelect";
            bcol.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol);

            cboDisplaySatuan.SelectedIndex = 0;
        }

        private void cboDisplaySatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBASupplier.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBASupplier.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBASupplier.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBASupplier.Instance.Count(""), dataGridView1, DBASupplier.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBASupplier.Instance.Count(""), dataGridView1, DBASupplier.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            FilteringData();
        }

        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplaySatuan.Text == "All") ? DBASupplier.Instance.Count("") : int.Parse(cboDisplaySatuan.Text);
            Pagging.BindingGrid(dataGridView1, DBASupplier.Instance, 0, countToDisplay, filter);
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (!string.IsNullOrEmpty(txtCari.Text))
            {
                returnValue = " kode like '%" + txtCari.Text + "%' or nama like '%" + txtCari.Text + "%' or alamat like '%" + txtCari.Text + "%' or kota like '%" 
                    + txtCari.Text + "%' or telp like '%" + txtCari.Text + "%'";
            }
            return returnValue;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnSelect")
            {
                if (obj.GetType().ToString().Contains("FrmInputHargaBeli"))
                {
                    ((FrmInputHargaBeli)obj).kodeSupplier = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmInputHargaBeli)obj).txtKodeSupplier.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmInputHargaBeli)obj).txtKodeSupplier.Focus();
                    ((FrmInputHargaBeli)obj).txtNamaSupplier.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    this.Close();
                }
                if (obj.GetType().ToString().Contains("FrmPenyesuaianHargaBeli"))
                {
                    ((FrmPenyesuaianHargaBeli)obj).kodeSupplier = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmPenyesuaianHargaBeli)obj).txtKodeSupplier.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmPenyesuaianHargaBeli)obj).txtKodeSupplier.Focus();
                    ((FrmPenyesuaianHargaBeli)obj).txtNamaSupplier.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    this.Close();
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
