﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Input;
using WarSyM_Management_Data.Forms.Penyesuaian;

namespace WarSyM_Management_Data.Forms.Master.Pencarian
{
    public partial class FrmCariBarang : Form
    {
        object obj;
        public FrmCariBarang(object _obj)
        {
            InitializeComponent();
            this.obj = _obj;
        }

        private void FrmCariBarang_Load(object sender, EventArgs e)
        {
            GridSettingTitle();
        }

        void GridSettingTitle()
        {
            dataGridView1.Columns.Add("clmnId", "ID");
            dataGridView1.Columns.Add("clmnKode", "Kode");
            dataGridView1.Columns.Add("clmnNama", "Nama");
            dataGridView1.Columns["clmnId"].Visible = false;

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Select";
            bcol.Name = "btnSelect";
            bcol.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol);

            cboDisplaySatuan.SelectedIndex = 0;
        }

        private void cboDisplaySatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBABarang.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBABarang.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBABarang.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBABarang.Instance.Count(""), dataGridView1, DBABarang.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBABarang.Instance.Count(""), dataGridView1, DBABarang.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            FilteringData();
        }

        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplaySatuan.Text == "All") ? DBABarang.Instance.Count("") : int.Parse(cboDisplaySatuan.Text);
            Pagging.BindingGrid(dataGridView1, DBABarang.Instance, 0, countToDisplay, filter);
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (!string.IsNullOrEmpty(txtCari.Text))
            {
                returnValue = " kode like '%" + txtCari.Text + "%' or nama like '%" + txtCari.Text + "%' ";
            }
            return returnValue;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnSelect")
            {
                if(obj.GetType().ToString().Contains("FrmInputProduk"))
                {
                    ((FrmInputProduk)obj).id_barang = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnId"].Value.ToString());
                    ((FrmInputProduk)obj).txtKodeBarang.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmInputProduk)obj).txtKodeBarang.Focus();
                    ((FrmInputProduk)obj).txtNamaItem.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    
                    this.Close();
                }
                else if (obj.GetType().ToString().Contains("FrmInputHargaBeli"))
                {
                    ((FrmInputHargaBeli)obj).idBarang = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnId"].Value.ToString());
                    ((FrmInputHargaBeli)obj).txtKodeBarang.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmInputHargaBeli)obj).txtNamaItem.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    this.Close();
                }
                else if (obj.GetType().ToString().Contains("FrmPenyesuaianStok"))
                {
                    ((FrmPenyesuaianStok)obj).idBarang = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnId"].Value.ToString());
                    ((FrmPenyesuaianStok)obj).txtKodeBarang.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmPenyesuaianStok)obj).txtNamaItem.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    this.Close();
                }
                else if (obj.GetType().ToString().Contains("FrmPenyesuaianHargaJual"))
                {
                    ((FrmPenyesuaianHargaJual)obj).id_barang = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnId"].Value.ToString());
                    ((FrmPenyesuaianHargaJual)obj).txtKodeBarang.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmPenyesuaianHargaJual)obj).txtNamaItem.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    this.Close();
                }
                else if (obj.GetType().ToString().Contains("FrmPenyesuaianHargaBeli"))
                {
                    ((FrmPenyesuaianHargaBeli)obj).id_barang = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnId"].Value.ToString());
                    ((FrmPenyesuaianHargaBeli)obj).txtKodeBarang.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmPenyesuaianHargaBeli)obj).txtNamaItem.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    this.Close();
                }
                else if (obj.GetType().ToString().Contains("FrmInputMenu"))
                {
                    ((FrmInputMenu)obj).txtKodeBarang.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmInputMenu)obj).txtNamaItem.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    this.Close();
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
