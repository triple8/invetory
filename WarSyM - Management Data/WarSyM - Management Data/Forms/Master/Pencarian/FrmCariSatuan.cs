﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Input;

namespace WarSyM_Management_Data.Forms.Master.Pencarian
{
    public partial class FrmCariSatuan : Form
    {
        object obj;
        bool isParent;
        public FrmCariSatuan(object _obj, bool _isParent)
        {
            InitializeComponent();
            this.obj = _obj;
            this.isParent = _isParent;
        }

        private void FrmCariSatuan_Load(object sender, EventArgs e)
        {
            GridSettingTitle();
        }

        private void GridSettingTitle()
        {
            dataGridView1.Columns.Add("clmnId", "ID");
            dataGridView1.Columns.Add("clmnKode", "Kode");
            dataGridView1.Columns.Add("clmnNama", "Nama");
            dataGridView1.Columns.Add("clmnKeterangan", "Keterangan");
            dataGridView1.Columns["clmnId"].Visible = false;

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Select";
            bcol.Name = "btnSelect";
            bcol.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol);

            cboDisplaySatuan.SelectedIndex = 0;
        }

        private void cboDisplaySatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBASatuan.Instance.Count(""), dataGridView1, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBASatuan.Instance.Count(""), dataGridView1, DBASatuan.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            FilteringData();
        }

        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplaySatuan.Text == "All") ? DBASatuan.Instance.Count("") : int.Parse(cboDisplaySatuan.Text);
            Pagging.BindingGrid(dataGridView1, DBASatuan.Instance, 0, countToDisplay, filter);
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (!string.IsNullOrEmpty(txtCari.Text))
            {
                returnValue = " kode like '%" + txtCari.Text + "%' or nama like '%" + txtCari.Text + "%' or keterangan like '%" + txtCari.Text + "%'";
            }
            return returnValue;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnSelect")
            {
                if (obj.GetType().ToString().Contains("FrmInputProduk"))
                {
                    if (this.isParent)
                    {
                        ((FrmInputProduk)obj).id_parent = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnId"].Value.ToString());
                        ((FrmInputProduk)obj).txtKodeSatuanParent.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                        ((FrmInputProduk)obj).txtKodeSatuanParent.Focus();
                        ((FrmInputProduk)obj).txtNamaParent.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                        this.Close();
                    }
                    else
                    {
                        ((FrmInputProduk)obj).id_satuan = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnId"].Value.ToString());
                        ((FrmInputProduk)obj).txtKodeSatuan.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                        ((FrmInputProduk)obj).txtKodeSatuan.Focus();
                        ((FrmInputProduk)obj).txtNamaSatuan.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                        this.Close();
                    }
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
