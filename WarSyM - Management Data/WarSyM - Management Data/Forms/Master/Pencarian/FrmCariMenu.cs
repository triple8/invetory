﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Penyesuaian;

namespace WarSyM_Management_Data.Forms.Master.Pencarian
{
    public partial class FrmCariMenu : Form
    {
        object obj;
        public FrmCariMenu(object _obj)
        {
            InitializeComponent();
            this.obj = _obj;
        }

        private void FrmCariMenu_Load(object sender, EventArgs e)
        {
            GridSettingTitle();
        }

        void GridSettingTitle()
        {
            dataGridView1.Columns.Add("clmnId", "ID");
            dataGridView1.Columns.Add("clmnKode", "Kode");
            dataGridView1.Columns.Add("clmnNama", "Nama");
            dataGridView1.Columns.Add("clmnHarga", "Harga");
            dataGridView1.Columns["clmnId"].Visible = false;

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Select";
            bcol.Name = "btnSelect";
            bcol.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol);

            cboDisplaySatuan.SelectedIndex = 0;
        }


        private void FilteringData()
        {
            string filter = GetFilter();
            int countToDisplay = (cboDisplaySatuan.Text == "All") ? DBAMenu.Instance.Count("") : int.Parse(cboDisplaySatuan.Text);
            Pagging.BindingGrid(dataGridView1, DBAMenu.Instance, 0, countToDisplay, filter);
        }

        private string GetFilter()
        {
            string returnValue = "";
            if (!string.IsNullOrEmpty(txtCari.Text))
            {
                returnValue = " kode like '%" + txtCari.Text + "%' or nama like '%" + txtCari.Text + "%' or harga like '%" + txtCari.Text + "%' ";
            }
            return returnValue;
        }

        private void btnCari_Click_1(object sender, EventArgs e)
        {
            FilteringData();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnSelect")
            {
                if (obj.GetType().ToString().Contains("FrmPenyesuaianHargaJualMenu"))
                {
                    ((FrmPenyesuaianHargaJualMenu)obj).idMenu = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnId"].Value.ToString());
                    ((FrmPenyesuaianHargaJualMenu)obj).txtKodeMenu.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnKode"].Value.ToString();
                    ((FrmPenyesuaianHargaJualMenu)obj).txtNamaMenu.Text = dataGridView1.Rows[e.RowIndex].Cells["clmnNama"].Value.ToString();
                    this.Close();
                }
            }
        }

        private void cboDisplaySatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBAMenu.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAMenu.Instance.Count(""), dataGridView1, DBAMenu.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAMenu.Instance.Count(""), dataGridView1, DBAMenu.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBAMenu.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBAMenu.Instance, lblPageSatuan, cboDisplaySatuan, "");
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
