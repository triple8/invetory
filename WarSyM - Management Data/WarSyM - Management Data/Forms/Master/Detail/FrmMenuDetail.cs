﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Input;
using WarSyM_Management_Data.Forms.Master.Ubah;
using WarSyM_Management_Data.GlobalClass;

namespace WarSyM_Management_Data.Forms.Master.Detail
{
    public partial class FrmMenuDetail : Form
    {
        int  menuId;
        
        public FrmMenuDetail(int _menuId)
        {
            InitializeComponent();
            this.menuId = _menuId;
        }

        private void FrmMenuDetail_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            GridSetting();
            Pagging.BindingGrid(dataGridView1, DBAMenuDetail.Instance, 0, DBAMenuDetail.Instance.Count(" md.id_menu='" + this.menuId + "' "), " md.id_menu='" + this.menuId + "' ");
        }

        private void GridSetting()
        {
            dataGridView1.Columns.Add("clmnIdMenu", "Id Menu");
            dataGridView1.Columns.Add("clmnKodeMenu", "Kode Menu");
            dataGridView1.Columns.Add("clmnIdBarang", "Id Barang");
            dataGridView1.Columns.Add("clmnKodeBarang", "Kode Barang");
            dataGridView1.Columns.Add("clmnIdSatuan", "Id Satuan");
            dataGridView1.Columns.Add("clmnKodeSatuan", "Kode Satuan");
            dataGridView1.Columns.Add("clmnJumlah", "Jumlah");

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Ubah";
            bcol.Name = "btnUbah";
            bcol.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol);

            DataGridViewButtonColumn bcol2 = new DataGridViewButtonColumn();
            bcol2.HeaderText = "";
            bcol2.Text = "Hapus";
            bcol2.Name = "btnHapus";
            bcol2.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol2);

            dataGridView1.Columns["clmnIdMenu"].Visible = false;
            dataGridView1.Columns["clmnIdBarang"].Visible = false;
            dataGridView1.Columns["clmnIdSatuan"].Visible = false;

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if(dataGridView1.Columns[e.ColumnIndex].Name == "btnUbah")
            {
                FrmUbahMenuDetail frmUbahMenuDetail =
                    new FrmUbahMenuDetail(this, int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnIdMenu"].Value.ToString()),
                        dataGridView1.Rows[e.RowIndex].Cells["clmnKodeMenu"].Value.ToString(), int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnIdBarang"].Value.ToString()),
                        dataGridView1.Rows[e.RowIndex].Cells["clmnKodeBarang"].Value.ToString(), int.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnIdSatuan"].Value.ToString()),
                        dataGridView1.Rows[e.RowIndex].Cells["clmnKodeSatuan"].Value.ToString(), decimal.Parse(dataGridView1.Rows[e.RowIndex].Cells["clmnJumlah"].Value.ToString())
                        , e.RowIndex);
                frmUbahMenuDetail.ShowDialog();
            }
            else if(dataGridView1.Columns[e.ColumnIndex].Name == "btnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data detail?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
                string message = DBAMenuDetail.Instance.DeleteByFilter(" id_menu='" + this.menuId.ToString() + "' and id_barang='"
                    + dataGridView1.Rows[e.RowIndex].Cells["clmnIdBarang"].Value.ToString() + "' and id_satuan='" + dataGridView1.Rows[e.RowIndex].Cells["clmnIdSatuan"].Value.ToString() + "'");
                if (message == "success")
                {
                    MessageBox.Show(this, "Data detail menu sudah dihapus.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.Rows.RemoveAt(e.RowIndex);
                }
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void UbahJumlah(int p1, decimal p2)
        {
            dataGridView1.Rows[p1].Cells["clmnJumlah"].Value = p2;
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            FrmInputMenuDetail frmInputMenuDetail = new FrmInputMenuDetail(this, this.menuId, (string)DBAMenu.Instance.GetSingleColumn(this.menuId, "kode"));
            frmInputMenuDetail.ShowDialog();
        }

        internal void InsertDetail(params object[] values)
        {
            dataGridView1.Rows.Add(values);
        }
    }
}
