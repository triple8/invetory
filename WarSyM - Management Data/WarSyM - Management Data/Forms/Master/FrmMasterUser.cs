﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Forms.Master.Input;
using WarSyM_Management_Data.Forms.Master.Ubah;
using WarSyM_Management_Data.Forms.Master.History;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master
{
    public partial class FrmMasterUser : Form
    {

        public FrmMasterUser()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmInputUser frmInputUser = new FrmInputUser(this);
            frmInputUser.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmHistoryUser frmHistoryUser = new FrmHistoryUser();
            frmHistoryUser.MdiParent = this.MdiParent;
            frmHistoryUser.StartPosition = FormStartPosition.CenterScreen;
            frmHistoryUser.Show();
        }

        private void FrmMasterUser_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            InitializeDataGridView();
            cboDisplay.SelectedIndex = 0;
            dgUser.Sort(dgUser.Columns["clmnUserName"], System.ComponentModel.ListSortDirection.Ascending);
        }

        private void InitializeDataGridView()
        {
            dgUser.Columns.Add("clmnUserName", "Username");
            dgUser.Columns.Add("clmnPassword", "Password");
            dgUser.Columns.Add("clmnNama", "Nama");
            dgUser.Columns.Add("clmnAlamat", "Alamat");
            dgUser.Columns.Add("clmnTelp", "Telp");
            dgUser.Columns.Add("clmnAkses", "Hak Akses");

            dgUser.Columns["clmnPassword"].Visible = false;

            dgUser.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Ubah";
            bcol.Name = "btnUbah";
            bcol.UseColumnTextForButtonValue = true;
            dgUser.Columns.Add(bcol);

            DataGridViewButtonColumn bcol2 = new DataGridViewButtonColumn();
            bcol2.HeaderText = "";
            bcol2.Text = "Hapus";
            bcol2.Name = "btnHapus";
            bcol2.UseColumnTextForButtonValue = true;
            dgUser.Columns.Add(bcol2);

            DataGridViewButtonColumn bcol3 = new DataGridViewButtonColumn();
            bcol3.HeaderText = "";
            bcol3.Text = "Ubah Hak Akses";
            bcol3.Name = "btnUbahHakAkses";
            bcol3.UseColumnTextForButtonValue = true;
            bcol3.Width = 120;
            dgUser.Columns.Add(bcol3);

        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dgUser, DBAUser.Instance, lblPage, cboDisplay, "");
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAUser.Instance.Count(""), dgUser, DBAUser.Instance, lblPage, cboDisplay, "");
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAUser.Instance.Count(""), dgUser, DBAUser.Instance, lblPage, cboDisplay, "");
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dgUser, DBAUser.Instance, lblPage, cboDisplay, "");
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dgUser, DBAUser.Instance, lblPage, cboDisplay, "");
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dgUser, "Master User");
        }

        public void InsertUser(params object[] values)
        {
            dgUser.Rows.Add(values);
        }

        private void dgUser_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if(dgUser.Columns[e.ColumnIndex].Name == "btnUbah")
            {
                FrmUbahUserData frmUbahUserData = new FrmUbahUserData(this, dgUser.Rows[e.RowIndex].Cells[0].Value.ToString(), 
                    dgUser.Rows[e.RowIndex].Cells[2].Value.ToString(), dgUser.Rows[e.RowIndex].Cells[3].Value.ToString(), dgUser.Rows[e.RowIndex].Cells[4].Value.ToString(), e.RowIndex);
                frmUbahUserData.ShowDialog();
            }
            else if(dgUser.Columns[e.ColumnIndex].Name == "btnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data user?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 
                    == System.Windows.Forms.DialogResult.No) return;
                string message = DBAUser.Instance.Delete(dgUser.Rows[e.RowIndex].Cells["clmnUsername"].Value.ToString());
                if(message == "success")
                {
                    MessageBox.Show(this, "Data user sudah diubah", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgUser.Rows.RemoveAt(e.RowIndex);
                }
                else if (message == "user in used in other data")
                {
                    MessageBox.Show(this, "Data user tidak dapat dihapus. Data user memiliki koresponden dengan data lain.", "Data not deleted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (dgUser.Columns[e.ColumnIndex].Name == "btnUbahHakAkses")
            {
                FrmUbahRoleUser frmUbahRoleUser = new FrmUbahRoleUser(this, dgUser.Rows[e.RowIndex].Cells["clmnUsername"].Value.ToString(), dgUser.Rows[e.RowIndex].Cells["clmnAkses"].Value.ToString(), e.RowIndex);
                frmUbahRoleUser.ShowDialog();
            }
        }

        public void UbahUserData(params object[] values)
        {
            dgUser.Rows[(int)values[0]].Cells[2].Value = values[1];
            dgUser.Rows[(int)values[0]].Cells[3].Value = values[2];
            dgUser.Rows[(int)values[0]].Cells[4].Value = values[3];
        }

        public void UbahUserRole(params object[] values)
        {
            dgUser.Rows[(int)values[0]].Cells["clmnAkses"].Value = values[1];
        }
    }
}
