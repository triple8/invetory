﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Input;
using WarSyM_Management_Data.Forms.Master.Ubah;

namespace WarSyM_Management_Data.Forms.Master
{
    public partial class FrmMasterToko : Form
    {
        public FrmMasterToko()
        {
            InitializeComponent();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmInputToko frmInputToko = new FrmInputToko(this);
            frmInputToko.ShowDialog();
        }

        private void FrmMasterToko_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            } 
            GridSetting();
            cboDisplay.SelectedIndex = 0;
            dgToko.Sort(dgToko.Columns["clmnKode"], System.ComponentModel.ListSortDirection.Ascending);
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dgToko, DBAToko.Instance, lblPage, cboDisplay, "");
        }

        private void GridSetting()
        {
            dgToko.Columns.Add("clmnId", "Id");
            dgToko.Columns.Add("clmnKode","Kode");
            dgToko.Columns.Add("clmnNama", "Nama");
            dgToko.Columns.Add("clmnAlamat", "Alamat");
            dgToko.Columns.Add("clmnTelp", "Telp");
            dgToko.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            DataGridViewButtonColumn bcol = new DataGridViewButtonColumn();
            bcol.HeaderText = "";
            bcol.Text = "Ubah";
            bcol.Name = "btnUbah";
            bcol.UseColumnTextForButtonValue = true;
            dgToko.Columns.Add(bcol);

            DataGridViewButtonColumn bcol2 = new DataGridViewButtonColumn();
            bcol2.HeaderText = "";
            bcol2.Text = "Hapus";
            bcol2.Name = "btnHapus";
            bcol2.UseColumnTextForButtonValue = true;
            dgToko.Columns.Add(bcol2);

            dgToko.Columns["clmnId"].Visible = false;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dgToko, DBAToko.Instance, lblPage, cboDisplay, "");
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dgToko, DBAToko.Instance, lblPage, cboDisplay, "");
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAToko.Instance.Count(""), dgToko, DBAToko.Instance, lblPage, cboDisplay, "");
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAToko.Instance.Count(""), dgToko, DBAToko.Instance, lblPage, cboDisplay, "");
        }

        private void dgToko_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if(dgToko.Columns[e.ColumnIndex].Name == "btnUbah")
            {
                FrmUbahToko frmUbahToko = new FrmUbahToko(this, int.Parse(dgToko["clmnId", e.RowIndex].Value.ToString()), e.RowIndex);
                frmUbahToko.ShowDialog();
            }
            else if(dgToko.Columns[e.ColumnIndex].Name == "btnHapus")
            {
                if (MessageBox.Show(this, "Anda yakin akan menghapus data toko?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 
                    == System.Windows.Forms.DialogResult.No) return;
                string message = DBAToko.Instance.Delete(int.Parse(dgToko.Rows[e.RowIndex].Cells["clmnId"].Value.ToString()));
                if (message == "success")
                {
                    MessageBox.Show(this, "Data toko sudah diubah", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgToko.Rows.RemoveAt(e.RowIndex);
                }
                else if (message == "toko in used in other table")
                {
                    MessageBox.Show(this, "Data toko tidak dapat dihapus. Data toko memiliki koresponden dengan data lain.", "Data not deleted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void InsertToko(params object[] values)
        {
            dgToko.Rows.Add(values);
        }

        public void UbahToko(params object[] values)
        {
            dgToko.Rows[(int)values[0]].Cells["clmnKode"].Value = values[1];
            dgToko.Rows[(int)values[0]].Cells["clmnNama"].Value = values[2];
            dgToko.Rows[(int)values[0]].Cells["clmnAlamat"].Value = values[3];
            dgToko.Rows[(int)values[0]].Cells["clmnTelp"].Value = values[4];
        }
    }
}
