﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;


namespace WarSyM_Management_Data.Forms.Master.Report
{
    public partial class FrmRptDetailPembelian : Form
    {

        public FrmRptDetailPembelian()
        {
            InitializeComponent();
        }

        private void FrmRptDetailPembelian_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
            ControlBinding.BindingComboBox(DBABarang.Instance, cboKodeBarang, 1, "");
            cboKodeBarang.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBASupplier.Instance, cboSupplier, 0, "");
            cboSupplier.SelectedIndex = 0;

            GridSetting();

            cboDisplay.SelectedIndex = 0;
        }

        private void GridSetting()
        {
            dataGridView1.Columns.Add("clmnNomorNota", "Nomor Nota");
            dataGridView1.Columns.Add("clmnNomorNotaSupplier","Nomor Nota Supplier");
            dataGridView1.Columns.Add("clmnIdBarang", "Id Barang");
            dataGridView1.Columns.Add("clmnNamaBarang", "clmnNamaBarang");
            dataGridView1.Columns.Add("clmnHarga", "Harga");
            dataGridView1.Columns.Add("clmnJumlah", "Jumlah");
            dataGridView1.Columns.Add("clmnDiskon", "Diskon");
            dataGridView1.Columns.Add("clmnSubTotal", "Sub Total");

            dataGridView1.Columns["clmnIdBarang"].Visible = false;

            dataGridView1.Columns["clmnHarga"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnHarga"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns["clmnJumlah"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnJumlah"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns["clmnDiskon"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnDiskon"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns["clmnSubTotal"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnSubTotal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
          
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "Report Detail Pembelian");
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBAPembelianDetail.Instance, lblPage, cboDisplay, "");
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBAPembelianDetail.Instance, lblPage, cboDisplay, "");
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAPembelianDetail.Instance.Count(""), dataGridView1, DBAPembelianDetail.Instance, lblPage, cboDisplay, "");
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAPembelianDetail.Instance.Count(""), dataGridView1, DBAPembelianDetail.Instance, lblPage, cboDisplay, "");
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBAPembelianDetail.Instance, lblPage, cboDisplay, "");
        }

        private void cboKodeBarang_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void cboSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            decimal jumlah = 0;
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                decimal _jumlah;
                decimal.TryParse(dataGridView1[5,i].Value.ToString(), out _jumlah);
                jumlah = jumlah + _jumlah;
            }
            lblInfo.Text = "Total item dibeli: " + string.Format("{0:#,#.#}", jumlah) + " pcs";
        }

    }
}
