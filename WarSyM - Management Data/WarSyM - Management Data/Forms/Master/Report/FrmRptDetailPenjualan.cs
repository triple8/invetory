﻿using System;
using System.Data;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Report
{
    public partial class FrmRptDetailPenjualan : Form
    {
        public FrmRptDetailPenjualan()
        {
            InitializeComponent();
        }

        private void FrmRptDetailPenjualan_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if(ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
            BindingCbo();

            cboDisplay.SelectedIndex = 0;
        }

        private void BindingCbo()
        {
            BindingCboBarang();
        }

        private void BindingCboBarang()
        {
            try
            {
                DataTable dt = DBABarang.Instance.GetAll(0, DBABarang.Instance.Count(""), "");
                cboKodeBarang.Items.Clear();
                cboKodeBarang.Items.Add("All");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cboKodeBarang.Items.Add(dt.Rows[i][0]);
                }
                cboKodeBarang.SelectedIndex = -1;
            }
            catch (Exception e)
            {
                if (ApplicationConnection.Instance.MySqlDatabaseConnection.State == ConnectionState.Open)
                    ApplicationConnection.Instance.MySqlDatabaseConnection.Close();
                MessageBox.Show(this, e.Message, "Error Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GridSetting()
        {
            if (dataGridView1.RowCount == 0) return;
            dataGridView1.Columns[2].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns[3].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns[4].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "Report Detail Penjualan");
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
        }

        private void btnNext_Click(object sender, EventArgs e)
        {

        }

        private void btnLast_Click(object sender, EventArgs e)
        {

        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void cboKodeBarang_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            decimal jumlah = 0;
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                decimal _jumlah;
                decimal.TryParse(dataGridView1[3, i].Value.ToString(), out _jumlah);
                jumlah = jumlah + _jumlah;
            }
            lblInfo.Text = "Total item terjual: " + string.Format("{0:#,#.#}", jumlah) + " pcs";
        }
    }
}
