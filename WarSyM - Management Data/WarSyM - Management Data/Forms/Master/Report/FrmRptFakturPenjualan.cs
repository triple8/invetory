﻿using System;
using System.Data;
using System.Windows.Forms;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Report
{
    public partial class FrmRptFakturPenjualan : Form
    {

        public FrmRptFakturPenjualan()
        {
            InitializeComponent();
        }

        private void FrmRptFakturPenjualan_Load(object sender, EventArgs e)
        {
            InitFirst();
        }

        private void InitFirst()
        {
            BindingCbo();
            cboDisplay.SelectedIndex = 0;
        }

        private void BindingCbo()
        {
            BindingCboUser();
        }

        private void BindingCboUser()
        {
            try
            {
                DataTable dt = DBAUser.Instance.GetAll(0, DBAUser.Instance.Count(""), "");
                cboUser.Items.Clear();
                cboUser.Items.Add("All");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cboUser.Items.Add(dt.Rows[i][0]);
                }
                cboUser.SelectedIndex = -1;
            }
            catch (Exception e)
            {
                if (ApplicationConnection.Instance.MySqlDatabaseConnection.State == ConnectionState.Open)
                    ApplicationConnection.Instance.MySqlDatabaseConnection.Close();
                MessageBox.Show(this, e.Message, "Error Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "Report Faktur Penjualan");
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            
        }

        private void GridSetting()
        {
            if (dataGridView1.RowCount == 0) return;
            dataGridView1.Columns[1].DefaultCellStyle.Format = "dd MMMM yyyy";
            dataGridView1.Columns[5].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns[6].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns[7].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
           
        }

        private void btnNext_Click(object sender, EventArgs e)
        {

        }

        private void btnLast_Click(object sender, EventArgs e)
        {
           
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void chkUser_CheckedChanged(object sender, EventArgs e)
        {
          
        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
         
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void cboKurs_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void cboUser_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmDetailPenjualan frmDetailPenjualan = new FrmDetailPenjualan();
            frmDetailPenjualan._id_penjualan = dataGridView1[0, dataGridView1.SelectedRows[0].Index].Value.ToString();
            frmDetailPenjualan.ShowDialog();
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            decimal jumlah = 0;
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                decimal _jumlah;
                decimal.TryParse(dataGridView1[5, i].Value.ToString(), out _jumlah);
                jumlah = jumlah + _jumlah;
            }
            if (cboKurs.Text != "All" && cboKurs.Text!="")
                lblInfo.Text = "Total penjualan: " + string.Format("{0:#,#.#}", jumlah);
            else
                lblInfo.Text = "";
        }

    }
}
