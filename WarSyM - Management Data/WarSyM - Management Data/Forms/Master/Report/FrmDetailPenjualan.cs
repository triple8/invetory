﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Report
{
    public partial class FrmDetailPenjualan : Form
    {
        public string _id_penjualan;
        public FrmDetailPenjualan()
        {
            InitializeComponent();
        }

        private void FrmDetailPenjualan_Load(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" select no_nota, kode_barang, harga, jumlah, (harga*jumlah) as sub_total from detailpenjualan where no_nota='" + _id_penjualan + "'");
                MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
                if (dataGridView1.Rows.Count != 0)
                {
                    dataGridView1.Columns[2].DefaultCellStyle.Format = "N2";
                    dataGridView1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[3].DefaultCellStyle.Format = "N2";
                    dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dataGridView1.Columns[4].DefaultCellStyle.Format = "N2";
                    dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
            }
            catch (Exception f)
            {
                if (ApplicationConnection.Instance.MySqlDatabaseConnection.State == ConnectionState.Open)
                    ApplicationConnection.Instance.MySqlDatabaseConnection.Close();
                MessageBox.Show(this, f.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
