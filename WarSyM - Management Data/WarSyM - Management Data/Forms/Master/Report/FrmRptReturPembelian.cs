﻿using System;
using System.Data;
using System.Windows.Forms;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Report
{
    public partial class FrmRptReturPembelian : Form
    {

        public FrmRptReturPembelian()
        {
            InitializeComponent();
        }

        private void FrmRptReturPembelian_Load(object sender, EventArgs e)
        {
            InitFirst();
        }

        private void InitFirst()
        {
            BindingCbo();
            cboDisplay.SelectedIndex = 0;
        }

        private void BindingCbo()
        {
            BindingCboBarang();
            BindingCBoSupplier();
        }

        private void BindingCBoSupplier()
        {
            try
            {
                DataTable dt = DBASupplier.Instance.GetAll(0, DBASupplier.Instance.Count(""), "");
                cboSupplier.Items.Clear();
                cboSupplier.Items.Add("All");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cboSupplier.Items.Add(dt.Rows[i][0]);
                }
                cboSupplier.SelectedIndex = -1;
            }
            catch (Exception e)
            {
                if (ApplicationConnection.Instance.MySqlDatabaseConnection.State == ConnectionState.Open)
                    ApplicationConnection.Instance.MySqlDatabaseConnection.Close();
                MessageBox.Show(this, e.Message, "Error Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BindingCboBarang()
        {
            try
            {
                DataTable dt = DBABarang.Instance.GetAll(0, DBABarang.Instance.Count(""), "");
                cboKodeBarang.Items.Clear();
                cboKodeBarang.Items.Add("All");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cboKodeBarang.Items.Add(dt.Rows[i][0]);
                }
                cboKodeBarang.SelectedIndex = -1;
            }
            catch (Exception e)
            {
                if (ApplicationConnection.Instance.MySqlDatabaseConnection.State == ConnectionState.Open)
                    ApplicationConnection.Instance.MySqlDatabaseConnection.Close();
                MessageBox.Show(this, e.Message, "Error Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetFilter()
        {
            string filter = "";
            if (chkRange.Checked)
            {
                if (filter != "")
                {
                    filter = filter + " and ";
                }
                filter = filter + " p.tanggal between '" + string.Format("{0:yyyy-MM-dd}", dateTimePicker1.Value) + "' and '"
                    + string.Format("{0:yyyy-MM-dd}", dateTimePicker2.Value) + "' ";
            }
            if (cboKodeBarang.Text != "" && cboKodeBarang.Text != "All")
            {

                if (filter != "")
                {
                    filter = filter + " and ";
                }
                filter = filter + " d.kode_barang='" + cboKodeBarang.Text + "' ";
            }

            if (cboSupplier.Text != "" && cboSupplier.Text != "All")
            {

                if (filter != "")
                {
                    filter = filter + " and ";
                }
                filter = filter + " p.kode_supplier='" + cboSupplier.Text + "' ";
            }

            if (cboJenis.Text != "" && cboJenis.Text != "All")
            {

                if (filter != "")
                {
                    filter = filter + " and ";
                }
                filter = filter + " d.jenis_retur='" + cboJenis.Text + "' ";
            }

            return filter;
        }

        private void GridSetting()
        {
            if (dataGridView1.RowCount == 0) return;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[3].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.Columns[4].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "Report Retur Pembelian");
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {

        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {

        }

        private void btnNext_Click(object sender, EventArgs e)
        {

        }

        private void btnLast_Click(object sender, EventArgs e)
        {

        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void cboKodeBarang_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void cboSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

    }
}
