﻿using System;
using System.Data;
using System.Windows.Forms;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Report
{
    public partial class FrmRptPembatalanItemPenjualan : Form
    {

        public FrmRptPembatalanItemPenjualan()
        {
            InitializeComponent();
        }

        private void FrmRptPembatalanItemPenjualan_Load(object sender, EventArgs e)
        {
            InitFirst();
        }

        private void InitFirst()
        {
            BindingCbo();
            cboDisplay.SelectedIndex = 0;
        }

        private void BindingCbo()
        {
            BindingCboBarang();
            BindingCboUser();
        }

        private void BindingCboUser()
        {
            try
            {
                DataTable dt = DBAUser.Instance.GetAll(0, DBAUser.Instance.Count(""), "");
                cboUser.Items.Clear();
                cboUser.Items.Add("All");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cboUser.Items.Add(dt.Rows[i][0]);
                }
                cboUser.SelectedIndex = -1;
            }
            catch (Exception e)
            {
                if (ApplicationConnection.Instance.MySqlDatabaseConnection.State == ConnectionState.Open)
                    ApplicationConnection.Instance.MySqlDatabaseConnection.Close();
                MessageBox.Show(this, e.Message, "Error Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BindingCboBarang()
        {
            try
            {
                DataTable dt = DBABarang.Instance.GetAll(0, DBABarang.Instance.Count(""), "");
                cboKodeBarang.Items.Clear();
                cboKodeBarang.Items.Add("All");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cboKodeBarang.Items.Add(dt.Rows[i][0]);
                }
                cboKodeBarang.SelectedIndex = -1;
            }
            catch (Exception e)
            {
                if (ApplicationConnection.Instance.MySqlDatabaseConnection.State == ConnectionState.Open)
                    ApplicationConnection.Instance.MySqlDatabaseConnection.Close();
                MessageBox.Show(this, e.Message, "Error Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GridSetting()
        {
            if (dataGridView1.RowCount == 0) return;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[2].DefaultCellStyle.Format = "dd MMMM yyyy";
            dataGridView1.Columns[4].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
        }

        private void btnLast_Click(object sender, EventArgs e)
        {

        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void cboKodeBarang_Click(object sender, EventArgs e)
        {
            
        }

        private void chkRange_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
          
        }

        private void cboUser_Click(object sender, EventArgs e)
        {
            
        }

        private void cboUser_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void cboKodeBarang_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "Report Pembatalan Item Penjualan");
        }
    }
}
