﻿namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    partial class FrmUbahMenuDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ntJumlah = new System.Windows.Forms.NumericUpDown();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.txtKodeMenu = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKodeBarang = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKodeSatuan = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ntJumlah)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jumlah:";
            // 
            // ntJumlah
            // 
            this.ntJumlah.DecimalPlaces = 2;
            this.ntJumlah.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntJumlah.Location = new System.Drawing.Point(101, 87);
            this.ntJumlah.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntJumlah.Name = "ntJumlah";
            this.ntJumlah.Size = new System.Drawing.Size(169, 21);
            this.ntJumlah.TabIndex = 7;
            this.ntJumlah.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntJumlah.ThousandsSeparator = true;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(101, 126);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(77, 31);
            this.btnSimpan.TabIndex = 88;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Location = new System.Drawing.Point(193, 126);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(77, 31);
            this.btnBatal.TabIndex = 89;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(9, 116);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(262, 4);
            this.radSeparator1.TabIndex = 90;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // txtKodeMenu
            // 
            this.txtKodeMenu.Location = new System.Drawing.Point(101, 7);
            this.txtKodeMenu.Name = "txtKodeMenu";
            this.txtKodeMenu.ReadOnly = true;
            this.txtKodeMenu.Size = new System.Drawing.Size(169, 21);
            this.txtKodeMenu.TabIndex = 91;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 15);
            this.label3.TabIndex = 92;
            this.label3.Text = "Kode menu:";
            // 
            // txtKodeBarang
            // 
            this.txtKodeBarang.Location = new System.Drawing.Point(101, 34);
            this.txtKodeBarang.Name = "txtKodeBarang";
            this.txtKodeBarang.ReadOnly = true;
            this.txtKodeBarang.Size = new System.Drawing.Size(169, 21);
            this.txtKodeBarang.TabIndex = 93;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 15);
            this.label2.TabIndex = 94;
            this.label2.Text = "Kode barang:";
            // 
            // txtKodeSatuan
            // 
            this.txtKodeSatuan.Location = new System.Drawing.Point(101, 60);
            this.txtKodeSatuan.Name = "txtKodeSatuan";
            this.txtKodeSatuan.ReadOnly = true;
            this.txtKodeSatuan.Size = new System.Drawing.Size(169, 21);
            this.txtKodeSatuan.TabIndex = 95;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 15);
            this.label4.TabIndex = 96;
            this.label4.Text = "Kode satuan:";
            // 
            // FrmUbahMenuDetail
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(278, 164);
            this.Controls.Add(this.txtKodeSatuan);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtKodeBarang);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtKodeMenu);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.ntJumlah);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmUbahMenuDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ubah Detail Menu";
            this.Load += new System.EventHandler(this.FrmUbahMenuDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ntJumlah)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ntJumlah;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.TextBox txtKodeMenu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtKodeBarang;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtKodeSatuan;
        private System.Windows.Forms.Label label4;


    }
}