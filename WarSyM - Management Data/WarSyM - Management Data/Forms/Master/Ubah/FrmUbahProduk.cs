﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahProduk : Form
    {
        FrmMasterBarang frmMasterBarang;
        int idBarang, idSatuan, idSatuanParent, rowIndex;

        public FrmUbahProduk(FrmMasterBarang _frmMasterbarang, int _id_barang, string _kodeBarang, string _namaBarang, int _id_satuan, string _kodeSatuan, int _id_satuan_parent, string _kodeSatuanparent, decimal retailStok, decimal hargaJual, decimal minimumStok, int _rowIndex)
        {
            InitializeComponent();
            this.idBarang = _id_barang;
            this.idSatuan = _id_satuan;
            this.idSatuanParent = _id_satuan_parent;
            txtKodeBarang.Text = _kodeBarang;
            txtNamaItem.Text = _namaBarang;
            txtKodeSatuan.Text = _kodeSatuan;
            txtNamaSatuan.Text = (string)DBASatuan.Instance.GetSingleColumn(this.idSatuan, "nama");
            txtKodeSatuanParent.Text = _kodeSatuanparent;
            txtNamaParent.Text = (string)DBASatuan.Instance.GetSingleColumn(this.idSatuanParent, "nama");
            ntMinimumStok.Value = minimumStok;
            ntRetailStok.Value = retailStok;

            this.frmMasterBarang = _frmMasterbarang;
            this.rowIndex = _rowIndex;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan melakukan perubahan data produk ini?", "Konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;

            string message = DBABarangDetail.Instance.EditSingleColumnMultipleId(new string[] { "retail", "minimum_stok" }, new object[] {ntRetailStok.Value, ntMinimumStok.Value },
                DBABarang.Instance.GetSingleColumn(txtKodeBarang.Text, "id"),
                DBASatuan.Instance.GetSingleColumn(txtKodeSatuan.Text, "id"),
                DBASatuan.Instance.GetSingleColumn(txtKodeSatuanParent.Text, "id"));

            if (message == "success")
            {
                MessageBox.Show(this, "Data produk sudah disimpan.", "Simpan Barang", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterBarang.UpdateProduk(this.rowIndex, ntRetailStok.Value, ntMinimumStok.Value);
                this.Close();
                ClearInput();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Kode sudah dimiliki barang lain. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKodeBarang.Text = "";
            txtNamaItem.Text = "";
            txtKodeSatuan.Text = "";
            txtNamaSatuan.Text = "";
            txtKodeSatuanParent.Text = "";
            txtNamaParent.Text = "";
            ntMinimumStok.Value = 0;
            ntRetailStok.Value = 0;
        }

        private void FrmUbahProduk_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
