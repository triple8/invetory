﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahMenu : Form
    {
        FrmMasterBarang frmMasterBarang;
        int id;
        int rowIndex;

        public FrmUbahMenu(FrmMasterBarang _frmMasterBarang, int _id, string kode, string nama,  int _rowIndex)
        {
            InitializeComponent();
            this.id = _id;
            this.frmMasterBarang = _frmMasterBarang;
            this.rowIndex = _rowIndex;
            lblID.Text = this.id.ToString();
            txtKode.Text = kode;
            txtNama.Text = nama;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            string message = DBAMenu.Instance.Edit(this.id, txtKode.Text, txtNama.Text);
            if (message == "success")
            {
                MessageBox.Show(this, "Data menu sudah disimpan.", "Simpan menu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterBarang.UpdateMenu(this.rowIndex, txtKode.Text, txtNama.Text);
                this.Close();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Kode sudah dimiliki menu lain. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmUbahMenu_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
