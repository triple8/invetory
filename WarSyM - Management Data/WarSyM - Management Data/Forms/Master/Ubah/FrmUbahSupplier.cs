﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahSupplier : Form
    {
        FrmMasterSupplier frmMasterSupplier;
        string kode;
        int rowIndex;

        public FrmUbahSupplier(FrmMasterSupplier _frmMasterSupplier, string _kode, string nama, string alamat, string kota, string telp, int _rowIndex)
        {
            InitializeComponent();
            this.kode = _kode;
            this.frmMasterSupplier = _frmMasterSupplier;
            this.rowIndex = _rowIndex;
            txtKode.Text = _kode;
            txtNama.Text = nama;
            txtAlamat.Text = alamat;
            txtKota.Text = kota;
            txtTelp.Text = telp;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show(this,"Anda yakin akan melakukan perubahan data supplier ini?","Konfirmasi", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                UbahSupplier();
        }

        private void UbahSupplier() 
        {
            string message = DBASupplier.Instance.Edit(this.kode, txtNama.Text.Replace("'", "''"), txtAlamat.Text.Replace("'", "''"),
                    txtKota.Text.Replace("'", "''"), txtTelp.Text.Replace("'", "''"));
            if (message == "success")
            {
                MessageBox.Show(this, "Data supplier sudah disimpan.", "Data saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterSupplier.UbahSupplier(this.rowIndex, txtNama.Text.Replace("'", "''"), txtAlamat.Text.Replace("'", "''"),
                    txtKota.Text.Replace("'", "''"), txtTelp.Text.Replace("'", "''"));
                ClearInput();
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtNama.Text = "";
            txtAlamat.Text = "";
            txtKota.Text = "";
            txtTelp.Text = "";
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmUbahSupplier_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsNull())
            {
                if (MessageBox.Show(this, "Anda yakin akan membatalkan proses perubahan data ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    e.Cancel = true;
            }
        }

        private bool IsNull()
        {
            return string.IsNullOrEmpty(txtNama.Text) && string.IsNullOrEmpty(txtAlamat.Text) && 
                string.IsNullOrEmpty(txtKota.Text) && string.IsNullOrEmpty(txtTelp.Text);
        }

        private void FrmUbahSupplier_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }
    }
}
