﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahBarang : Form
    {
        FrmMasterBarang frmMasterBarang;
        int row;

        public FrmUbahBarang(FrmMasterBarang _frmMasterBarang, int _id, string _kode, string _nama, int _row)
        {
            InitializeComponent();
            frmMasterBarang = _frmMasterBarang;
            this.row = _row; 
            lblID.Text = _id.ToString();
            txtKode.Text = _kode;
            txtNama.Text = _nama;
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {            
            this.Close();
        }

        private bool IsNull()
        {
            return string.IsNullOrEmpty(txtKode.Text) && string.IsNullOrEmpty(txtNama.Text);
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan melakukan perubahan data barang ini?", "Konfirmasi", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
            
            string message = DBABarang.Instance.Edit(int.Parse(lblID.Text), txtKode.Text.Replace("'", "''"), txtNama.Text.Replace("'", "''"));

            if (message == "success")
            {
                MessageBox.Show(this, "Data barang sudah disimpan.", "Simpan Barang", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterBarang.UpdateBarang(this.row, txtKode.Text, txtNama.Text);
                ClearInput();
                this.Close();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Kode sudah dimiliki barang lain. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtKode.Text = "";
            txtNama.Text = "";
        }

        private void FrmUbahBarang_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsNull()) return;
            if (MessageBox.Show(this, "Anda yakin akan membatalkan perubahan data ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
        }

        private void FrmUbahBarang_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }
    }
}
