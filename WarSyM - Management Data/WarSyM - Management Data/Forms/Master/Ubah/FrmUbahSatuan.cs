﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahSatuan : Form
    {
        FrmMasterBarang frmMasterSatuan;
        int rowIndex;

        public FrmUbahSatuan(FrmMasterBarang _frmMasterSatuan, int _id, string kode, string nama, string keterangan, int _rowIndex)
        {
            InitializeComponent();
            frmMasterSatuan = _frmMasterSatuan;
            lblID.Text = _id.ToString();
            this.rowIndex = _rowIndex;
            txtKode.Text = kode;
            txtNama.Text = nama;
            txtKeterangan.Text = keterangan;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan melakukan perubahan data satuan?", "Konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            UbahSatuan();
        }

        private void UbahSatuan()
        {
           
                string message = DBASatuan.Instance.Edit(int.Parse(lblID.Text), txtKode.Text.Replace("'", "''"), txtNama.Text.Replace("'", "''"), txtKeterangan.Text.Replace("'", "''"));
                if (message == "success")
                {
                    MessageBox.Show(this, "Data satuan sudah disimpan.", "Simpan satuan", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.frmMasterSatuan.UbahSatuan(this.rowIndex, txtKode.Text.Replace("'", "''"), txtNama.Text.Replace("'", "''"), txtKeterangan.Text.Replace("'", "''"));
                    ClearInput();
                    this.Close();
                }
                else if (message == "kode already exist")
                {
                    MessageBox.Show(this, "Kode sudah dimiliki satuan lain. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }
        private void ClearInput()
        {
            txtKode.Text = "";
            txtNama.Text = "";
            txtKeterangan.Text = "";
        }

        private void FrmUbahSatuan_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
