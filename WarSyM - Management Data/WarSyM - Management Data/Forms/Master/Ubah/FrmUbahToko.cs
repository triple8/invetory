﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahToko : Form
    {
        FrmMasterToko frmMasterToko;
        int id;
        int rowIndex;
        public FrmUbahToko(FrmMasterToko _frmMasterToko, int _id, int _rowIndex)
        {
            InitializeComponent();
            this.frmMasterToko = _frmMasterToko;
            this.id = _id;
            this.rowIndex = _rowIndex;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan melakukan perubahan data toko?", "Konfirmasi",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            UbahToko();
        }

        private void UbahToko()
        {
            string message = DBAToko.Instance.Edit(this.id, txtKode.Text.Replace("'", "''"), txtNama.Text.Replace("'", "''"), txtAlamat.Text.Replace("'", "''"), txtTelp.Text.Replace("'","''"));
            if (message == "success")
            {
                MessageBox.Show(this, "Data toko sudah disimpan.", "Simpan toko", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterToko.UbahToko(this.rowIndex, txtKode.Text, txtNama.Text, txtAlamat.Text, txtTelp.Text);
                ClearInput();
                this.Close();
            }
            else if (message == "kode already exist")
            {
                MessageBox.Show(this, "Kode sudah dimiliki toko lain. Mohon diperiksa kembali.", "Duplicate code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtAlamat.Text = "";
            txtKode.Text = "";
            txtNama.Text = "";
            txtTelp.Text = "";
        }

        private void FrmUbahToko_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
