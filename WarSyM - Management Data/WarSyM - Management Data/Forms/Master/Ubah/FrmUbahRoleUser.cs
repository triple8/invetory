﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahRoleUser : Form
    {
        FrmMasterUser frmMasterUser;
        string username;
        int rowIndex;

        public FrmUbahRoleUser(FrmMasterUser _frmMasterUser, string _username, string hakAksesUser, int _rowIndex)
        {
            InitializeComponent();
            this.username = _username;
            this.frmMasterUser = _frmMasterUser;
            this.rowIndex = _rowIndex;
            if (hakAksesUser.Contains("Administrator"))
                rdoAdministrator.Checked = true;
            else
            {
                rdoPegawai.Checked = true;
                chkPembelian.Checked = (hakAksesUser.Contains("Pembelian")) ? true : false;
                chkPenjualan.Checked = (hakAksesUser.Contains("Penjualan")) ? true : false;
                chkPenyesuaian.Checked = (hakAksesUser.Contains("Penyesuaian")) ? true : false;
                chkMaintenanceMaster.Checked = (hakAksesUser.Contains("Maintenance")) ? true : false;
                chkCetakLaporan.Checked = (hakAksesUser.Contains("Cetak")) ? true : false;
                chkPengaturan.Checked = (hakAksesUser.Contains("Pengaturan")) ? true : false;
            }
        }

        private void rdoAdministrator_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoAdministrator.Checked)
                ClearCheckboxAkses(false);
            else
                ClearCheckboxAkses(true);
        }

        private void ClearCheckboxAkses(bool _state)
        {
            chkCetakLaporan.Enabled = _state;
            chkMaintenanceMaster.Enabled = _state;
            chkPembelian.Enabled = _state;
            chkPengaturan.Enabled = _state;
            chkPenjualan.Enabled = _state;
            chkPenyesuaian.Enabled = _state;
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string GetAkses()
        {
            string hakAkses = "";
            if (rdoAdministrator.Checked)
                hakAkses = "Administrator";
            else
            {
                if (rdoPegawai.Checked)
                {
                    if (chkCetakLaporan.Checked) hakAkses = (hakAkses == "") ? chkCetakLaporan.Text : hakAkses + ", " + chkCetakLaporan.Text;
                    if (chkMaintenanceMaster.Checked) hakAkses = (hakAkses == "") ? chkMaintenanceMaster.Text : hakAkses + ", " + chkMaintenanceMaster.Text;
                    if (chkPembelian.Checked) hakAkses = (hakAkses == "") ? chkPembelian.Text : hakAkses + ", " + chkPembelian.Text;
                    if (chkPengaturan.Checked) hakAkses = (hakAkses == "") ? chkPengaturan.Text : hakAkses + ", " + chkPengaturan.Text;
                    if (chkPenjualan.Checked) hakAkses = (hakAkses == "") ? chkPenjualan.Text : hakAkses + ", " + chkPenjualan.Text;
                    if (chkPenyesuaian.Checked) hakAkses = (hakAkses == "") ? chkPenyesuaian.Text : hakAkses + ", " + chkPenyesuaian.Text;
                }
            }
            return hakAkses;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            string hakAkses = GetAkses();
            string message = DBAUser.Instance.EditSingleColumn(this.username, "akses", hakAkses);
            if (message == "success")
            {
                MessageBox.Show(this, "Data user sudah diubah.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterUser.UbahUserRole(this.rowIndex, hakAkses);
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmUbahRoleUser_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }
    }
}
