﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahUserData : Form
    {
        FrmMasterUser frmMasterUser;
        string username;
        int rowIndex;

        public FrmUbahUserData(FrmMasterUser _frmMasterUser, string _username, string nama, string alamat, string telp, int _rowIndex)
        {
            InitializeComponent();
            username = _username;
            frmMasterUser = _frmMasterUser;
            this.rowIndex = _rowIndex;
            txtUsername.Text = _username;
            txtNama.Text = nama;
            txtAlamat.Text = alamat;
            txtTelp.Text = telp;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            string message = DBAUser.Instance.Edit(this.username, txtNama.Text, txtAlamat.Text, txtTelp.Text);
            if(message == "success")
            {
                MessageBox.Show(this, "Data user sudah diubah.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMasterUser.UbahUserData(this.rowIndex, txtNama.Text, txtAlamat.Text, txtTelp.Text);
                ClearInput();
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtNama.Text = "";
            txtAlamat.Text = "";
            txtTelp.Text = "";
        }

        private void FrmUbahUserData_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsNullInput())
            {
                if (MessageBox.Show(this, "Anda yakin akan membatalkan perubahan data ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 
                    == System.Windows.Forms.DialogResult.No)
                    e.Cancel = true;
            }
        }

        private bool IsNullInput()
        {
            return string.IsNullOrEmpty(txtNama.Text) && string.IsNullOrEmpty(txtAlamat.Text) 
                && string.IsNullOrEmpty(txtTelp.Text);
        }

        private void FrmUbahUserData_Load(object sender, EventArgs e)
        {
            //foreach (Control c in this.Controls)
            //{
            //    if (c is TextBox)
            //    {
            //       if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
            //    }
            //}
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
