﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Detail;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahMenuDetail : Form
    {
        FrmMenuDetail frmMenuDetail;
        int idMenu, idBarang, idSatuan, rowIndex;

        public FrmUbahMenuDetail(FrmMenuDetail _frmMenuDetail, int _idMenu, string _kodeMenu, int _idBarang, string _kodeBarang, int _idSatuan, 
            string _kodeSatuan, decimal _jumlah, int _rowIndex)
        {
            InitializeComponent();
            this.frmMenuDetail = _frmMenuDetail;
            txtKodeBarang.Text = _kodeBarang;
            txtKodeMenu.Text = _kodeMenu;
            txtKodeSatuan.Text = _kodeSatuan;
            this.idMenu = _idMenu;
            this.idBarang = _idBarang;
            this.idSatuan = _idSatuan;
            ntJumlah.Value = _jumlah;
            this.rowIndex = _rowIndex;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            string message = DBAMenuDetail.Instance.EditSingleColumnMultipleId(new string[]{ "jumlah" }, new object[] { ntJumlah.Value }, this.idMenu, this.idBarang, this.idSatuan);
            if (message == "success")
            {
                MessageBox.Show(this, "Data detail menu sudah diubah.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.frmMenuDetail.UbahJumlah(this.rowIndex, ntJumlah.Value);
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmUbahMenuDetail_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
