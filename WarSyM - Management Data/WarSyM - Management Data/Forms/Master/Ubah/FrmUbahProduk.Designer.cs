﻿namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    partial class FrmUbahProduk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ntMinimumStok = new System.Windows.Forms.NumericUpDown();
            this.ntRetailStok = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNamaParent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaSatuan = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNamaItem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKodeSatuanParent = new System.Windows.Forms.TextBox();
            this.txtKodeSatuan = new System.Windows.Forms.TextBox();
            this.txtKodeBarang = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ntMinimumStok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntRetailStok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            this.SuspendLayout();
            // 
            // ntMinimumStok
            // 
            this.ntMinimumStok.DecimalPlaces = 2;
            this.ntMinimumStok.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntMinimumStok.Location = new System.Drawing.Point(133, 215);
            this.ntMinimumStok.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntMinimumStok.Name = "ntMinimumStok";
            this.ntMinimumStok.Size = new System.Drawing.Size(223, 21);
            this.ntMinimumStok.TabIndex = 101;
            this.ntMinimumStok.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntMinimumStok.ThousandsSeparator = true;
            // 
            // ntRetailStok
            // 
            this.ntRetailStok.DecimalPlaces = 2;
            this.ntRetailStok.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ntRetailStok.Location = new System.Drawing.Point(133, 186);
            this.ntRetailStok.Maximum = new decimal(new int[] {
            -727379969,
            232,
            0,
            0});
            this.ntRetailStok.Name = "ntRetailStok";
            this.ntRetailStok.Size = new System.Drawing.Size(223, 21);
            this.ntRetailStok.TabIndex = 91;
            this.ntRetailStok.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ntRetailStok.ThousandsSeparator = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(60, 186);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 114;
            this.label9.Text = "Retail stok:";
            // 
            // txtNamaParent
            // 
            this.txtNamaParent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaParent.Location = new System.Drawing.Point(133, 157);
            this.txtNamaParent.Name = "txtNamaParent";
            this.txtNamaParent.ReadOnly = true;
            this.txtNamaParent.Size = new System.Drawing.Size(222, 21);
            this.txtNamaParent.TabIndex = 100;
            this.txtNamaParent.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 113;
            this.label7.Text = "Nama satuan:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 15);
            this.label8.TabIndex = 112;
            this.label8.Text = "Kode parent satuan:";
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(133, 254);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(106, 31);
            this.btnSimpan.TabIndex = 103;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Location = new System.Drawing.Point(250, 254);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(106, 31);
            this.btnBatal.TabIndex = 104;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // radSeparator1
            // 
            this.radSeparator1.Location = new System.Drawing.Point(10, 242);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(345, 5);
            this.radSeparator1.TabIndex = 111;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 15);
            this.label3.TabIndex = 109;
            this.label3.Text = "Minimum stok:";
            // 
            // txtNamaSatuan
            // 
            this.txtNamaSatuan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaSatuan.Location = new System.Drawing.Point(133, 98);
            this.txtNamaSatuan.Name = "txtNamaSatuan";
            this.txtNamaSatuan.ReadOnly = true;
            this.txtNamaSatuan.Size = new System.Drawing.Size(222, 21);
            this.txtNamaSatuan.TabIndex = 97;
            this.txtNamaSatuan.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 15);
            this.label5.TabIndex = 108;
            this.label5.Text = "Nama satuan:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 15);
            this.label6.TabIndex = 107;
            this.label6.Text = "Kode satuan:";
            // 
            // txtNamaItem
            // 
            this.txtNamaItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtNamaItem.Location = new System.Drawing.Point(133, 39);
            this.txtNamaItem.Name = "txtNamaItem";
            this.txtNamaItem.ReadOnly = true;
            this.txtNamaItem.Size = new System.Drawing.Size(222, 21);
            this.txtNamaItem.TabIndex = 94;
            this.txtNamaItem.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 106;
            this.label2.Text = "Nama barang:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 15);
            this.label1.TabIndex = 105;
            this.label1.Text = "Kode barang:";
            // 
            // txtKodeSatuanParent
            // 
            this.txtKodeSatuanParent.Location = new System.Drawing.Point(133, 127);
            this.txtKodeSatuanParent.MaxLength = 50;
            this.txtKodeSatuanParent.Name = "txtKodeSatuanParent";
            this.txtKodeSatuanParent.ReadOnly = true;
            this.txtKodeSatuanParent.Size = new System.Drawing.Size(222, 21);
            this.txtKodeSatuanParent.TabIndex = 117;
            // 
            // txtKodeSatuan
            // 
            this.txtKodeSatuan.Location = new System.Drawing.Point(133, 69);
            this.txtKodeSatuan.MaxLength = 50;
            this.txtKodeSatuan.Name = "txtKodeSatuan";
            this.txtKodeSatuan.ReadOnly = true;
            this.txtKodeSatuan.Size = new System.Drawing.Size(222, 21);
            this.txtKodeSatuan.TabIndex = 116;
            // 
            // txtKodeBarang
            // 
            this.txtKodeBarang.Location = new System.Drawing.Point(133, 9);
            this.txtKodeBarang.MaxLength = 50;
            this.txtKodeBarang.Name = "txtKodeBarang";
            this.txtKodeBarang.ReadOnly = true;
            this.txtKodeBarang.Size = new System.Drawing.Size(222, 21);
            this.txtKodeBarang.TabIndex = 115;
            // 
            // FrmUbahProduk
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(365, 293);
            this.Controls.Add(this.txtKodeSatuanParent);
            this.Controls.Add(this.txtKodeSatuan);
            this.Controls.Add(this.txtKodeBarang);
            this.Controls.Add(this.ntMinimumStok);
            this.Controls.Add(this.ntRetailStok);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtNamaParent);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNamaSatuan);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNamaItem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmUbahProduk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ubah Produk";
            this.Load += new System.EventHandler(this.FrmUbahProduk_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ntMinimumStok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ntRetailStok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown ntMinimumStok;
        private System.Windows.Forms.NumericUpDown ntRetailStok;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtNamaParent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtNamaSatuan;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtNamaItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtKodeSatuanParent;
        public System.Windows.Forms.TextBox txtKodeSatuan;
        public System.Windows.Forms.TextBox txtKodeBarang;
    }
}