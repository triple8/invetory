﻿namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    partial class FrmUbahRoleUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUbahRoleUser));
            this.chkPenyesuaian = new System.Windows.Forms.CheckBox();
            this.chkMaintenanceMaster = new System.Windows.Forms.CheckBox();
            this.chkPengaturan = new System.Windows.Forms.CheckBox();
            this.chkCetakLaporan = new System.Windows.Forms.CheckBox();
            this.chkPenjualan = new System.Windows.Forms.CheckBox();
            this.chkPembelian = new System.Windows.Forms.CheckBox();
            this.rdoPegawai = new System.Windows.Forms.RadioButton();
            this.rdoAdministrator = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.radSeparator1 = new Telerik.WinControls.UI.RadSeparator();
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).BeginInit();
            this.SuspendLayout();
            // 
            // chkPenyesuaian
            // 
            this.chkPenyesuaian.AutoSize = true;
            this.chkPenyesuaian.Location = new System.Drawing.Point(108, 133);
            this.chkPenyesuaian.Name = "chkPenyesuaian";
            this.chkPenyesuaian.Size = new System.Drawing.Size(161, 19);
            this.chkPenyesuaian.TabIndex = 49;
            this.chkPenyesuaian.Text = "Penyesuaian Stok/Harga";
            this.chkPenyesuaian.UseVisualStyleBackColor = true;
            // 
            // chkMaintenanceMaster
            // 
            this.chkMaintenanceMaster.AutoSize = true;
            this.chkMaintenanceMaster.Location = new System.Drawing.Point(108, 108);
            this.chkMaintenanceMaster.Name = "chkMaintenanceMaster";
            this.chkMaintenanceMaster.Size = new System.Drawing.Size(139, 19);
            this.chkMaintenanceMaster.TabIndex = 46;
            this.chkMaintenanceMaster.Text = "Maintenance Master";
            this.chkMaintenanceMaster.UseVisualStyleBackColor = true;
            // 
            // chkPengaturan
            // 
            this.chkPengaturan.AutoSize = true;
            this.chkPengaturan.Location = new System.Drawing.Point(108, 185);
            this.chkPengaturan.Name = "chkPengaturan";
            this.chkPengaturan.Size = new System.Drawing.Size(135, 19);
            this.chkPengaturan.TabIndex = 50;
            this.chkPengaturan.Text = "Pengaturan Aplikasi";
            this.chkPengaturan.UseVisualStyleBackColor = true;
            // 
            // chkCetakLaporan
            // 
            this.chkCetakLaporan.AutoSize = true;
            this.chkCetakLaporan.Location = new System.Drawing.Point(108, 160);
            this.chkCetakLaporan.Name = "chkCetakLaporan";
            this.chkCetakLaporan.Size = new System.Drawing.Size(146, 19);
            this.chkCetakLaporan.TabIndex = 47;
            this.chkCetakLaporan.Text = "Cetak Laporan/Report";
            this.chkCetakLaporan.UseVisualStyleBackColor = true;
            // 
            // chkPenjualan
            // 
            this.chkPenjualan.AutoSize = true;
            this.chkPenjualan.Location = new System.Drawing.Point(108, 82);
            this.chkPenjualan.Name = "chkPenjualan";
            this.chkPenjualan.Size = new System.Drawing.Size(82, 19);
            this.chkPenjualan.TabIndex = 48;
            this.chkPenjualan.Text = "Penjualan";
            this.chkPenjualan.UseVisualStyleBackColor = true;
            // 
            // chkPembelian
            // 
            this.chkPembelian.AutoSize = true;
            this.chkPembelian.Location = new System.Drawing.Point(108, 56);
            this.chkPembelian.Name = "chkPembelian";
            this.chkPembelian.Size = new System.Drawing.Size(143, 19);
            this.chkPembelian.TabIndex = 45;
            this.chkPembelian.Text = "Pembelian/Retur Beli";
            this.chkPembelian.UseVisualStyleBackColor = true;
            // 
            // rdoPegawai
            // 
            this.rdoPegawai.AutoSize = true;
            this.rdoPegawai.Location = new System.Drawing.Point(86, 30);
            this.rdoPegawai.Name = "rdoPegawai";
            this.rdoPegawai.Size = new System.Drawing.Size(73, 19);
            this.rdoPegawai.TabIndex = 44;
            this.rdoPegawai.TabStop = true;
            this.rdoPegawai.Text = "Pegawai";
            this.rdoPegawai.UseVisualStyleBackColor = true;
            // 
            // rdoAdministrator
            // 
            this.rdoAdministrator.AutoSize = true;
            this.rdoAdministrator.Location = new System.Drawing.Point(86, 7);
            this.rdoAdministrator.Name = "rdoAdministrator";
            this.rdoAdministrator.Size = new System.Drawing.Size(97, 19);
            this.rdoAdministrator.TabIndex = 43;
            this.rdoAdministrator.TabStop = true;
            this.rdoAdministrator.Text = "Administrator";
            this.rdoAdministrator.UseVisualStyleBackColor = true;
            this.rdoAdministrator.CheckedChanged += new System.EventHandler(this.rdoAdministrator_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 42;
            this.label3.Text = "Hak Akses:";
            // 
            // radSeparator1
            // 
            this.radSeparator1.BackColor = System.Drawing.SystemColors.Control;
            this.radSeparator1.Location = new System.Drawing.Point(10, 211);
            this.radSeparator1.Name = "radSeparator1";
            this.radSeparator1.Size = new System.Drawing.Size(259, 4);
            this.radSeparator1.TabIndex = 53;
            this.radSeparator1.Text = "radSeparator1";
            // 
            // btnBatal
            // 
            this.btnBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBatal.Image = ((System.Drawing.Image)(resources.GetObject("btnBatal.Image")));
            this.btnBatal.Location = new System.Drawing.Point(147, 224);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(121, 35);
            this.btnBatal.TabIndex = 52;
            this.btnBatal.Text = "&Batal";
            this.btnBatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Image = ((System.Drawing.Image)(resources.GetObject("btnSimpan.Image")));
            this.btnSimpan.Location = new System.Drawing.Point(12, 224);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(121, 35);
            this.btnSimpan.TabIndex = 51;
            this.btnSimpan.Text = "&Simpan";
            this.btnSimpan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // FrmUbahRoleUser
            // 
            this.AcceptButton = this.btnSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.btnBatal;
            this.ClientSize = new System.Drawing.Size(279, 269);
            this.Controls.Add(this.radSeparator1);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.chkPenyesuaian);
            this.Controls.Add(this.chkMaintenanceMaster);
            this.Controls.Add(this.chkPengaturan);
            this.Controls.Add(this.chkCetakLaporan);
            this.Controls.Add(this.chkPenjualan);
            this.Controls.Add(this.chkPembelian);
            this.Controls.Add(this.rdoPegawai);
            this.Controls.Add(this.rdoAdministrator);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmUbahRoleUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ubah User Role";
            this.Load += new System.EventHandler(this.FrmUbahRoleUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSeparator1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkPenyesuaian;
        private System.Windows.Forms.CheckBox chkMaintenanceMaster;
        private System.Windows.Forms.CheckBox chkPengaturan;
        private System.Windows.Forms.CheckBox chkCetakLaporan;
        private System.Windows.Forms.CheckBox chkPenjualan;
        private System.Windows.Forms.CheckBox chkPembelian;
        private System.Windows.Forms.RadioButton rdoPegawai;
        private System.Windows.Forms.RadioButton rdoAdministrator;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadSeparator radSeparator1;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnSimpan;
    }
}