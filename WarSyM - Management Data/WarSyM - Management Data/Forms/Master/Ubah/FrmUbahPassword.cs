﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.DBAccess;
using System.Security.Cryptography;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master.Ubah
{
    public partial class FrmUbahPassword : Form
    {
        public FrmUbahPassword()
        {
            InitializeComponent();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Anda yakin akan mengubah password?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) return;
            
            if(string.IsNullOrEmpty(txtPasswordLama.Text) || string.IsNullOrEmpty(txtPasswordBaru.Text)) 
            {
                MessageBox.Show(this, "Password lama dan password baru tidak boleh kosong.", "Data tidak lengkap", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtPasswordLama.Focus();
                return;
            }
            if(Common.GetMd5Hash(MD5.Create(), txtPasswordLama.Text) != UserLoginInfo.Password)
            {
                MessageBox.Show(this, "Password lama Anda salah!", "Wrong Old Password", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtPasswordLama.Focus();
                return;
            }
            string message = DBAUser.Instance.EditSingleColumn(UserLoginInfo.Username, "password", Common.GetMd5Hash(MD5.Create(), txtPasswordBaru.Text));
            if(message == "success")
            {
                UserLoginInfo.Password = Common.GetMd5Hash(MD5.Create(), txtPasswordBaru.Text);
                MessageBox.Show(this, "Password sudah diubah.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearInput();
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Terdapat kesalahan inputan atau koneksi, mohon periksa kembali.", "Data not saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                MessageBox.Show(this, message, "Message from system", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearInput()
        {
            txtPasswordBaru.Text = "";
            txtPasswordLama.Text = "";
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmUbahPassword_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPasswordBaru.Text) && string.IsNullOrEmpty(txtPasswordLama.Text)) return;
            if (MessageBox.Show(this, "Anda yakin akan membatalkan proses ini?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
        }
    }
}
