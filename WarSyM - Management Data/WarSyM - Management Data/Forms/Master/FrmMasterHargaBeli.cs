﻿using System;
using System.Windows.Forms;
using WarSyM_Management_Data.DBAccess;
using WarSyM_Management_Data.Forms.Master.Input;
using WarSyM_Management_Data.Forms.Penyesuaian;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.Forms.Master
{
    public partial class FrmMasterHargaBeli : Form
    {
        bool initLoad = false;
        public FrmMasterHargaBeli()
        {
            InitializeComponent();
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            Pagging.First(dataGridView1, DBAHargaBeli.Instance, lblPage, cboDisplay, GetFilterProduct());
        }
        
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            Pagging.Previous(dataGridView1, DBAHargaBeli.Instance, lblPage, cboDisplay, GetFilterProduct());
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Pagging.Next(DBAHargaBeli.Instance.Count(GetFilterProduct()), dataGridView1, DBAHargaBeli.Instance, lblPage, cboDisplay, GetFilterProduct());
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            Pagging.Last(DBAHargaBeli.Instance.Count(GetFilterProduct()), dataGridView1, DBAHargaBeli.Instance, lblPage, cboDisplay, GetFilterProduct());
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            Pagging.DataCountChange(dataGridView1, DBAHargaBeli.Instance, lblPage, cboDisplay, GetFilterProduct());
        }

        private void FrmMasterHargaBeli_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "normal")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Normal;
                    else if (ApplicationSettings.Instance.UseCapitalForInputTextBox.ToLower() == "upper")
                        ((TextBox)c).CharacterCasing = CharacterCasing.Upper;
                    else
                        ((TextBox)c).CharacterCasing = CharacterCasing.Lower;
                }
                else if (c is NumericUpDown)
                {
                    ((NumericUpDown)c).DecimalPlaces = ApplicationSettings.Instance.DecimalPlaceForNumeric;
                }
            }
            GridSetting();
            cboDisplay.SelectedIndex = 0;

            ControlBinding.BindingComboBox(DBABarang.Instance, cboKodeBarang, 1, "");
            cboKodeBarang.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBASupplier.Instance, cboKodeSupplier, 0, "");
            cboKodeSupplier.SelectedIndex = 0;
            ControlBinding.BindingComboBox(DBASatuan.Instance, cboKodeSatuan, 1, "");
            cboKodeSatuan.SelectedIndex = 0;
            cboKodeSatuan.Items.RemoveAt(1);
            initLoad = true;
            dataGridView1.Sort(dataGridView1.Columns["clmnKodeBarang"], System.ComponentModel.ListSortDirection.Ascending);
        }

        private void GridSetting()
        {
            dataGridView1.Columns.Add("clmnKodeSupplier", "Kode Supplier");
            dataGridView1.Columns.Add("clmnIdBarang", "Id Barang");
            dataGridView1.Columns.Add("clmnKodeBarang", "Kode Barang");
            dataGridView1.Columns.Add("clmnNamaBarang", "Nama Barang");
            dataGridView1.Columns.Add("clmnIdSatuan", "Id Satuan");
            dataGridView1.Columns.Add("clmnKodeSatuan", "Kode Satuan");
            dataGridView1.Columns.Add("clmnNamaSatuan", "Nama Satuan");
            dataGridView1.Columns.Add("clmnHarga", "Harga");

            dataGridView1.Columns["clmnIdBarang"].Visible = false;
            dataGridView1.Columns["clmnIdSatuan"].Visible = false;

            dataGridView1.DefaultCellStyle.Padding = new Padding(4, 2, 4, 2);

            DataGridViewButtonColumn bcol3 = new DataGridViewButtonColumn();
            bcol3.HeaderText = "";
            bcol3.Text = "Penyesuaian Harga Beli";
            bcol3.Name = "btnHargaBeli";
            bcol3.UseColumnTextForButtonValue = true;
            dataGridView1.Columns.Add(bcol3);
            dataGridView1.Columns["btnHargaBeli"].Width = 180;

            dataGridView1.Columns["clmnHarga"].DefaultCellStyle.Format = "N2";
            dataGridView1.Columns["clmnHarga"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            FrmMain frmmain = new FrmMain(frmLogin);
            frmmain.ExportToExcel(dataGridView1, "Master Harga Beli Barang");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmInputHargaBeli frmInputHargaBeli = new FrmInputHargaBeli(this);
            frmInputHargaBeli.ShowDialog();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (dataGridView1.Columns[e.ColumnIndex].Name == "btnHargaBeli")
            {
                FrmPenyesuaianHargaBeli frmPenyesuaianHargaBeli =
                    new FrmPenyesuaianHargaBeli(this, int.Parse(dataGridView1["clmnIdBarang", e.RowIndex].Value.ToString()), dataGridView1["clmnKodeBarang", e.RowIndex].Value.ToString(),
                        dataGridView1["clmnNamaBarang", e.RowIndex].Value.ToString(), int.Parse(dataGridView1["clmnIdSatuan", e.RowIndex].Value.ToString()),
                        dataGridView1["clmnKodeSatuan", e.RowIndex].Value.ToString(), dataGridView1["clmnNamaSatuan", e.RowIndex].Value.ToString(),
                        dataGridView1["clmnKodeSupplier", e.RowIndex].Value.ToString(), decimal.Parse(dataGridView1["clmnHarga", e.RowIndex].Value.ToString()), e.RowIndex);
                frmPenyesuaianHargaBeli.ShowDialog();
            }
        }

        public void UpdateHargaBeli(int p1, decimal p2)
        {
            dataGridView1.Rows[p1].Cells["clmnHarga"].Value = p2;
        }

        private void cboKodeBarang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void FilteringData()
        {
            string filter = GetFilterProduct();
            int countToDisplay = (cboDisplay.Text == "All") ? DBAHargaBeli.Instance.Count("") : int.Parse(cboDisplay.Text);
            Pagging.BindingGrid(dataGridView1, DBAHargaBeli.Instance, 0, countToDisplay, filter);
        }

        private string GetFilterProduct()
        {
            string returnValue = "";
            if (cboKodeBarang.Text != "" && cboKodeBarang.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " b.kode='" + cboKodeBarang.Text + "' " : returnValue + " and b.kode='" + cboKodeBarang.Text + "' ";

            if (cboKodeSatuan.Text != "" && cboKodeSatuan.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " s.kode='" + cboKodeSatuan.Text + "' " : returnValue + " and s.kode='" + cboKodeSatuan.Text + "' ";

            if (cboKodeSupplier.Text != "" && cboKodeSupplier.SelectedIndex != 0)
                returnValue = (returnValue == "") ? " sp.kode='" + cboKodeSupplier.Text + "' " : returnValue + " and sp.kode='" + cboKodeSupplier.Text + "' ";

            return returnValue;
        }

        private void cboKodeSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        private void cboKodeSatuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!initLoad) return;
            lblPage.Text = "1";
            FilteringData();
        }

        internal void InsertData(params object[] values)
        {
            dataGridView1.Rows.Add(values);
        }
    }
}
