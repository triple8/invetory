﻿using System.Text;
using System.Management;
using System.Security.Cryptography;
using WarSyM_Management_Data.Core;
using MySql.Data.MySqlClient;
using System.Data;
using System.Collections.Generic;

namespace WarSyM_Management_Data.GlobalClass
{
    public static class InitializeApplication
    {
        public static void InitializeWarsymManagementData()
        {
            //read setting file local
            string[] arrayOfSettings = Common.ReadLocalSettingsFile();

            ApplicationSettings.Instance.DataBaseHost = arrayOfSettings[0];
            ApplicationSettings.Instance.DatabasePort = arrayOfSettings[1];
            ApplicationSettings.Instance.DatabaseName = arrayOfSettings[2];
            ApplicationSettings.Instance.DatabaseUser = arrayOfSettings[3];
            ApplicationSettings.Instance.DatabasePassword = arrayOfSettings[4];

            StringBuilder sb = new StringBuilder();
            sb.Append(" select `key`, `value` from settings where 1=1 ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            DataTable dt = new DataTable();

            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ApplicationSettings.Instance.databaseSettings.Add(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString());
            }
        }        
    }
}
