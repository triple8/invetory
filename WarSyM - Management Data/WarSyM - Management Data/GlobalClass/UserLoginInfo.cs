﻿
namespace WarSyM_Management_Data.GlobalClass
{
    public static class UserLoginInfo
    {
        public static string IdLogin;
        public static string Username;
        public static string Password;
        public static string HakAkses;
        public static bool IsLogin;
        public static string Nama;
        public static string Alamat;
        public static string Telp;
        public static bool isSakti;

        public static void SetUserLogin(string _username, string _password, string _nama, string _alamat, string _telp, string _hakAkses, bool _isLogin)
        {

            Username = _username;
            Password = _password;
            HakAkses = _hakAkses;
            IsLogin = _isLogin;
            Nama = _nama;
            Alamat = _alamat;
            Telp = _telp;
        }
    }
}
