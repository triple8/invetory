﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WarSyM_Management_Data.GlobalClass;

namespace WarSyM_Management_Data
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            InitializeApplication.InitializeWarsymManagementData();
            Application.Run(new FrmLogin());
        }
    }
}
