﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WarSyM_Management_Data.IDBAccess;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAReturJual : IData
    {
        public DataTable GetAllData(int start, int count, string filter)
        {
            throw new NotImplementedException();
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            throw new NotImplementedException();
        }


        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }

        public DataTable GetAll(int start, int count, string filter)
        {
            throw new NotImplementedException();
        }

        public string Insert(params object[] values)
        {
            throw new NotImplementedException();
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }
        

        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
