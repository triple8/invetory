﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Text;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAPenyesuaianHargaBeli : IData
    {
        private static DBAPenyesuaianHargaBeli instance;

        public static DBAPenyesuaianHargaBeli Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAPenyesuaianHargaBeli();
                return instance;
            }
        }

        private DBAPenyesuaianHargaBeli() { }
        
        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select hb.`tanggal` as Tanggal,b.kode as \"Kode Barang\", s.kode as \"Kode Satuan\", sp.`kode` as \"Kode Supplier\",hb.`harga_beli_awal` as \"Harga Beli Awal\",hb.`harga_beli_sekarang` as \"Harga Beli Sekarang\",hb.`info` as Info,hb.`user` as User ");
            sb.Append(" from historypenyesuaianhargabeli hb, barang as b, satuan as s, supplier as sp ");
            sb.Append(" where hb.id_barang=b.id and hb.id_satuan=s.id and sp.kode=hb.kode_supplier ");
            if (filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);

            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(user) ");
            sb.Append(" from historypenyesuaianhargabeli ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }

        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }

        public string Insert(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertpenyesuaianhargabeli";

                    cmd.Parameters.AddWithValue("@VIdBarang", values[0]);
                    cmd.Parameters["@VIdBarang"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdSatuan", values[1]);
                    cmd.Parameters["@VIdSatuan"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKodeSupplier", values[2]);
                    cmd.Parameters["@VKodeSupplier"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VHargaLama", values[3]);
                    cmd.Parameters["@VHargaLama"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VHargaBaru", values[4]);
                    cmd.Parameters["@VHargaBaru"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VInfo", values[5]);
                    cmd.Parameters["@VInfo"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VUser", values[6]);
                    cmd.Parameters["@VUser"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string DeleteByFilter(string filter)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" delete from historypenyesuaianhargabeli where " + filter);
                    cmd.CommandText = sb.ToString();

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? "success" : returnQuery;
                }
            }
            return returnValue;
        }

        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }
        

        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
