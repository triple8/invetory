﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBASupplier : IData
    {
        private static DBASupplier instance;

        public static DBASupplier Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBASupplier();
                return instance;
            }
        }

        private DBASupplier() { }


        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select kode as Kode, nama as Nama, alamat as Alamat, kota as Kota, telp as Telp ");
            sb.Append(" from supplier ");
            sb.Append(" where 1=1 ");
            if(filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);

            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(kode) ");
            sb.Append(" from supplier ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "hapussupplier";

                    cmd.Parameters.AddWithValue("@VKode", kode);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "0" || returnQuery == "1") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }


        public string Insert(params object[] values)
        {
            string ret = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                cn.Open();
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertsupplier";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VNama", values[0]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@Valamat", values[1]);
                    cmd.Parameters["@VAlamat"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKota", values[2]);
                    cmd.Parameters["@Vkota"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VTelp", values[3]);
                    cmd.Parameters["@VTelp"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    ret = (string)cmd.Parameters["@VMessage"].Value;//get output value
                }
            }
            return ret;
        }

        public string Edit(string kode, params object[] values)
        {
            string ret = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                cn.Open();
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "updatesupplier";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VKode", kode);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", values[0]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VAlamat", values[1]);
                    cmd.Parameters["@VAlamat"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKota", values[2]);
                    cmd.Parameters["@VKota"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VTelp", values[3]);
                    cmd.Parameters["@VTelp"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    ret = (string)cmd.Parameters["@VMessage"].Value;//get output value
                }
            }
            return ret;
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from supplier where kode='" + kode + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
