﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAUser : IData
    {
        private static DBAUser instance;

        public static DBAUser Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAUser();
                return instance;
            }
        }

        private DBAUser() { }

        public DataTable CheckUserLogin(string _username, string _password)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select username, password, nama, alamat, telp, akses ");
            sb.Append(" from user ");
            sb.Append(" where username='" + _username + "' and password=md5('" + _password + "') ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            DataTable dt = new DataTable();

            da.Fill(dt);
            
            return dt;
        }
        
        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select username as Username, password as Password, nama as Nama, alamat as Alamat, telp as Telp, akses as Akses ");
            sb.Append(" from user ");
            sb.Append(" where 1=1 ");
            if(filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);

            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public DataRow GetSingle(string kode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select username, password, nama, alamat, telp, akses ");
            sb.Append(" from user ");
            sb.Append(" where username='" +  kode + "'");

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return dt.Rows[0];
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "hapususer";

                    cmd.Parameters.AddWithValue("@VUsername", kode);
                    cmd.Parameters["@VUsername"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(username) ");
            sb.Append(" from user ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }

        public string Insert(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertuser";

                    cmd.Parameters.AddWithValue("@VUsername", values[0]);
                    cmd.Parameters.AddWithValue("@VPass", values[1]);
                    cmd.Parameters.AddWithValue("@VNama", values[2]);
                    cmd.Parameters.AddWithValue("@VAlamat", values[3]);
                    cmd.Parameters.AddWithValue("@VTelp", values[4]);
                    cmd.Parameters.AddWithValue("@VAkses", values[5]);

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;  
                }
            }
            return returnValue;
        }

        public string Edit(string kode, params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "updateuser";

                    cmd.Parameters.AddWithValue("@VUsername", kode);
                    cmd.Parameters.AddWithValue("@VNama", values[0]);
                    cmd.Parameters.AddWithValue("@VAlamat", values[1]);
                    cmd.Parameters.AddWithValue("@VTelp", values[2]);

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " from user where username='" + kode + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt.Rows[0][0];
        }

        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            string returnValue = "";            
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" update user set " + columnName + "='" + value + "' where username='" + kode + "'");

                    cmd.CommandText = sb.ToString();

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    
                    returnValue = (returnQuery == "") ? "success" : returnQuery;
                }
            }
            return returnValue;
        }

        public string DeleteByFilter(string filter)
        {
           throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
