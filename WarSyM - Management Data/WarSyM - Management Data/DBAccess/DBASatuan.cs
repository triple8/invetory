﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBASatuan : IData
    {
        private static DBASatuan instance;

        public static DBASatuan Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBASatuan();
                return instance;
            }
        }

        private DBASatuan() { }


        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(id) ");
            sb.Append(" from satuan ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }
        
        public DataRow GetSingle(int id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select id as ID, kode as Kode, nama as Nama, keterangan as Keterangan ");
            sb.Append(" from satuan ");
            sb.Append(" where id='" + id.ToString() + "'");

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);

            return dt.Rows[0];
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "hapussatuan";

                    cmd.Parameters.AddWithValue("@VId", id.ToString());
                    cmd.Parameters["@VId"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "updatesatuan";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VId", id);
                    cmd.Parameters["@VId"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKode", values[0]);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", values[1]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKeterangan", values[2]);
                    cmd.Parameters["@VKeterangan"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select id as ID, kode as Kode, nama as Nama, keterangan as Keterangan ");
            sb.Append(" from satuan ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            sb.Append(" limit " + start.ToString() + ", " + count.ToString());
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return dt;
        }

        public string Insert(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertsatuan";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VKode", values[0]);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", values[1]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKeterangan", values[2]);
                    cmd.Parameters["@VKeterangan"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from satuan where id='" + id + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from satuan where kode='" + kode + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
