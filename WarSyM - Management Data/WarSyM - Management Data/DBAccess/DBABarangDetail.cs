﻿using System;
using System.Data;
using System.Text;
using System.Linq;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;
using MySql.Data.MySqlClient;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBABarangDetail : IData
    {
        private static DBABarangDetail instance;

        public static DBABarangDetail Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBABarangDetail();
                return instance;
            }
        }

        private DBABarangDetail() { }

        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append("select b.id as id_barang, b.kode as \"Kode Barang\", b.nama as \"Nama Barang\", s.id as id_satuan, s.kode as \"Kode Satuan\", s1.id as id_satuan_parent, s1.kode as \"Kode Satuan Parent\", bd.retail as \"Retail Stok\", bd.harga_jual as \"Harga Jual\", bd.minimum_stok as \"Minimum Stok\" ");
            sb.Append(" from barang as b, barangdetail as bd, satuan as s, satuan as s1 ");
            sb.Append(" where b.id = bd.id_barang and s.id = bd.id_satuan and s1.id = bd.id_satuan_parent ");
            if (filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return dt;
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select count(id_barang) ");
            sb.Append(" from barangdetail ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }
        
        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }


        public string Insert(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertbarangdetail";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VIdBarang", values[0]);
                    cmd.Parameters["@VIdBarang"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdSatuan", values[1]);
                    cmd.Parameters["@VIdSatuan"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdSatuanParent", values[2]);
                    cmd.Parameters["@VIdSatuanParent"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VRetail", values[3]);
                    cmd.Parameters["@VRetail"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VMinimumStok", values[4]);
                    cmd.Parameters["@VMinimumStok"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VHargaJual", values[5]);
                    cmd.Parameters["@VHargaJual"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;

                }
            }

            return returnValue;
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " from barangdetail where kode='" + kode + "' ");
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0];
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from barangdetail ");
            sb.Append(" where " + filter);

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return dt.Rows[0][0];
        }


        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" update barangdetail set ");

                    string columnToUpdate = "";
                    for (int i = 0; i < columnName.Count(); i++)
                    {
                        columnToUpdate = (i == 0) ? columnName[i] + " ='" + value[i] + "' " : columnToUpdate + " , " + columnName[i] + " ='" + value[i] + "' ";
                    }

                    sb.Append(columnToUpdate);
                    sb.Append(" where id_barang='" + id[0] + "' and id_satuan='" + id[1] + "' and id_satuan_parent='" + id[2] + "'");

                    cmd.CommandText = sb.ToString();

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? "success" : returnQuery;
                }
            }
            return returnValue;
        }


        public string DeleteByMultipleId(params object[] id)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "hapusbarangdetail";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VIdBarang", id[0]);
                    cmd.Parameters["@VIdBarang"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdSatuan", id[1]);
                    cmd.Parameters["@VIdSatuan"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdSatuanParent", id[2]);
                    cmd.Parameters["@VIdSatuanParent"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;

                }
            }

            return returnValue;
        }
    }
}
