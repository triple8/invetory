﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAMenu : IData
    {
        private static DBAMenu instance;

        public static DBAMenu Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAMenu();
                return instance;
            }
        }


        private DBAMenu() { }

        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select id as ID, kode as Kode, nama as Nama, harga as Harga ");
            sb.Append(" from menu ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);

            return dt;
        }


        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(id) ");
            sb.Append(" from menu ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public string InsertMenu(string _kode, string _nama, string _harga, string[] _detailKodeBarang, string [] _detailKodeSatuan, string[] _detailJumlah, int dataCount)
        {
            string ret = "";
            using(MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                cn.Open();
                using(MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertmenu";
                    //parameter input
                    cmd.Parameters.AddWithValue("@VKode", _kode);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", _nama);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VHarga", _harga);
                    cmd.Parameters["@VHarga"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKodeBarangDetails", string.Join(",", _detailKodeBarang));
                    cmd.Parameters["@VKodeBarangDetails"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKodeSatuanDetails", string.Join(",", _detailKodeSatuan));
                    cmd.Parameters["@VKodeSatuanDetails"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VJumlahDetails", string.Join(",", _detailJumlah));
                    cmd.Parameters["@VJumlahDetails"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VItemCount", dataCount);
                    cmd.Parameters["@VItemCount"].Direction = ParameterDirection.Input;
                    //parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;
                    //ex
                    cmd.ExecuteNonQuery();

                    ret = (string)cmd.Parameters["@VMessage"].Value;
                }
            }            

            return ret;
        }

        public void HapusMenu(string p)
        {
            throw new NotImplementedException();
        }

        public DataRow GetSingle(int id)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select id, kode, nama, harga ");
            sb.Append(" from menu ");
            sb.Append(" where id='" + id + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);

            return (dt.Rows[0]);
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "hapusmenu";

                    cmd.Parameters.AddWithValue("@VId", id.ToString());
                    cmd.Parameters["@VId"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }

        public string Edit(int id, params object[] values)
        {
            string ret = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                cn.Open();
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "updatemenu";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VId", id);
                    cmd.Parameters["@VId"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKode", values[0]);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", values[1]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    ret = (string)cmd.Parameters["@VMessage"].Value;//get output value
                }
            }
            return ret;
        }
        
        public string Insert(params object[] values)
        {
            throw new NotImplementedException();
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }
        
        public object GetSingleColumn(int id, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from menu where id='" + id + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from menu where kode='" + kode + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }

        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from menu ");
            sb.Append(" where " + filter);

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return dt.Rows[0][0];
        }


        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }

        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
