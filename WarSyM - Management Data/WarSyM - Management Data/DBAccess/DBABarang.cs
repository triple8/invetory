﻿using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using WarSyM_Management_Data.GlobalClass;
using System;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBABarang : IData
    {
        private static DBABarang instance;

        public static DBABarang Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBABarang();
                return instance;
            }
        }

        private DBABarang() { }

        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select id as ID, kode as Kode, nama as Nama ");
            sb.Append(" from barang where 1=1");
            if (filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return dt;
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select count(id) ");
            sb.Append(" from barang ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "hapusbarang";

                    cmd.Parameters.AddWithValue("@VId", id.ToString());
                    cmd.Parameters["@VId"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "updatebarang";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VId", id);
                    cmd.Parameters["@VId"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKode", values[0]);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", values[1]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }


        public string Insert(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertbarang";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VKode", values[0]);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", values[1]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from barang where id='" + id + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from barang where kode='" + kode + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }

        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
