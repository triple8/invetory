﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WarSyM_Management_Data.GlobalClass;
using System.Data;
using MySql.Data.MySqlClient;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAPembelianDetail : IData
    {
        private static DBAPembelianDetail instance;

        public static DBAPembelianDetail Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAPembelianDetail();
                return instance;
            }
        }

        private DBAPembelianDetail() { }


        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select d.no_nota as \"No Nota\", p.no_nota_supplier as \"No Nota Supplier\", d.id_barang, b.nama as \"Nama Barang\", d.harga as Harga, d.jumlah as Jumlah,	d.diskon as Diskon, (d.harga*d.jumlah)-(d.harga*d.jumlah*d.diskon/100) as \"Sub Total\" ");
            sb.Append(" from pembeliandetail as d, (select id, no_nota_supplier, tanggal_beli, kode_supplier, user from pembelian) as p, barang as b ");
            sb.Append(" where b.id=d.id_barang and p.id=d.no_nota ");

            if (filter != "")
                sb.Append(" and " + filter);

            sb.Append(" limit " + start.ToString() + ", " + count.ToString());
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);

            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(no_nota) ");
            sb.Append(" from pembeliandetail ");
            sb.Append(" where 1=1 ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }


        public string Insert(params object[] values)
        {
            throw new NotImplementedException();
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
