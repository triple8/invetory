﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAMenuDetail : IData
    {
        private static DBAMenuDetail instance;

        public static DBAMenuDetail Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAMenuDetail();
                return instance;
            }
        }

        private DBAMenuDetail() { }

        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select md.id_menu, m.kode as \"Kode Menu\", md.id_barang, b.kode as \"Kode Barang\", md.id_satuan, s.kode as \"Kode Satuan\", md.jumlah as Jumlah ");
            sb.Append(" from menudetail as md, menu as m, barang as b, satuan as s ");
            sb.Append(" where m.id=md.id_menu and md.id_barang=b.id and md.id_satuan=s.id ");

            if (filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select count(md.id_menu) ");
            sb.Append(" from menudetail as md, menu as m, barang as b, satuan as s ");
            sb.Append(" where m.id=md.id_menu and md.id_barang=b.id and md.id_satuan=s.id ");

            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }        

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }


        public string Insert(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "insertmenudetail";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VIdMenu", values[0]);
                    cmd.Parameters["@VIdMenu"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdBarang", values[1]);
                    cmd.Parameters["@VIdBarang"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdSatuan", values[2]);
                    cmd.Parameters["@VIdSatuan"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VJumlah", values[3]);
                    cmd.Parameters["@VJumlah"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string DeleteByFilter(string filter)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" delete from menudetail where " + filter);
                    cmd.CommandText = sb.ToString();

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? "success" : returnQuery;
                }
            }
            return returnValue;
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" update menudetail set ");

                    string columnToUpdate = "";
                    for (int i = 0; i < columnName.Count(); i++)
                    {
                        columnToUpdate = (i == 0) ? columnName[i] + " ='" + value[i] + "' " : columnToUpdate + " , " + columnName[i] + " ='" + value[i] + "' ";
                    }

                    sb.Append(columnToUpdate);
                    sb.Append(" where id_menu='" + id[0] + "' and id_barang='" + id[1] + "' and id_satuan='" + id[2] + "'");

                    cmd.CommandText = sb.ToString();

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? "success" : returnQuery;
                }
            }
            return returnValue;
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
