﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAPenjualan : IData
    {
        private static DBAPenjualan instance;

        public static DBAPenjualan Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAPenjualan();
                return instance;
            }
        }

        private DBAPenjualan() { }


        public DataTable GetAll(int start, int count, string filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select p.no_nota as \"No Nota\", p.tanggal_transaksi as \"Tanggal Transaksi\", p.diskon as Diskon, p.jenis_pembayaran as \"Jenis Pembayaran\", p.debit as Debit, (d.GrandTotal-(d.GrandTotal*p.diskon/100)) as \"Grand Total\", p.id_toko, t.nama as \"Nama Toko\", p.user as User ");
            sb.Append(" from penjualan p, toko as t, ");
            sb.Append(" (select no_nota, sum(harga*jumlah) as GrandTotal ");
            sb.Append(" from penjualandetail ");
            sb.Append(" group by no_nota) as d ");
            sb.Append(" where p.id_toko=t.id ");
            if (filter == "")
                sb.Append(" and p.no_nota=d.no_nota ");
            else
                sb.Append(" and p.no_nota=d.no_nota and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);

            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(no_nota) ");
            sb.Append(" from penjualan ");
            sb.Append(" where 1=1 ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }


        public string Insert(params object[] values)
        {
            throw new NotImplementedException();
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }
        

        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
