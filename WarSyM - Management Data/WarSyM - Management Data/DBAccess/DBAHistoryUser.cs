﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Text;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAHistoryUser : IData
    {
        private static DBAHistoryUser instance;

        public static DBAHistoryUser Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAHistoryUser();
                return instance;
            }
        }

        private DBAHistoryUser() { }

        public void UpdateLogoutUser(string _id_login, DateTime _date_logout, int _id_toko)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" update historyloginuser ");
            sb.Append(" set logout=NOW() ");
            sb.Append(" where id='" + _id_login.Replace("'", "''") + "' and id_toko=" + _id_toko.ToString());
            MySqlCommand cmd = new MySqlCommand(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            if (cmd.Connection.State == ConnectionState.Open)
                cmd.Connection.Close();
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }

        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select h.user as User, date(h.login) as \"Tanggal Login\", time(h.login) as \"Jam Login\", date(h.logout) as \"Tanggal Logout\", time(h.logout) as \"Jam Logout\", h.`aplikasi` as Aplikasi, t.kode ");
            sb.Append(" from historyloginuser h, toko as t ");
            sb.Append(" where h.id_toko = t.id ");
            if (filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(user) ");
            sb.Append(" from historyloginuser ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);

            return int.Parse(dt.Rows[0][0].ToString());
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }


        public string Insert(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "inserthistoryloginuser";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VUsername", values[0]);
                    cmd.Parameters["@VUsername"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VApplikasi", values[1]);
                    cmd.Parameters["@VApplikasi"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VIdToko", values[2]);
                    cmd.Parameters["@VIdToko"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {            
            string returnValue = "";       
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using(MySqlCommand cmd = cn.CreateCommand())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" delete from historyloginuser where " + filter);
                    cmd.CommandText = sb.ToString();

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? "success" : returnQuery; 
                }
            }
            return returnValue;
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }
        

        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
