﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;
using WarSyM_Management_Data.Core;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAPembelian : IData
    {
        private static DBAPembelian instance;

        public static DBAPembelian Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAPembelian();
                return instance;
            }
        }

        private DBAPembelian() { }


        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select p.id as ID, p.no_nota_supplier as \"No Nota Supplier\", p.kode_supplier as \"Kode Supplier\", p.tanggal_beli as \"Tanggal Beli\", p.diskon as Diskon, (d.Grand_Total)-(d.Grand_Total*diskon/100) as \"Grand Total\", p.info as Info, p.id_toko, t.nama as \"Nama Toko\", p.user as User ");
            sb.Append(" from pembelian as p, toko as t, ");
            sb.Append(" (select no_nota, sum((harga*jumlah)-(harga*jumlah*diskon/100)) as Grand_Total from pembeliandetail group by no_nota) as d ");
            sb.Append(" where t.id=p.id_toko ");
            if (filter == "")
                sb.Append(" and p.id=d.no_nota ");
            else
                sb.Append(" and p.id=d.no_nota and " + filter);

            sb.Append(" limit " + start.ToString() + ", " + count.ToString());
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);

            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(id) ");
            sb.Append(" from pembelian ");
            sb.Append(" where 1=1 ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }


        public string Insert(params object[] values)
        {
            throw new NotImplementedException();
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }
        

        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
