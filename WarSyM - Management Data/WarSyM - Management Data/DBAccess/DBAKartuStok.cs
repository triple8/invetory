﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAKartuStok : IData
    {
        private static DBAKartuStok instance;

        public static DBAKartuStok Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAKartuStok();
                return instance;
            }
        }

        private DBAKartuStok() { }
        
        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select b.id as id_barang, b.kode as \"Kode Barang\", b.nama as \"Nama Barang\", s.id as id_satuan, s.kode as \"Kode Satuan\", t.id as id_toko, t.kode as \"Kode Toko\", k.stok as Stok, bd.minimum_stok as \"Minimum Stok\" ");
            sb.Append(" from kartustok as k, barang as b, satuan as s, toko as t, barangdetail as bd ");
            sb.Append(" where k.id_barang=b.id and k.id_satuan=s.id and k.id_toko=t.id and bd.id_barang=b.id and bd.id_satuan=s.id ");
            if (filter != "")
                sb.Append( " and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return dt;
        }

        public DataRow GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select count(id_barang) ");
            sb.Append(" from kartustok ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }


        public DataRow GetSingle(string kode)
        {
            throw new NotImplementedException();
        }

        public string Delete(int id)
        {
            throw new NotImplementedException();
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            throw new NotImplementedException();
        }


        public string Insert(params object[] values)
        {
            throw new NotImplementedException();
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            throw new NotImplementedException();
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            throw new NotImplementedException();
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from kartustok ");
            sb.Append(" where " + filter);

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return dt.Rows[0][0];
        }


        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
