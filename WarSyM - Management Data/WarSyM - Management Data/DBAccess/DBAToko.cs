﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WarSyM_Management_Data.Core;
using WarSyM_Management_Data.GlobalClass;
using WarSyM_Management_Data.IDBAccess;

namespace WarSyM_Management_Data.DBAccess
{
    public class DBAToko : IData
    {
        private static DBAToko instance;

        public static DBAToko Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBAToko();
                return instance;
            }
        }

        private DBAToko() { }
        
        
        public DataTable GetAll(int start, int count, string filter)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.Append(" select id as ID, kode as Kode, nama as Nama, alamat as Alamat, telp as Telp ");
            sb.Append(" from toko ");
            sb.Append(" where 1=1 ");
            if(filter != "")
                sb.Append(" and " + filter);
            sb.Append(" limit " + start.ToString() + ", " + count.ToString());

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);
            da.Fill(dt);
            return dt;
        }

        public DataRow GetSingle(int id)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select id, kode, nama, alamat, telp ");
            sb.Append(" from toko ");
            sb.Append(" where id='" + id.ToString() + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return dt.Rows[0];
        }

        public int Count(string filter)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select count(id) ");
            sb.Append(" from toko ");
            sb.Append(" where 1=1 ");
            if (filter != "")
                sb.Append(" and " + filter);

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }
        
        public DataRow GetSingle(string kode)
        {
            DataTable dt = new DataTable();

            StringBuilder sb = new StringBuilder();
            sb.Append(" select id, kode, nama, alamat, telp ");
            sb.Append(" from toko ");
            sb.Append(" where kode='" + kode + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            da.Fill(dt);
            return dt.Rows[0];
        }

        public string Delete(int id)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "hapustoko";

                    cmd.Parameters.AddWithValue("@VId", id);
                    cmd.Parameters["@VId"].Direction = ParameterDirection.Input;

                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;

                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Delete(string kode)
        {
            throw new NotImplementedException();
        }


        public string Edit(int id, params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "updatetoko";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VId", id);
                    cmd.Parameters["@VId"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VKode", values[0]);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", values[1]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VAlamat", values[2]);
                    cmd.Parameters["@VAlamat"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VTelp", values[3]);
                    cmd.Parameters["@VTelp"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;


                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);
                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }

            return returnValue;
        }


        public string Insert(params object[] values)
        {
            string returnValue = "";
            using (MySqlConnection cn = ApplicationConnection.Instance.MySqlDatabaseConnection)
            {
                using (MySqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "inserttoko";//sp name

                    //sp paramter input
                    cmd.Parameters.AddWithValue("@VKode", values[0]);
                    cmd.Parameters["@VKode"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VNama", values[1]);
                    cmd.Parameters["@VNama"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VAlamat", values[2]);
                    cmd.Parameters["@VAlamat"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@VTelp", values[3]);
                    cmd.Parameters["@VTelp"].Direction = ParameterDirection.Input;

                    //sp parameter output
                    cmd.Parameters.Add(new MySqlParameter("@VMessage", MySqlDbType.VarChar));
                    cmd.Parameters["@VMessage"].Direction = ParameterDirection.Output;


                    string returnQuery = Common.ExecuteNonQuery(cn, cmd);

                    returnValue = (returnQuery == "") ? (string)cmd.Parameters["@VMessage"].Value : returnQuery;
                }
            }
            return returnValue;
        }

        public string Edit(string kode, params object[] values)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumn(int id, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from toko where id='" + id + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }

        public object GetSingleColumn(string kode, string columnName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select " + columnName + " ");
            sb.Append(" from toko where kode='" + kode + "' ");

            MySqlDataAdapter da = new MySqlDataAdapter(sb.ToString(), ApplicationConnection.Instance.MySqlDatabaseConnection);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt.Rows[0][0];
        }


        public string EditSingleColumn(int id, string columnName, object value)
        {
            throw new NotImplementedException();
        }

        public string EditSingleColumn(string kode, string columnName, object value)
        {
            throw new NotImplementedException();
        }


        public string DeleteByFilter(string filter)
        {
            throw new NotImplementedException();
        }


        public object GetSingleColumnByFilter(string columnName, string filter)
        {
            throw new NotImplementedException();
        }
        

        public string EditSingleColumnMultipleId(string[] columnName, object[] value, params object[] id)
        {
            throw new NotImplementedException();
        }


        public string DeleteByMultipleId(params object[] id)
        {
            throw new NotImplementedException();
        }
    }
}
